
if(typeof window.folderPrefix == 'undefined'){
	var folderPrefix = './html/templates/';
}
if(typeof window.indexTemplate == 'undefined'){
	var indexTemplate = 'index.html';
}

var spApp = angular.module('spApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'ngCookies',
	'ngTouch',
	'vcRecaptcha',
	'ui.bootstrap',
	'ui.bootstrap.datetimepicker', 
	/*'ui.tinymce',*/ 
	'ui.tree',
	'angularMoment',
	/*'angular-carousel',*/
	'ngScrollbars',
	'slickCarousel',
	'pdfjsViewer',
	'rzModule'
])
.run(['$rootScope', '$state', '$stateParams', '$interval', '$timeout', '$location', '$window', 
	function ($rootScope, $state, $stateParams, $interval, $timeout, $location, $window) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.recaptchaKey = settings.recaptchaKey;
		
		$rootScope.lastRestrictedPage = '';
		
		$rootScope.ready = false;
		
		$rootScope.folderPrefix = folderPrefix;
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			
			if($rootScope.$state.params.ln){
				var languageKey = searchJsonKeyInArray($rootScope.languages, 'code', $rootScope.$state.params.ln);
				if(languageKey != -1){
					$rootScope.language = $rootScope.languages[languageKey];
				}
			}
			
			if($rootScope.ready){
				checkState();
			}else{
				var checkStateWatch = $rootScope.$watch(function(){return $rootScope.ready;}, function(){
					if($rootScope.ready){
						checkStateWatch();
						checkState();
					}
				});
			}
			function checkState(){
				if (toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedIn && !$rootScope.isLogged) {
					//$rootScope.$state.go('ln.login', {ln: $rootScope.$state.params.ln});
					$rootScope.lastRestrictedPage = toState;
					$rootScope.$state.go('ln.please-login', {ln: $rootScope.$state.params.ln, toState: toState.name});
					return;
				}
				if(toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedOut && $rootScope.isLogged){
					$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
					return;
				}
				if (!isUndefined(toState.minUserStepAccessType) && $rootScope.loggedUser.userStep.id < toState.minUserStepAccessType) {
					var fromStateRedirectTo = fromState.redirectTo instanceof Function ? fromState.redirectTo() : fromState.redirectTo;
					if (fromState.url === '^') {
						$state.go('ln.index', {ln: $state.params.ln});
					} else if (fromStateRedirectTo !== toState.name) {
						$state.go(fromState.name, fromParams);
					} else {
						$state.go('ln.index');
					}
					return;
				}
			}
			
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			var redirectTo = $rootScope.$state.current.redirectTo
			var redirectToState = redirectTo instanceof Function ? redirectTo($rootScope) : redirectTo;
			if (!isUndefined(redirectToState)) {
				$state.go(redirectToState, {ln: $rootScope.$state.params.ln}, {location: 'replace'});
			}
			
			if($rootScope.menu){
				$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
			}
			$rootScope.$broadcast('resetGlobalErrorMsg');
			
			//Call GTM
			window['dataLayer'] = window['dataLayer'] || [];
			dataLayer.push({
				event: 'routeChange',
				attributes: {
					route: $location.path()
				}
			});
			$timeout(function(){
				window['dataLayer'].push({
					'gtm.start': new Date().getTime(),
					event: 'gtm.js'
				});
			}, 0);
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				$rootScope.targetState = toState;
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				//$rootScope.closeCopyOpPopup({all:true});
			})
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$provide', '$uiViewScrollProvider', 
	function ($httpProvider, $stateProvider, $urlRouterProvider, $provide, $uiViewScrollProvider) {
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		//delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		
		$httpProvider.interceptors.push(['$rootScope', '$injector', function($rootScope, $injector) {
			return {
				request: function(config) {
					if (config.method === 'POST' && Object(config.data) === config.data) {
						config.data.actionSource = settings.actionSources.WEB.id;
					}
					return config;
				},
				response: function(response) {
					// Logout User if session expired.
					if (response.config.method === 'POST' && $rootScope.isLogged) {
						switch (response.data.responseCode) {
							case errorCodeMap.access_denied: // 601
							case errorCodeMap.authentication_failure: // 602
								$rootScope.isLogged = false;
								$rootScope.setLoggedUser(null);
								// Navigate to allowed page if necessary
								var $state = $injector.get('$state'); // Injecting $state occurs Circular Dependency.
								if($state.current.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedIn){
									setTimeout(function() {
										$state.reload();
									});
								}
								break;
						}
					}
					return response;
				}
			};
		}]);
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		$urlRouterProvider
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
		/*.when('/', '/en/')
		.otherwise('/en/');*/
		.when('/', '/check/')
		.otherwise('/check/');

		var initResponsePromise;
		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: folderPrefix + 'template.html',
				redirectTo: 'ln.index',
				controller: 'spCtr',
				resolve: {
					initResponse: ['$http', function($http) {
						if (!initResponsePromise) {
							initResponsePromise = $http.post('app/init', {});
						}
						return initResponsePromise;
					}],
					getMsgsJsonResponse: ['$http', '$stateParams', 'FiltersService', '$rootScope', 'initResponse', '$cookies', function($http, $stateParams, FiltersService, $rootScope, initResponse, $cookies) {
						return FiltersService.getFilters(['languages'])
							.then(function(Filters) {
								$rootScope.languages = Object.keys(Filters.languages).map(function(key) {
									return Filters.languages[key];
								});
								var initResponseData = initResponse.data.data;
								var stateLangName = $stateParams.ln;
								var cookieLangId = $cookies.get('lang');
								var languageId = 0;
								if(initResponseData['user'] && initResponseData['user']['languageId']){//User is logged in
									languageId = initResponseData['user']['languageId']
								}else if(stateLangName && stateLangName != 'check'){//There is an explicit language in the URL
									languageId = $rootScope.languages.filter(function(language) {
										return language.code == stateLangName;
									})[0].id;
								}else if(cookieLangId){//User has set a cookie, take it's value
									languageId = cookieLangId;
								}else if(stateLangName == 'check' && initResponseData['userLogout'] && initResponseData['userLogout']['country']){//User is logged out, hits root page
									languageId = initResponseData['userLogout']['country']['defaultLanguageId'];
								}else{//Default to en
									languageId = $rootScope.languages.filter(function(language) {
										return language.code == 'en';
									})[0].id;
								}
								$rootScope.language = $rootScope.languages.filter(function(language) {
									return language.id == languageId;
								})[0];
								
								if(!$rootScope.language){//Some mixup happened, we don't have this language, use the first available
									$rootScope.language = $rootScope.languages[0];
									languageId = $rootScope.language.id;
								}
								
								if(stateLangName != $rootScope.language.code){
									$rootScope.$state.go($rootScope.targetState, {ln: $rootScope.language.code}, {location: 'replace'});
									return;
								}
								
								var methodRequest = {
									data: languageId
								};
								return $http.post('message/getMessageResources', methodRequest);
							});
					}]
				}
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: folderPrefix + indexTemplate
					}
				},
				title: '<a href="" ng-href="{{$state.href(\'ln.index\', {ln: language.code})}}" ng-if="!isMobile"><div class="logo"></div></a>',
				metaTitle: 'meta-title-index',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				titleTransclude: true
			})
			.state('ln.please-login', {
				url: '/login',
				params: {
					toState: null,
					credentialsState: null
				},
				views: {
					'main': {
						templateUrl: folderPrefix + 'please-login.html',
						controller: 'LoginCtr'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedOut,
				title: 'login',
				metaTitle: 'meta-title-login',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.register', {
				url: '/register',
				params: {
					toState: null,
					credentialsState: null
				},
				views: {
					'main': {
						templateUrl: folderPrefix + 'sign-up-register.html',
						controller: 'RegisterCtr',
						resolve: {
							restriction: ['$http', function($http) {
								return $http.post('app/restriction', {});
							}]
						}
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedOut,
				title: 'signup',
				metaTitle: 'meta-title-register',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.sign-up', {
				abstract: true,
				views: {
					'main': {
						templateUrl: folderPrefix + 'sign-up-screen.html',
					}
				},
				title: 'signup',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.sign-up.personal-details', {
				url: '/sign-up-personal-details',
				templateUrl: folderPrefix + 'sign-up-personal-details.html',
				controller: 'signUpPersonalDetailsCtrl',
				controllerAs: '$ctrl',
				title: 'personal-details',
				metaTitle: 'meta-title-sign-up-personal-details',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.sign-up.questionnaire', {
				url: '/questionnaire',
				templateUrl: folderPrefix + 'questionnaire.html',
				controller: 'QuestionnaireCtrl',
				controllerAs: '$ctrl',
				title: 'questionnaire',
				metaTitle: 'meta-title-sign-up-questionnaire',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.reset-password', {
				url: '/reset-password?email',
				views: {
					'main': {
						templateUrl: folderPrefix + 'reset-password.html',
						controller: 'resetPasswordCtrl',
						controllerAs: '$ctrl'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'reset-password'
			})
			.state('ln.about', {
				url: '/about',
				views: {
					'main': {
						templateUrl: folderPrefix + 'about.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'about-us',
				metaTitle: 'meta-title-about-us',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.faq', {
				url: '/faq',
				views: {
					'main': {
						templateUrl: folderPrefix + 'faq.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'faq',
				metaTitle: 'meta-title-faq',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.basics', {
				url: '/basics/:id',
				views: {
					'main': {
						templateUrl: folderPrefix + 'basics.html',
						controller: 'BasicsCtrl',
						controllerAs: '$ctrl'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'basics',
				metaTitle: 'meta-title-basics',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.contact-us', {
				url: '/contact-us',
				views: {
					'main': {
						templateUrl: folderPrefix + 'contact-us.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'contact-us',
				metaTitle: 'meta-title-contact-us',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.privacy-policy', {
				url: '/privacy-policy',
				views: {
					'main': {
						templateUrl: folderPrefix + 'privacy-policy.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'privacy-policy',
				metaTitle: 'meta-title-privacy-policy',
				metaDescription: 'meta-description-3',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.terms', {
				url: '/terms',
				views: {
					'main': {
						templateUrl: folderPrefix + 'terms.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'terms',
				metaTitle: 'meta-title-terms',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.security', {
				url: '/security-secured-trading',
				views: {
					'main': {
						templateUrl: folderPrefix + 'security.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'security',
				metaTitle: 'meta-title-security',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.legal-documents', {
				url: '/legal-documents',
				views: {
					'main': {
						templateUrl: folderPrefix + 'legal-documents.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'legal-documentation',
				metaTitle: 'meta-title-legal-documentation',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.products', {
				url: '/products?assets&type&maturity&protection',
				reloadOnSearch: false,
				views: {
					'main': {
						templateUrl: folderPrefix + 'products/products.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'all-notes',
				metaTitle: 'meta-title-products',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.product', {
				url: '/product/:productId',
				views: {
					'main': {
						templateUrl: folderPrefix + 'products/products-single.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				mobileConfig: {
					showHeader: false
				},
				metaTitle: 'meta-title-products',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			//My account
			.state('ln.my-account', {
				url: '/my-account',
				views: {
					'main': {
						templateUrl: folderPrefix + 'my-account/index.html'
					}
				},
				redirectTo: function($rootScope) {
					if (!$rootScope.loggedUser.userStep) return;
					var userStepId = $rootScope.loggedUser.userStep.id;
					switch (userStepId) {
						case 1:
							return 'ln.sign-up.personal-details';
						case 2:
							return 'ln.sign-up.questionnaire';
						case 3:
							return 'ln.my-account.settings.questionnaire';
						case 4:
							return 'ln.my-account.funding.upload-documents';
						case 5:
						default:
							return 'ln.my-account.my-notes';
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.my-notes', {
				url: '/my-notes',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/my-notes.html'
					}
				},
				redirectTo: 'ln.my-account.my-notes.open',
				title: 'my-notes'
			})
			.state('ln.my-account.my-notes.open', {
				url: '/open',
				views: {
					'my-notes': {
						templateUrl: folderPrefix + 'my-account/my-notes-open.html'
					}
				},
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-my-notes-open',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.my-notes.closed', {
				url: '/closed',
				views: {
					'my-notes': {
						templateUrl: folderPrefix + 'my-account/my-notes-closed.html'
					}
				},
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-my-notes-closed',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding', {
				url: '/funding',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/funding.html'
					}
				},
				redirectTo: 'ln.my-account.funding.deposit',
				title: 'funding'
			})
			.state('ln.my-account.funding.upload-documents', {
				url: '/upload-documents',
				templateUrl: folderPrefix + 'my-account/upload-documents.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-upload-documents',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.deposit', {
				url: '/deposit',
				templateUrl: folderPrefix + 'my-account/deposit.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-deposit',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.withdraw', {
				url: '/withdraw',
				templateUrl: folderPrefix + 'my-account/withdraw.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-withdraw',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.cancel-withdraw', {
				url: '/cancel-withdraw',
				templateUrl: folderPrefix + 'my-account/cancel-withdraw.html',
				controller: 'CancelWithdrawCtr',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-cancel-withdraw',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.banking-history', {
				url: '/banking-history',
				templateUrl: folderPrefix + 'my-account/banking-history.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-banking-history',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings', {
				url: '/settings',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/settings.html'
					}
				},
				redirectTo: 'ln.my-account.settings.personal-details',
				title: 'my-profile'
			})
			.state('ln.my-account.settings.personal-details', {
				url: '/personal-details',
				templateUrl: folderPrefix + 'my-account/personal-details.html',
				metaTitle: 'meta-title-settings-personal-details',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings.questionnaire', {
				url: '/questionnaire',
				templateUrl: folderPrefix + 'questionnaire.html',
				controller: 'QuestionnaireCtrl',
				controllerAs: '$ctrl',
				minUserStepAccessType: 2,
				metaTitle: 'meta-title-settings-questionnaire',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings.password', {
				url: '/password',
				templateUrl: folderPrefix + 'my-account/password.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-settings-password',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			});
			
			// enable html5Mode for pushstate ('#'-less URLs) //requires base tag in the head
			/*if (settings.isLive) {
				$locationProvider.html5Mode(true).hashPrefix('!');
			}*/
	}]);
	
spApp.config(['$compileProvider', function ($compileProvider) {
	//$compileProvider.debugInfoEnabled(false);
}]);

spApp.config(['ScrollBarsProvider', function (ScrollBarsProvider) {
	ScrollBarsProvider.defaults = {
		autoHideScrollbar: true,
		theme: 'dark',
		scrollButtons: {
			enable: false
		},
		advanced: {
			updateOnContentResize: true
		},
		axis: 'y',
		scrollInertia: 100
	}
}]);


spApp.service('Utils', [function(){
	var parseResponse = function(response){
		if(!response || !response.data){
			return null;
		}
		return response.data.data;
	}
	
	var DateManager = function(params){
		if(!params){
			params = {};
		}
		
		this.openDates = {};
		if(params.openDates && Array.isArray(params.openDates)){
			for(var i = 0; i < params.openDates.length; i++){
				this.openDates[params.openDates[i]] = false;
			}
		}
		this.format = params.format;
		this.onOpenCallback = params.onOpenCallback;
		
		if(!this.format){
			this.format = 'dd/MM/yyyy';
		}
		
		this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00',
			ngModelOptions: {
				timezone: 'UTC'
			}
		};
		
		this.openCalendar = function(e, date){
			if(this.onOpenCallback){
				this.onOpenCallback();
			}
			this.openDates[date] = true;
		}
		
		this.save = function(date){
			return date;
		}
	}
	
	var loadingCounter = [];
	var defaultLoadingContainer = '.content';
	function showLoading(params){
		if(!params){
			params = {};
		}
		if(!params.container){
			params.container = defaultLoadingContainer;
		}
		var container = params.container;
		var globalOverlay = params.globalOverlay;
		loadingCounter.push(params);
		$(params.container).addClass('loading');
		if(globalOverlay){
			$(defaultLoadingContainer).addClass('loading-global');
		}
	}
	function hideLoading(){
		var overlay = loadingCounter.pop();
		var overlaysCount = 0;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i].container == overlay.container){
				overlaysCount++;
			}
		}
		if(overlaysCount == 0){
			$(overlay.container).removeClass('loading');
		}
		
		var hasGlobal = false;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i].globalOverlay){
				hasGlobal = true;
			}
		}
		if(!hasGlobal){
			$(defaultLoadingContainer).removeClass('loading-global');
		}
	}
	
	function handleNetworkError(response) {
		logIt({'type':1,'msg':response.data});
	}
	
	function escapeRegExp(text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
	}
	
	return {
		settings: settings,
		parseResponse: parseResponse,
		DateManager: DateManager,
		escapeRegExp: escapeRegExp,
		showLoading: showLoading,
		hideLoading: hideLoading,
		handleNetworkError: handleNetworkError
	};
}]);

spApp.factory('scrollService', ['$window', function($window) {
	var fnList = [];
	
	$window.addEventListener('scroll', $.throttle(100, function() {
		fnList.forEach(function(fn) {
			fn();
		});
	}));
	
	function subscribe(fn) {
		var index = fnList.indexOf(fn);
		if (index === -1) {
			fnList.push(fn);
		}
	}
	
	function unsubscribe(fn) {
		var index = fnList.indexOf(fn);
		if (index !== -1) {
			fnList.splice(index, 1);
		}
	}
	
	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe
	};
}]);

spApp.factory('PdfPreviewService', ['$uibModal', function($uibModal) {
	var modalInstance;

	function open(url) {
		close();
		modalInstance = $uibModal.open({
			templateUrl: folderPrefix + 'pdfPreview.html',
			controller: ['url', '$uibModalInstance', PdfController],
			controllerAs: '$ctrl',
			windowClass: 'pdf-preview-modal',
			resolve: {
				url: function() {
					return url;
				}
			}
		});
		
		function PdfController(url, $uibModalInstance) {
			var ctrl = this;
			ctrl.pdfUrl = url;
			
			ctrl.pdfContainerClickHandler = function($event) {
				if ($($event.target).hasClass('pdfViewer')) {
					closeModal('overlay');
				}
			};
			
			ctrl.closeBtnClickHandler = function() {
				closeModal('close');
			};
			
			function closeModal(msg) {
				$uibModalInstance.close(msg || 'close');
			}
		}
	}
	
	function close() {
		if (modalInstance) {
			modalInstance.dismiss();
		}
	}
	
	return {
		open: open,
		close: close
	};
}]);

spApp.factory('QuestionnaireService', ['$http', '$q', function($http, $q) {
	function getQuestionnaire(isWithAnswers) {
		var url = isWithAnswers ? '/questionnaire/user/answer/get' : '/questionnaire/question/getAll';
		return questionnaire = $http.post(url, {})
			.then(function(res) {
				var resData = res.data;
				if (resData.responseCode === 0 && Object(resData.data) === resData.data) {
					var questionnaire = Object.keys(resData.data).map(function(key) {
						return resData.data[key];
					});
					return questionnaire;
				}
				return $q.reject(res);
			})
			.catch(function(err) {
				questionnaire = null;
				return $q.reject(err);
			});
	}
	
	function sendQuestionnaire(questionnaire, user) {
		var data = questionnaire.map(function(question) {
			var userId = user.id;
			var questionId = question.id;
			var answerId;
			switch (question.questionTypeId) {
				case 1:
					answerId = parseInt(question.answerId);
					break;
				case 2:
					var answer = question.qmAnswerList.filter(function(a) {
						return a.name.toLowerCase() === "yes" === question.answerId;
					});
					answerId = answer[0].id;
					break;
				case 3:
					answerId = null;
					break;
				case 4:
					answerId = question.qmAnswerListForSelect.model.id;
					break;
				default: 
					answerId = null;
			}
			var textAnswer = '';
			return {
				id: 0,
				userId: userId,
				questionId: questionId,
				answerId: answerId,
				textAnswer: textAnswer,
				actionSourceId: settings.actionSources.WEB.id
			};
		});
		
		var methodRequest = {
			data: data
		};
		return $http.post('/regulation/questionnaire/insert', methodRequest);
	}

	return {
		getQuestionnaire: getQuestionnaire,
		sendQuestionnaire: sendQuestionnaire
	};
}]);

spApp.factory('UserService', ['$http', '$q', '$rootScope', 'Utils', function($http, $q, $rootScope, Utils) {
	var refreshLoggedUserCanceler;
	function refreshLoggedUser() {
		if (!$rootScope.isLogged) return;
		if (refreshLoggedUserCanceler) {
			refreshLoggedUserCanceler.resolve();
		}
		refreshLoggedUserCanceler = $q.defer();
		$http.post('user/getById', {}, {timeout: refreshLoggedUserCanceler.promise})
			.then(function(response) {
				refreshLoggedUserCanceler = null;
				if (response.data.responseCode === 0) {
					$rootScope.setLoggedUser(Utils.parseResponse(response));
				}
			});
	}
	return {
		refreshLoggedUser: refreshLoggedUser
	};
}]);

spApp.factory('SignUpPersonalDetailsService', ['$http', '$rootScope', function($http, $rootScope) {
	function sendPersonalDetails(personalDetails) {
		var day = parseInt(personalDetails.dayOfBirth.day.getTextValue());
		var mounth = parseInt(personalDetails.dayOfBirth.mounth.getId()) - 1;
		var year = parseInt(personalDetails.dayOfBirth.year.getTextValue());
		
		var localBirthDate = new Date(year, mounth, day);
		
		var id = $rootScope.loggedUser.id || 0;

		var timeBirthDate = localBirthDate.getTime();
		var street = personalDetails.street;
		var cityName = personalDetails.cityName;
		var zipCode = personalDetails.zipCode;
		var countryByUser = parseInt(personalDetails.country.getId());
		
		var data = {  
			id: id,
			timeBirthDate: timeBirthDate,
			street: street,
			cityName: cityName,
			zipCode: zipCode,
			countryByUser: countryByUser
		};
		
		var methodRequest = {
			data: data
		};
		
		return $http.post('/user/update/stepB', methodRequest);
	}
	
	return {
		sendPersonalDetails: sendPersonalDetails
	};
}]);

spApp.factory('AuthService', ['$http', '$rootScope', '$q', '$state', 'Utils', 'AgreementModal', 'FiltersService', function($http, $rootScope, $q, $state, Utils, AgreementModal, FiltersService) {
	var rememberMeKey = 'rememberEmail';

	function login(credentials, handleErrors) {
		var methodRequest = {
			data: {
				username: credentials.userName,
				password: credentials.userPass,
				utcOffset: new Date().getTimezoneOffset()
			}
		};
		
		return $http.post('/login', methodRequest).then(function(response) {
			if (!handleErrors(response, 'login')) {
				doLogin(response, credentials);
				return $q.resolve(response);
			} else {
				return $q.reject(response);
			}
		});
	}
	
	function doLogin(response, credentials) {
		var isRemember = credentials ? credentials.rememberMe : false;
		var user = response.data.data;
		
		$rootScope.isLogged = true;
		$rootScope.setLoggedUser(Utils.parseResponse(response));
		
		FiltersService.getFilters(['languages'])
			.then(function(Filters) {
				var languageId = $rootScope.loggedUser.languageId;
				$rootScope.language = $rootScope.languages.filter(function(language) {
					return language.id == languageId;
				})[0];
				
				var languageCode = $rootScope.language.code;

				if (isRemember) {
					// Handle Safari Private Mode
					try {
						localStorage.setItem(rememberMeKey, user.email);
					} catch (e) {
						$log.log('No localStorage');
					}
				} else {
					localStorage.removeItem(rememberMeKey);
				}
				
				redirectAfterLogin(user, languageCode);
				
				if (user.isAcceptedTerms == 0) {
					AgreementModal.open();
				}
			});
	}
	
	function redirectAfterLogin(user, languageCode) {
		if(!languageCode){
			languageCode = $state.params.ln;
		}
		switch (user.userStep.id) {
			case 1:
				var personalDetailsState = 'ln.sign-up.personal-details';
				$state.go(personalDetailsState, {ln: languageCode});
				break;
			case 2:
				var questionnaireState = 'ln.sign-up.questionnaire';
				$state.go(questionnaireState, {ln: languageCode});
				break;
			case 3:
				$state.go('ln.my-account.settings.questionnaire', {ln: languageCode});
				break;
			case 4:
				$state.go('ln.my-account.funding.upload-documents', {ln: languageCode});
				break;
			case 5:
			default:
				$state.go('ln.products', {ln: languageCode});
		}
	}
	
	function logout(handleErrors) {
		return $http.post('logout', {}).then(function(response) {
			if (!handleErrors(response, 'logout')) {
				deauthorize();
				return $q.resolve(response);
			} else {
				return $q.reject(response);
			}
		});
	}
	
	function deauthorize() {
		if (!$rootScope.isLogged) return;
		$rootScope.isLogged = false;
		$rootScope.setLoggedUser(null);
		$state.go('ln.index');
	}
	
	function signup(credentials, handleErrors) {
		var data = {
			firstName: credentials.firstName,
			lastName: credentials.lastName,
			email: credentials.email,
			mobilePhone: credentials.phone,
			password: credentials.userPass,
			countryByPrefix: parseInt(credentials.phoneCode.getId()),
			languageId: $rootScope.language.id,
			isAcceptedTerms: credentials.acceptTerms ? 1 : 0
		};
		var methodRequest = {
			data: data
		};
		return $http.post('/user/signup', methodRequest).then(
			function(response) {
				if (!handleErrors(response, 'signup')) {
					doLogin(response);
					return $q.resolve(response);
				} else {
					return $q.reject(response);
				}
			});
	}
	
	function getRememberMeEmail() {
		return localStorage.getItem(rememberMeKey);
	}
	
	function resetPassword(credentials) {
		var methodRequest = {
			data: credentials.userEmail
		};
		
		return $http.post('/user/resetPassword', methodRequest);
	}
	
	function forgotPassword(credentials) {
		var communicationChannel;
		switch (credentials.via) {
			case "2":
				communicationChannel = "SMS";
				break;
			case "1":
			default:
				communicationChannel = "EMAIL";
		}

		var methodRequest = {
			data: {
				communicationChannel: communicationChannel,
				email: credentials.email
			}
		};
		
		return $http.post('/user/forgotPassword', methodRequest);
	}
	
	return {
		login: login,
		logout: logout,
		deauthorize: deauthorize,
		signup: signup,
		resetPassword: resetPassword,
		forgotPassword: forgotPassword,
		getRememberMeEmail: getRememberMeEmail,
		redirectAfterLogin: redirectAfterLogin
	};
}]);

spApp.service('FiltersService', ['$http', '$q', function($http, $q) {
	var cache = {};
	
	this.getFilters = function(filtersList) {
		var data = filtersList.filter(function(f) {
			return !(f in cache);
		});
		
		if (!data.length) {
			return $q.resolve(cache);
		}
		
		var methodRequest = {
			data: data
		};
		
		return $http.post('filters/get', methodRequest).then(function(res) {
			if (res.data && res.data.responseCode === 0) {
				var filters = res.data.data;
				cache = angular.merge({}, cache, filters);
				return $q.resolve(cache);
			}
			return $q.reject(res);
		});	
	};
}]);

spApp.animation('.slide-animation', function() {
	var defaultDuration = 250;
	function getAnimationDuration($element) {
		return parseInt($element.attr('data-slide-animation-duration')) || defaultDuration;
	}
	return {
		enter: function($element, done) {
			var duration = getAnimationDuration($element);
			var isFade = $element[0].hasAttribute('data-slide-animation-fade');
			$element
				.css('display', 'none')
				.stop(true, true)
				.slideDown(duration, done);
			if (isFade) {
				$element
					.css('opacity', 0)
					.animate({opacity: 1}, {queue: false, duration: duration + 100});
			}
		},
		leave: function(element, done) {
			var $element = $(element);
			var duration = getAnimationDuration($element);
			var isFade = $element[0].hasAttribute('data-slide-animation-fade');
			$element
				.stop(true, true)
				.slideUp(duration, done);
			if (isFade) {
				$element
					.animate({opacity: 0}, {queue: false, duration: duration - 50});
			}
		}
	};
});

// Accepts element or selector
spApp.constant('scrollToElement', function(el, additional) {
	var element = $(el).first();
	if (!element.length) return;
	var menuEl = document.querySelector('.menu');
	var menuBottom = menuEl.offsetTop + menuEl.offsetHeight;
	var additionalOffset = isUndefined(additional) ? 0 : parseInt(additional) || 0;
	$('body, html').stop().animate({
		scrollTop: element.offset().top - menuBottom - additionalOffset
	}, 500);
});

spApp.constant('lockScroll', function(isLock) {
	if (isLock) {
		$('body').addClass('scroll-lock');
	} else {
		$('body').removeClass('scroll-lock');
	}
});

spApp.factory('activeSection', ['$http', '$rootScope', '$q', '$state', 'Utils', function($http, $rootScope, $q, $state, Utils) {
	var currentSection = '';
	
	function get() {
		return currentSection;
	}
	
	function set(name) {
		currentSection = name;
	}
	
	return {
		get: get,
		set: set
	};
}]);

spApp.service('UnderlyingAssetsService', ['$http', '$q', function($http, $q) {
	var cache;
	
	this.getUnderlyingAssets = function() {
		if (cache) {
			return $q.resolve(cache);
		}
		
		return $http.post('market/underlyingAssets/get', {}).then(function(res) {
			if (res.data && res.data.responseCode === 0) {
				cache = res;
				return $q.resolve(cache);
			}
			return $q.reject(res);
		});	
	};
}]);

spApp.factory('AgreementModal', ['$uibModal', function($uibModal) {
	var modalInstance;
	
	function open() {
		close();
		modalInstance = $uibModal.open({
			templateUrl: folderPrefix + 'agreementModal.html',
			controller: ['$uibModalInstance', '$rootScope', '$http', AgreementController],
			controllerAs: '$ctrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'agreement-modal'
		});
		
		function AgreementController($uibModalInstance, $rootScope, $http) {
			var ctrl = this;
			
			ctrl.getMsgs = $rootScope.getMsgs;
			
			ctrl.acceptAgreementSubmitHandler = function(form) {
				if (form.$invalid) return;
				$http.post('/user/update/acceptTerms', {})
				.then(function(res) {
					if (res.data.responseCode === 0) {
						close();
					}
				});
			};
		}
	}
	
	function close() {
		if (modalInstance) {
			modalInstance.dismiss();
		}
	}
	
	return {
		open: open,
		close: close
	};
}]);

spApp.constant('setMetaTitle', function(title) {
	$('title').text(title || '');
});
spApp.component('productsGlossary', {
	templateUrl: folderPrefix + 'components/products-glossary.html',
	bindings: {
		template: '<'
	},
	controller: ['$rootScope', '$scope', function($rootScope, $scope) {
		this.getMsgs = $rootScope.getMsgs;
		this.isProductsGlossaryContentVisible = false;

		this.toggleProductsGlossaryContent = function() {
			this.isProductsGlossaryContentVisible = !this.isProductsGlossaryContentVisible;
			if(this.template == 'inline-popup'){
				$rootScope.blurWrapper = this.isProductsGlossaryContentVisible;
			}
		}.bind(this);

		this.close = function() {
			if(this.isProductsGlossaryContentVisible && this.template == 'inline-popup'){
				$rootScope.blurWrapper = false;
			}
			this.isProductsGlossaryContentVisible = false;
		}.bind(this);
		
		$scope.$on('$destroy', function() {
			if(this.template == 'inline-popup'){
				$rootScope.blurWrapper = false;
			}
		});
	}]
});

spApp.component('productsScenarioTooltip', {
	templateUrl: folderPrefix + 'products/products-scenario-tooltip.html',
	bindings: {
		formula: '<',
		denominationFormulaVisible: '<',
		isCentered: '<'
	},
	controller: ['$rootScope', function($rootScope) {
		this.getMsgs = $rootScope.getMsgs;
		this.isProductsScenarioTooltipVisible = false;
	}]
});

spApp.component('articles', {
	templateUrl: folderPrefix + 'articles.html',
	controller: ['articlesList', '$sce', '$rootScope', '$scope', 'PdfPreviewService', function(articlesList, $sce, $rootScope, $scope, PdfPreviewService) {
		this.articles = articlesList;
		this.trustAsHtml = $sce.trustAsHtml;
		this.getMsgs = $rootScope.getMsgs;
		this.isMobile = $rootScope.isMobile;
		
		this.articleLinkClickHandler = function(e, url) {
			if (!url) return;
			e.preventDefault();
			PdfPreviewService.open($sce.trustAsResourceUrl(url));
		};
		
		$scope.$on('$destroy', function() {
			PdfPreviewService.close();
		});
	}],
	controllerAs: 'articlesCtrl'
});

spApp.constant('articlesList', [
	{
		id: 1,
		logoUrl: 'images/articles/moneyadvice.svg',
		hpLogoUrl: 'images/articles/hp-moneyadvice.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneyadviceservice_structured-deposits-and-investment-product.pdf',
		title: 'article-1-title',
		subTitle: 'article-1-subTitle',
		shortText: 'article-1-shortText',
		shortDescription: 'article-1-short-description'
	},
	{
		id: 2,
		logoUrl: 'images/articles/bloomberg.svg',
		hpLogoUrl: 'images/articles/hp-bloomberg.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/bloomberg_asian-structured-products.pdf',
		title: 'article-2-title',
		subTitle: 'article-2-subTitle',
		shortText: 'article-2-shortText',
		shortDescription: 'article-2-short-description'
	},
	{
		id: 3,
		logoUrl: 'images/articles/bloomberg.svg',
		hpLogoUrl: 'images/articles/hp-bloomberg.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/bloomberg_sales-of-structured-products.pdf',
		title: 'article-3-title',
		subTitle: 'article-3-subTitle',
		shortText: 'article-3-shortText',
		shortDescription: 'article-3-short-description'
	},
	{
		id: 4,
		logoUrl: 'images/articles/instreet.svg',
		hpLogoUrl: 'images/articles/hp-instreet.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/instreet_structured-products-long-time.pdf',
		title: 'article-4-title',
		subTitle: 'article-4-subTitle',
		shortText: 'article-4-shortText',
		shortDescription: 'article-4-short-description'
	},
	{
		id: 5,
		logoUrl: 'images/articles/risk.svg',
		hpLogoUrl: 'images/articles/hp-risk.svg',
		pdfUrl: '',
		title: 'article-5-title',
		subTitle: 'article-5-subTitle',
		shortText: 'article-5-shortText',
		shortDescription: 'article-5-short-description'
	},
	{
		id: 6,
		logoUrl: 'images/articles/ddv.svg',
		hpLogoUrl: 'images/articles/hp-ddv.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/ddv_express-savers-advice-sp.pdf',
		title: 'article-6-title',
		subTitle: 'article-6-subTitle',
		shortText: 'article-6-shortText',
		shortDescription: 'article-6-short-description'
	},
	{
		id: 7,
		logoUrl: 'images/articles/moneywise.svg',
		hpLogoUrl: 'images/articles/hp-moneywise.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneywise_structured-products-under-the-microscope.pdf',
		title: 'article-7-title',
		subTitle: 'article-7-subTitle',
		shortText: 'article-7-shortText',
		shortDescription: 'article-7-short-description'
	},
	{
		id: 8,
		logoUrl: 'images/articles/moneywise.svg',
		hpLogoUrl: 'images/articles/hp-moneywise.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneywise_investment-guide-structured-products.pdf',
		title: 'article-8-title',
		subTitle: 'article-8-subTitle',
		shortText: 'article-8-shortText',
		shortDescription: 'article-8-short-description'
	},
	{
		id: 9,
		logoUrl: 'images/articles/express.svg',
		hpLogoUrl: 'images/articles/hp-express.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/express_savers-advice-structured-products.pdf',
		title: 'article-9-title',
		subTitle: 'article-9-subTitle',
		shortText: 'article-9-shortText',
		shortDescription: 'article-9-short-description'
	},
	{
		id: 10,
		logoUrl: 'images/articles/investorschronicle.svg',
		hpLogoUrl: 'images/articles/hp-investorschronicle.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/investors-chronicle_beat-stormy-markets-with-structured-products.pdf',
		title: 'article-10-title',
		subTitle: 'article-10-subTitle',
		shortText: 'article-10-shortText',
		shortDescription: 'article-10-short-description'
	},
	{
		id: 11,
		logoUrl: 'images/articles/moneyweek.svg',
		hpLogoUrl: 'images/articles/hp-moneyweek.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneyweek_its-time-to-welcome-back-structured-products.pdf',
		title: 'article-11-title',
		subTitle: 'article-11-subTitle',
		shortText: 'article-11-shortText',
		shortDescription: 'article-11-short-description'
	},
	{
		id: 12,
		logoUrl: 'images/articles/wallstreet.svg',
		hpLogoUrl: 'images/articles/hp-wallstreet.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/wsj_voices-when-structured-notes-make-sense.pdf',
		title: 'article-12-title',
		subTitle: 'article-12-subTitle',
		shortText: 'article-12-shortText',
		shortDescription: 'article-12-short-description'
	},
	{
		id: 13,
		logoUrl: 'images/articles/yahoo.svg',
		hpLogoUrl: 'images/articles/hp-yahoo.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/yahoo_an-introduction-to-structured-products.pdf',
		title: 'article-13-title',
		subTitle: 'article-13-subTitle',
		shortText: 'article-13-shortText',
		shortDescription: 'article-13-short-description'
	}
]);

spApp.component('profitProfileDiagramComponent', {
	templateUrl: folderPrefix + 'profit-profile-diagram.html',
	controller: ['$rootScope', '$timeout', 'ProfitProfileDiagrams', function($rootScope, $timeout, ProfitProfileDiagrams) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.isMobile = $rootScope.isMobile;
		
		ctrl.profitProfileTypes = ['capital-protection', 'yield-enhancement', 'participation'];
		ctrl.activeProfitProfileDiagram = 0;
		ctrl.profitProfileDiagrams = ProfitProfileDiagrams[ctrl.activeProfitProfileDiagram];
		
		ctrl.setActiveProfitProfileDiagram = function(activeNum) {
			if (ctrl.activeProfitProfileDiagram === activeNum) return;
			ctrl.activeProfitProfileDiagram = activeNum;
			ctrl.profitProfileDiagrams = [];
			$timeout(function() {
				ctrl.profitProfileDiagrams = ProfitProfileDiagrams[ctrl.activeProfitProfileDiagram];
			}, 200);
		};
		
		ctrl.slickSettings = {
			mobile: {
				event: {
					init: addClone,
					beforeChange: addClone
				}
			}
		};
		
		$rootScope.$watch('isMobile', function(isMobile) {
			ctrl.isMobile = isMobile;
		});
		
		function addClone(e) {
			var $slick = $(e.target);
			var $dots = $slick.find('.slick-dots').not('.slick-dots-clone');
			var $dotsClone = $dots.clone().addClass('slick-dots-clone');
			$slick.find('.slick-dots-clone').remove();
			$slick.append($dotsClone);
			$dotsClone.on('click', 'li', function() {
				var clickedEl = this;
				$dotsClone.find('li').each(function(index) {
					if (this === clickedEl) {
						$($dots.find('li')[index]).click();
						return false;
					}
				});
			});
		}
	}],
	controllerAs: '$ctrl'
});

spApp.constant('ProfitProfileDiagrams', [
	// 0
	[{
		id: 1,
		title: 'profit-profile-title-cpcp',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-rising-volatility', 'profit-profile-market-expectation-sharply-falling-underlying'],
		profitLossImageName: 'cpcp.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-participation-underlying'
		],
		sampleStateParam: {
			type: 1
		}
	},
	{
		id: 2,
		title: 'profit-profile-title-bcpc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-sharply-falling-underlying', 'profit-profile-market-expectation-underlying-not-going-to-touch'],
		profitLossImageName: 'bcpc.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-participation-underlying-up-to-barrier',
			'profit-profile-characteristic-possibility-of-rebate-payment',
			'profit-profile-characteristic-limited-profit-potential'
		],
		sampleStateParam: {
			type: 2
		}
	},
	{
		id: 3,
		title: 'profit-profile-title-cpcc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-sharply-falling-underlying'],
		profitLossImageName: 'cpcc.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-copupon-amount-dependent',
			'profit-profile-characteristic-periodic-coupon-payment',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 3
		}
	}],
	// 1
	[{
		id: 4,
		title: 'profit-profile-title-ibrc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways-decreasing', 'profit-profile-market-expectation-falling-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'ibrc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-barrier-never-reached',
			'profit-profile-characteristic-due-to-barrier-possibility-maximum-redemption-higher',
			'profit-profile-characteristic-if-barrier-breached-product-changes',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk-selling-underlying',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 6
		}
	},
	{
		id: 5,
		title: 'profit-profile-title-rc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-falling-volatility'],
		profitLossImageName: 'rc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-close-below',
			'profit-profile-characteristic-should-close-above',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-higher-coupons',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 4
		}
	},
	{
		id: 6,
		title: 'profit-profile-title-brc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-falling-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'brc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-barrier-never-reached',
			'profit-profile-characteristic-due-to-barrier-possibility-maximum-redemption-higher',
			'profit-profile-characteristic-if-barrier-breached-product-changes',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 5
		}
	},
	{
		id: 7,
		title: 'profit-profile-title-ec',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-decreasing-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'ec.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-underlying-trade-above',
			'profit-profile-characteristic-offers-possibility-early-redemption',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity'
		],
		sampleStateParam: {
			type: 7
		}
	}],
	// 2
	[{
		id: 8,
		title: 'profit-profile-title-oc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-rising-volatility'],
		profitLossImageName: 'oc.svg',
		characteristicsList: [
			'profit-profile-characteristic-participation-in-development',
			'profit-profile-characteristic-disproportionate-participation',
			'profit-profile-characteristic-reflects-underlying-price',
			'profit-profile-characteristic-risk-comparable'
		],
		sampleStateParam: {
			type: 8
		}
	}, {
		id: 9,
		title: 'profit-profile-title-bc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'bc.svg',
		characteristicsList: [
			'profit-profile-characteristic-participation-in-development',
			'profit-profile-characteristic-minimum-redemption-equal-nominal',
			'profit-profile-characteristic-if-barrier-breached-product-changes-tc',
			'profit-profile-characteristic-with-greater-risk-multiple-underlyings',
			'profit-profile-characteristic-reduced-risk'
		],
		sampleStateParam: {
			type: 9
		}
	}
	// , {
	// 	id: 10,
	// 	title: 'profit-profile-title-boc',
	// 	marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
	// 	profitLossImageName: 'boc.svg',
	// 	characteristicsList: [
	// 		'profit-profile-characteristic-participation-in-development',
	// 		'profit-profile-characteristic-disproportionate-participation',
	// 		'profit-profile-characteristic-minimum-redemption-equal-nominal',
	// 		'profit-profile-characteristic-if-barrier-breached-product-changes-oc',
	// 		'profit-profile-characteristic-with-greater-risk-multiple-underlyings',
	// 		'profit-profile-characteristic-reduced-risk'
	// 	],
	// 	sampleStateParam: {
	// 		type: 10
	// 	}
	// }
	]
]);


spApp.component('tutorialsComponent', {
	templateUrl: folderPrefix + 'tutorials.html',
	controller: ['$rootScope', 'tutorialsList', '$uibModal', function($rootScope, tutorialsList, $uibModal) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		
		ctrl.visibleProducts = {
			count: $rootScope.isMobile ? 3 : 6,
			increment: $rootScope.isMobile ? 3 : 6
		};
		
		ctrl.tutorialsList = tutorialsList;
		
		ctrl.loadMoreClickHandler = function() {
			ctrl.visibleProducts.count += ctrl.visibleProducts.increment;
		};
		
		ctrl.openVideoModal = function(tutorial) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: folderPrefix + 'components/video-modal.html',
				controller: ['$scope', '$uibModalInstance', 'video', function($scope, $uibModalInstance, video) {
					$scope.close = function(reason) {
						reason = reason || 'close';
						$uibModalInstance.close(reason);
					};
					
					$scope.video = video;
				}],
				size: 'lg',
				windowClass: 'video-modal',
				resolve: {
					video: ['$sce', function($sce) {
						return {
							name: tutorial.name,
							url: $sce.trustAsResourceUrl(tutorial.url)
						};
					}]
				}
			});
		};
	}],
	controllerAs: '$ctrl'
});

spApp.constant('tutorialsList', [
	{
		id: 1,
		name: 'The name of video',
		previwImage: 'tutorial-1.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 2,
		name: 'The name of video',
		previwImage: 'tutorial-2.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 3,
		name: 'The name of video',
		previwImage: 'tutorial-3.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 4,
		name: 'The name of video',
		previwImage: 'tutorial-4.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 5,
		name: 'The name of video',
		previwImage: 'tutorial-5.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 6,
		name: 'The name of video',
		previwImage: 'tutorial-6.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 7,
		name: 'The name of video',
		previwImage: 'tutorial-2.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	}
]);


spApp.component('underlyingAssets', {
	templateUrl: folderPrefix + 'underlying-assets.html',
	controller: ['$rootScope', '$timeout', 'UnderlyingAssetsService', function($rootScope, $timeout, UnderlyingAssetsService) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.isMobile = $rootScope.isMobile;
		
		var assetsList = [];
		ctrl.underlyingAssetsList = [];
		ctrl.underlyingAssetsFiltersList = [];
		
		ctrl.assetSearchValue = '';
		ctrl.underlyingAssetsFormHandler = function(form) {
			if (!ctrl.assetSearchValue || form.$invalid || isDuplicate(ctrl.underlyingAssetsFiltersList, ctrl.assetSearchValue)) return;
			ctrl.underlyingAssetsFiltersList.push(ctrl.assetSearchValue);
			ctrl.assetSearchValue = '';
			applyFiltersToAssets();
		};
		
		ctrl.deleteAsset = function(assetName) {
			var index = ctrl.underlyingAssetsFiltersList.indexOf(assetName);
			if (index === -1) return;
			ctrl.underlyingAssetsFiltersList.splice(index, 1);
			applyFiltersToAssets();
		};
		
		UnderlyingAssetsService.getUnderlyingAssets().then(function(res) {
			var assets = res.data.data;
			assetsList = Object.keys(assets).map(function(key) {
				return assets[key];
			});
			applyFiltersToAssets();
		});
		
		function isDuplicate(arr, value) {
			return arr.indexOf(value) !== -1;
		}
		
		function applyFiltersToAssets() {
			ctrl.underlyingAssetsList = [];
			showLoading();
			$timeout(function() {
				if (!ctrl.underlyingAssetsFiltersList.length) {
					ctrl.underlyingAssetsList = assetsList.slice();
					hideLoading();
					return;
				}
				ctrl.underlyingAssetsList = assetsList.filter(function(asset) {
					var displayName = ctrl.getMsgs(asset.displayName).toLowerCase();
					for (var i = 0; i < ctrl.underlyingAssetsFiltersList.length; i++) {
						var searchValue = ctrl.underlyingAssetsFiltersList[i];
						var index = displayName.indexOf(searchValue);
						if (index !== -1) {
							hideLoading();
							return true;
						}
					}
					hideLoading();
					return false;
				});
			});
		}
		
		function showLoading() {
			ctrl.loading = true;
		}
		
		function hideLoading() {
			$timeout(function() {
				ctrl.loading = false;
			}, 50);
		}
	}],
	controllerAs: '$ctrl'
});

spApp.component('loadingDots', {
	template: '<div class="three-bounce"><i class="dot"></i><i class="dot"></i><i class="dot"></i></div>'
});

spApp.component('basicsScrollableViewPort', {
	templateUrl: folderPrefix + 'basics-scrollable-viewPort.html',
	controller: ['$rootScope', '$scope', 'scrollToElement', 'activeSection', function($rootScope, $scope, scrollToElement, activeSection) {
		var ctrl = this;
		
		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.scrollToElement = scrollToElement;
		ctrl.getActiveSection = activeSection.get;
		
		ctrl.viewportList = [
			{
				id: 1,
				title: 'the-basics',
				selector: '[name="basics-the-basics"]'
			},
			{
				id: 2,
				title: 'basics-product-categories-title',
				selector: '[name="product-categories"]'
			},
			{
				id: 3,
				title: 'basics-profit-profile-diagram-title',
				selector: '[name="profit-profile-diagram"]'
			},
			{
				id: 4,
				title: 'glossary',
				selector: '[name="basics-glossary"]'
			},
			{
				id: 5,
				title: 'underlying-assets',
				selector: '[name="basics-underlying-assets"]'
			},
			{
				id: 6,
				title: 'articles',
				selector: '[name="basics-articles"]'
			},
		];
	}],
	controllerAs: '$ctrl'
});

spApp.controller('AboutCtr', ['$rootScope', '$scope', '$sce', function($rootScope, $scope, $sce) {
	$scope.videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/4F9ifFqaOX4?rel=0');
	var intervals = [];
	
	var $stepsInterval = setInterval(function(){
		$('.step').each(function(index, el){
			if(isElementInViewport(el)){
				$(el).addClass('visible');
			}
		});
		var counter = 0;
		$('.step').each(function(index, el){
			if($(el).hasClass('visible')){
				counter++;
			}
		});
		if(counter == $('.step').length){
			clearInterval($stepsInterval);
		}
	}, 500);
	intervals.push($stepsInterval);
	
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$(document).on('scroll', setAllParalax);
		$(window).on('resize', setAllParalax);
		
		var $bounceInterval = setInterval(function(){
			$('.left-right-section .inner-half').each(function(index, el){
				if(isElementInViewport(el)){
					$(el).addClass('visible');
					$(el).addClass($(el).data('animation-name'));
				}
			});
			var counter = 0;
			$('.left-right-section .inner-half').each(function(index, el){
				if($(el).hasClass('visible')){
					counter++;
				}
			});
			if(counter == $('.left-right-section .inner-half').length){
				clearInterval($bounceInterval);
			}
		}, 500);
		intervals.push($bounceInterval);
	}
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, textRatio: 0.66, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 120});
		setParalax({container: '.video', imageRatio: 0.8, imageWidth: 1920, imageHeight: 960, textRatio: 0, doubleMovement: true});
		setParalax({container: '.bottom-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 1274, imageMinHeight: 800, textRatio: 0});
	}
	
	$scope.$on('$destroy', function() {
		intervals.forEach(function(interval) {
			clearInterval(interval);
		});
		$(document).off('scroll', setAllParalax);
		$(window).off('resize', setAllParalax);
	});
}]);
spApp.controller('ContactCtr', ['$rootScope', '$scope', '$http', '$interval', 'SingleSelect', function($rootScope, $scope, $http, $interval, SingleSelect) {
	$scope.filter = {};
	$scope.contactUs = {};
	
	$scope.contactUs.phoneCode = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var phoneCodeList = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			phoneCodeList.push({
				id: key,
				suffixOptionName: '+' + $rootScope.countries[key].phoneCode,
				suffixDisplayName: false,
				name: $scope.getMsgs($rootScope.countries[key].displayName),
				displayName: '+' + $rootScope.countries[key].phoneCode,
				phoneCode: $rootScope.countries[key].phoneCode,
				flagCode: $rootScope.countries[key].a2.toLowerCase()
			});
		}
	}
	$scope.contactUs.phoneCode.fillOptions(phoneCodeList);
	if ($rootScope.userLogout && $rootScope.userLogout.country) {
		$scope.contactUs.phoneCode.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	}
	
	var intervals = [];
	
	$scope.CONTACT_SCREEN_STATES = {
		initial: 1,
		submitted: 2
	}
	
	$scope.state = $scope.CONTACT_SCREEN_STATES.initial;

	$scope.filter.country = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase(),
				supportPhone: $rootScope.countries[key].supportPhone
			});
		}
	}
	$scope.filter.country.fillOptions(countries);
	$scope.filter.country.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	
	$scope.contactUs.issue = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'issue'}, mutateCallback: $scope.getMsgs, noSort: true});
	//$scope.contactUs.issue.fillOptions([{id: 1, name: 'issue 1'}, {id: 2, name: 'issue 2'}]);
	var issueOptions = [];
	for(issueKey in settings.contactUsIssues){
		if(settings.contactUsIssues.hasOwnProperty(issueKey)){
			issueOptions.push(settings.contactUsIssues[issueKey]);
		}
	}
	$scope.contactUs.issue.fillOptions(issueOptions);
	
	$scope.patterns = {
		email: regEx_email,
		charsOnly: regEx_charsOnly,
		phone: regEx_phone
	};
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, textRatio: 0.66, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 280});
	}

	if($rootScope.isMobile === false){
		setAllParalax();
		$(document).on('scroll', setAllParalax);
		$(window).on('resize', setAllParalax);
		
		$bounceInterval = $interval(function(){
			$('.left-right-section .inner-half').each(function(index, el){
				if(isElementInViewport(el)){
					$(el).addClass('visible');
					$(el).addClass($(el).data('animation-name'));
				}
			});
			var counter = 0;
			$('.left-right-section .inner-half').each(function(index, el){
				if($(el).hasClass('visible')){
					counter++;
				}
			});
			if(counter == $('.left-right-section .inner-half').length){
				$interval.cancel($bounceInterval);
			}
		}, 500);
		intervals.push($bounceInterval);
	}
	
	$scope.$on('$destroy', function() {
		intervals.forEach(function(interval) {
			clearInterval(interval);
		});
		$(document).off('scroll', setAllParalax);
		$(window).off('resize', setAllParalax);
	});
	
	$scope.send = function(form) {
		if (form.$invalid) return;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			firstName: $scope.contactUs.firstName,
			lastName: $scope.contactUs.lastName,
			email: $scope.contactUs.email,
			countryId: $scope.contactUs.phoneCode.getId(),
			mobilePhone: $scope.contactUs.mobilePhone,
			issue: $scope.contactUs.issue.getId(),
			languageId: $scope.language.id,
			comments: $scope.contactUs.comments
		};
		$http.post('contact/insertContactUs', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'insertContactUs')) {
					$scope.state = $scope.CONTACT_SCREEN_STATES.submitted;
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
}]);
spApp.controller('FAQCtr', ['$rootScope', '$scope', function($rootScope, $scope) {
	
	$scope.FAQ_CATEGORIES = {
		trading: 1,
		product_types: 2,
		deposit_and_withdraw: 3,
		my_account: 4,
		technical: 5
	};
	
	$scope.filter = {};
	$scope.filter.currentCategory = $scope.FAQ_CATEGORIES.trading;

	$scope.faqItems = [
		{title: $rootScope.getMsgs('faq-trading-structured-product-title'), content: $rootScope.getMsgs('faq-trading-structured-product-content'), category: $scope.FAQ_CATEGORIES.trading, active: true},
		{title: $rootScope.getMsgs('faq-trading-underlying-asset-title'), content: $rootScope.getMsgs('faq-trading-underlying-asset-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-term-sheet-title'), content: $rootScope.getMsgs('faq-trading-term-sheet-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-barrier-event-title'), content: $rootScope.getMsgs('faq-trading-barrier-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-capital-protected-product-title'), content: $rootScope.getMsgs('faq-trading-capital-protected-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-yield-enhancement-product-title'), content: $rootScope.getMsgs('faq-trading-yield-enhancement-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-participation-product-title'), content: $rootScope.getMsgs('faq-trading-participation-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-credit-risk-title'), content: $rootScope.getMsgs('faq-trading-credit-risk-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-calculate-redemption-amount-title'), content: $rootScope.getMsgs('faq-trading-calculate-redemption-amount-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-events-affecting-product-title'), content: $rootScope.getMsgs('faq-trading-events-affecting-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-buy-sp-different-currency-title'), content: $rootScope.getMsgs('faq-trading-buy-sp-different-currency-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-invest-in-sp-title'), content: $rootScope.getMsgs('faq-trading-invest-in-sp-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-issuer-knows-his-sp-title'), content: $rootScope.getMsgs('faq-trading-issuer-knows-his-sp-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-case-of-credit-event-title'), content: $rootScope.getMsgs('faq-trading-case-of-credit-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-insured-against-credit-event-title'), content: $rootScope.getMsgs('faq-trading-insured-against-credit-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-secondary-market-price-title'), content: $rootScope.getMsgs('faq-trading-secondary-market-price-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-initial-fixing-level-title'), content: $rootScope.getMsgs('faq-trading-initial-fixing-level-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-final-fixing-level-title'), content: $rootScope.getMsgs('faq-trading-final-fixing-level-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-issue-price-title'), content: $rootScope.getMsgs('faq-trading-issue-price-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-Quanto-title'), content: $rootScope.getMsgs('faq-trading-Quanto-content'), category: $scope.FAQ_CATEGORIES.trading},
		
		{title: $rootScope.getMsgs('faq-product-types-capital-guarantee-products-title'), content: $rootScope.getMsgs('faq-product-types-capital-guarantee-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		{title: $rootScope.getMsgs('faq-product-types-yield-enhancement-products-title'), content: $rootScope.getMsgs('faq-product-types-yield-enhancement-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		{title: $rootScope.getMsgs('faq-product-types-participation-products-title'), content: $rootScope.getMsgs('faq-product-types-participation-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-deposit-options-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-deposit-options-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-currency-in-my-account-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-currency-in-my-account-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-minimal-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-minimal-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-withdraw-profit-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-withdraw-profit-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-must-deposit-register-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-must-deposit-register-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-cancel-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-cancel-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-request-wire-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-request-wire-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		
		{title: $rootScope.getMsgs('faq-my-account-forgotten-password-title'), content: $rootScope.getMsgs('faq-my-account-forgotten-password-content'), category: $scope.FAQ_CATEGORIES.my_account},
		{title: $rootScope.getMsgs('faq-my-account-transaction-history-title'), content: $rootScope.getMsgs('faq-my-account-transaction-history-content'), category: $scope.FAQ_CATEGORIES.my_account},
		{title: $rootScope.getMsgs('faq-my-account-update-personal-details-title'), content: $rootScope.getMsgs('faq-my-account-update-personal-details-content'), category: $scope.FAQ_CATEGORIES.my_account},
		
		{title: $rootScope.getMsgs('faq-technical-download-software-title'), content: $rootScope.getMsgs('faq-technical-download-software-content'), category: $scope.FAQ_CATEGORIES.technical},
		{title: $rootScope.getMsgs('faq-technical-change-language-title'), content: $rootScope.getMsgs('faq-technical-change-language-content'), category: $scope.FAQ_CATEGORIES.technical},
	];

	$scope.selectCategory = function(category){
		$scope.filter.currentCategory = category;
	};
	$scope.isCategoryActive = function(category){
		return $scope.filter.currentCategory == category;
	};
	
	$scope.toggleItem = function(item){
		for(var i = 0; i < $scope.faqItems.length; i++){
			if($scope.faqItems[i] != item){
				$scope.faqItems[i].active = false;
			}
		}
		item.active = !item.active;
	};
	
}]);
spApp.controller('MyAccountCtr', ['$scope', function($scope) {
	$scope.isMainMenuVisisbleForMobile = false;
	$scope.toggleMainMenuVisibility = function(isVisisble) {
		if (typeof(isVisisble) === 'boolean') {
			$scope.isMainMenuVisisbleForMobile = isVisisble;
			return;
		}
		$scope.isMainMenuVisisbleForMobile = !$scope.isMainMenuVisisbleForMobile;
	};
}]);

spApp.controller('PersonalDetailsCtr', ['$rootScope', '$scope', '$http', 'Utils', 'SingleSelect', 'ScrollTop', function($rootScope, $scope, $http, Utils, SingleSelect, ScrollTop) {
	$scope.patterns = {};
	
	$scope.isPersonalDetailsChanged = false;
	
	$scope.filter = {};
	$scope.personalDetails = {};
	$scope.dateManager = new Utils.DateManager();
	
	$scope.filter.country = new SingleSelect.InstanceClass({mutateCallback: $scope.getMsgs, isSmart: true, filterPlaceholder: $scope.getMsgs('search')});
	$scope.filter.phoneCode = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	
	var countriesList = [];
	var phoneCodeList = [];
	
	// Add setTimeout to reduce UI response, when user goes to this state.
	$scope.action = 'loading';
	setTimeout(function() {
		Object.keys($rootScope.countries).forEach(function(key) {
			var country = $rootScope.countries[key];
			countriesList.push({id: key, name: country.displayName});
			phoneCodeList.push({
				id: key,
				suffixOptionName: '+' + country.phoneCode,
				suffixDisplayName: false,
				name: $scope.getMsgs(country.displayName),
				displayName: '+' + country.phoneCode,
				phoneCode: country.phoneCode,
				flagCode: country.a2.toLowerCase()
			});
		});
		
		$scope.filter.country.fillOptions(countriesList);
		$scope.filter.phoneCode.fillOptions(phoneCodeList);
		
		getById();
	});

	
	$scope.filter.gender = new SingleSelect.InstanceClass({noSort: true});
	var genders = [{id: 'MALE', name: 'Male'}, {id: 'FEMALE', name: 'Female'}];
	$scope.filter.gender.fillOptions(genders);

	$scope.edit = function(form){
		if (form.$invalid) return;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var personalDetails = $scope.personalDetails;
		var data = angular.copy(personalDetails);
		data.timeBirthDate = personalDetails.timeBirthDate.getTime() - (personalDetails.timeBirthDate.getTimezoneOffset() * 60 * 1000);
		data.gender = $scope.filter.gender.getId();
		data.countryByUser = parseInt($scope.filter.country.getId()) || 0;
		data.countryByUser === -1 ? 0 : data.countryByUser;
		data.countryByPrefix = parseInt($scope.filter.phoneCode.getId()) || 0;
		data.countryByPrefix === -1 ? 0 : data.countryByPrefix;

		var methodRequest = {
			data: data
		};

		$http.post('user/edit', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'edit')) {
					$scope.isPersonalDetailsChanged = true;
				}
				ScrollTop();
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
	
	function getById() {
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		$http.post('user/getById', {}).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getById')) {
					$scope.personalDetails = Utils.parseResponse(response);
					var timeBirthDateLocal = new Date($scope.personalDetails.timeBirthDate);
					$scope.personalDetails.timeBirthDate = new Date(timeBirthDateLocal.getTime() + timeBirthDateLocal.getTimezoneOffset() * 60 * 1000);
					$scope.filter.gender.setById($scope.personalDetails.gender);
					$scope.filter.country.setById($scope.personalDetails.countryByUser || -1);
					$scope.filter.phoneCode.setById($scope.personalDetails.countryByPrefix || -1);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
}]);

spApp.controller('PasswordCtr', ['$scope', '$http', 'vcRecaptchaService', 'ScrollTop', function($scope, $http, recaptcha, ScrollTop) {
	$scope.response = null;
	$scope.widgetId = null;
	
	$scope.setWidgetId = function (widgetId) {
		$scope.widgetId = widgetId;
	};

	$scope.setResponse = function (response) {
		$scope.response = response;
	};

	$scope.cbExpiration = function() {
		$scope.response = null;
		recaptcha.reload($scope.widgetId);
	};
	
	$scope.checkRecaptchaError = function(_form){
		return (_form.$submitted && _form.$error.recaptcha);
	};
	
	$scope.password = {
		currentPassword: '',
		newPassword: '',
		retypeNewPassword: ''
	};
	
	$scope.isPasswordChanged = false;
	
	$scope.submit = function(form) {
		if (form.$invalid) return;
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			currentPassword: $scope.password.currentPassword,
			newPassword: $scope.password.newPassword,
			retypePassword: $scope.password.retypeNewPassword
		};
		$http.post('user/changePassword', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'changePassword')) {
					$scope.isPasswordChanged = true;
				}
				ScrollTop();
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
}]);


spApp.controller('BankingHistoryCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$interval', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'Pagination', 'SingleSelect', 'Multiselect', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $interval, $compile, $filter, $uibModal, Utils, Permissions, Pagination, SingleSelect, Multiselect) {
	
	$scope.TRANSACTION_OPERATION_CREDIT = 1;

	var to = new Date();
	var from = new Date();
	from.setMonth(to.getMonth() - 3);
	$scope.filter = {
		from: from,
		to: to
	};
	
	$scope.dateManager = new Utils.DateManager();
	
	$scope.results = [];
	
	$scope.filtered = {};
	$scope.filtered.results = [];
	
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	
	$scope.updatePages = function(){
		$timeout(function(){
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.results.length, $scope.resultsPerPage);
		});
	}
	
	$scope.changePage = function(page){
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.results.length, $scope.resultsPerPage);
	}
	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideSummary: true, hideGroupNavigation: true});
	
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		$scope.getByUserId();
	}
	
	$scope.getByUserId = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId,
			from: $scope.filter.from ? $scope.filter.from.getTime() : null,
			to: $scope.filter.to ? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
		}
		$http.post('transaction/bankingHistory', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getByUserId')) {
					$scope.results = Utils.parseResponse(response);
					$scope.pagination.setPages($scope.page, $scope.results ? $scope.results.length : 0, $scope.resultsPerPage);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
//	$scope.datesFilter = function(result){
//		var hide = false;
//		
//		if($scope.filter.from && result.timeCreated < $scope.filter.from.getTime()){
//			hide = true;
//		}
//		if($scope.filter.to){
//		var toDate = new Date($scope.filter.to);
//			toDate.setDate(toDate.getDate() + 1);
//			if(result.timeCreated > toDate.getTime()){
//				hide = true;
//			}
//		}
//		
//		return !hide;
//	}
	
}]);

spApp.controller('WithdrawCtr', ['$sce', '$rootScope', '$scope', '$http', 'Utils', function($sce, $rootScope, $scope, $http, Utils) {
	$scope.withdraw = {};
	
	$scope.WITHDRAW_SCREEN_STATES = {
		initial: 1,
		waiting_confirmation: 2,
		submitted: 3,
		not_submitted: 4
	}
	
	$scope.state = $scope.WITHDRAW_SCREEN_STATES.initial;
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.submitInitial = function(form){
		if (form.$invalid) return;
		sendWithdraw();
	};
	
	$scope.submitFinal = function() {
		sendWithdraw(true);
	};
	function sendWithdraw(isConfirm) {
		$scope.withdrawError = null;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var data = {
			details: getDetails(),
			paymentType: { 
				id: Utils.settings.paymentTypes.BANK_WIRE.id
			},
			amount: $scope.formatAmountForDb({amount: $scope.withdraw.withdrawAmount})
		};
		var methodRequest = {
			data: data
		};
		var url = isConfirm ? 'payment/withdraw/confirm' : 'payment/withdraw';
		$http.post(url, methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdraw')) {
					if (isConfirm) {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.submitted;
					} else {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.waiting_confirmation;
						$scope.withdrawalFeeAmount = response.data.data.minimumFeeAmount;
					}
				} else {
					if (isConfirm) {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.not_submitted;
					} else {
						$scope.withdrawError = response.data;
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	$scope.maxLength = {
		bankName: 20,
		branchNumber: 30,
		withdrawAmount: 9
	};
	$scope.patterns = {
		digits: regEx_digits
	};
	
	function getDetails() {
		return {
			beneficiaryName: $scope.withdraw.beneficiaryName,
			iban: $scope.withdraw.iban,
			swift: $scope.withdraw.swift,
			accountNum: parseInt($scope.withdraw.accountNumber),
			bankName: $scope.withdraw.bankName,
			branchNumber: parseInt($scope.withdraw.branchNumber),
			branchAddress: $scope.withdraw.branchAddress
		};
	}
}]);

spApp.controller('CancelWithdrawCtr', ['$sce', '$rootScope', '$scope', '$http', 'Utils', 'Pagination', function($sce, $rootScope, $scope, $http, Utils, Pagination) {
	$scope.withdrawals = [];
	
	$scope.resultsPerPage = 15;
	$scope.page = 1;
	
	$scope.changePage = function(page){
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.withdrawals.length, $scope.resultsPerPage);
	};

	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideSummary: true, hideGroupNavigation: true});
	
	getWithdrawalReverseList();
	
	function getWithdrawalReverseList() {
		$scope.action = 'loading';
		$http.post('/transaction/withdrawal/reverse/list', {}).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdrawal-reverse')) {
					$scope.withdrawals = response.data.data || [];
					$scope.pagination.setPages($scope.page, $scope.withdrawals ? $scope.withdrawals.length : 0, $scope.resultsPerPage);
				}
			},
			function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			}
		);
	}

	// for(var i = 0; i < 5; i++){
	// 	var d = new Date();
	// 	d.setMonth(d.getMonth() - 1);
	// 	d.setDate(i + 1);
	// 	$scope.withdrawals.push({
	// 		id: i + 1, 
	// 		date: d, 
	// 		description: 'Lorem ipsum dolor sit amet'+ (i + 1) +', consectetur adipisicing elit.', 
	// 		amount: 125000*(i + 1), 
	// 		checked: false, 
	// 		currencyId: 3
	// 	});
	// }
	
	$scope.pagination.setPages($scope.page, $scope.withdrawals.length, $scope.resultsPerPage);
	
	$scope.uncheckOthers = function(id) {
		$scope.withdrawals.forEach(function(withdrawal) {
			if (withdrawal.id !== id && withdrawal.checked) {
				withdrawal.checked = false;
			}
		});
	};
	
	$scope.checkedCount = function(){
		var count = 0;
		for(var i = 0; i < $scope.withdrawals.length; i++){
			if($scope.withdrawals[i].checked){
				count++;
			}
		}
		return count;
	};
	
	$scope.submit = function() {
		$scope.action = 'loading';
		var checkedIds = $scope.withdrawals.reduce(function(idList, withdrawal) {
			if (withdrawal.checked) {
				return idList.concat({id: withdrawal.id});
			}
			return idList;
		}, []);
		var methodRequest = {
			data: checkedIds[0]
		};
		$http.post('/transaction/withdrawal/reverse', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdrawal-reverse')) {
					getWithdrawalReverseList();
				}
			},
			function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			}
		);
	};
}]);
spApp.controller('MyNotesCtr', ['$rootScope', '$scope', '$http', 'Utils', 'ScrollTop', function($rootScope, $scope, $http, Utils, ScrollTop) {
	
	$scope.results = [];
	$scope.products = [];
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		if($rootScope.$state.includes('ln.my-account.my-notes.open')){
			$scope.getNotesOpen();
		}else{
			$scope.getNotesClosed();
		}
	}
	
	$scope.getNotesOpen = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		return $http.post('investment/get/open', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getNotesOpen')) {
					$scope.results = Utils.parseResponse(response);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getNotesClosed = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		$http.post('investment/get/close', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getNotesClosed')) {
					var results = Utils.parseResponse(response);
					$scope.notes = [];
					$scope.products = {};
					for(var i = 0; i < results.length; i++){
						$scope.products[results[i].product.id] = results[i].product;
						for(var j = 0; j < results[i].myNotes.length; j++){
							$scope.notes.push(results[i].myNotes[j]);
						}
					}
					for(key in $scope.products){
						if($scope.products.hasOwnProperty(key)){
							$scope.productLoadTypesConfig($scope.products[key]);
						}
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.sumInvestmentsAmount = function(productGroup){
		var sum = 0;
		
		for(var i = 0; i < productGroup.myNotes.length; i++){
			sum += productGroup.myNotes[i].investment.amount;
		}
		
		return sum;
	}
	
	$scope.investmentSellForAmount = function(productGroup, investment){
		return investment.returnAmount;
	}
	
	$scope.sellForAmount = function(productGroup){
		var sellForAmount = 0;
		
		for(var i = 0; i < productGroup.myNotes.length; i++){
			sellForAmount += $scope.investmentSellForAmount(productGroup, productGroup.myNotes[i].investment);
		}
		
		return sellForAmount;
	}
	
	$scope.isOpenInvestment = function(investment){
		return investment.investmentStatus.id == settings.investmentStatuses.OPEN;
	}
	
	$scope.openSellPopup = function(productGroup, note){
		for(var i = 0; i < $scope.results.length; i++){
			$scope.closeSellPopup($scope.results[i]);
			for(var j = 0; j < $scope.results[i].myNotes.length; j++){
				$scope.closeSellPopup($scope.results[i], $scope.results[i].myNotes[j]);
			}
		}
		if(note){
			note._sellPopupVisible = true;
		}else{
			productGroup._sellPopupVisible = true;
		}
	}
	$scope.closeSellPopup = function(productGroup, note){
		if(note){
			note._sellPopupVisible = false;
		}else{
			productGroup._sellPopupVisible = false;
		}
	}
	
	$scope.openSellErrorPopup = function(productGroup, note, sellError){
		for(var i = 0; i < $scope.results.length; i++){
			$scope.closeSellErrorPopup($scope.results[i]);
			for(var j = 0; j < $scope.results[i].myNotes.length; j++){
				$scope.closeSellErrorPopup($scope.results[i], $scope.results[i].myNotes[j]);
			}
		}
		if(note){
			note._sellError = sellError;
			note._sellErrorPopupVisible = true;
		}else{
			productGroup._sellError = sellError;
			productGroup._sellErrorPopupVisible = true;
		}
	}
	$scope.closeSellErrorPopup = function(productGroup, note){
		if(note){
			note._sellError = '';
			note._sellErrorPopupVisible = false;
		}else{
			productGroup._sellError = '';
			productGroup._sellErrorPopupVisible = false;
		}
	}
	
	$scope.reload = function(){
		$rootScope.$state.reload();
	}
	
	$scope.sell = function(productGroup, note){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading-sell';
		$scope.sellDate = '';
		var investmentsId = [];
		if(note){
			investmentsId.push(note.investment.id);
		}else{
			for(var i = 0; i < productGroup.myNotes.length; i++){
				investmentsId.push(productGroup.myNotes[i].investment.id);
			}
		}
		var methodRequest = {};
		methodRequest.data = {
			product: {
				id: productGroup.product.id,
				bid: productGroup.product.bid
			},
			investmentsId: investmentsId
		}
		$http.post('investment/sell/now', methodRequest).then(
			function(response) {
				var data = Utils.parseResponse(response);
				$scope.action = '';
				$scope.closeSellPopup(productGroup, note);
				
				var sellError = $scope.handleSellErrors(response);
				if(sellError){
					$scope.openSellErrorPopup(productGroup, note, sellError);
				}else{
					if (!$scope.handleErrors(response, 'sell')) {
						if(data.length > 0){
							$rootScope.loggedUser.balance = data[data.length - 1].user.balance;
						}
						$scope.sellDate = new Date();
						$scope.openSellConfirmedPopup(productGroup, note);
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	$scope.handleSellErrors = function(response){
		if(response.data.responseCode == errorCodeMap.invalid_input){
			return 'ok';
		}else if(response.data.responseCode == errorCodeMap.sell_now_bid){
			return 'refresh';
		}else{
			return false;
		}
	}
	
	$scope.openSellConfirmedPopup = function(productGroup, note){
		if(note){
			note._sellConfirmedPopupVisible = true;
		}else{
			productGroup._sellConfirmedPopupVisible = true;
		}
	}
	$scope.closeSellConfirmedPopup = function(productGroup, note){
		if(note){
			note._sellConfirmedPopupVisible = false;
			for(var i = 0; i < productGroup.myNotes.length; i++){
				if(productGroup.myNotes[i] == note){
					productGroup.myNotes.splice(i, 1);
				}
			}
		}else{
			productGroup._sellConfirmedPopupVisible = false;
		}
		if(!note || productGroup.myNotes.length == 0){
			var index = -1;
			for(var i = 0; i < $scope.results.length; i++){
				if($scope.results[i].product.id == productGroup.product.id){
					index = i;
					break;
				}
			}
			if(index > -1){
				$scope.results.splice(index, 1);
			}
		}
		//Wiat for animation finished
		setTimeout(function() {
			$scope.getNotesOpen();
			ScrollTop();
		}, 250);
	}
}]);
spApp.controller('ProductsCtr', ['$sce', '$rootScope', '$scope', '$state', '$http', '$timeout', '$location', 'Utils', 'SingleSelect', 'setMetaTitle', function($sce, $rootScope, $scope, $state, $http, $timeout, $location, Utils, SingleSelect, setMetaTitle) {
	if ($state.includes('ln.product') && typeof($state.params.productId) === "string") {
		$rootScope.redirectAfterLogin = {
			state: $state.$current.name,
			params: $state.params
		};
	}
	var MJrendererInTimeout = false;
	var MJrerender = function(){
		if(!MJrendererInTimeout){
			MJrendererInTimeout = true;
			setTimeout(function(){
				MathJax.Hub.Queue(["Rerender",MathJax.Hub]);
				MJrendererInTimeout = false;
			}, 500);
		}
	};
	$scope.loadMathJax = function(){
		//Load MathJax
		MathJax.Hub.Config({
			messageStyle: "none",
			"HTML-CSS": {
				showMathMenu: false,
				availableFonts: ["TeX"],
				linebreaks: {automatic: $rootScope.isMobile ? true : false, width: "container"},
				styles: {
					".MathJax": {
						"font-weight": 500,
						"font-size": "1.2em",
						"line-height": "1.5"
					}
				}
			},
			MMLorHTML: {prefer: "HTML"}
		});
		MathJax.Hub.Register.StartupHook("HTML-CSS Jax Ready",function () {
			var VARIANT = MathJax.OutputJax["HTML-CSS"].FONTDATA.VARIANT;
			VARIANT["normal"].fonts.unshift("MathJax_SansSerif");
			VARIANT["bold"].fonts.unshift("MathJax_SansSerif-bold");
			VARIANT["italic"].fonts.unshift("MathJax_SansSerif-italic");
			VARIANT["-tex-mathit"].fonts.unshift("MathJax_SansSerif-italic");
		});
		MathJax.Hub.Register.StartupHook("SVG Jax Ready",function () {
			var VARIANT = MathJax.OutputJax.SVG.FONTDATA.VARIANT;
			VARIANT["normal"].fonts.unshift("MathJax_SansSerif");
			VARIANT["bold"].fonts.unshift("MathJax_SansSerif-bold");
			VARIANT["italic"].fonts.unshift("MathJax_SansSerif-italic");
			VARIANT["-tex-mathit"].fonts.unshift("MathJax_SansSerif-italic");
		});
		MathJax.Hub.Configured();
		
		$(window).on('resize', MJrerender);
	}
	
	$scope.showCredentials = {state: Utils.settings.credentialsView.NONE};
	
	$scope.action = 'loading';
	
	var CURRENCY_ID_EURO = 3;
	
	$scope.INVESTMENT_STATES = {
		initial: 1,
		purchased: 2,
		insufficient_funds: 3,
		general_error: 4,
		insufficient_funds_above_minumum: 5,
		insufficient_funds_below_minimum_secondary: 6,
		below_minumum: 7,
		above_maximum: 8,
		already_secured: 9,
		suspended: 10,
		unavailable: 11,
		already_issued: 12,
		deviation: 13,
		deviation_secondary_primary: 14,
		complete_registration: 15
	}
	
	$scope.investmentState = $scope.INVESTMENT_STATES.initial;
	
	$scope.MATURITY_OPTIONS = {
		primary_all: 1,
		primary_3_6: 2,
		primary_6_12: 3,
		primary_12: 4,
		secondary_all: 5,
		secondary_0_3: 6,
		secondary_3_6: 7,
		secondary_6: 8
	};
	
	$scope.PRODUCT_CATEGORIES = {
		capital_protection: 1,
		yield_enhancement: 2,
		participation: 3
	}
	
	$scope.init = {};
	
	$scope.filter = {};
	
	$scope.singleProduct = true;
	
	$scope.selectedProduct = {};
	
	$scope.products = [];
	$scope.filtered = {
		products: []
	}
	
	$scope.investment = {};
	
	var intervals = [];
	
	$scope.filtered = {products: []};
	$scope.visibleProducts = {count: 12, increment: 12};
	
	$scope.isInvAmountManual = false;
	
	$scope.initFiltersOnReady = function(){
		$scope.loadMathJax();
		
		$scope.filter.assets = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, isSmart: true, filterPlaceholder: $scope.getMsgs('search'), iconClosed: '', iconOpen: ''});
		$scope.filter.type = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, noSort: true});
		$scope.filter.maturity = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, mutateOnlyAggregate: true, noSort: true});
		$scope.filter.protection = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, noSort: true});
		
		var maturityOptions = [
			{id: $scope.MATURITY_OPTIONS.primary_all, name: $scope.getMsgs('primary'), type: 'group'},
			{id: $scope.MATURITY_OPTIONS.primary_3_6, name: '3-6 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.primary_6_12, name: '6-12 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.primary_12, name: '12 ' + $scope.getMsgs('months-and-up'), type: 'groupItem'},
			// {id: $scope.MATURITY_OPTIONS.primary_all, name: $scope.getMsgs('all'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_all, name: $scope.getMsgs('secondary'), type: 'group'},
			{id: $scope.MATURITY_OPTIONS.secondary_0_3, name: '0-3 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_3_6, name: '3-6 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_6, name: '6 ' + $scope.getMsgs('months-and-up'), type: 'groupItem'},
			// {id: $scope.MATURITY_OPTIONS.secondary_all, name: $scope.getMsgs('all'), type: 'groupItem'}
		];
		$scope.filter.maturity.fillOptions(maturityOptions);
		var searchParams = $location.search();
		if (searchParams.maturity) {
			$scope.filter.maturity.setById(searchParams.maturity);
		}
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.productId){
			$scope.singleProduct = true;
			$scope.productId = $rootScope.$stateParams.productId;
			$scope.getProductFull();
		}else{
			$scope.singleProduct = false;
			$scope.isHomePage = false;
			$scope.productsPerRow = 3;
			$scope.productsRowsPerDisclaimer = 1;
			if($rootScope.isMobile){
				$scope.productsRowsPerDisclaimer = 2;
			}
			if($rootScope.$state.includes("ln.index")){
				$scope.isHomePage = true;
			}else{
				$scope.getAllProductsFilters();
			}
			$scope.getAllProducts();
		}
	}
	
	$scope.$on('$destroy', function(){
		$(window).off('resize', MJrerender);
		for(var i = 0; i < intervals.length; i++){
			clearInterval(intervals[i]);
		}
		$rootScope.blurWrapper = false;
	});
	
	$scope.showConversionRate = function(selectedProduct){
		return $scope.selectedProduct._invCurrencyId != CURRENCY_ID_EURO;//Not Euro
	}
	
	$scope.invAmountInputKeydownHandler = function() {
		$scope.isInvAmountManual = true;
	}
	
	$scope.getProductFull = function(dependencyName){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		$http.post('product/getProductFull', methodRequest).then(
			function(response) {
				$scope.action = '';
				$rootScope.markDependencyDone($scope, dependencyName, 'getProductFull');
				if (!$scope.handleErrors(response, 'getProductFull')) {
					$scope.selectedProduct = Utils.parseResponse(response).product;
					$scope.investmentSum = Utils.parseResponse(response).amount;
					if($scope.investmentSum > 0){
						$scope.selectedProduct._hasInvestment = true;
						$scope.selectedProduct._investmentSum = $scope.investmentSum;
						$scope.selectedProduct._investmentSell = Utils.parseResponse(response).returnAmonut;
					}
					
					$scope.euroRate = Utils.parseResponse(response).rate;
					
					var investmentSettings = $rootScope.investmentPossibilities[$scope.selectedProduct.currencyId];
					$scope.minInvAmount = investmentSettings.minimumAmount/100;
					$scope.selectedProduct._minInvAmount = investmentSettings.minimumAmount/100;
					$scope.selectedProduct._maxInvAmount = investmentSettings.maximumAmount/100;
					$scope.selectedProduct._invCurrencyId = investmentSettings.currencyId;
					$scope.invAmountOptions = [];
					for(var i = 0; i < investmentSettings.investmentPossibilityTab.length; i++){
						$scope.invAmountOptions.push(investmentSettings.investmentPossibilityTab[i].amount/100);
					}
					$scope.investment.invAmount = investmentSettings.defaultAmount/100;
					
					$scope.invAmountGetterSetter = function(amount){
						if($rootScope.isEmpty($scope.selectedProduct)){
							return '';
						}
						var val = 0;
						if(arguments.length){
							amount = getClearedAmount(amount);
							if((amount.split('.')[0] + '').length > 9){
								val = $scope.investment.invAmount;
							}else{
								val = $scope.investment.invAmount = amount;
								if(angular.element('#invAmountInput')[0] && angular.element('#invAmountInput')[0].selectionStart != 'undefined'){
									var selStart = angular.element('#invAmountInput')[0].selectionStart;
									var sel = angular.element('#invAmountInput')[0].value.substring(0, selStart);
									
									var centsPart = 2;
									if((val + '').indexOf('.') != -1){
										centsPart = 0;
									}
									var endVal = $rootScope.formatAmount({amount: val, currencyId: $scope.selectedProduct._invCurrencyId, centsPart: centsPart, skipAmountDivide: true});
									if(endVal.length > angular.element('#invAmountInput')[0].value.length){
										if(findFirstDiffPos(endVal, angular.element('#invAmountInput')[0].value) < selStart){
											selStart++;
										}
									}else if(endVal.length < angular.element('#invAmountInput')[0].value.length){
										if(findFirstDiffPos(endVal, angular.element('#invAmountInput')[0].value) < selStart){
											selStart--;
										}
									}
									
									setTimeout(function(){
										setCaretPosition(angular.element('#invAmountInput')[0], selStart);
									});
								}
							}
						}else{
							val = $scope.investment.invAmount;
						}
						var finalCentsPart = 2;
						if((val + '').indexOf('.') != -1){
							finalCentsPart = 0;
						}
						return $rootScope.formatAmount({amount: val, currencyId: $scope.selectedProduct._invCurrencyId, centsPart: finalCentsPart, skipAmountDivide: true});
					}
					
					function getClearedAmount(value){
						var temp = value.replace(',', '').split('.');
						value = temp[0].replace(regEx_digits_reverse, '');
						if (!isUndefined(temp[1])) {
							value += '.' + temp[1].replace(regEx_digits_reverse, '');
						}
						return value;
					}
					//$scope.selectedProduct.productBarrier.barrierOccur = true;
					$scope.productLoadTypesConfig($scope.selectedProduct);
					
					$scope.refactorScenarioRows($scope.selectedProduct);
					
					$scope.prepareCharts($scope.selectedProduct);
					
					// $timeout(function(){
						$scope.action = '';
					// });
					
					if(!$rootScope.isMobile){
						setTimeout(function(){
							alignAmountsVertical();
							startAlignAmountsVertical();
						});
					}
					
					//Init the interactive calculator
					$scope.interactiveCalculatorFloor = $scope.selectedProduct.productType.sliderMin;
					$scope.interactiveCalculatorCeil = $scope.selectedProduct.productType.sliderMax;
					$scope.interactiveCalculator = {};
					$scope.interactiveCalculator.barrierOccurred = false;
					$scope.interactiveCalculator.barrierOccurredManually = false;
					$scope.barrierOccurredCheckboxHandler = function() {
						if ($scope.selectedProduct.productTypesConfig.hasBarrier && $scope.selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American) {
							$scope.interactiveCalculator.barrierOccurredManually = $scope.interactiveCalculator.barrierOccurred;
							setBarrierOccurredCheckbox($scope.interactiveCalculator.slider.value);
						}
					};
					$scope.interactiveCalculator.states = [];
					$scope.interactiveCalculator.slider = {
						value: 0,
						options: {
							floor: $scope.interactiveCalculatorFloor,
							ceil: $scope.interactiveCalculatorCeil,
							translate: function(value) {
								return value + '%';
							},
							onChange: function(sliderId, modelValue) {
								setBarrierOccurredCheckbox(modelValue);
							},
							autoHideLimitLabels: false,
							boundPointerLabels: false,
							showTicksValues: true,
							ticksArray: shouldProductHaveBarrier($scope.selectedProduct) ? [$scope.selectedProduct.productBarrier.barrierLevel - 100] : []
						}
					};
					setBarrierOccurredCheckbox($scope.interactiveCalculator.slider.value);
					//Convinience functions
					$scope.interactiveCalculator.getState = function(){
						if($scope.interactiveCalculator.slider && typeof $scope.interactiveCalculator.slider.value != 'undefined'){
							return $scope.interactiveCalculator.states[$scope.interactiveCalculator.slider.value - $scope.interactiveCalculatorFloor];
						}
						return {};
					}
					$scope.interactiveCalculator.getAmount = function(selectedProduct, investmentAmount){
						var denomination = $scope.productIsPrimary(selectedProduct) ? investmentAmount : investmentAmount * 100 / selectedProduct.ask;
						var formulaResult = $scope.interactiveCalculator.getState().formulaResult;
						if($scope.interactiveCalculator.barrierOccurred){
							formulaResult = $scope.interactiveCalculator.getState().formulaResultBarrier;
						}
						return $scope.formatAmount({amount: formulaResult * denomination, currencyId: selectedProduct.currencyId, centsPart: 2, skipAmountDivide: true});
					}
					$scope.interactiveCalculator.getProfitLoss = function(){
						var result = '';
						var formulaResult = $scope.interactiveCalculator.getState().formulaResult;
						if($scope.interactiveCalculator.barrierOccurred){
							formulaResult = $scope.interactiveCalculator.getState().formulaResultBarrier;
						}
						if(formulaResult >= 1){
							result = round((formulaResult*100 - 100), 2) + '% ' + $scope.getMsgs('profit');
						}else{
							result = round((100 - formulaResult*100), 2) + '% ' + $scope.getMsgs('loss');
						}
						return result;
					}
					$scope.interactiveCalculator.getZeroMarkerWidth = function(){
						var result = '';
						var floor = $scope.interactiveCalculator.slider.options.floor;
						var ceil = $scope.interactiveCalculator.slider.options.ceil;
						result = Math.abs((floor/(ceil - floor))*2*100);
						return result;
					}
					//Fill the states
					for(var i = 0; i < $scope.selectedProduct.productSliders.length; i++){
						$scope.interactiveCalculator.states.push({formulaResult: $scope.selectedProduct.productSliders[i].formulaResult, formulaResultBarrier: $scope.selectedProduct.productSliders[i].formulaResultBarrier});
					}

					var kidList = $scope.selectedProduct.productKidLanguages;
					if (kidList) {
						for (var i = 0; i < kidList.length; i++) {
							if (kidList[i].language.id == $scope.language.id) {
								$scope.selectedProduct.kidLink = $scope.selectedProduct.productKidLanguages[i].url;	
								break;
							}
						}
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getAllProductsFilters = function(){
		var methodRequest = {};
		methodRequest.data = ['markets', 'product_type_group', 'product_type_protection_level'];
		$http.post('filters/get', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'getAllProductsFilters')) {
					var searchParams = $location.search();
					var filters = Utils.parseResponse(response);
					
					var assetsOptions = [];
					for(key in filters.markets){
						if(filters.markets.hasOwnProperty(key)){
							assetsOptions.push({id: key, name: filters.markets[key].displayName});
						}
					}
					$scope.filter.assets.fillOptions(assetsOptions);
					if (searchParams.assets) {
						$scope.filter.assets.setById(searchParams.assets);
					}
					
					var typeOptions = [];
					for(key in filters.product_type_group){
						if(filters.product_type_group.hasOwnProperty(key)){
							var typeGroupList = [];
							if(filters.product_type_group[key].list){
								for(var i = 0; i < filters.product_type_group[key].list.length; i++){
									typeGroupList.push(filters.product_type_group[key].list[i].id);
								}
							}
							typeOptions.push({
								id: 'g' + key,
								name: filters.product_type_group[key].groupName,
								groupName: filters.product_type_group[key].groupName,
								type: 'group',
								list: typeGroupList
							});
							if(filters.product_type_group[key].list){
								for(var i = 0; i < filters.product_type_group[key].list.length; i++){
									typeOptions.push({
										id: filters.product_type_group[key].list[i].id,
										name: filters.product_type_group[key].list[i].displayName,
										groupName: filters.product_type_group[key].groupName,
										type: 'groupItem'
									});
								}
							}
						}
					}
					$scope.filter.type.fillOptions(typeOptions);
					if (searchParams.type) {
						$scope.filter.type.setById(searchParams.type);
						var currentType = typeOptions.filter(function(type) {
							return type.id === searchParams.type;
						})[0];
						if (Object(currentType) === currentType && currentType.groupName) {
							var title = $rootScope.getMsgs(currentType.groupName) + ' Product | AnyCapital';
							setMetaTitle(title);
						}
					}
					
					var protectionOptions = [];
					for(key in filters.product_type_protection_level){
						if(filters.product_type_protection_level.hasOwnProperty(key)){
							var protectionGroupList = [];
							if(filters.product_type_protection_level[key].list){
								for(var i = 0; i < filters.product_type_protection_level[key].list.length; i++){
									protectionGroupList.push(filters.product_type_protection_level[key].list[i].id);
								}
							}
							protectionOptions.push({id: key, name: filters.product_type_protection_level[key].groupName, list: protectionGroupList});
						}
					}
					$scope.filter.protection.fillOptions(protectionOptions);
					if (searchParams.protection) {
						$scope.filter.protection.setById(searchParams.protection);
					}
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getAllProducts = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		$http.post('product/getAllProducts', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getAllProducts')) {
					$scope.products = Utils.parseResponse(response).products;
					$scope.investmentsProducts = Utils.parseResponse(response).investmentsProductId;
					
					//$scope.products.sort(function(a, b){return b.timeCreated - a.timeCreated;});
					
					if($scope.isHomePage){
						$scope.products.splice(6);
					}
					
					if($scope.investmentsProducts){
						for(var i = 0; i < $scope.products.length; i++){
							if($scope.investmentsProducts[$scope.products[i].id]){
								$scope.products[i]._hasInvestment = true;
								$scope.products[i]._investmentSum = $scope.investmentsProducts[$scope.products[i].id];
							}
						}
					}
					
					for(var i = 0; i < $scope.products.length; i++){
						var investmentSettings = $rootScope.investmentPossibilities[$scope.products[i].currencyId];
						$scope.products[i]._minInvAmount = investmentSettings.minimumAmount/100;
						$scope.products[i]._maxInvAmount = investmentSettings.maximumAmount/100;
						$scope.products[i]._invCurrencyId = investmentSettings.currencyId;
						
						$scope.products[i]._scenarioAmount = 10000;
						
						$scope.productLoadTypesConfig($scope.products[i]);
						
						$scope.refactorScenarioRows($scope.products[i], true);
						
						$scope.prepareCharts($scope.products[i]);
					}
					
					$scope.updateProductsList($scope.products);
					
					// $timeout(function(){
						$scope.action = '';
					// });
					
					// var alignProductBlocksInterval = setInterval(function(){
					// 	alignProductBlocks();
					// }, 2000);
					// intervals.push(alignProductBlocksInterval);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.updateProductsList = function(){
		if (lastExpandedProduct) {
			$scope.collapse(lastExpandedProduct);
		}
		var updateDelay = 1000;//Must be higher than transition duration + animation duration
		$scope.filtered.products = $scope.products.filter($scope.listProductsFilter);
		setTimeout(function(){
			$rootScope.$broadcast('filteredProducts');
		}, updateDelay);
		return $scope.filtered.products;
	}
	
	$scope.filterChangeHandler = function() {
		for (filterName in $scope.filter) {
			if ($scope.filter.hasOwnProperty(filterName)) {
				var currentFilter = $scope.filter[filterName]
				var id = currentFilter.getId();
				var value = id == -1 ? null : id;
				$state.params[filterName] = value;
				$location.search(filterName, value);
				if (filterName === 'type' && !isUndefined(value) && currentFilter.model && currentFilter.model.groupName) {
					var title = $rootScope.getMsgs(currentFilter.model.groupName) + ' Product | AnyCapital';
					setMetaTitle(title);
				} else if (filterName === 'type') {
					setMetaTitle($rootScope.getMetaTitle());
				}
			}
		}
	};
	
	$scope.listProductsFilter = function(product){
		var hide = false;
		
		//Asset
		if($scope.filter.assets.getId() > -1){
			if($scope.filter.assets.getId() != product.productMarkets[0].market.id){
				hide = true;
			}
		}
		
		//Type
		if($scope.filter.type.model.type == 'groupItem' && $scope.filter.type.getId() > -1){
			if($scope.filter.type.getId() != product.productType.id){
				hide = true;
			}
		}
		if($scope.filter.type.model.type == 'group'){
			var typeFound = false;
			for(var i = 0; i < $scope.filter.type.model.list.length; i++){
				if($scope.filter.type.model.list[i] == product.productType.id){
					typeFound = true;
					break;
				}
			}
			if(!typeFound){
				hide = true;
			}
		}
		
		//Maturity
		if($scope.filter.maturity.getId() > -1){
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_all && !$scope.isPrimary(product)){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_3_6 && !($scope.isPrimary(product) && (product.redemptionDate <= moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_6_12 && !($scope.isPrimary(product) && (product.redemptionDate > moment().add(6, 'months').toDate().getTime() && product.redemptionDate <= moment().add(12, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_12 && !($scope.isPrimary(product) && (product.redemptionDate > moment().add(12, 'months').toDate().getTime()))){
				hide = true;
			}
			
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_all && !$scope.isSecondary(product)){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_0_3 && !($scope.isSecondary(product) && (product.finalFixingDate <= moment().add(3, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_3_6 && !($scope.isSecondary(product) && (product.finalFixingDate > moment().add(3, 'months').toDate().getTime() && product.finalFixingDate <= moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_6 && !($scope.isSecondary(product) && (product.finalFixingDate > moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
		}
		
		//Protection
		if($scope.filter.protection.getId() > -1){
			var typeFound = false;
			for(var i = 0; i < $scope.filter.protection.model.list.length; i++){
				if($scope.filter.protection.model.list[i] == product.productType.id){
					typeFound = true;
					break;
				}
			}
			if(!typeFound){
				hide = true;
			}
		}
		
		return !hide;
	}
	
	
	$scope.refactorScenarioRows = function(selectedProduct, listView){
		if(!selectedProduct.productScenarios){
			return;
		}
		selectedProduct.productScenarios.sort(function(a, b){return b.formulaResult - a.formulaResult;});
		
		var count = selectedProduct.productScenarios.length;
		var arr = [];
		var counter = 0;
		for(var i = 0; i < selectedProduct.productScenarios.length; i++){
			selectedProduct.productScenarios[i]._level = selectedProduct.productScenarios[i].finalFixingLevelForTxt;
			selectedProduct.productScenarios[i]._formulaResult = selectedProduct.productScenarios[i].formulaResult;
			if(selectedProduct.productTypesConfig['scenario-tooltips']){
				selectedProduct.productScenarios[i]._tooltip = selectedProduct.productTypesConfig['scenario-tooltips'][i];
			}
			if(selectedProduct.productTypesConfig['override-main-scenarios'] && selectedProduct.productTypesConfig['override-main-scenarios'].override && selectedProduct.productTypesConfig['override-main-scenarios'].scenarios && (selectedProduct.productTypesConfig['override-main-scenarios'].scenarios.length > i)){
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].template == 'skip'){
					count--;
					continue;
				}
				selectedProduct.productScenarios[i]._template = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].template;
				selectedProduct.productScenarios[i]._shortTextKey = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].shortTextKey;
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].level){
					selectedProduct.productScenarios[i]._level = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].level;
				}
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].formulaResult){
					selectedProduct.productScenarios[i]._formulaResult = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].formulaResult;
				}
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].amountType == 'static'){
					selectedProduct.productScenarios[i]._amountType = 'static';
					selectedProduct.productScenarios[i]._amount = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].amount;
				}
			}
			// if(parseFloat(selectedProduct.productScenarios[i]._level) > 0){
			// 	selectedProduct.productScenarios[i]._level = '+' + selectedProduct.productScenarios[i]._level;
			// }
			if(!listView){
				if(!arr[Math.floor(counter/3)]){
					arr[Math.floor(counter/3)] = [];
				}
				arr[Math.floor(counter/3)].push(selectedProduct.productScenarios[i]);
			}else{
				arr.push(selectedProduct.productScenarios[i]);
			}
			counter++;
		}
		selectedProduct._productScenarios = arr;
		selectedProduct._scenariosCount = count;
	}
	$scope.getScenarioAmount = function(selectedProduct, scenario, invAmount){
		var price = 1;
		if($scope.isSecondary(selectedProduct)){
			price = selectedProduct.ask/100;
		}
		return (invAmount/price)*scenario._formulaResult;
	}
	
	
	$scope.getSvgChartColor = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct){
			return false;
		}
		if(!selectedProduct.productType){
			return false;
		}
		if(selectedProduct.productType.riskLevel == 1){
			return '#2b8fd9';
		}else if(selectedProduct.productType.riskLevel == 2){
			return '#209c6c';
		}else if(selectedProduct.productType.riskLevel == 3){
			return '#f7a508';
		}
	}
	
	$scope.prepareCharts = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct){
			return false;
		}
		
		$scope.svgWidth = 452;
		$scope.svgHeight = 200;
		$scope.svgPaddingPercent = {left: 0.0178, bottom: 0.05, right: 4*0.0178, top: 0.025};
		$scope.svgOffset = {};
		if(!$scope.singleProduct){
			$scope.svgWidth = 334;
			$scope.svgHeight = 200;
			$scope.svgPaddingPercent = {left: 0.0178, bottom: 0.05, right: 0.1, top: 0.025};
		}
		
		if(!selectedProduct.productMarkets[0].market.historyMarketPrice){
			return false;
		}
		
		selectedProduct.productMarkets[0].market.historyMarketPrice.sort(function(a, b){return a.priceDate - b.priceDate;});
		
		selectedProduct._charts = {
			month: {startTime: moment().subtract(1, 'months').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: moment().daysInMonth(), isActive: true, name: $scope.getMsgs('month'), texts: [], textType: 'days'},
			sixMonths: {startTime: moment().subtract(6, 'months').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: 6, isActive: false, name: '6 ' + $scope.getMsgs('months'), texts: [], textType: 'months'},
			year: {startTime: moment().subtract(1, 'years').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: 12, isActive: false, name: $scope.getMsgs('year'), texts: [], textType: 'year'}
		};
		for(var i = 0; i < selectedProduct.productMarkets[0].market.historyMarketPrice.length; i++){
			for(key in selectedProduct._charts){
				if(selectedProduct._charts.hasOwnProperty(key)){
					setPoints(selectedProduct.productMarkets[0].market.historyMarketPrice[i], selectedProduct._charts[key]);
				}
			}
			
			function setPoints(historyMarketPrice, chart){
				if(historyMarketPrice.priceDate > chart.startTime.getTime()){
					chart.points.push(historyMarketPrice);
					if(chart.minPrice == ''){
						chart.minPrice = chart.maxPrice = historyMarketPrice.price;
					}else if(historyMarketPrice.price > chart.maxPrice){
						chart.maxPrice = historyMarketPrice.price;
					}else if(historyMarketPrice.price < chart.minPrice){
						chart.minPrice = historyMarketPrice.price;
					}
				}
			}
		}
		for(key in selectedProduct._charts){
			if(selectedProduct._charts.hasOwnProperty(key)){
				setSvgPath(selectedProduct._charts[key]);
				
				function setSvgPath(chart){
					//Set up constants
					var priceCeil = Math.ceil(chart.maxPrice*10)/10;
					var priceFloor = Math.floor(chart.minPrice*10)/10;
					//var divider = (Math.ceil(chart.maxPrice) - Math.floor(chart.minPrice));
					var divider = priceCeil - priceFloor;
					var chartWidth = $scope.svgWidth*(1 - $scope.svgPaddingPercent.left - $scope.svgPaddingPercent.right);
					var chartHeight = $scope.svgHeight*(1 - $scope.svgPaddingPercent.bottom - $scope.svgPaddingPercent.top);
					var paddingLeft = $scope.svgWidth*$scope.svgPaddingPercent.left;
					var paddingRight = $scope.svgWidth*$scope.svgPaddingPercent.right;
					var paddingBottom = $scope.svgHeight*$scope.svgPaddingPercent.bottom;
					var paddingTop = $scope.svgHeight*$scope.svgPaddingPercent.top;
					//Create graph
					for(var i = 0; i < chart.points.length; i++){
						var x = (i/chart.points.length)*chartWidth + paddingLeft;
						var y = paddingTop + chartHeight*(1 - ((chart.points[i].price - priceFloor)/divider));
						if(i == 0){
							chart.path += 'M' + paddingLeft + ',' + (chartHeight + paddingTop);
						}
						chart.path += 'L' + x + ',' + y;
						if(i == chart.points.length - 1){
							chart.path += 'L' + x + ',' + (chartHeight + paddingTop);
						}
					}
					//Create horizontal lines and texts
					for(var i = 0; i < 6; i++){
						var text = '';
						if(i < 5){
							text = Math.round((chart.maxPrice - i*((chart.maxPrice - chart.minPrice)/5))*100)/100;
						}
						var line = {start: {x: 0, y: i*(chartHeight/5) + paddingTop}, end: {x: $scope.svgWidth - paddingRight, y: i*(chartHeight/5) + paddingTop}, text: text, textY: i*(chartHeight/5) + paddingTop + 2};
						chart.lines.push(line);
					}
					//Create vertical rectangles and texts
					var rectWidth = ($scope.svgWidth*(1 - $scope.svgPaddingPercent.left - $scope.svgPaddingPercent.right))/chart.rectangleCount;
					for(var i = 0; i < chart.rectangleCount; i++){
						var text = '';
						if(chart.textType == 'days' && i % 7 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'days').date()) + '-' + padNumber(moment().subtract(chart.rectangleCount - i, 'days').month() + 1);
						}else if(chart.textType == 'months' && i % 2 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'months').month() + 1) + '/' + (moment().subtract(chart.rectangleCount - i, 'months').year() + '').substring(2);
						}else if(chart.textType == 'year' && i % 3 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'months').month() + 1) + '/' + (moment().subtract(chart.rectangleCount - i, 'months').year() + '').substring(2);
						}
						var rectangle = {x: i*rectWidth + paddingLeft, y: paddingTop, width: rectWidth, height: chartHeight, text: text};
						chart.rectangles.push(rectangle);
					}
					function padNumber(number){
						if(parseInt(number) < 10){
							return '0' + number;
						}
						return number;
					}
				}
			}
		}
	}
	
	
	$scope.isPrimary = function(selectedProduct){
		return $scope.productIsPrimary(selectedProduct);
	}
	$scope.isSecondary = function(selectedProduct){
		return $scope.productIsSecondary(selectedProduct);
	}
	
	$scope.setActiveChart = function(chart, selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		for(key in selectedProduct._charts){
			if(selectedProduct._charts.hasOwnProperty(key)){
				selectedProduct._charts[key].isActive = false;
			}
		}
		chart.isActive = true;
	}
	
	$scope.getClassNames = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		return 'risk-level-' + selectedProduct.productType.riskLevel + ' ' + 'product-type-' + selectedProduct.productType.id;
	}
	
	$scope.getPurchasedTooltip = function(selectedProduct){
		var message = $scope.getMsgs('product-you-have-a-note', {amount: $scope.formatAmount({amount: selectedProduct._investmentSum, centsPart: 2, currencyId: settings.CURRENCY_EUR})});
		return message;
	}
	
	$scope.getInitialFixingLevel = function(selectedProduct){
		return $scope.productGetInitialFixingLevel(selectedProduct);
	}
	$scope.getBarrierObservationPeriod = function(separator, selectedProduct){
		return $scope.productGetBarrierObservationPeriod(separator, selectedProduct);
	}
	$scope.isBarrierTypeEuropean = function(selectedProduct){
		return $scope.productIsBarrierTypeEuropean(selectedProduct);
	}
	
	$scope.isProtected = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		if(selectedProduct.productType.productCategory.id == $scope.PRODUCT_CATEGORIES.capital_protection){
			return true;
		}
		return false;
	}
	
	$scope.getOneLiner = function(selectedProduct, showShortText){
		var result = '';
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		// var lastPrice = $scope.isPrimary(selectedProduct) ? selectedProduct.productMarkets[0].market.lastPrice.price : selectedProduct.productMarkets[0].startTradeLevel;
		var centsLength = selectedProduct.productMarkets[0] && selectedProduct.productMarkets[0].market && !isUndefined(selectedProduct.productMarkets[0].market.decimalPoint) ? selectedProduct.productMarkets[0].market.decimalPoint : 2;
		var params = {};
		if($scope.isPrimary(selectedProduct)){
			params = {
				'protection-level': selectedProduct.levelOfProtection + '%',
				'100%-protection-level': (100 - selectedProduct.levelOfProtection) + '%',
				'participation-level': selectedProduct.levelOfParticipation + '%',
				'asset': $scope.getMsgs(selectedProduct.productMarkets[0].market.displayName),
				'initial-fixing-level': $scope.getInitialFixingLevel(selectedProduct) + '%',
				'barrier-level': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel + '%' : '',
				'barrier-level%': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel + '%' : '',
				'100% - barrier-level': selectedProduct.productBarrier ? 100 - selectedProduct.productBarrier.barrierLevel + '%' : '',
				'barrier-level - 100%': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel - 100 + '%' : '',
				'market-barrier-level': '',
				'coupon-amount': selectedProduct.productCoupons.length ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel + '%' : '',
				'coupon-trigger-level%': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel + '%' : '',
				'coupon-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level%\\*initial-fixing-level': '',
				'strike-level': selectedProduct.strikeLevel + '%',
				'caplevel - 100%': selectedProduct.capLevel - 100 + '%',
				'bonus-level%': selectedProduct.bonusLevel + '%',
				'final-fixing-date': $scope.dateGetSimple(selectedProduct.finalFixingDate, '.'),
				'bonus-level': ''
			}
		}else{
			params = {
				'protection-level': selectedProduct.levelOfProtection + '%',
				'100%-protection-level': (100 - selectedProduct.levelOfProtection) + '%',
				'participation-level': selectedProduct.levelOfParticipation + '%',
				'asset': $scope.getMsgs(selectedProduct.productMarkets[0].market.displayName),
				'initial-fixing-level': $scope.getInitialFixingLevel(selectedProduct).toFixed(2),
				'barrier-level': selectedProduct.productBarrier ? round((selectedProduct.productBarrier.barrierLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'100% - barrier-level': '',
				'coupon-amount': selectedProduct.productCoupons.length ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'market-barrier-level': selectedProduct.productBarrier ? round((selectedProduct.productBarrier.barrierLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'coupon-trigger-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? round((selectedProduct.productCoupons[0].triggerLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'coupon-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level%\\*initial-fixing-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? round((selectedProduct.productCoupons[0].triggerLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'strike-level': round((selectedProduct.strikeLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength),
				'caplevel - 100%': selectedProduct.capLevel - 100 + '%',
				'bonus-level%': selectedProduct.bonusLevel + '%',
				'final-fixing-date': $scope.dateGetSimple(selectedProduct.finalFixingDate, '.'),
				'bonus-level': round((selectedProduct.bonusLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength)
			}
		}
		
		var productTypeSuffix = '';
		if(selectedProduct.productTypesConfig.headerShortSuffix){
			productTypeSuffix = '-' + selectedProduct.productTypesConfig.headerShortSuffix;
		}
		var barrierSuffix = '';
		if($scope.isBarrierTypeEuropean(selectedProduct)){
			barrierSuffix = '-european';
		}
		var productStateSuffix = '-primary';
		if($scope.isSecondary(selectedProduct)){
			productStateSuffix = '-secondary';
		}
		if(showShortText){
			result += '<div>';
			result += '<b>' + $scope.getMsgs('the-attraction') + ': </b>' + $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-short' + '-attraction' + productStateSuffix + barrierSuffix, params);
			result += '</div>';
			result += '<div>';
			result += '<b>' + $scope.getMsgs('worst-case') + ': </b>' + $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-short' + '-worstcase' + productStateSuffix + barrierSuffix, params);
			result += '</div>';
		}else{
			result += $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-long' + productStateSuffix + barrierSuffix, params);
		}
		
		return result;
	}
	
	$scope.getRiskAppetiteText = function(selectedProduct){
		selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct || !selectedProduct.productType){
			return '';
		}
		switch (selectedProduct.productType.riskLevel) {
			case 1:
				return $scope.getMsgs('capital-protection');
			case 2:
				return $scope.getMsgs('conditional-protection');
			case 3:
				return $scope.getMsgs('no-capital-protection');
			default:
				return '';
		}
	}
	
	$scope.getDateCloseText = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		if([settings.marketGroups.Commodities, settings.marketGroups.Rates, settings.marketGroups.Currencies].indexOf(selectedProduct.productMarkets[0].market.marketGroupId) != -1){
			return $scope.getMsgs('10-NY-time-close');
		}else{
			return $scope.getMsgs('exchange-market-close');
		}
	}
	
	$scope.priceSelectorClickHandler = function(invAmountOption) {
		$scope.investment.invAmount = invAmountOption;
		$scope.isInvAmountManual = false;
	}

	$scope.selectOtherAmount = function(){
		$scope.invAmountGetterSetter("0");
		$scope.isInvAmountManual = true;
		// $timeout(function(){
		// 	angular.element('#invAmountInput').focus();
		// 	$timeout(function(){
		// 		$scope.investment.invAmount = '';
		// 		angular.element('#invAmountInput').val('');
		// 	}, 100);
		// });
	}
	
	$scope.updateInvestments = function(){
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {};
		methodRequest.data.id = $scope.selectedProduct.id;
		$http.post('investment/get/product/sum', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				var data = Utils.parseResponse(response);
				$scope.investmentSum = data.amount;
				
				if($scope.investmentSum > 0){
					$scope.selectedProduct._hasInvestment = true;
					$scope.selectedProduct._investmentSum = $scope.investmentSum;
					$scope.selectedProduct._investmentSell = data.returnAmonut;
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
	
	$scope.insertInvestment = function(){
		if(!$rootScope.isLogged){
			$scope.showCredentials.state = Utils.settings.credentialsView.LOGIN;
		}else{
			$scope.investmentAction = 'loading';
			$scope.investment.data = {};
			var methodRequest = {};
			methodRequest.data = {};
			methodRequest.data.productId = $scope.selectedProduct.id;
			methodRequest.data.originalAmount = $scope.formatAmountForDb({amount: $scope.investment.invAmount});
			methodRequest.data.investmentType = {id: 1};
			methodRequest.data.investmentMarkets = [];
			methodRequest.data.investmentMarkets.push({marketId: $scope.selectedProduct.productMarkets[0].market.id, price: $scope.selectedProduct.productMarkets[0].market.lastPrice.price});
			if(!$scope.isPrimary($scope.selectedProduct)){
				methodRequest.data.investmentType = {id: 2};
				methodRequest.data.ask = $scope.selectedProduct.ask;
			}
			$http.post('investment/insert', methodRequest).then(
				function(response) {
					$scope.investmentAction = '';
					$scope.updateInvestments();
					// var exceptions = [errorCodeMap.investment_general_error, errorCodeMap.investment_insufficient_funds_above_minumum, errorCodeMap.investment_below_minumum, errorCodeMap.investment_insufficient_funds_below_minimum_secondary, errorCodeMap.investment_above_maximum, errorCodeMap.investment_already_secured, errorCodeMap.investment_suspended, errorCodeMap.investment_unavailable, errorCodeMap.investment_already_issued, errorCodeMap.investment_deviation_secondary_primary, errorCodeMap.investment_deviation];
					// if (!$scope.handleErrors(response, 'insert', null, exceptions)) {
						var data = Utils.parseResponse(response);
						$scope.investment.data = data;
						if(response.data.responseCode == 0 && data.investmentStatus.id == 2){//Success purchase
							$scope.investmentState = $scope.INVESTMENT_STATES.purchased;
							$rootScope.loggedUser.balance = data.user.balance;
						}else if(response.data.responseCode == 0 && data.investmentStatus.id == 1){//When a customer is trying to purchase an inv in primary product, has balance lower than min inv amount and doesn't have an already existing pending inv on the same product
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds;
						}else if(response.data.responseCode == errorCodeMap.investment_insufficient_funds_above_minumum){//Not enough for current investment, enough for minimum
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds_above_minumum;
						}else if(response.data.responseCode == errorCodeMap.investment_below_minumum){//When a customer is trying to insert an amount lower than the miv inv amount
							$scope.investmentState = $scope.INVESTMENT_STATES.below_minumum;
						}else if(response.data.responseCode == errorCodeMap.investment_insufficient_funds_below_minimum_secondary){//When a customer does not have sufficient balance for the inv amount inserted and his balance is lower than the min inv amount, relevant only for secondary
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds_below_minimum_secondary;
						}else if(response.data.responseCode == errorCodeMap.investment_above_maximum){//When a customer is trying to insert an amount higher than the max inv amount
							$scope.investmentState = $scope.INVESTMENT_STATES.above_maximum;
						}else if(response.data.responseCode == errorCodeMap.investment_already_secured){//When a customer already secured a note in primary on this product, balance is lower than the min inv amount and is trying to secure again on the same product in primary
							$scope.investmentState = $scope.INVESTMENT_STATES.already_secured;
						}else if(response.data.responseCode == errorCodeMap.investment_suspended){//When a customer tries inserting an inv but the product is in suspend state
							$scope.investmentState = $scope.INVESTMENT_STATES.suspended;
						}else if(response.data.responseCode == errorCodeMap.investment_unavailable){//When a customer tries inserting an inv but according to the "product time definitions"
							$scope.investmentState = $scope.INVESTMENT_STATES.unavailable;
						}else if(response.data.responseCode == errorCodeMap.investment_already_issued){//Customer trying to insert a primary type inv while the product is already in secondary state (similar to the previous)
							$scope.investmentState = $scope.INVESTMENT_STATES.already_issued;
						}else if(response.data.responseCode == errorCodeMap.investment_deviation_secondary_primary){//Deviation on ask price secondary?
							$scope.investmentState = $scope.INVESTMENT_STATES.deviation_secondary_primary;
						}else if(response.data.responseCode == errorCodeMap.investment_deviation){//Deviation on ask price
							$scope.investmentState = $scope.INVESTMENT_STATES.deviation;
						}else if(response.data.responseCode == errorCodeMap.investment_complete_registration){//Registration not finished error
							var step = $rootScope.loggedUser.userStep.id;
							switch (step) {
								case 1:
									$scope.investment.data.href = $state.href('ln.sign-up.personal-details');
									break;
								case 2:
								case 3:
									$scope.investment.data.href = $state.href('ln.sign-up.questionnaire');
									break;
								case 4:
									$scope.investment.data.href = $state.href('ln.my-account.funding.upload-documents');
									break;
							}
							$scope.investmentState = $scope.INVESTMENT_STATES.complete_registration;
							$scope.investment.data.errorMessage = response.data.error ? response.data.error.reason : '';
						}else if(response.data.responseCode == errorCodeMap.investment_general_error){//General error
							$scope.investmentState = $scope.INVESTMENT_STATES.general_error;
							$scope.investment.data.errorMessage = response.data.error ? $scope.getMsgs(response.data.error.reason) : '';
						}
					// }
				}, function(response) {
					$scope.investmentAction = '';
					$scope.handleNetworkError(response);
				});
		}
	}
	
	$scope.setInvestmentState = function(state){
		$scope.investmentState = state;
	}
	
	$scope.getMyNotesLink = function(){
		return $rootScope.$state.href('ln.my-account.my-notes', {ln: $rootScope.language.code});
	}
	
	var alignAmountsVerticalInterval;
	function startAlignAmountsVertical(){
		clearInterval(alignAmountsVerticalInterval);
		alignAmountsVerticalInterval = setInterval(alignAmountsVertical, 1000);
		intervals.push(alignAmountsVerticalInterval);
	}
	function alignAmountsVertical(){
		//Align investment with scenario receive values
		$('[data-height-compensation-block]').css('height', '');
		var offsets = [];
		var positions = [];
		$('[data-height-target]').each(function(index, el){
			offsets.push({position: $(el).data().heightTarget, offset: $(el).offset().top + $(el).outerHeight()});
			if(positions.indexOf($(el).data().heightTarget) == -1){
				positions.push($(el).data().heightTarget);
			}
		});
		if(offsets.length > 1){
			var compensators = $('[data-height-compensation-block]');
			if(compensators.length > 0){
				var min = null;
				var max = null;
				for(var i = 0; i < offsets.length; i++){
					if(i == 0){
						min = offsets[i];
						max = offsets[i];
					}else{
						if(offsets[i].offset < min.offset){
							min = offsets[i];
						}
						if(offsets[i].offset > max.offset){
							max = offsets[i];
						}
					}
				}
				var diff = max.offset - min.offset;
				if(diff > 0){
					$('[data-height-compensation-block="' + min.position + '"]').each(function(index, el){
						$(el).css('height', $(el).outerHeight() + diff/$('[data-height-compensation-block="' + min.position + '"]').length);
					});
				}
			}
		}
		//Align scenario blocks height
		var maxHeight = 0;
		$('[data-scenario-block]').each(function(index, el){
			var outerHeight = $(el).outerHeight();
			if(outerHeight > maxHeight){
				maxHeight = outerHeight;
			}
		});
		$('[data-scenario-block]').css('min-height', maxHeight + 'px');
	}
	
	// Should be more then .product-medium-holder animation-duration.
	var lastExpandedProduct;
	var expandCollapseDefaultDelay = 410;
	$scope.expand = function(selectedProduct, event){
		lastExpandedProduct = selectedProduct;
		if($rootScope.isMobile){
			$rootScope.$state.go('ln.product', {ln: $rootScope.$state.params.ln, productId: selectedProduct.id});
			return;
		}
		
		var $target = $(event.target).parents('[data-list-single-product]');
		
		var delay = 0;
		
		//Allow only one expanded product at a time
		for(i = 0; i < $scope.products.length; i++){
			if($scope.products[i]._isExpanded){
				$scope.collapse($scope.products[i]);
				delay = expandCollapseDefaultDelay;
				break;
			}
		}
		
		var hideIndexes = [];
		var currentIndex = $target.data('list-product-index');
		if(currentIndex % 3 == 0){
			hideIndexes = [currentIndex + 1, currentIndex + 2];
		}else if (currentIndex % 3 == 1){
			hideIndexes = [currentIndex - 1, currentIndex + 1];
		}else{
			hideIndexes = [currentIndex - 1, currentIndex - 2];
		}
		for(var i = 0; i < hideIndexes.length; i++){
			$('[data-list-product-index="' + hideIndexes[i] + '"]').addClass('hidden');
		}
		
		
		setTimeout(function(){
			var dummy = null;
			if(document.getElementById('dummy')){
				dummy = document.getElementById('dummy');
			}else{
				dummy = document.createElement('div');
				dummy.id = 'dummy';
				$(dummy).css({
					'float': 'left',
					'display': 'inline-block',
					'vertical-align': 'top'
				});
			}
			$(dummy).css({
				'width': $target.outerWidth(),
				'height': $target.outerHeight(),
				'margin-top': $target.css('margin-top'),
				'margin-bottom': $target.css('margin-bottom'),
				'margin-left': $target.css('margin-left'),
				'margin-right': $target.css('margin-right')
			});
			
			$target.addClass('transition');
			
			$timeout(function(){
				$target.before(dummy);
				selectedProduct._isExpanded = true;
				// setTimeout(function(){
				// 	$target.find('.expanded-middle-col').addClass('visible');
				// 	$target.find('.expanded-right-col').addClass('visible');
				// });
			});
		}, delay);
	}
	$scope.collapse = function(selectedProduct, event){
		lastExpandedProduct = null;
		selectedProduct._isExpanded = false;
		var $dataListSingleProduct = $('[data-list-single-product]');
		var $dataListSingleProductHidden = $dataListSingleProduct.filter('.hidden');
		// $dataListSingleProduct.find('.expanded-middle-col').removeClass('visible');
		// $dataListSingleProduct.find('.expanded-right-col').removeClass('visible');
		setTimeout(function(){
			$('#dummy').remove();
			$dataListSingleProduct.removeClass('transition');
			$dataListSingleProductHidden.removeClass('hidden');
		}, expandCollapseDefaultDelay);
	}
	
	$scope.gotoProduct = function(productId){
		$rootScope.$state.go('ln.product', {ln: $rootScope.$state.params.ln, productId: productId});
	}
	
	
	$scope.popupOpen = function(selectedProduct){
		selectedProduct._showMedium = true;
		$rootScope.blurWrapper = true;
	}
	$scope.popupClose = function(selectedProduct){
		selectedProduct._showMedium = false;
		$rootScope.blurWrapper = false;
	}
	$scope.popupIsOpen = function(selectedProduct){
		return selectedProduct._showMedium == true;
	}
	
	
	$scope.toggleInteractiveCalculator = function(selectedProduct, toState){
		if(typeof toState == 'undefined'){
			selectedProduct._calculatorVisible = !selectedProduct._calculatorVisible;
		}else{
			selectedProduct._calculatorVisible = !!toState;
		}
		$rootScope.blurWrapper = selectedProduct._calculatorVisible;
	}
	
	$scope.isAmericanBarrier = function() {
		return $scope.selectedProduct.productTypesConfig.hasBarrier && $scope.selectedProduct.productBarrier.productBarrierType.id == $scope.settings.productBarrierTypes.American;
	};
	
	function shouldProductHaveBarrier(selectedProduct) {
		if (selectedProduct.productTypesConfig.hasBarrier && selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American) {
			return true;
		} else {
			var productTypeId = selectedProduct.productType.id;
			switch (productTypeId) {
				case 2:
				case 5:
				case 7:
				case 9:
					return true;
				default:
					return false;
			}
		}
	}
	
	function setBarrierOccurredCheckbox(value) {
		var barrierLevel = $scope.interactiveCalculator.slider.options.ticksArray[0];
		if (shouldProductHaveBarrier($scope.selectedProduct) && !$scope.interactiveCalculator.barrierOccurredManually) {
			$scope.interactiveCalculator.barrierOccurred = barrierLevel >= 0 ? value >= ($scope.selectedProduct.productBarrier.barrierLevel - 100) : value <= ($scope.selectedProduct.productBarrier.barrierLevel - 100);
		}
	}
	
	// function alignProductBlocks(){
	// 	var counter = 0;
	// 	var rowEls = [];
	// 	var products = $('[data-list-single-product]');
	// 	products.css('min-height', '');
	// 	products.each(function(index, el){
	// 		if(counter % $scope.productsPerRow == 0){//First in row
	// 			rowEls = [];
	// 			rowEls.push(el);
	// 			$(el).addClass('first-in-row');
	// 			$(el).removeClass('last-in-row');
	// 			// displayMidRow(false, index);
	// 		}else if(counter % $scope.productsPerRow == $scope.productsPerRow - 1){//Last in row
	// 			rowEls.push(el);
	// 			fixHeights(rowEls);
	// 			rowEls = [];
	// 			$(el).addClass('last-in-row');
	// 			$(el).removeClass('first-in-row');
	// 			// displayMidRow(true, index);
	// 		}else{
	// 			rowEls.push(el);
	// 			$(el).removeClass('first-in-row');
	// 			$(el).removeClass('last-in-row');
	// 			// displayMidRow(false, index);
	// 		}
	// 		counter++;
	// 		// if(index == products.length - 1){
	// 		// 	displayMidRow(true, index);
	// 		// }
	// 	});
	// 	if(rowEls.length > 0){
	// 		fixHeights(rowEls);
	// 	}
	// 	if($scope.isHomePage){
	// 		$('[data-products-mid-row]:visible:last').css('display', '');
	// 	}
	// 	
	// 	function fixHeights(rowEls){
	// 		var maxHeight = 0;
	// 		for(var i = 0; i < rowEls.length; i++){
	// 			var temp = $(rowEls[i]).outerHeight();
	// 			if(temp > maxHeight){
	// 				maxHeight = temp;
	// 			}
	// 		}
	// 		for(var i = 0; i < rowEls.length; i++){
	// 			$(rowEls[i]).css('min-height', maxHeight);
	// 		}
	// 	}
	// 	
	// 	// function displayMidRow(visible, index){
	// 	// 	if(visible){
	// 	// 		$('[data-products-mid-row]:eq(' + index + ')').css('display', 'block');
	// 	// 	}else{
	// 	// 		$('[data-products-mid-row]:eq(' + index + ')').css('display', '');
	// 	// 	}
	// 	// }
	// }
}]);
spApp.controller('spCtr', ['$sce', '$rootScope', '$scope', '$http', '$state', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', '$interval', '$document', '$window', 'Utils', 'Permissions', 'MenuTree', 'initResponse', 'getMsgsJsonResponse', 'AuthService', 'UserService',
					function($sce, $rootScope, $scope, $http, $state, $templateCache, $timeout, $compile, $filter, $uibModal, $interval, $document, $window, Utils, Permissions, MenuTree, initResponse, getMsgsJsonResponse, AuthService, UserService
	) {
	//Expose the static methods of the service to the template
	$scope.MenuTree = {};
	$scope.MenuTree.toggleNode = MenuTree.InstanceClass.toggleNode;
	
	//Translation functions
	$rootScope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($rootScope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $rootScope.msgsParam($rootScope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($rootScope.msgsParam($rootScope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($rootScope.msgsParam($rootScope.msgs[key], params));
					// return $rootScope.msgsParam($rootScope.msgs[key], params);
				} else {
					return $rootScope.msgs[key];
				}
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key + "]?";
			}
		} else {
			if(key == null){
				return params && params.hideMissing ? '' : "?msgs[null]?";
			}else if ($rootScope.msgs.hasOwnProperty(key[0])) {
				if ($rootScope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($rootScope.msgsParam($rootScope.msgs[key[0]][key[1]], params));
					} else {
						return $rootScope.msgs[key[0]][key[1]];
					}
				} else {
					return params && params.hideMissing ? '' : "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	$rootScope.isTranslated = function(str, checkEmpty){
		return str.indexOf('?msgs[') == -1 && (!checkEmpty ? true : str != '');
	}
	
	//get msg with param
	$rootScope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}
	
	
	//Common screen controllers functions
	$rootScope.isEmpty = function(obj){
		if(!obj){
			return true;
		}
		if(Array.isArray(obj) && obj.length == 0){
			return true;
		}
		if(Object.keys(obj).length === 0 && obj.constructor === Object){
			return true;
		}
		return false;
	}
	
	$rootScope.confirm = function(message, successCallback, cancelCallback, buttonTexts){
		$rootScope.confirmMessage = message;
		$rootScope.buttonTexts = buttonTexts;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/confirm-popup.html',
			scope: $scope,
			windowClass: 'confirm-popup'
		});
		previewModal.result.then(function () {
			if(successCallback){
				successCallback();
			}
		}, function () {
			if(cancelCallback){
				cancelCallback();
			}
		});
	}
	$rootScope.translateJsonKeyInArray = function(arr, key){
		if(arr && Array.isArray(arr) && key){
			for(var i = 0; i < arr.length; i++){
				if(arr[i][key]){
					arr[i][key] = $rootScope.getMsgs(arr[i][key]);
				}
			}
		}
		return arr;
	}
	$rootScope.fillFilter = function(params){
		if(params.dontPreserveFirstEl){
			params.arr = [];
		}else{
			params.arr.splice(1, params.arr.length);
		}
		var holder = null;
		if(params.data){
			holder = params.data.screenFilters;
		}
		if(params.holder){
			holder = params.holder;
		}
		if(holder && holder[params.filter]){
			for(var el in holder[params.filter]){
				if(holder[params.filter].hasOwnProperty(el)){
					var name = holder[params.filter][el];
					if(params.translate){
						name = $rootScope.getMsgs(name);
					}
					params.arr.push({id: el, name: name});
				}
			}
		}
	}
	$rootScope.getFilterElById = function(arr, value){
		return (typeof value != "undefined" && value != null) ? arr[searchJsonKeyInArray(arr, 'id', value)] : null;
	}
	
	$rootScope.sortFilter = function(filter){
		if(!Array.isArray(filter)){
			return filter.id == -1 ? '' : filter.name.trim();
		}else{
			return $filter('orderBy')(filter, $rootScope.sortFilter);
		}
	}
	
	$rootScope.formatAmountForDb = function(params){
		var amount = params.amount;
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
					amount = amount*100;
				}
			}
		}catch(e){}
		return amount;
	}
	
	$rootScope.formatAmount = function(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
		var amount = params.amount;
		var currency = settings.currencies['3'];//EUR
		if(params.currencyId && settings.currencies[params.currencyId]){
			currency = settings.currencies[params.currencyId];
		}
		var centsPart = params.centsPart;
		var centsLength = params.centsLength;
		if(centsLength !== 0 && !centsLength){
			centsLength = 2;
		}
		var withoutDecimalPoint = params.withoutDecimalPoint;
		var round = params.round;
		
		var skipAmountDivide = params.skipAmountDivide;
		
		if(params.amountOnly){
			try{
				if (!isNaN(amount)) {
					if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
						amount = amount/100;
					}
				}
			}catch(e){}
			return amount;
		}
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
					amount = amount/100;
				}
				var rtn = '';
				if (amount.toString().charAt(0) == '-') {
					rtn = '-';
					amount *= -1;
				}
				if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
					amount = amountToFloat(amount);
				}
				if (typeof round == 'undefined' || !round || amount < 1000 ) {
					amount = Math.floor(amount*Math.pow(10, centsLength))/Math.pow(10, centsLength);
					var tmp = amount.toString().split('.');
					var decimal = parseInt(tmp[0]);
					var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
					
					/*if((cents === '') || (cents === 0)){cents = '00';}
					else if(cents.length == '1'){cents = cents + '0';}
					else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}*/
					if((cents === '') || (cents === 0)){
						cents = '';
						for(var i = 0; i < centsLength; i++){
							cents += '0';
						}
					}else{
						cents = parseInt(cents);
						var zeroCount = centsLength - (cents + '').length;
						for(var i = 0; i < zeroCount; i++){
							cents += '0';
						}
					}
					
					if(currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
					rtn += numberWithCommas(decimal);
					if(currency.decimalPointDigits == 0){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
						rtn += "<span>"+cents+"</span>";
					}
					else if((centsPart == 3) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 2) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else{
						rtn += "."+cents;
					}
					if(!currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
				} else {
					if(currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
					rtn += (Math.round(amount / 100)) / 10 + "K";
					if(!currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
				}
				return rtn;
			} else {
				return ' ';
			}
		} catch(e) {
			logIt({'type':3,'msg':e});
		}
	}
	
	$rootScope.getCurrencySymbol = function(currencyId){
		if(currencyId && settings.currencies[currencyId]){
			return settings.currencies[currencyId].currencySymbol;
		}
	}
	$rootScope.getCurrencyName = function(currencyId){
		if(currencyId && settings.currencies[currencyId]){
			return $rootScope.getMsgs(settings.currencies[currencyId].currencyName);
		}
	}
	
	
	//Date functions
	$rootScope.dateFormatDisplay = function(d){
		var date = new Date();
		if(d){
			date = new Date(d);
		}
		date.setHours(date.getHours() + date.getTimezoneOffset()/60 + $scope.loggedUser.utcOffsetHours);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes()) + ':' + $rootScope.dateFillToTwoDigits(date.getSeconds()) + ' ' + $rootScope.dateFillToTwoDigits(date.getDate()) + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
	}
	
	// $rootScope.dateParse = function(params){
	// 	var date = new Date(params.date);
	// 	var hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 	var minute = $rootScope.dateFillToTwoDigits(date.getMinutes());
	// 	if(params.startOfDay){
	// 		date.setHours(0/* + $rootScope.loggedUser.utcOffsetHours*/);
	// 		date.setMinutes(0);
	// 		date.setSeconds(0);
	// 		date.setMilliseconds(0);
	// 		hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 		minute = '00';
	// 	}else if(params.endOfDay){
	// 		date.setHours(24/* + $rootScope.loggedUser.utcOffsetHours*/);
	// 		date.setMinutes(0);
	// 		date.setSeconds(0);
	// 		date.setMilliseconds(0);
	// 		hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 		minute = '00';
	// 	}
	// 	if(params.timestamp){
	// 		return date.getTime();
	// 	}
	// 	return date.getFullYear() + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + $rootScope.dateFillToTwoDigits(date.getDate()) + ' ' + hour + ':' + minute + ' ' + $rootScope.loggedUser.utcOffset;
	// }
	
	$rootScope.dateFillToTwoDigits = function(num){
		if(num >= 0 && num < 10){
			return '0' + num;
		}else{
			return num;
		}
	}
	
	$rootScope.dateGetSimple = function(d, separator){
		if(!d){
			return '';
		}
		if(!separator){
			separator = '/';
			
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getDate()) + separator + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + separator + date.getFullYear();
	}
	
	$rootScope.timeGetSimple = function(d){
		if(!d){
			return '';
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes());
	}
	
	
	//Dependencies functions
	$rootScope.addDependency = function(scope, dependency, callback){
		if(!checkOwnProperty(scope, 'dependencies')){
			scope.dependencies = {};
		}
		scope.dependencies[dependency.name] = dependency;
		scope.dependencies[dependency.name].callback = callback;
	}
	
	$rootScope.markDependencyDone = function(scope, dependencyName, dependencyItem){
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName] && scope.dependencies[dependencyName].dependencies){
			scope.dependencies[dependencyName].dependencies[dependencyItem] = true;
		}
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName]){
			var isReady = true;
			for(var dependencyItem in scope.dependencies[dependencyName].dependencies){
				if(scope.dependencies[dependencyName].dependencies.hasOwnProperty(dependencyItem)){
					if(!scope.dependencies[dependencyName].dependencies[dependencyItem]){
						isReady = false;
					}
				}
			}
			if(isReady){
				$timeout(function(){scope.dependencies[dependencyName].callback();}, 0);
			}
		}
	}
	
	//Init screen controllers
	$rootScope.initScreenCtr = function(scope, dependencies, initCallback){
		var that = scope;
		that.ready = false;
		
		$rootScope.enrichScope(that);
		
		that.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if(that.ready){
				if(that.checkAction){
					that.checkAction();
				}
			}
		});
		that.mainInitWatch = that.$watch(function(){return $rootScope.ready;}, function(){
			if($rootScope.ready){
				that.mainInitWatch();
				that.init();
			}
		});
		that.init = function(){
			that.showLoading();
			that.errors = {};
			that.errors.globalErrorMsg = '';
			that.errors.localErrorMsgs = [];
			that.$on('resetGlobalErrorMsg', function(event, args){
				if(that.resetGlobalErrorMsg){
					that.resetGlobalErrorMsg(that);
				}
			});
			if(dependencies){
				$rootScope.addDependency(that, dependencies, function(){that.initDone();});
			}else{
				that.initDone();
			}
			if(that.initFiltersOnReady){
				that.initFiltersOnReady();
			}
			if(initCallback){
				initCallback();
			}
		}
		that.initDone = function(){
			that.ready = true;
			that.hideLoading();
			if(that.checkAction){
				that.checkAction();
			}
		}
	}
	
	$rootScope.enrichScope = function(scope){
		var that = scope;
		
		that.settings = Utils.settings;
		that.showLoading = Utils.showLoading;
		that.hideLoading = Utils.hideLoading;
		
		//File init
		$scope.usingFlash = FileAPI && FileAPI.upload != null;
		$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
		$scope.fileTooBig = false;
		
		that.handleNetworkError = Utils.handleNetworkError;
		
		that.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
		
		that.handleErrors = function(response, name, holder, exceptions) {//returns true on error
			var data = response.data;
			that.requestsStatus[data.serviceName] = 1;
			if (data == '') {
				logIt({'type':3,'msg':name});
				logIt({'type':3,'msg':data});
				$timeout(function(){
					alert('Response is empty string. Check eclipse console for exeptions.');
				});
				return true;
			} else if (data.responseCode == null) {
				logIt({'type':3,'msg':name});
				logIt({'type':3,'msg':data});
				$timeout(function(){
					alert('Error code is null. Check eclipse console for exeptions. ');
				});
				return true;
			} else if (data.responseCode > errorCodeMap.success && (!exceptions || exceptions.indexOf(data.responseCode) == -1)) {//0
				var defMsg = $rootScope.getMsgs('error-' + data.responseCode);
				logIt({'type':3,'msg':name + " (" + defMsg + ")"});
				logIt({'type':3,'msg':data});
				
				that.resetGlobalErrorMsg(this, holder);
				
				if(data.responseCode == errorCodeMap.authentication_failure){ //session expired or unauthenticated - 602
					AuthService.deauthorize();
					return true;
				} else if (data.responseCode == errorCodeMap.access_denied) { //session expired or unauthenticated - 601
					AuthService.deauthorize();
				} else if (data.responseCode == errorCodeMap.account_locked) { // 605
					if (data.messages.length) {
						that.addGlobalErrorMsg(this, $rootScope.getMsgs(data.messages[0]), holder);
						return true;
					}
				} else if (data.responseCode == errorCodeMap.invalid_input) { // 701
					if (data.error && data.error.reason) {
						that.addGlobalErrorMsg(this, $rootScope.getMsgs(data.error.reason), holder);
						return true;
					}
				}
				if (data.error && data.error.messages != null) {
					for(var i = 0; i < data.error.messages.length; i++){
						that.addLocalErrorMsg(this, data.error.messages[i]);
					}
				}
				
				var autoTranslationFound = false;
				for(key in errorCodeMap){
					if(errorCodeMap.hasOwnProperty(key)){
						if(errorCodeMap[key] == data.responseCode){
							autoTranslationFound = true;
							// if($rootScope.isTranslated($rootScope.getMsgs('error.' + key), true)){
							that.addGlobalErrorMsg(this, $rootScope.getMsgs('error.' + key), holder);
							// }
							break;
						}
					}
				}
				
				if(!autoTranslationFound) {
					logIt({'type':3,'msg':'unhandled errorCode: ' + data.responseCode});
					that.addGlobalErrorMsg(this, defMsg, holder);
				}
				
				return true;
			} else {
				that.resetGlobalErrorMsg(this, holder);
				logIt({'type':1,'msg':data});
				return false;
			}
		}
		
		that.addLocalErrorMsg = function(scope, data){
			if(!scope.errors){
				scope.errors = {};
			}
			if(!scope.errors.localErrorMsgs){
				scope.errors.localErrorMsgs = [];
			}
			scope.errors.localErrorMsgs.push(data);
		}
		that.clearLocalErrorMsg = function(scope){
			if(!scope.errors){
				scope.errors = {};
			}
			scope.errors.localErrorMsgs = [];
		}
		
		that.addGlobalErrorMsg = function(scope, data, holder){
			if(holder){
				holder.push(data);
			}else{
				if(!scope.errors){
					scope.errors = {};
				}
				scope.errors.globalErrorMsg += '<span class="error">' + data + '</span>';
			}
		}
		
		that.resetGlobalErrorMsg = function(scope, holder){
			if(holder){
				holder.length = 0;
			}else{
				if(!scope.errors){
					scope.errors = {};
				}
				scope.errors.globalErrorMsg = '';
			}
		}
		
		
		that.productIsPrimary = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			if(!selectedProduct || !selectedProduct.productStatus){
				return false;
			}
			if(selectedProduct.productStatus.id <= settings.productStatuses.SUBSCRIPTION){
				return true;
			}
			return false;
		}
		
		that.productIsSecondary = function(selectedProduct){
			return !that.productIsPrimary(selectedProduct) && !that.productIsPendingSettlement(selectedProduct);
		}
		
		that.productIsPendingSettlement = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			if(!selectedProduct || !selectedProduct.productStatus){
				return false;
			}
			if(selectedProduct.productStatus.id == settings.productStatuses.PENDING_SETTLEMENT){
				return true;
			}
			return false;
		}
		
		that.productGetInitialFixingLevel = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			var initialFixingLevel = selectedProduct.productMarkets ? selectedProduct.productMarkets[0].startTradeLevel : '';
			if(that.productIsPrimary(selectedProduct)){
				//Probably the row below is irrelevant, keeping it for now
				//initialFixingLevel = selectedProduct.productMarkets[0].market.lastPrice;
				initialFixingLevel = 100;
			}
			return initialFixingLevel;
		}
		
		that.productGetBarrierObservationPeriod = function(separator, selectedProduct){
			var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
			if(selectedProduct.productBarrier){
				if(!separator){
					separator = '.';
				}
				if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
					return $scope.dateGetSimple(selectedProduct.productBarrier.barrierStart, separator) + ' - ' + $scope.dateGetSimple(selectedProduct.productBarrier.barrierEnd, separator);
				}else{
					return $scope.dateGetSimple(selectedProduct.productBarrier.barrierEnd, separator);
				}
			}
		}
		
		that.productIsBarrierTypeEuropean = function(selectedProduct){
			var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
			if($rootScope.isEmpty(selectedProduct)){
				return '';
			}
			if(selectedProduct.productBarrier){
				if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.European){
					return true;
				}else{
					return false;
				}
			}
		}
		
		that.productLoadTypesConfig = function(selectedProduct){
			var typeConfig = null;
			switch (selectedProduct.productType.id) {
				case 1: typeConfig = new CPCPConfig(that, selectedProduct);
					break;
				case 2: typeConfig = new BCPCConfig(that, selectedProduct);
					break;
				case 3: typeConfig = new CPCCConfig(that, selectedProduct);
					break;
				case 4: typeConfig = new RCConfig(that, selectedProduct);
					break;
				case 5: typeConfig = new BRCConfig(that, selectedProduct);
					break;
				case 6: typeConfig = new IBRCConfig(that, selectedProduct);
					break;
				case 7: typeConfig = new ECConfig(that, selectedProduct);
					break;
				case 8: typeConfig = new OCConfig(that, selectedProduct);
					break;
				case 9: typeConfig = new BCConfig(that, selectedProduct);
					break;
				case 10: typeConfig = new BOCConfig(that, selectedProduct);
					break;
				default:
					//
			}
			selectedProduct._typeConfig = typeConfig;
			selectedProduct.productTypesConfig = typeConfig.loadConfig();
		}
		
		that.getMsgs = $rootScope.getMsgs;
		that.isTranslated = $rootScope.isTranslated;
		that.msgsParam = $rootScope.msgsParam;
		that.isEmpty = $rootScope.isEmpty;
		that.confirm = $rootScope.confirm;
		that.translateJsonKeyInArray = $rootScope.translateJsonKeyInArray;
		that.fillFilter = $rootScope.fillFilter;
		that.getFilterElById = $rootScope.getFilterElById;
		that.sortFilter = $rootScope.sortFilter;
		that.formatAmountForDb = $rootScope.formatAmountForDb;
		that.formatAmount = $rootScope.formatAmount;
		that.getCurrencySymbol = $rootScope.getCurrencySymbol;
		that.getCurrencyName = $rootScope.getCurrencyName;
		that.dateFormatDisplay = $rootScope.dateFormatDisplay;
		// that.dateParse = $rootScope.dateParse;
		that.dateFillToTwoDigits = $rootScope.dateFillToTwoDigits;
		that.dateGetSimple = $rootScope.dateGetSimple;
		that.timeGetSimple = $rootScope.timeGetSimple;
		that.round = round;
	}
	
	$rootScope.initScreenCtr($scope);
	
	
	
	$rootScope.msgs = {};
	$scope.initRequestStatus = {};
	function getMsgsJson(getMsgsJsonResponse) {
		if (!getMsgsJsonResponse) {
			return;
		}
		
		$scope.initRequestStatus.getMsgsJson = false;
		initMsgsJson(getMsgsJsonResponse);
		
		function initMsgsJson(response) {
			if (!$scope.handleErrors(response, 'getMsgsJson')) {
				if(Utils.parseResponse(response) && Utils.parseResponse(response)[$rootScope.language.id]){
					$rootScope.msgs = Utils.parseResponse(response)[$rootScope.language.id];
				}
				$scope.initRequestStatus.getMsgsJson = true;
				
				if (!isUndefined($rootScope.$state.current.metadataKey)) {
					$rootScope.metadataTitle = $rootScope.$state.current.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
			}
		}
	}
	
	$scope.initRequestStatus.init = false;
	function init(response) {
		var init = Utils.parseResponse(response);
		$scope.initRequestStatus.init = true;
		$rootScope.isMobile = false;
		$rootScope.deviceInfo = init.deviceInfo;
		if($rootScope.deviceInfo.type != 'DESKTOP'){
			$rootScope.isMobile = true;
		}
		
		$rootScope.investmentPossibilities = init.investmentPossibilities;
		$rootScope.countries = init.countriesHM;
		$rootScope.currencies = init.currenciesHM;
		$rootScope.Company_Address = init.Company_Address;
		$rootScope.support_email = init.support_email;
		$rootScope.support_phone = init.support_phone;
		for(key in $rootScope.currencies){
			if($rootScope.currencies.hasOwnProperty(key)){
				settings.currencies[key] = {
					decimalPointDigits: 2,
					currencySymbol: $rootScope.currencies[key].symbol,
					currencyLeftSymbol: true,
					currencyName: $rootScope.currencies[key].displayName
				};
			}
		}
		if(init.user){
			$rootScope.isLogged = true;
			$rootScope.setLoggedUser(init.user);
			UserService.refreshLoggedUser(); //init returns wrong ballance
		}
		$rootScope.userLogout = init.userLogout;
		
		$rootScope.$on('$stateChangeSuccess', function() {
			UserService.refreshLoggedUser();
		});
	}
	
	$rootScope.getMenu = function(){
		if(!$rootScope.menu){
			$rootScope.menu = new MenuTree.InstanceClass(menu, false, $rootScope.permissions);
			$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
		}
		$rootScope.menuVisible = false;
		return $rootScope.menu;
	}
	
	
	//Set loggedUser
	$rootScope.setLoggedUser = function(user){
		$rootScope.loggedUser = user || {};
		$rootScope.loggedUser.utcOffsetHours = 3;
		$rootScope.loggedUser.utcOffset = 'GMT+03:00';
	}
	
	//Init loggedUser
	$rootScope.setLoggedUser();
	
	
	//Init user
	$scope.userId = 0;
	if(window.userId){
		$scope.userId = parseInt(userId);
	}
	if(window.userCurrency){
		$scope.userCurrency = userCurrency;
	}
	$scope.getUserCurrency = function(){
		return $scope.userCurrency;
	}
	
	
	$scope.checkInitStatus = function() {
		for (var key in $scope.initRequestStatus) {
			if (!$scope.initRequestStatus[key]) {
				$timeout(function() {
					$scope.checkInitStatus();
				}, 50);
				return;
			}
		}
		$rootScope.loaded = $rootScope.ready = $scope.ready = true;
		if(window.menu){
			$rootScope.menu = $rootScope.getMenu();
		}
	};
	
	
	$scope.initRequests = function() {
		$rootScope.ready = false;
		getMsgsJson(getMsgsJsonResponse);
		init(initResponse);
		$scope.checkInitStatus();
	}

	$scope.initRequests();
	
	$scope.isLnListVisible = false;
	$scope.toggleLanguageList = function(forceClose) {
		if (forceClose) {
			$scope.isLnListVisible = false;
			return;
		}
		$scope.isLnListVisible = !$scope.isLnListVisible;
	};
	$rootScope.changeLanguage = function(language){
		if(language == $rootScope.language){
			return false;
		}
		$scope.toggleLanguageList(true);
		$rootScope.ready = false;
		$rootScope.language = language;
		var methodRequest = {
			data: {
				id: $rootScope.language.id
			}
		};
		$http.post('/language/change', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'changePasswordLogout')) {
					$rootScope.$state.go($rootScope.$state.current, {ln: $rootScope.language.code}).then(function(){
						$scope.checkInitStatus();
					});
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			}
		);
	}
	
	$rootScope.getMetaTitle = function(){
		var metaTitle = $rootScope.$state.current.metaTitle;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaTitle && parent) {
			metaTitle = parent.self.metaTitle;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaTitle || 'meta-title-index');
	};
	
	$rootScope.getMetaDescription = function(){
		var metaDescription = $rootScope.$state.current.metaDescription;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaDescription && parent) {
			metaDescription = parent.self.metaDescription;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaDescription || 'meta-description-1');
	};
	
	$rootScope.getMetaKeywords = function(){
		var metaKeywords = $rootScope.$state.current.metaKeywords;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaKeywords && parent) {
			metaKeywords = parent.self.metaKeywords;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaKeywords || 'meta-keywords-1');
	};
	
	$rootScope.getPageTitle = function(){
		if($rootScope.$state.current.titleTransclude){
			return $rootScope.$state.current.title;
		}
		
		var title = $rootScope.$state.current.title;
		var parent = $rootScope.$state.$current.parent;
		
		while (!title && parent) {
			title = parent.self.title;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(title);
	}
	
	$rootScope.isMenuVisible = function(){
		if($rootScope.isMobile && !$rootScope.menuVisible){
			return false;
		}
		return true;
	}
	
	$rootScope.toggleMenu = function(forceState){
		if($rootScope.isMobile){
			if(forceState === true){
				$rootScope.menuVisible = true;
			}else if(forceState === false){
				$rootScope.menuVisible = false;
			}else{
				$rootScope.menuVisible = !$rootScope.menuVisible;
			}
		}
	}
	
	$rootScope.hideHeader = function(){
		if($rootScope.isMobile && $rootScope.$state.current.mobileConfig && $rootScope.$state.current.mobileConfig.showHeader === false){
			return true;
		}
		return false;
	}
	
	$rootScope.goBack = function(){
		$window.history.back();
	}
	
	//Hide menu bottom border(scrolled/not scrolled) on some screens
	$scope.isHideBottomLine = function() {
		var isHide;
		if ($rootScope.isMobile) {
			isHide = /(\.products?|\.register|\.sign-up\.personal-details|\.sign-up\.questionnaire|\.please-login|\.my-account)/i.test($state.current.name);
		} else {
			isHide = /(\.products$)/i.test($state.current.name);
		}
		return isHide;
	};
	
	//Disable menu bottom border if not scrolled
	$scope.isBottomDisabled = function() {
		var isDisabled;
		if ($rootScope.isMobile) {
			isDisabled = /(\.index)/i.test($state.current.name);
		} else {
			isDisabled = /(\.index|\.products?|\.register|\.please-login|\.basics)/i.test($state.current.name);
		}
		return isDisabled;
	};
	
	$scope.isMenuTransparent = function() {
		return /(\.index)/i.test($state.current.name);
	};
	
	$scope.footerIconsList = [
		{name: 'nasdaq.svg', width: 120},
		{name: 'nyse.svg', width: 44},
		{name: 'deutsche-borse.svg', width: 77},
		{name: 'nyse-euronext.svg', width: 109}	
	];
	
	$scope.isUpArrowHidden = function() {
		return $rootScope.isMobile && /^ln\.my-account/i.test($state.current.name);
	};
	
	$(document).on('scroll', function(e){
		if($(document).scrollTop() > 0){
			$('body').addClass('scrolled');
		}else{
			$('body').removeClass('scrolled');
		}
	});
	
	$scope.depositClickHandler = function() {
		if (!$rootScope.loggedUser.userStep) return;
		var userStepId = $rootScope.loggedUser.userStep.id;
		switch (userStepId) {
			case 1:
				$state.go('ln.sign-up.personal-details');
				break;
			case 2:
				$state.go('ln.sign-up.questionnaire');
				break;
			case 3:
				$state.go('ln.my-account.settings.questionnaire');
				break;
			case 4:
				$state.go('ln.my-account.funding.upload-documents');
				break;
			case 5:
			default:
				return $state.go('ln.my-account.funding.deposit');
		}
		$state.go('ln.my-account.funding.deposit', {ln: $state.params.ln});
	};
	
	$scope.logout = function(){
		$scope.showLoading();
		$rootScope.redirectAfterLogin = null;
		
		AuthService.logout($scope.handleErrors.bind($scope))
			.then(function() {
				$scope.hideLoading();
			})
			.catch(function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	};
	
}]);

spApp.controller('RegisterCtr', ['$rootScope', '$scope', '$state', 'Utils', 'SingleSelect', 'AuthService', 'restriction', function($rootScope, $scope, $state, Utils, SingleSelect, AuthService, restriction) {
	$scope.settings = Utils.settings;
	$scope.$state = $state;
	
	if (restriction && restriction.data && restriction.data.responseCode === 604) {
		$scope.isRegistrationRestricted = true;
	}
	
	$scope.patterns = {
		phone: regEx_phone,
		charsOnly: regEx_charsOnly
	};
	
	$scope.credentials = {
		signup: {
			firstName: '',
			lastName: '',
			email: '',
			phone: '',
			userPass: '',
			acceptTerms: false,
			phoneCode: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')})
		}
	};
	
	var phoneCodeList = Object.keys($rootScope.countries).map(function(key) {
		var country = $rootScope.countries[key];
		return {
			id: key,
			suffixOptionName: '+' + country.phoneCode,
			suffixDisplayName: false,
			name: $scope.getMsgs(country.displayName),
			displayName: '+' + country.phoneCode,
			phoneCode: country.phoneCode,
			flagCode: country.a2.toLowerCase(),
			isBlocked: country.isBlocked
		};
	});
	$scope.credentials.signup.phoneCode.fillOptions(phoneCodeList);

	if ($rootScope.userLogout && $rootScope.userLogout.country) {
		$scope.credentials.signup.phoneCode.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	}
	
	$scope.signup = function(form){
		if (form.$invalid || $scope.isRegistrationRestricted || $scope.credentials.signup.phoneCode.model.isBlocked) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.signup($scope.credentials.signup, $scope.handleErrors.bind($scope))
			.then(function() {
				Utils.hideLoading({container: loadingOverlayContainer});
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
}]);

spApp.controller('CredentialsCtr', ['$sce', '$rootScope', '$scope', '$http', '$state', 'Utils', 'SingleSelect', '$cookies', '$log', 'AuthService', function($sce, $rootScope, $scope, $http, $state, Utils, SingleSelect, $cookies, $log, AuthService) {
	var rememberMeEmail = AuthService.getRememberMeEmail() || '';

	$scope.settings = Utils.settings;
	$scope.getMsgs = $rootScope.getMsgs;
	$scope.$state = $state;
	
	$scope.credentials = {};
	$scope.credentials.login = {};
	$scope.credentials.login.userName = rememberMeEmail;
	$scope.credentials.login.userPass = '';
	$scope.credentials.login.rememberMe = rememberMeEmail ? true : false;
	
	$scope.credentials.forgotPassword = {};
	$scope.credentials.forgotPassword.userEmail = '';
	
	$scope.credentials.resetPassword = {};
	$scope.credentials.resetPassword.via = 1;
	$scope.credentials.resetPassword.phone = '*';
	$scope.credentials.resetPassword.email = '*';
	
	$scope.patterns = {
		phone: regEx_phone
	};
	
	$scope.emailFromCookie = $cookies.get('e');
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	$scope.goToSupport = function(){
		$state.go('ln.contact-us', {ln: $state.params.ln});
	};
	
	$scope.doLogin = function(){
		if($scope.afterLogin){
			$scope.afterLogin();
		}
	};
	
	$scope.login = function(form){
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.login($scope.credentials.login, $scope.handleErrors.bind($scope))
			.then(function() {
				Utils.hideLoading({container: loadingOverlayContainer});
				$scope.doLogin();
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.reset = function(form){
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.resetPassword($scope.credentials.forgotPassword, $scope.handleErrors.bind($scope))
			.then(function(response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				var userInfo = response.data.data;
				if (response.data.responseCode === 0 && Object(userInfo) === userInfo) {
					$scope.credentials.resetPassword.phone = userInfo.mobilePhone;
					$scope.credentials.resetPassword.email = userInfo.email;
					$scope.gotoState($scope.settings.credentialsView.RESET_SUBMIT);
				} else {
					$scope.gotoState($scope.settings.credentialsView.RESET_FAILED);
				}
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.submitResetPassword = function(form) {
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.forgotPassword($scope.credentials.resetPassword, $scope.handleErrors.bind($scope))
			.then(function(response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				var data = response.data.data;
				if (response.data.responseCode === 0 && Object(data) === data) {
					$scope.gotoState($scope.settings.credentialsView.RESET_APPROVED);
				} else {
					$scope.gotoState($scope.settings.credentialsView.RESET_FAILED);
				}
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.gotoState = function(state){
		$scope.resetGlobalErrorMsg($scope);
		$scope.viewState = state;
	};
	
	$rootScope.$watch('isMobile', function(isMobile) {
		$scope.isMobile = isMobile;
	});
	
}]);

spApp.controller('resetPasswordCtrl', ['$stateParams', '$state', '$rootScope', '$scope', '$http', function($stateParams, $state, $rootScope, $scope, $http) {
	var ctrl = this;
	
	ctrl.getMsgs = $rootScope.getMsgs;
	
	ctrl.isPasswordChanged = false;
	
	ctrl.setPassword = {
		email: $stateParams.email,
		newPassword: '',
		retypeNewPassword: ''
	};
	
	ctrl.submitSetPassword = function(form) {
		if (form.$invalid) return;
		var methodRequest = {
			data: {
				newPassword: ctrl.setPassword.newPassword,
				retypePassword: ctrl.setPassword.retypeNewPassword
			}
		};
		
		$http.post('/user/changePasswordLogout', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'changePasswordLogout')) {
					ctrl.isPasswordChanged = true;
					// $state.go('ln.please-login');
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			}
		);
	};
	
	$scope.$watch('errors.globalErrorMsg', function(globalErrorMsg) {
		ctrl.globalErrorMsg = globalErrorMsg;
	}); 
}]);

spApp.controller('LoginCtr', ['$rootScope', '$scope', '$stateParams', function($rootScope, $scope, $stateParams) {
	$scope.currentState = $stateParams.credentialsState === null ? $scope.settings.credentialsView.LOGIN : $stateParams.credentialsState;
	$stateParams.credentialsState = null;
	$scope.hideLoginHeader = true;
	// $rootScope.redirectAfterLogin = $rootScope.$stateParams.toState;
	// if(!$rootScope.redirectAfterLogin){
	// 	$rootScope.redirectAfterLogin = 'ln.index';
	// }
}]);

spApp.controller('HomeCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$interval', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'articlesList', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $interval, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, articlesList) {
	
	var intervals = [];
	
	$scope.articles = [articlesList[11], articlesList[6], articlesList[2]];
	
	$timeout(function(){
		$('.slide-text-holder h1, .slide-text-holder p, .slide-text-holder button').addClass(' animated fadeInSlideUp');
	});
	$(document).on('scroll', scrollHandler);
	
	$stepsInterval = setInterval(function(){
		$('.step').each(function(index, el){
			if(isElementInViewport(el)){
				$(el).addClass('visible');
			}
		});
		var counter = 0;
		$('.step').each(function(index, el){
			if($(el).hasClass('visible')){
				counter++;
			}
		});
		if(counter == $('.step').length){
			clearInterval($stepsInterval);
		}
	}, 400);
	intervals.push($stepsInterval);
	
	$bounceInterval = setInterval(function(){
		$('.about-us-articles .inner-half').each(function(index, el){
			if(isElementInViewport(el)){
				$(el).addClass('visible');
				$(el).addClass($(el).data('animation-name'));
			}
		});
		var counter = 0;
		$('.about-us-articles .inner-half').each(function(index, el){
			if($(el).hasClass('visible')){
				counter++;
			}
		});
		if(counter == $('.about-us-articles .inner-half').length){
			clearInterval($bounceInterval);
		}
	}, 400);
	intervals.push($bounceInterval);
	
	var hpFirstVisitFlag = angular.fromJson(localStorage.getItem('hpFirstVisitFlag', true));
	if (!hpFirstVisitFlag) {
		setTimeout(function() {
			$('body').animate({
				scrollTop: 85
			}, 500);
		});
		try {
			localStorage.setItem('hpFirstVisitFlag', true);
		} catch(e) {
			console.log('No localStorage');
		}
	}
	
	$scope.$on('$destroy', function() {
		intervals.forEach(function(interval) {
			clearInterval(interval);
		});
		$(document).off('scroll', scrollHandler);
	});
	
	function scrollHandler(e) {
		var $slide = $('.slide-holder').find('.slide').not('.ng-hide');
		var delta = 0;
		var ratio = 0.4;
		var height = parseInt($slide.find('img').css('height'));
		var maxScroll = 600;
		var scrollTop = $(document).scrollTop();
		if(scrollTop == 0){
			//chill
		}else if(scrollTop > maxScroll){
			delta = ratio*maxScroll;
		}else{
			delta = ratio*scrollTop;
		}
		
		$slide.css('max-height', (height - delta));
		
		$slide.find('.slide-text').each(function() {
			var slideText = this;
			var slideTextRect = slideText.getBoundingClientRect();
			var menuRect = $('.menu')[0].getBoundingClientRect();

			if (slideTextRect.top < menuRect.bottom) {
				var opacity = (slideTextRect.bottom - menuRect.bottom + 85) / (slideTextRect.height * 1.3);
				slideText.style.opacity = opacity;
			} else {
				slideText.style.opacity = 1;
			}
		});
	}
}]);

spApp.controller('SlidesCtr', ['$sce', '$rootScope', '$scope', '$window', '$uibModal', function($sce, $rootScope, $scope, $window, $uibModal) {
	$scope.visibleIndex = isUndefined($window.gtmSlideIndex) ? 1 : $window.gtmSlideIndex;
	$scope.slides = [
		{image: 'slide-hand.jpg', imageMobile: 'slide-hand-mobile.jpg', align: 'left', text: parseSlideText('home-page-slide-hand')},
		{image: 'slide-boat-lake.jpg', imageMobile: 'slide-boat-lake-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-boat-lake')},
		{image: 'slide-boat.jpg', imageMobile: 'slide-boat-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-boat')},
		{image: 'slide-kids-lake.jpg', imageMobile: 'slide-kids-lake-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-kids-lake')},
		{image: 'slide-mountain-man.jpg', imageMobile: 'slide-mountain-man-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-mountain-man')}
	];
	function parseSlideText(key){
		var result = $rootScope.getMsgs(key);
		
		var params = [{key: '${risk-warning}', value: $rootScope.getMsgs('risk-warning')}];
		for(var i = 0; i < params.length; i++){
			result = result.replace(new RegExp(params[i].key.replace(/\$/gi, '\\$'), "gi"), params[i].value);
		}
		
		return $sce.trustAsHtml(result);
	}
	$scope.openIntroVideoModal = function(e) {
		if (!$(e.target).closest('.video-button').length) return;
		$uibModal.open({
			animation: true,
			templateUrl: folderPrefix + 'components/video-modal.html',
			controller: ['$scope', '$uibModalInstance', 'video', function($scope, $uibModalInstance, video) {
				$scope.close = function(reason) {
					reason = reason || 'close';
					$uibModalInstance.close(reason);
				};
				
				$scope.video = video;
			}],
			size: 'lg',
			windowClass: 'video-modal',
			resolve: {
				video: ['$sce', function($sce) {
					return {
						name: 'Intro Video',
						url: $sce.trustAsResourceUrl('https://www.youtube.com/embed/4F9ifFqaOX4?rel=0')
					};
				}]
			}
		});
	};
}]);

spApp.controller('PrivacyPolicyAndTermsCtrl', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
	
	$scope.parseText = function(key){
		var result = $rootScope.getMsgs(key);
		var params = [{key: '${company_email}', value: $rootScope.support_email}];
		for(var i = 0; i < params.length; i++){
			result = result.replace(new RegExp(params[i].key.replace(/\$/gi, '\\$'), "gi"), params[i].value);
		}
		return $sce.trustAsHtml(result);
	}
}]);

spApp.controller('SecurityCtr', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
}]);

spApp.controller('LegalDocumentsCtr', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
}]);

spApp.controller('signUpPersonalDetailsCtrl', ['$rootScope', '$scope', '$state', 'SingleSelect', 'SignUpPersonalDetailsService', function($rootScope, $scope, $state, SingleSelect, SignUpPersonalDetailsService) {
	var ctrl = this;
	ctrl.getMsgs = $rootScope.getMsgs;
	ctrl.personalDetails = {
		dayOfBirth: {
			day: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'DD'}, noSort: true}),
			mounth: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'MM'}, noSort: true, mutateCallback: $rootScope.getMsgs}),
			year: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'YYYY'}, noSort: true})
		},
		street: '',
		cityName: '',
		zipCode: '',
		country: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')})
	};
	
	var startDay = 1;
	var endDay = 31;
	var days = [];
	for(var day = startDay; day <= endDay; day++) {
		days.push({id: day, name: day.toString()});
	}
	ctrl.personalDetails.dayOfBirth.day.fillOptions(days);
	
	var startMonth = 1;
	var endMonth = 12;
	var months = [];
	for(var month = startMonth; month <= endMonth; month++) {
		months.push({id: month, name: 'month-' + month});
	}
	ctrl.personalDetails.dayOfBirth.mounth.fillOptions(months);

	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var startYear = currentYear - 18;
	var endYear = currentYear - 100;
	var years = [];
	for(var year = startYear; year >= endYear; year--) {
		years.push({id: year, name: year.toString()});
	}
	ctrl.personalDetails.dayOfBirth.year.fillOptions(years);

	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase()
			});
		}
	}
	ctrl.personalDetails.country.fillOptions(countries);
	
	ctrl.submitPersonalDetailsHandler = function(form) {
		if (form.$invalid) {
			return;
		}
		
		SignUpPersonalDetailsService.sendPersonalDetails(ctrl.personalDetails)
			.then(function(response) {
				if (!$scope.handleErrors(response, 'PersonalDetailsInsert')) {
					$rootScope.loggedUser.step++;
					$state.go('ln.sign-up.questionnaire');
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			});
	};
	
}]);

spApp.controller('QuestionnaireCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'QuestionnaireService', 'AuthService', 'SingleSelect', 'ScrollTop', 'Utils', 'scrollToElement', function($scope, $rootScope, $state, $stateParams, QuestionnaireService, AuthService, SingleSelect, ScrollTop, Utils, scrollToElement) {
	var ctrl = this;
	ctrl.$state = $state;
	ctrl.getMsgs = $rootScope.getMsgs;
	ctrl.support_email = $rootScope.support_email;
	
	ctrl.questionType = {
		radio: 1,
		checkbox: 2,
		input: 3,
		select: 4
	};
	
	$rootScope.$watch('loggedUser.userStep.id', function(userStepId) {
		ctrl.userStepId = userStepId;
	});
	
	ctrl.questionsList = [];
	ctrl.checkboxesList = [];
	ctrl.action = 'loading';
	var isWithAnswers = $rootScope.loggedUser.userStep.id > 3;
	QuestionnaireService.getQuestionnaire(isWithAnswers)
		.then(function(questionsList) {
			ctrl.action = '';
			if (!questionsList.length) {
				return;
			}
			var processedQuestionsList = questionsList
				.sort(function(a, b) {
					return a.orderId - b.orderId;
				})
				.map(function(q) {
					var qmAnswerListSorted = q.qmAnswerList.sort(function(a, b) {return a.orderId - b.orderId;});
					var answerSelectedByUser = q.qmAnswerList.filter(function(a) {return a.selectedAnswer;})[0];
					var qNewOrUpdatedFields = {
						qmAnswerList: qmAnswerListSorted
					};
					switch(q.questionTypeId) {
						case ctrl.questionType.radio:
							if (answerSelectedByUser) {
								qNewOrUpdatedFields.answerId = answerSelectedByUser.id;
							}
							break;
						case ctrl.questionType.checkbox:
							if (answerSelectedByUser) {
								qNewOrUpdatedFields.answerId = answerSelectedByUser.name.toLowerCase() === 'yes';
							} else if (!isWithAnswers) {
								qNewOrUpdatedFields.answerId = false; //hardcode to set pre-checked.
							}
							break;
						case ctrl.questionType.input:
							break;
						case ctrl.questionType.select:
							var qmAnswerListForSelect = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'select'}, isSmart: true, noSort: true, filterPlaceholder: $rootScope.getMsgs('search'), mutateCallback: $rootScope.getMsgs});
							var answers = qmAnswerListSorted.map(function(a) {return angular.merge({}, a, {name: a.displayName});});
							var selectedId = answerSelectedByUser ? answerSelectedByUser.id : -1;
							qmAnswerListForSelect.fillOptions(answers);
							qmAnswerListForSelect.setById(selectedId);
							qNewOrUpdatedFields.qmAnswerListForSelect = qmAnswerListForSelect;
							break;
					}
					return angular.merge({}, q, qNewOrUpdatedFields);
				});
			ctrl.questionsList = processedQuestionsList.filter(function(q) {
				return q.questionTypeId !== ctrl.questionType.checkbox;
			});
			ctrl.checkboxesList = processedQuestionsList.filter(function(q) {
				return q.questionTypeId === ctrl.questionType.checkbox;
			});
		})
		.catch(function(response) {
			ctrl.action = '';
		});
	
	ctrl.visibleQuestionnaire = {
		offset: 0,
		limit: 7
	};

	ctrl.prevHandler = function() {
		ctrl.visibleQuestionnaire.offset -=  ctrl.visibleQuestionnaire.limit;
		setTimeout(ScrollTop);
	};
	
	ctrl.isLastPage = function() {
		var offset = ctrl.visibleQuestionnaire.offset + ctrl.visibleQuestionnaire.limit;
		return offset >= ctrl.questionsList.length;
	};
	
	ctrl.submitHandler = function(form) {
		if (form.$invalid) {
			if ($('[name="'+form.$name+'"]').find('.form-error-msg').length) {
				scrollToElement('.form-error-msg', 15);
			} else {
				scrollToElement('.ng-invalid + .checkbox', 15);
			}
			return;
		}
		var offset = ctrl.visibleQuestionnaire.offset + ctrl.visibleQuestionnaire.limit;
		if (offset >= ctrl.questionsList.length) {
			QuestionnaireService.sendQuestionnaire(Array.prototype.concat(ctrl.questionsList, ctrl.checkboxesList), $rootScope.loggedUser)
				.then(function (response) {
					if (!$scope.handleErrors(response, 'questionnaireInsert')) {
						var user = Utils.parseResponse(response);
						$rootScope.loggedUser.userStep = user.userStep;
						AuthService.redirectAfterLogin(user);
					}
				}, function(response) {
					$scope.handleNetworkError(response);
				});
		} else {
			ctrl.visibleQuestionnaire.offset = offset;
			form.$setPristine();
			setTimeout(ScrollTop);
		}
	};
}]);


spApp.controller('BasicsCtrl', ['$scope', '$rootScope', '$document', '$state', 'SingleSelect', 'scrollService', 'activeSection', 'scrollToElement', function($scope, $rootScope, $document, $state, SingleSelect, scrollService, activeSection, scrollToElement) {
	var ctrl = this;
	ctrl.getMsgs = $rootScope.getMsgs;
	if ($state.params.id) {
		setTimeout(function() {
			scrollToElement('[name="'+$state.params.id+'"]', -100);
		}, 750);
	}
	
	ctrl.countries = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase(),
				supportPhone: $rootScope.countries[key].supportPhone
			});
		}
	}
	ctrl.countries.fillOptions(countries);
	ctrl.countries.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	setTimeout(function() {
		setCurrentViewPort();
	});
	scrollService.subscribe(setCurrentViewPort);
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
		scrollService.unsubscribe(setCurrentViewPort);
		activeSection.set('');
	});
	
	function setAllParalax() {
		setParalax({container: '.basics-header-section', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 240});
		setParalax({container: '.support-section', imageRatio: 1.4, imageWidth: 1920, imageHeight: 2000, imageStaticOffset: -1400});
		setParalax({container: '.profit-profile-diagram-section', imageRatio: 0.7, imageWidth: 1920, imageHeight: 1280, imageStaticOffset: 0});
	}
	
	function setCurrentViewPort() {
		var $menu = $('.menu');
		var topScrollPosition = $(window).scrollTop() + $menu[0].offsetTop + $menu.outerHeight();
		$('.basics-screen').find('.section')
			.each(function() {
				var $section = $(this);
				var sectionTop = $section.offset().top;
				var sectionBottom = sectionTop + $section.outerHeight();
				if (topScrollPosition >= sectionTop && topScrollPosition < sectionBottom) {
					$rootScope.$applyAsync(function() {
						var activeSectionName = $section.attr('data-nav-marker');
						activeSection.set(activeSectionName);
						ctrl.isDark = $section[0].hasAttribute('data-is-dark');
					});
					return false;
				}
			});
	}
}]);

spApp.directive('autoFocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if (attrs.autoFocus) {
				focus(scope.$eval(attrs.autoFocus));
			} else {
				focus(true);
			}
			function focus(condition) {
				if (!condition) return;
				$timeout(function() {
					element[0].focus();
				});
			}
		}
	};
}]);
/*
spApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
});
*/
spApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.$eval(attributes.compareTo);
			};

		scope.$watch(function(){return scope.$eval(attributes.compareTo)}, function() {
				ngModel.$validate();
			});
		}
	};
});

spApp.directive('validCcNum', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
				return value;
			});
		}
	};
});

spApp.directive('validEmail', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validEmail', regEx_email.test(value));
				return value;
			});
		}
	};
});

spApp.directive('remoteValidation', ['$http', function ($http) {
	var remoteValidationValue = 'remoteValidation';
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			var cache = {};
			
			ngModel.$parsers.push(function (value) {
				ngModel.$setValidity(remoteValidationValue, false);
				setTimeout(function() {
					var isValidLocaly = !Object.keys(ngModel.$error).filter(function(key) {return key !== remoteValidationValue;}).length;
					if (isValidLocaly && attrs.remoteValidation) {
						if (value in cache) {
							ngModel.$setValidity(remoteValidationValue, cache[value]);
						} else {
							var url = attrs.remoteValidation;
							var methodRequest = {
								data: value
							};
							$http.post(url, methodRequest).then(function(response) {
								if (response.data && response.data) {
									var isValid = response.data.data;
									cache[value] = isValid;
									ngModel.$setValidity(remoteValidationValue, isValid);
								}
							});
						}
					} else {
						ngModel.$setValidity(remoteValidationValue, true);
					}
				});
				return value;
			});
		}
	};
}]);

spApp.directive('restrictNickname', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				// ngModel.$setValidity('nickname', (regEx_nickname.test(value)));
				value = value.replace(regEx_nickname_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictDigits', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				value = value.replace(regEx_digits_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictFloat', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '.').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictLetters', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				//var regEx_letters_reverse = new RegExp((skinMap[settings.skinId].regEx_letters+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				var regEx_letters_reverse = new XRegExp((regEx_lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				value = value.replace(regEx_letters_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

//Use restrict-currency only with ng-model-options="{updateOn: 'blur'}"
spApp.directive('restrictCurrency', function() {
	return {
		require: 'ngModel',
		scope: {
			currency: '=restrictCurrency'
		},
		link: function(scope, elem, attr, ngModel) {
			$(elem[0]).on('keydown', function(e){
				//Forbid space
				if(e.keyCode == 32){
					return false;
				}
				//Forbid messing with the currency symbol
				if(scope.currency && typeof elem[0].selectionStart != 'undefined'){
					var currencySymbol = scope.currency.currencySymbol;
					var symbolPos = elem[0].value.indexOf(currencySymbol);
					var symbolLength = currencySymbol.length;
					if(symbolPos < 0){
						return true;
					}
					//Allow certain system keys
					if(e.keyCode <= 47 && e.keyCode != 8 && e.keyCode != 46){//Forbid backspace and delete
						return true;
					}
					if(elem[0].selectionStart == symbolPos + symbolLength && e.keyCode == 8){
						return false;
					}
					if(elem[0].selectionStart > symbolPos && elem[0].selectionStart < symbolPos + symbolLength){
						return false;
					}
					if(elem[0].selectionStart == 0 && scope.currency.currencyLeftSymbol){
						return false;
					}
					if(elem[0].selectionStart == elem[0].value && !scope.currency.currencyLeftSymbol){
						return false;
					}
				}
				//Allow system keys
				if(e.keyCode <= 47){
					return true;
				}
				//Allow one dot
				if(e.key == '.' && (elem[0].value.match(/\./g) || []).length == 0){
					return true;
				}
				//Forbid certain keys
				if(e.key == '.' && (elem[0].value.match(/\./g) || []).length > 0){//More than one dot
					return false;
				}else if(e.key != parseInt(e.key)){//Not Integer
					return false;
				}
			});
			$(elem[0]).on('keyup click', function(e){
				if(scope.currency && typeof elem[0].selectionStart != 'undefined'){
					var currencySymbol = scope.currency.currencySymbol;
					var symbolPos = elem[0].value.indexOf(currencySymbol);
					var symbolLength = currencySymbol.length;
					if(symbolPos < 0){
						return true;
					}
					if(elem[0].selectionStart >= symbolPos && elem[0].selectionStart < symbolPos + symbolLength){
						setCaretPosition(elem[0], symbolPos + symbolLength);
					}
				}
			});
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('device', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				var device = detectDevice();
				if (device != '') {
					$element.addClass('mobile-device');
				}
				$element.addClass(device);
			});
		}
	}
}]);

spApp.directive('compile', ['$compile', function($compile) {
	// directive factory creates a link function
	return function(scope, elem, attrs) {
		scope.$watch(
			function(scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(attrs.compile);
			},
			function(value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				elem.html(value);

				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(elem.contents())(scope);
			}
		);
	}
}]);

spApp.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				if(iAttrs.fallbackSrc != ""){
					angular.element(this).attr("src", iAttrs.fallbackSrc);
					angular.element(this).css("visibility", "visible");
				}else{
				//	angular.element(this).css("visibility", "hidden"); //Causes problems, sometimes, with some markets
				}
			});
		}
	}
	return fallbackSrc;
});

spApp.directive('loading', ['$timeout','$compile', function($timeout, $compile) {
	return {
		restrict: "A",
		link: function(scope,element,attrs,ngCtrl) {
			scope.ready;
			var loader = angular.element('<i class="loading-section-el" ng-class="{dN: ready}" ng-hide="ready" ></i>');
			element.addClass('loading-section');
			element.append($compile(loader)(scope));
		}
	};
}]);

spApp.directive('pagination', ['$rootScope', function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'components/pagination.html', // markup for template
		scope: {
			pagination: '=model'
		},
		link: function(scope, element, attrs) {
			scope.getMsgs = $rootScope.getMsgs;
		}
	};
}]);

spApp.directive('multiselect', ['$document', '$timeout', function($document, $timeout) {
	return {
		restrict: 'E',
		require: '?ngModel',
		templateUrl: folderPrefix + 'components/multiselect.html', // markup for template
		scope: {
			multiselect: '=ngModel',
			ngDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.toggleOpen = function(forceClose){
				ngModel ? ngModel.$setTouched() : '';
				if(!scope.multiselect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.multiselect.visible = true;
					$timeout(function() {
						$(element[0]).find('[data-filter]:eq(0)').focus();
					});
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.multiselect.visible = false;
					scope.multiselect.filterText = '';
					$document.unbind('mousedown', documentClickBind);
				}
			}
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.ngDisabled){
					return;
				}
				
				if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}
			});
			
			scope.$watch(function(){
				return scope.multiselect ? scope.multiselect.getCheckedIds() : null;
			}, function(){
				scope.updateScroll();
			}, true);
			
			scope.updateScroll = function(){
				$timeout(function(){
					if($(element[0]).find('.multiselect-selected-options:eq(0)').height() > $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()){
						scope.showScroll = true;
					}else{
						scope.showScroll = false;
					}
				});
			}
			
			scope.selectedOptionsScrollUp = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() - $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
			
			scope.selectedOptionsScrollDown = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() + $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
		}
	};
}]);

spApp.directive('singleSelect', ['$document', '$timeout', '$rootScope', 'lockScroll', function($document, $timeout, $rootScope, lockScroll) {
	return {
		restrict: 'E',
		require: '?ngModel',
		//templateUrl: $rootScope.isMobile ? folderPrefix + 'components/single-select-mobile.html' : folderPrefix + 'components/single-select.html', // markup for template
		template: '<ng-include src="template()"></ng-include>',
		scope: {
			ngDisabled: '=',
			autoFocusDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.templateName = '';
			scope.template = function(){
				scope.templateName = attrs.template;
				return scope.templateName ? folderPrefix + 'components/single-select-' + scope.templateName + '.html' : folderPrefix + 'components/single-select.html';
			};
			
			scope.getMsgs = $rootScope.getMsgs;
			
			ngModel.$options = {
				allowInvalid: true
			};
			ngModel.$isEmpty = function(value){
				if(!value.model || !value.model.model || !value.model.model.name || value.model.model.id === -1){
					return true;
				}
				return false;
			};
			ngModel.$formatters.push(function(model){
				return {model: model};
			});
			ngModel.$parsers.push(function(viewValue){
				return viewValue.model;
			});
			ngModel.$render = function(){
				scope.singleSelect = ngModel.$viewValue.model;
			};
			scope.selectOption = function(option){
				scope.toggleOpen();
				scope.updateModel(option);
			};
			scope.updateModel = function(option){
				if(scope.ngDisabled){
					return;
				}
				ngModel.$modelValue.setModel(option);
				ngModel.$setViewValue({model: ngModel.$modelValue.clone()});
				ngModel.$commitViewValue();
				ngModel.$render();
			};
			scope.$watch(
				function() {
					return ngModel.$modelValue.model;
				}, 
				function() {
					ngModel.$validate();
				}
			);
			scope.toggleOpen = function(forceClose){
				ngModel.$setTouched();
				if(!scope.singleSelect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.singleSelect.visible = true;
					element.on('transitionend', toggleOpenTransitionEndHandler);
					$document.bind('mousedown', documentClickBind);
					if (attrs.lockScroll === 'true') {
						lockScroll(true);
					}
				}else{
					scope.singleSelect.visible = false;
					scope.singleSelect.filterText = '';
					element.off('transitionend', toggleOpenTransitionEndHandler);
					$document.unbind('mousedown', documentClickBind);
					lockScroll(false);
				}
			};
			
			function toggleOpenTransitionEndHandler() {
				if (scope.autoFocusDisabled) return;
				element.find('[data-filter]:eq(0)').focus();
			}
			
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.singleSelect.isSmart){
					return;
				}
				if(scope.ngDisabled){
					return;
				}
				if(e.keyCode == 38){//Up key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getPreviousOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 40){//Down key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}else if(e.key && !e.ctrlKey){
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOptionByKey(e.key);
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
							return false;
						}
					});
				}
			});
			
			scope.$watch(function(){
				return scope.singleSelect ? scope.singleSelect.options : null;
			}, function(){
				scope.updateModel(ngModel.$modelValue.model);
				scope.updateSize();
			}, true);
			
			scope.updateSize = function(){
				setTimeout(function(){
					var width;
					if($(element[0]).find('[data-options-box]:eq(0)') && $(element[0]).find('[data-options-box]:eq(0)').length > 0){
						var rect = $(element[0]).find('[data-options-box]:eq(0)')[0].getBoundingClientRect();
						if(rect.width){
							width = rect.width;
						}else{
							width = rect.right - rect.left;
						}
					}
					if(width > 0){
						$(element[0]).width(Math.ceil(width));
					}
				});
			};
			
			scope.updateScroll = function(option){
				setTimeout(function(){
					var optionEl = $(element[0]).find('.single-select-options').find('.option:eq(' + option.index + ')');
					if(optionEl.position().top < 0 || (optionEl.position().top + optionEl.outerHeight() > optionEl.offsetParent().height())){
						optionEl.offsetParent().scrollTop(optionEl.offsetParent().scrollTop() + optionEl.position().top);
					}
					optionEl = null;
				});
			};
		}
	};
}]);

spApp.directive('checkLastCallback', ['$parse', function($parse) {
	return {
		restrict: 'A',
		scope: true,
		link: function(scope, element, attrs) {
			if(attrs.checkLastCallback != ''){
				if(scope.$last === true){
					var invoker = $parse(attrs.checkLastCallback);
					invoker(scope)();
				}
			}
		}
	};
}]);

spApp.directive('convertToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(val) {
				return parseInt(val, 10);
			});
			ngModel.$formatters.push(function(val) {
				return '' + val;
			});
		}
	};
});

spApp.directive('numbersOnly', function() {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			ngModelCtrl.$parsers.push(function(text) {
				if (!text) return '';
				var transformedInput = text.replace(/[^\d]/g, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			});
		}
	};
});

spApp.directive('charsOnly', function() {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			ngModelCtrl.$parsers.push(function(text) {
				if (!text) return '';
				var transformedInput = text.replace(/[^a-z]/gi, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			});
		}
	};
});

spApp.directive('unselectable', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
		}
	};
}]);

spApp.directive('ngSrcNoCache', function() {
	return {
		priority: 99,
		link: function(scope, element, attrs) {
			attrs.$observe('ngSrcNoCache', function(ngSrcNoCache) {
				var prefix = '?';
				if(ngSrcNoCache.indexOf('?') != -1){
					prefix = '&';
				}
				ngSrcNoCache += prefix + (new Date()).getTime();
				attrs.$set('src', ngSrcNoCache);
			});
		}
	}
});

spApp.directive('toggableOn', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-on', tAttrs.toggableOn);
		}
	}
}]);
spApp.directive('toggableOff', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-off', tAttrs.toggableOff);
		}
	}
}]);
spApp.directive('toggler', ['$timeout', function($timeout) {
	return {
		restrict: 'E',
		template: '<span></span>',
		scope: {
			onToggle: '='
		},
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element).on('click', function(){
				if($(element).data('isOn')){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
				}else{
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
				}
				toggleTogglerState();
			});
			
			function setDefaultState(){
				if(attrs.defaultState == 'on'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					toggleTogglerState({forceOn: true, preventOnToggle: true});
				}else if(attrs.defaultState == 'off'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					toggleTogglerState({forceOn: false, preventOnToggle: true});
				}
			};
			
			setDefaultState();
			
			function toggle(el, isOn){
				if(isOn){
					$(el).css('display', '');
				}else{
					$(el).css('display', 'none');
				}
			}
			function toggleTogglerState(params){
				if(!params){
					params = {};
				}
				var isOn = $(element).data('isOn');
				if(typeof params.forceOn == 'undefined' && isOn || typeof params.forceOn != 'undefined' && !params.forceOn){
					$(element).removeClass('block-toggler-on');
					$(element).data('isOn', false);
				}else{
					$(element).addClass('block-toggler-on');
					$(element).data('isOn', true);
				}
				if(scope.onToggle && !params.preventOnToggle){
					scope.$apply(function(){
						scope.onToggle(attrs.toggle, !isOn);
					});
				}
			}
		}
	};
}]);

spApp.directive('onKeydown', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var onKeydown = scope.$eval(attrs.onKeydown);
			$(element[0]).on('keydown', function(e){
				for(key in onKeydown){
					if(onKeydown.hasOwnProperty(key)){
						if(e.keyCode == key){
							var func = onKeydown[key];
							scope.$eval(func);
						}
					}
				}
			});
		}
	}
}]);

spApp.directive('onEnter', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('keydown', function(e){
				if(e.keyCode == 13){
					scope.$eval(attrs.onEnter);
				}
			});
		}
	}
}]);


//This directive is used to force UTC for the datepicker (otherwise there are problems with daylight saving time and wrong day)
spApp.directive('datepickerTimezone', function () {
	return {
		restrict: 'A',
		priority: 1,
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {
			ctrl.$formatters.push(function (value) {
				var date = new Date(Date.parse(value));
				date = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
				//if (!dateFormat || !modelValue) return "";
				//var retVal = moment(modelValue).format(dateFormat);
				return date;
			});

			ctrl.$parsers.push(function (value) {
				var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
				return date;
			});
		}
	};
});



spApp.directive('smartPlaceholder', ['$compile', function($compile) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
			var posTopSmallDefault = scope.isMobile ? 0 : '3px';
			//Wrap the element
			var wrapper = angular.element('<div class="smart-placeholder-wrapper"><div class="smart-placeholder-text">{{placeholderText}}</div><i></i></div>');
			var smartPlaceholderText = wrapper.find('.smart-placeholder-text');
			$compile(wrapper)(scope);
			element.after(wrapper);
			wrapper.find('i').before(element);
			
			var posTopSmall = posTopSmallDefault;
			
			//Watch for changes
			scope.$watch(function(){return scope.$eval(attrs.smartPlaceholder);}, function(newValue){
				smartPlaceholderText.text(newValue);
			});
			scope.$watch(function(){return scope.$eval(attrs.placeholderTop);}, function(top){
				posTopSmall = top ? top : posTopSmallDefault;
			});
			
			scope.$watch(function(){return element.val() ? true : false;}, function(forceClear){
				togglePlaceholder(forceClear);
			});			
			
			//Handle focus/blur
			element.on('focus', function(e){
				togglePlaceholder(true);
				$(wrapper).addClass('focused');
			});
			element.on('blur', function(e){
				togglePlaceholder();
				$(wrapper).removeClass('focused');
			});

			function togglePlaceholder(forceClear){
				setTimeout(function(){
					var clear = false;
					if(forceClear){
						clear = true;
					}else{
						if(element[0].value == ''){
							clear = false;
						}else{
							clear = true;
						}
					}
					if(clear){
						$(wrapper).find('.smart-placeholder-text').addClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTopSmall);
					}else{
						var posTop = parseInt(element.css('padding-top')) + parseInt(element.css('margin-top'));
						$(wrapper).find('.smart-placeholder-text').removeClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTop);
					}
				}, 0);
			}
			
			//Cleanup
			scope.$on("$destroy", function() {
				wrapper.after(element);
				wrapper.remove();
			});
		}
	};
}]);

spApp.directive('credentials', [function() {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'credentials.html',
		scope: {
			viewState: '=',
			hideLoginHeader: '=',
			showTitles: '=',
			afterLogin: '='
		},
		controller: 'CredentialsCtr'
	};
}]);

spApp.directive('mathjaxBind', function() {
	return {
		restrict: 'A',
		controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			$element.addClass('loading');
			$scope.$watch($attrs.mathjaxBind, function(value) {
				var $script = angular.element('<script type="math/tex; mode=display">').html(value == undefined ? '' : value);
				$element.html('');
				$element.append($script);
				MathJax.Hub.Queue(['Reprocess', MathJax.Hub, $element[0]], [removeLoading, $element[0]]);
				function removeLoading(el){
					$(el).removeClass('loading');
				}
			});
		}]
	};
});

spApp.constant('ScrollTop', function() {
	$('html, body').stop().animate({scrollTop: 0}, 500, 'swing');
});
spApp.directive('scrollUpArrow', ['$window', 'ScrollTop', function($window, ScrollTop) {
	return {
		restrict: 'E',
		link: function(scope, element, attrs, ngModel) {
			$(window).on('scroll resize', $.throttle(50, function(e) {
				if($(window).scrollTop() > 0){
					$(element[0]).addClass('visible');
				}else{
					$(element[0]).removeClass('visible');
				}
			}));
			$(element[0]).on('click', ScrollTop);
		}
	};
}]);

spApp.directive('animateHashScrolls', ['$window', function($window) {
	return {
		restrict: 'A',
		priority: 100,
		link: function(scope, element, attrs, ngModel) {
			//scope.$watch(function(){return element[0].childNodes.length;}, function(newValue, oldValue){
			scope.$watch(function(){return element[0].innerHTML;}, function(newValue, oldValue){
				//if(newValue !== oldValue){
					element.find('a').each(function(index, el){
						//if($(el).attr('href').substring(0, 1) == '#'){
						if($(el).attr('href').indexOf('#') != -1){
							var hashArr = $(el).attr('href').split('#');
							var link = hashArr[hashArr.length - 1];
							$(el).on('click', function(e){
								e.preventDefault();
								var target = document.getElementById(link);
								if(target){
									$('html, body').stop().animate({scrollTop:$(target).offset().top - 100}, '500', 'swing');
								}
								target = null;
								return false;
							});
						}
					});
				//}
			});
		}
	};
}]);

spApp.directive('currentTime', ['dateFilter', function(dateFilter) {
	return function(scope, element, attrs) {
		var format = 'HH:mm dd/MM/y';
		var stopTime = setInterval(updateTime, 1000);
		
		function updateTime() {
			element.text(dateFilter(new Date(), format));
		}
		
		element.on('$destroy', function() {
			clearInterval(stopTime);
		});
	}
}]);

spApp.directive('fitToParentFont', ['$window', function($window) {
	return function(scope, element, attrs) {
		var fontTarget = null;
		var dataFitToParentFontTargetElement = $(element).find('[data-fit-to-parent-font-target]')[0];
		if(dataFitToParentFontTargetElement){
			fontTarget = dataFitToParentFontTargetElement;
		}
		scope.$watch(function(){return element[0].innerHTML.length;}, function(){
			fixFontSize(element[0], fontTarget);
			setTimeout(function(){
				fixFontSize(element[0], fontTarget);
			});
		});
		
		$window.addEventListener('resize', resizeHandler, true);
		scope.$on('$destroy', function(){
			$window.removeEventListener('resize', resizeHandler, true);
		});
		
		function resizeHandler(e){
			fixFontSize(element[0], fontTarget);
		}
	}
}]);

//<div click-outside="currentCtrl.actionFunction()">Some content</div>
spApp.directive('clickOutside', ['$document', function($document) {
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, el, attrs) {
			$document[0].addEventListener('click', clickOutsideHandler, true);
			scope.$on('$destroy', function(){
				$document[0].removeEventListener('click', clickOutsideHandler, true);
			});

			function clickOutsideHandler(e) {
				if (el !== e.target && !el[0].contains(e.target)) {
					scope.$applyAsync(function () {
						scope.$eval(attrs.clickOutside);
					});
				}
			}
		}
	};
}]);

spApp.directive('makeVisibleWhenInViewport', ['scrollService', function(scrollService) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs) {
			var $el = $(el);
			var timeoutId;
			var timeoutIteration = 0;
			var timeoutMaxIteration = 30;

			function makeElementVisible() {
				if (isElementInViewport(el)) {
					$el.addClass('visible');
					//After element become visible no need to watch if it in the Viewport
					scrollService.unsubscribe(makeElementVisible);
					clearTimeout(timeoutId);
				} else if (timeoutIteration < timeoutMaxIteration) {
					timeoutIteration++;
					timeoutId = setTimeout(makeElementVisible, 400);
				}
			}
			
			scrollService.subscribe(makeElementVisible);

			//After element apperas we should check if it in the Viewport
			setTimeout(makeElementVisible);
			
			scope.$on('$destroy', function() {
				scrollService.unsubscribe(makeElementVisible);
				clearTimeout(timeoutId);
			});
		}
	};
}]);

spApp.directive('centerOnScreen', ['$window', function($window) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs) {
			if(scope.$eval(attrs.centerOnScreen)){
				var $el = $(el);
				function centerOnScreen(){
					$el.css('right', 'auto');
					$el.css('left', '0px');
					setTimeout(function(){
						var offset = $el.offset();
						var width = $el.outerWidth();
						var screenWidth = $(window).width();
						
						var leftPos = (screenWidth - width)/2 - offset.left;
						
						$el.css('left', leftPos);
					}, 0);
				}
				centerOnScreen();
				
				$window.addEventListener('resize', resizeHandler, true);
				scope.$on('$destroy', function(){
					$window.removeEventListener('resize', resizeHandler, true);
				});
				
				function resizeHandler(e){
					centerOnScreen();
				}
			}
		}
	};
}]);

spApp.directive('remainingTime', ['$rootScope', function($rootScope) {
	return {
		restrict: 'A',
		scope: {
			remainingTime: '='
		},
		link: function (scope, el) {
			var $el = $(el);
			
			//Show remaining time immediately
			$el.text(formatRemainingTime(scope.remainingTime));
			
			//Update remaining time every second
			var intervalId = setInterval(function() {
				$el.text(formatRemainingTime(scope.remainingTime));
			}, 1000);
			
			scope.$on('$destroy', function() {
				clearInterval(intervalId);
			});
			
			function formatRemainingTime(subscriptionEndDate){
				//Consider using this module for timers: https://github.com/siddii/angular-timer
				var current = new Date();
				var end = new Date(subscriptionEndDate);
				var diff = new Date(end - current);
				
				var offset = diff.getTimezoneOffset()/60
				
				diff.setHours(diff.getHours() + offset);
				
				if((end - current) < 0){
					return '';
				}
				
				var days = 0;
				for(var i = 0; i <= end.getUTCFullYear() - current.getUTCFullYear(); i++){
					for(j = 0; j < 12; j++){
						if(i == 0 && j < current.getUTCMonth()){
							continue;
						}
						if(i == (end.getUTCFullYear() - current.getUTCFullYear()) && j > end.getUTCMonth()){
							break;
						}
						days += new Date(current.getUTCFullYear() + i, j + 1, 0).getDate();
					}
				}
				days -= current.getUTCDate();
				days -= new Date(end.getUTCFullYear(), end.getUTCMonth() + 1, 0).getDate() - end.getUTCDate() + 1;
				return (days > 0 ? days + 'd' + ' ' : '') + $rootScope.dateFillToTwoDigits(diff.getHours()) + ':' + $rootScope.dateFillToTwoDigits(diff.getMinutes()) + ':' + $rootScope.dateFillToTwoDigits(diff.getSeconds());
			}
		}
	};
}]);

spApp.directive('appendToBody', [function() {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs){
			var appendToBodyClass = 'append-to-body-wrapper';
			var parent = null;
			var originalParent = el.parent()[0];
			var next = el.next()[0];
			
			scope.$watch(function(){return attrs.appendToBody == '' || scope.$eval(attrs.appendToBody);}, function(newValue, oldValue){
				if(newValue){
					append();
				}else{
					cleanup();
				}
			});
			
			function append(){
				parent = document.createElement('div');
				parent.className = appendToBodyClass;
				$('body').append(parent);
				$(parent).append(el);
			}
			
			function cleanup(){
				if(parent){
					if(next){
						el.insertBefore(next);
					}else if(originalParent){
						$(originalParent).append(el);
					}else{
						el.remove();
					}
					parent.remove();
				}
			}
			
			scope.$on('$destroy', function() {
				cleanup();
			});
		}
	};
}]);

spApp.directive('autoScrollLeft', [function() {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, $parent, attrs) {
			var childSelector = attrs.autoScrollLeft || 'li';
			
			setTimeout(function () {
				var activeClass = attrs.autoScrollLeftActiveElementSelector || '.active';
				var $activeChildrenElement = $parent.find(activeClass);
				autoScroll($activeChildrenElement);
			});
			
			$parent.on('click', childSelector, function() {
				var $children = $(this);
				autoScroll($children);
			});
			
			function autoScroll($children) {
				if (!$children.length) {
					return;
				}
				var childLeftPos = $children.offset().left;
				var childWidth = $children.outerWidth();
				var parentWidth = $parent.outerWidth();
				var parentScroll = $parent.scrollLeft();
				
				var scrollLeft = parentScroll + childLeftPos + childWidth / 2 - parentWidth / 2;
				
				$parent.animate({scrollLeft: scrollLeft}, 750);
			}
		}
	};
}]);

spApp.directive('ngParallax', ['$window', function ($window) {
	return {
		restrict: 'AE',
		scope:{
			pattern: '=',
			speed: '='
		},
		link: function(scope, elememnt) {
			var el = elememnt[0];
			el.style.backgroundRepeat = "no-repeat";
			el.style.height = "100%";
			el.style.margin = "0 auto";
			el.style.position = "relative";
			el.style.background = "url(" + scope.pattern + ")";
			// el.style.backgroundAttachment = 'fixed';
			
			execute();
			
			$window.addEventListener("scroll", execute);
			scope.$on('destroy', function() {
				$window.removeEventListener("scroll", execute);
			});
			
			function execute() {
				var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
				var speed = (scrollTop / scope.speed);
				console.log(speed);
				if(speed == 0){
					el.style.backgroundPosition = 'center '+ 0 + '%';
				} else {
					el.style.backgroundPosition = 'center '+ (0 + speed) + '%';
					// el.style.transform = 'translateY(-'+ scrollTop / 20 +'px)';
				}
			}
		},
	};
}]);

spApp.directive('rzSliderDecorator', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var rzSliderOptions = scope.$eval(attrs.rzSliderOptions);
			var barrierValue = rzSliderOptions && rzSliderOptions.ticksArray && rzSliderOptions.ticksArray[0];
			setTimeout(function() {
				var $barrierAreaHolder = element.find('.rz-tick').last();
				if (!$barrierAreaHolder.length) {
					return;
				}
				var barrierAreaHolderRight = parseInt($barrierAreaHolder.css('transform').split(/[()]/)[1].split(', ')[4]);
				var marginLeft = parseInt($barrierAreaHolder.css('margin-left'));
				var $barrierArea = $('<div class="barrier-area"></div>');
				if (barrierValue < 0) {
					var barrierAreaWidth = barrierAreaHolderRight + marginLeft;
					$barrierArea.addClass('negativ').css('width', barrierAreaWidth);
				}
				var $barrierOccuredLabel = $('<div class="barrier-occured-label"></div>');
				$barrierAreaHolder
					.addClass('barrier-area-holder')
					.css('right', barrierAreaHolderRight)
					.prepend($barrierArea)
					.children('.rz-tick-value')
					.append($barrierOccuredLabel);
					
				attrs.$observe('rzSliderDecoratorBarrierOccuredLabel', function(newValue) {
					$barrierOccuredLabel.text(scope.$eval(newValue));
				});
			});
		}
	};
}]);

spApp.directive('scrollElementTop', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var additionalOffset = parseInt(attrs.scrollElementTopExtra) || 0;
			element.on('click', function() {
				var offset = additionalOffset
				if (scope.isMobile) {
					offset = additionalOffset * $(window).outerWidth() / 640;
				}
				var menuEl = document.querySelector('.menu');
				var menuBottom = menuEl.offsetTop + menuEl.offsetHeight;
				$('body, html').animate({
					scrollTop: element.offset().top + element.outerHeight() - menuBottom + offset
				}, 500);
			});
		}
	};
}]);

spApp.directive('bindHeight', ['makeSameHeight', function (makeSameHeight) {
	return {
		restrict: 'A',
		link: function ($scope, element) {
			// makeSameHeight();
			element.on('DOMSubtreeModified', $.debounce(10, makeSameHeight));
		}
	};
}]);

spApp.constant('makeSameHeight', function () {
	$('[bind-height]').each(function () {
		doForThisElem(this);
	});
	function doForThisElem(me) {
		var value = $(me).attr('bind-height');
		var elems = $('[bind-height="' + value + '"]');
		var heights = elems.toArray().map(function (elem) { return $(elem).height(); });
		if (Math.max.apply(me, heights) > Math.min.apply(me, heights)) $(me).height(Math.max.apply(me, heights));
	}
});

spApp.directive('basicsGlossary', function() {
	return {
		restrict: 'E',
		scope: true,
		templateUrl: folderPrefix + 'basics-glossary.html',
		controller: ['$rootScope', function($rootScope) {
			var ctrl = this;
			
			ctrl.getMsgs = $rootScope.getMsgs;
			ctrl.alphabet = [];
			ctrl.glossaryList = [];
			
			var defaultLimit = $rootScope.isMobile ? 10 : null;
			
			ctrl.visibleDefinitions = {
				increment: 5,
				limit: defaultLimit
			};
			
			$rootScope.$watch("getMsgs('glossary-content')", function(glossary) {
				$(glossary).find('li').each(function(index) {
					var $li = $(this);
					var title = $li.find('strong').text();
					var description = $li.find('span').text();
					ctrl.glossaryList.push({
						id: index,
						title: title,
						description: description
					});
				});
			});
			
			$rootScope.$watch("getMsgs('alphabet-list')", function(alphabet) {
				if (/^\?.*\?$/i.test(alphabet)) return;
				ctrl.alphabet = alphabet.split('');
			});
			
			$rootScope.$watch('isMobile', function(isMobile) {
				ctrl.isMobile = isMobile;
			});
			
			ctrl.activeLetter = '';
			ctrl.toggleActiveLetter = function(letter) {
				if (ctrl.activeLetter === letter) {
					ctrl.activeLetter = '';
				} else {
					ctrl.activeLetter = letter;
				}
				ctrl.visibleDefinitions.limit = defaultLimit;
			};
			
			ctrl.startsWithActiveLetter = function(actual) {
				var lowerStr = actual.title.toLowerCase();
				return lowerStr.indexOf(ctrl.activeLetter) === 0;
			};
			
			ctrl.loadMore = function() {
				if (ctrl.visibleDefinitions.limit >= ctrl.glossaryList.length) return;
				ctrl.visibleDefinitions.limit += ctrl.visibleDefinitions.increment;
			};
		}],
		controllerAs: '$ctrl'
	};
});

spApp.directive('fitMobileCharts', function() {
	return {
		restrict: 'A',
		scope: false,
		link: {
			post: function(scope, element) {
				if (scope.isMobile) {
					setTimeout(setZoom);
					$(window).on('resize', setZoom);
					scope.$on('$destroy', function() {
						$(window).off('resize', setZoom);
					});
				}
				
				function setZoom() {
					var svgWidth = element.width();
					var maxWidth = element.parent().width();
					var ratio = maxWidth / svgWidth;
					element.css('zoom', ratio);
				}
			}
		}
	};
});

//Class definitions
spApp.service('Permissions', [function(){
	var Permissions = function(permissions){
		var rawPermissions = cloneObj(permissions);
		
		var parsePermissions = function(tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					parsePermissions(tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					parsePermissions(tree[i], list);
				}
			}else{
				list.push({id: tree.id, allowed: false});
			}
			
			return list;
		}
		
		this.permissionsList = parsePermissions(rawPermissions);
		rawPermissions = null;
		
		this.get = function(permission){
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permission){
					return this.permissionsList[i].allowed;
				}
			}
			return false;
		}
		
		this.allow = function(permissionId){
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permissionId){
					this.permissionsList[i].allowed = true;
				}
			}
		}
	}
	return {
		InstanceClass: Permissions
	}
}]);


spApp.service('BaseTree', [function(){
	var BaseTree = function(tree, active){
		var that = this;
		
		this.root = cloneObj(tree);
		this.active = (typeof active == "undefined") ? false : active;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					tree[i].parent = parent;
					tree[i].root = that.root;
					if(!tree[i].active){
						tree[i].active = that.active;
					}
					if(!tree[i].collapsed){
						tree[i].collapsed = false;
					}
					if(tree[i].nodes){
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.getTreeTop = function(node){
			if(node.nodes && node.parent){
				return getTreeTop(node.parent);
			}else if(node.parent == null){
				return node;
			}else{
				return null;
			}
		}
	}
	
	BaseTree.toggleNode = function(node){
		node.collapsed = !node.collapsed;
	}
	
	return {
		InstanceClass: BaseTree
	}
}]);

spApp.service('MenuTree', ['BaseTree', function(BaseTree){
	var MenuTree = function(tree, active, permissions){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		this.uncollapseCurrent = function(tree){
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(tree[i].current){
						var node = tree[i].parent;
						while(node != null){
							if(node.hasOwnProperty('collapsed')){
								node.current = true;
								node.collapsed = false;
							}
							node = node.parent;
						}
						parent = null;
					}
					if(tree[i].nodes){
						this.uncollapseCurrent(tree[i].nodes);
					}
				}
			}
		}
		
		this.uncollapseCurrent(this.root);
		
		this.updateMenuNodes = function(validateCallback){
			updateMenuNodesRecursive(this.root, validateCallback);
			this.uncollapseCurrent(this.root);
		}
		
		function updateMenuNodesRecursive(tree, validateCallback){
			for(var i = 0; i < tree.length; i++){
				if(validateCallback(tree[i].link)){
					tree[i].current = true;
				}else{
					tree[i].current = false;
				}
				if(tree[i].nodes){
					tree[i].collapsed = true;
					updateMenuNodesRecursive(tree[i].nodes, validateCallback);
				}else{
					/*if(!permissions.get(tree[i].permission)){
						tree.splice(i, 1);
						i--;
					}*/
				}
			}
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			MenuTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	return {
		InstanceClass: MenuTree
	}
}]);

spApp.service('CheckboxTree', ['BaseTree', function(BaseTree){
	var CheckboxTree = function(tree, active){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(!tree[i].nodes){
						if(!tree[i].checked){
							tree[i].checked = false;
						}
					}else{
						if(!tree[i].checkedStatus){
							tree[i].checkedStatus = 0;
						}
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.normalizeCheckedStatus = function(){
			this.normalizeCheckedStatusRecursive(this.root);
		}
		
		this.normalizeCheckedStatusRecursive = function(tree){
			if(tree.nodes){
				var numLeaves = 0;
				var countUnchecked = 0;
				var countSemiChecked = 0;
				var countChecked = 0;
				for(var i = 0; i < tree.nodes.length; i++){
					if(!tree.nodes[i].nodes){
						numLeaves++;
						if(tree.nodes[i].checked){
							countChecked++;
						}else{
							countUnchecked++;
						}
					}else{
						var temp = this.normalizeCheckedStatusRecursive(tree.nodes[i]);
						if(temp == 2){
							countChecked++;
						}else if(temp == 1){
							countSemiChecked++;
						}else{
							countUnchecked++;
						}
					}
				}
				if(countSemiChecked == 0 && countChecked == 0){
					tree.checkedStatus = 0;
				}else if(countUnchecked == 0 && countSemiChecked == 0){
					tree.checkedStatus = 2;
				}else{
					tree.checkedStatus = 1;
				}
				return tree.checkedStatus;
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.normalizeCheckedStatusRecursive(tree[i]);
				}
			}else{
				return;
			}
		}
		
		this.checkNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = true;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.checkNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.checkNode(tree[i], value);
				}
			}
		}
		
		this.uncheckNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = false;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.uncheckNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.uncheckNode(tree[i], value);
				}
			}
		}
		
		this.getCheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getCheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getCheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
		
		this.getUncheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getUncheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getUncheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(!tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			CheckboxTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	CheckboxTree.updateTree = function(node){
		if(node.active){
			CheckboxTree.updateTreeCheckedDown(node);
			CheckboxTree.updateTreeCheckedUp(node);
		}
	}
	
	CheckboxTree.updateTreeCheckedDown = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(checkedStatus == 0){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else if(checkedStatus == 1){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else{
			CheckboxTree.updateNodeStatusDown(node, 0);
		}
	}
	
	CheckboxTree.updateNodeStatusDown = function(node, status){
		if(node.nodes){
			node.checkedStatus = status;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					node.nodes[i].checkedStatus = status;
					CheckboxTree.updateNodeStatusDown(node.nodes[i], status);
				}else{
					if(!node.nodes[i].changed){
						node.nodes[i].changed = true;
						node.nodes[i].originalChecked = node.nodes[i].checked;
					}
					node.nodes[i].checked = (status == 0) ? false : true;
				}
			}
		}else{
			if(!node.changed){
				node.changed = true;
				node.originalChecked = node.checked;
			}
			node.checked = (status == 0) ? false : true;
		}
	}
	
	CheckboxTree.updateTreeCheckedUp = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(node.parent){
			if(checkedStatus == 0){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else if(checkedStatus == 1){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else{
				CheckboxTree.updateNodeStatusUp(node.parent, 0);
			}
		}
	}
	
	CheckboxTree.updateNodeStatusUp = function(node){
		if(node.nodes){
			var status = 0;
			var countUnchecked = 0;
			var countSemiChecked = 0;
			var countChecked = 0;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					if(node.nodes[i].checkedStatus == 0){
						countUnchecked++;
					}else if(node.nodes[i].checkedStatus == 1){
						countSemiChecked++;
					}else if(node.nodes[i].checkedStatus == 2){
						countChecked++;
					}
				}else{
					if(node.nodes[i].checked){
						countChecked++;
					}else{
						countUnchecked++;
					}
				}
			}
			if(countSemiChecked == 0 && countChecked == 0){
				status = 0;
			}else if(countUnchecked == 0 && countSemiChecked == 0){
				status = 2;
			}else{
				status = 1;
			}
			node.checkedStatus = status;
			if(node.parent){
				CheckboxTree.updateNodeStatusUp(node.parent);
			}
		}
	}
	
	CheckboxTree.getCheckedStatus = function(node){
		var checkedStatus = 0;
		if(node.nodes){
			checkedStatus = node.checkedStatus;
		}else{
			checkedStatus = node.checked ? 2 : 0;
		}
		return checkedStatus;
	}
	
	return {
		InstanceClass: CheckboxTree
	}
}]);


spApp.service('Pagination', [function(){
	var Pagination = function(params){
		var that = this;
		
		this.pages = [];
		this.currentPage = 1;
		this.maxPage = 1;
		this.totalCount = 0;
		this.resultsPerPage = 20;
		
		this.callback = params.callback;
		this.hideSummary = params.hideSummary;
		this.hideGroupNavigation = params.hideGroupNavigation;
		
		this.setPages = function(currentPage, totalCount, resultsPerPage){
			this.currentPage = currentPage;
			this.maxPage = Math.ceil(totalCount/resultsPerPage);
			this.totalCount = totalCount;
			this.resultsPerPage = resultsPerPage;
			this.pages = [];
			for(var i = Math.max(0, currentPage - 4); i < Math.min(this.maxPage, Math.max(0, currentPage - 4) + 10); i++){
				this.pages.push(i+1);
			}
		}
		
		this.validatePage = function(page){
			if(typeof page != 'undefined' && page == this.currentPage){
				return false;
			}
			if(typeof page == 'undefined'){
				page = 1;
			}
			return page;
		}
		
		this.setPages(this.currentPage, this.maxPage);
		
		this.previousPage = function(){
			return Math.max(this.currentPage - 1, 1);
		}
		this.nextPage = function(){
			return Math.min(this.currentPage + 1, this.maxPage);
		}
		this.previousPageGroup = function(){
			return Math.max(this.currentPage - 10, 1);
		}
		this.nextPageGroup = function(){
			return Math.min(this.currentPage + 10, this.maxPage);
		}
	}
	
	return {
		InstanceClass: Pagination
	}
}]);


spApp.service('SingleSelect', ['$timeout', '$rootScope', 'Utils', function($timeout, $rootScope, Utils){
	var SingleSelect = function(params){
		var that = this;
		
		if(!params){
			params = {};
		}
		
		this.visible = false;
		
		this.externalModel = params.externalModel;
		if(this.externalModel){
			if(this.externalModel.bindById !== false){
				this.externalModel.bindById = true;
			}
			if(this.externalModel.bindByIdInt !== false){
				this.externalModel.bindByIdInt = true;
			}
		}
		this.validateExternalModel = function(){
			if(this.externalModel && typeof this.externalModel.objCallback == 'function' && (typeof this.externalModel.objCallback() != 'undefined' && this.externalModel.objCallback() != null)){
				return true;
			}
			return false;
		}
		$timeout(function(){
			$rootScope.$watch(function(){
				return that.validateExternalModel() ? that.externalModel.objCallback()[that.externalModel.key] : '';
			}, function(newValue, oldValue){
				if(!that.validateExternalModel()){
					return;
				}
				if(that.externalModel.bindById){
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model.id){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i].id){
							that.model = (typeof that.model == 'string') ? that.options[i] + '' : that.options[i];
						}
					}
				}else{
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i]){
							that.model = that.options[i];
						}
					}
				}
			}, true);
		});
		
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.sortByNameInt = params.sortByNameInt;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.iconClosed = params.iconClosed;
		this.iconOpen = params.iconOpen;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.filterPlaceholder = params.filterPlaceholder;
		
		this.model = null;
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		
		this.setModel = function(option){
			this.model = option;
			if(this.externalModel && typeof this.externalModel.objCallback == 'function'){
				if(this.externalModel.bindById){
					this.externalModel.objCallback()[this.externalModel.key] = this.externalModel.bindByIdInt ? parseInt(this.model.id) : (this.model.id ? this.model.id : null);
				}else{
					this.externalModel.objCallback()[this.externalModel.key] = this.model;
				}
			}
		}
		
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.options.push(this.aggregateOption);
			this.setModel(this.aggregateOption);
		}
		
		this.fillOptions = function(options, setModelToFirst){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						var name = options[el];
						this.options.push({id: el, name: name});
					}
				}
			}
			if(setModelToFirst && this.options.length > 0){
				this.setModel(this.options[0]);
			}
			this.filter();
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		
		this.filteredOptions = [];
		this.filter = function(){
			that.filteredOptions = that.options.filter(that.filterFunction);
			if(!that.noSort){
				that.filteredOptions.sort(that.sortFunction);
			}
		}
		this.filterFunction = function(option){
			var filterTextMatch = true;
			var includesText = Utils.escapeRegExp(that.filterText);
			var startsWithText = that.filterText ? ('^' + includesText) : includesText;
			var includesRegExp = new RegExp(includesText, 'i');
			var startsWithRegExp = new RegExp(startsWithText, 'i');
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = startsWithRegExp.test(that.mutateCallback(option.name));
			}else{
				filterTextMatch = startsWithRegExp.test(option.name) || includesRegExp.test(option.suffixOptionName);
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(that.model == option){
						//TODO - perhaps find the first option that would not be filtered by filterCallback and set the model to that option
						if(that.aggregateOption){
							that.model = that.aggregateOption;
						}else{
							that.model = null;
						}
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sortFunction = function(a, b){
			if(a == that.aggregateOption){
				return -1;
			}
			if(b == that.aggregateOption){
				return 1;
			}
			if(that.sortById){
				return parseInt(a.id) - parseInt(b.id);
			}
			if(that.sortByNameInt){
				return parseInt(a.name) - parseInt(b.name);
			}
			aName = a.name;
			bName = b.name
			if(that.mutateCallback && (!that.mutateOnlyAggregate || a == that.aggregateOption)){
				aName = that.mutateCallback(aName);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || b == that.aggregateOption)){
				bName = that.mutateCallback(bName);
			}
			aName = aName.trim();
			bName = bName.trim();
			if(aName < bName){
				return -1;
			}else if(aName > bName){
				return 1;
			}else{
				return 0;
			}
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option == that.aggregateOption ? -1000 : parseInt(option.id);
			}
			if(that.sortByNameInt){
				return option == that.aggregateOption ? -1000 : parseInt(option.name);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option == that.aggregateOption)){
				return option == that.aggregateOption ? '' : that.mutateCallback(option.name).trim();
			}
			return option == that.aggregateOption ? '' : option.name.trim();
		}
		
		if(params.options){
			this.fillOptions(params.options);
		}
		
		this.getTextValue = function(){
			if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
				this.model != null ? this.mutateCallback(this.model.name) : '';
			}
			return this.model != null ? this.model.name : '';
		}
		
		this.getId = function(){
			if(!this.model){
				return null;
			}
			return this.model.id;
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
				this.mutateCallback(val.name);
			}
			return val.name;
		}
		
		this.setById = function(id){
			this.setModel(this.getOptionById(id));
		}
		
		this.getPreviousOption = function(){
			if(this.model.index == 0){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index - 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOption = function(){
			if(this.model.index == this.options.length - 1){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index + 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOptionByKey = function(key){
			var firstMatch = null;
			var nextMatch = null;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index != this.model.index){
					var name = this.options[i].name;
					if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
						name = this.mutateCallback(name);
					}
					if(name && name.substring(0, 1).localeCompare(key, undefined, {sensitivity: 'base'}) == 0){
						if(this.options[i].index < this.model.index && (firstMatch == null || firstMatch.index > this.options[i].index)){
							firstMatch = this.options[i];
						}
						if(this.options[i].index > this.model.index && (nextMatch == null || nextMatch.index > this.options[i].index)){
							nextMatch = this.options[i];
						}
					}
				}
			}
			if(nextMatch){
				return nextMatch;
			}else if(firstMatch){
				return firstMatch;
			}
			return null;
		}
		
		this.clone = function(){
			var clone = new SingleSelect(cloneObj(params));
			var options = [];
			var modelIndex = null;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i] == this.model){
					modelIndex = i;
				}
				if(!this.options[i].isAggregate){
					options.push(angular.copy(this.options[i]));
				}
			}
			clone.fillOptions(options);
			if (modelIndex === null && Object(this.model) === this.model) {
				for(var j = 0; j < this.options.length; j++){
					if(this.options[j].id == this.model.id){
						modelIndex = j;
					}
				}
			}
			clone.setModel(clone.options[modelIndex]);
			return clone;
		}
	}
	
	return {
		InstanceClass: SingleSelect
	}
}]);


spApp.service('Multiselect', [function(){
	var Multiselect = function(params){
		var that = this;
		
		this.visible = false;
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.aggregateOption.checked = true;
			this.aggregateOption.visible = true;
			this.options.push(this.aggregateOption);
		}
		
		this.fillOptions = function(options){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					if(!options[i].checked){
						options[i].checked = false;
					}
					if(!options[i].visible){
						options[i].visible = true;
					}
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						this.options.push({id: el, name: options[el], checked: false, visible: true});
					}
				}
			}
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		this.filter = function(option){
			var filterTextMatch = true;
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
			}else{
				filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(option.checked){
						that.toggle(option);
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option.isAggregate ? -1000 : parseInt(option.id);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				return option.isAggregate ? '' : that.mutateCallback(option.name).trim();
			}
			return option.isAggregate ? '' : option.name.trim();
		}
		
		this.toggle = function(option){
			if(option.isAggregate){
				if(option.checked){
					return;
				}
				option.checked = !option.checked;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].isAggregate){
						continue;
					}
					this.options[i].checked = false;
				}
			}else{
				option.checked = !option.checked;
				if(this.aggregateOption){
					this.aggregateOption.checked = false;
				}
				var checkedCount = 0;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].checked){
						checkedCount++;
					}
				}
				if(checkedCount == 0){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
					}
				}else if((!this.aggregateOption && checkedCount == this.options.length) || (this.aggregateOption && checkedCount == this.options.length - 1)){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
						for(i = 0; i < this.options.length; i++){
							if(!this.options[i].isAggregate){
								this.options[i].checked = false;
							}
						}
					}
				}
			}
		}
		
		this.getCheckedIds = function(includeAggregate){
			var arr = [];
			for(var i = 0; i < this.options.length; i++){
				if((includeAggregate || !this.options[i].isAggregate) && this.options[i].checked){
					arr.push(this.options[i].id);
				}
			}
			return arr;
		}
		
		this.setSelectedOptions = function(ids){
			for(var i = 0; i < ids.length; i++){
				var key = -1;
				if((key = searchJsonKeyInArray(this.options, 'id', ids[i])) > -1){
					this.toggle(this.options[key]);
				}
			}
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || val.isAggregate)){
				return this.mutateCallback(val.name);
			}
			return val.name;
		}
	}
	
	return {
		InstanceClass: Multiselect
	}
}]);
spApp.filter('orderByName', function () {
	// custom value function for sorting
	function myValueFunction(card) {
		return card.values.opt1 + card.values.opt2;
	}

	return function (obj) {
		var array = [];
		Object.keys(obj).forEach(function (key) {
			// inject key into each object so we can refer to it from the template
			obj[key].name = key;
			array.push(obj[key]);
		});
		// apply a custom sorting function
		/*array.sort(function (a, b) {
			return b.id == -1 || b.name.localeCompare(a.name);
			//return myValueFunction(b) - myValueFunction(a);
		});*/
		return array;
	};
});

spApp.filter('inArray', function($filter){
	return function(list, value, element){
		if(value){
			return $filter("filter")(list, function(listItem){
				return listItem[element].indexOf(value) != -1;
			});
		}
	};
});

spApp.filter('floor', function() {
	return function(n) {
		return Math.floor(n);
	};
});
/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}


if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	'use strict';
	if (this == null) {
	  throw new TypeError();
	}
	var n, k, t = Object(this),
		len = t.length >>> 0;

	if (len === 0) {
	  return -1;
	}
	n = 0;
	if (arguments.length > 1) {
	  n = Number(arguments[1]);
	  if (n != n) { // shortcut for verifying if it's NaN
		n = 0;
	  } else if (n != 0 && n != Infinity && n != -Infinity) {
		n = (n > 0 || -1) * Math.floor(Math.abs(n));
	  }
	}
	if (n >= len) {
	  return -1;
	}
	for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
	  if (k in t && t[k] === searchElement) {
		return k;
	  }
	}
	return -1;
  };
}

function cloneObj(obj){
	if(obj == null || typeof(obj) != 'object'){
		return obj;
	}
	if(obj instanceof Date){
		return new Date(obj);
	}

	var temp = obj.constructor(); // changed

	for(var key in obj){
		temp[key] = cloneObj(obj[key]);
	}
	return temp;
}

//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

function detectDevice() {
	if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		return 'ios';
	} else if (/Android/i.test(navigator.userAgent)) {
		return 'android';
	} else {
		return '';
	}
}

//use ?logType=num to load page with loggin on
//0:do not log,1:log info,2:log warnings,3:log errors,4:log all
var logIt_type_timer = ''; 
function logIt(params){//type,msg
	if((params.type === 1) && (settings.logIt_type === 1 || settings.logIt_type === 4)){//info
		console.info(params.msg);
	}
	else if((params.type === 2) && (settings.logIt_type === 2 || settings.logIt_type === 4)){//warnings
		console.warn(params.msg);
	}
	else if((params.type == 3) && (settings.logIt_type === 3 || settings.logIt_type === 4)){//errors
		console.error(params.msg);
	}
	// console.log('test.log') - just info
	// console.debug('test.debug') - just info
	// console.info('test.info') - with info ico
	// console.warn('test.warn') - with warning info
	// console.error('test.error') - with error ico
	if(logIt_type_timer == null){ 
		logIt_type_timer = setInterval(function(){console.clear()},1000*60*5);
	}
}

var error_stack = [];
function handleErrors(data, name) {//returns true on error
	angular.element('#spAppHolder').scope().requestsStatus[data.serviceName] = 1;
	if (data == '') {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Response is empty string. Check eclipse console for exeptions.');
		return true;
	} else if (data.errorCode == null) {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Error code is null. Check eclipse console for exeptions. ');
		return true;
	} else if (data.errorCode > errorCodeMap.success) {//0
		var defMsg = angular.element('#spAppHolder').scope().getMsgs('error-' + data.errorCode);
		logIt({'type':3,'msg':name + " (" + defMsg + ")"});
		logIt({'type':3,'msg':data});
		var globalErrorField;
		if (typeof data.globalErrorField != 'undefined') {
			globalErrorField = g(data.globalErrorField);
		} else if (g('globalErrorField') != null) {
			globalErrorField = g('globalErrorField');
		} else {
			globalErrorField = document.createElement('span');
			globalErrorField.id = 'globalErrorField';
			document.body.appendChild(globalErrorField);
		}
		globalErrorField.innerHTML = '';
		resetErrorMsgs(data);
		if (data.errorCode == errorCodeMap.session_expired) { //session expired 6999
			if (isUndefined(data.loginPage)) {
				try {
					/*angular.element('#spAppHolder').scope().changeHeader(false);
					angular.element('#spAppHolder').scope().$state.go('ln.login', {ln: settings.skinLanguage});
					angular.element('#spAppHolder').scope().initSettings();*/
					window.location.hash = '';
					window.location.replace(window.location.pathname.replace(/index.html/, ''));
				} catch(e) {}
			}
		} else if(data.errorCode == errorCodeMap.date_range_too_large){
			addGlobalErrorMsg(angular.element('#spAppHolder').scope().getMsgs('sales.deposits.date.range'));
		} else if(data.errorCode == errorCodeMap.role_name_exists){
			addGlobalErrorMsg(angular.element('#spAppHolder').scope().getMsgs('permissions.role.name.exists'));
		} else {
			if (data.userMessages != null) {
				for (var i = 0; i < data.userMessages.length; i++) {
					if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
						globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
					} else {
						var input = g(data.userMessages[i].field);
						if (input != null) {
							input.className += " error";
						}
						var error_el_id = data.userMessages[i].field + '-error';
						error_stack.push(data.userMessages[i].field);
						var err_place = g(error_el_id);
						if (err_place != null) {
							err_place.innerHTML = data.userMessages[i].message;
						} else {
							var field_place = g(data.userMessages[i].field + '-container');
							if (field_place != null) {
								var el = document.createElement('span');
								el.id = error_el_id;
								el.className = 'error-message icon';
								el.innerHTML = data.userMessages[i].message;
								field_place.appendChild(el);
							} else {
								globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
							}
						}
					}
				}
			} else {
				logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
				globalErrorField.innerHTML += "<span>" + defMsg + "</span>";
			}
		}
		return true;
	} else {
		logIt({'type':1,'msg':data});
		return false;
	}
}

function handleSuccess(data) {
	if (data.userMessages != null && data.userMessages[0].message != null) {
		alert(data.userMessages[0].message);
	} else {
		alert('Great, it works!')
	}
	logIt({'type':1,'msg':data});
}

function handleNetworkError(data) {
	// alert('Network error (aka 404, 500 etc)');
	logIt({'type':1,'msg':data});
}

function resetErrorMsgs(data) {
	if (typeof data.stopClear == 'undefined' || !data.stopClear) {
		for (var i = 0; i < error_stack.length; i++) {
			var error_holder = g(error_stack[i]);
			if (error_holder != null) {
				g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
			}
			var error_holder = g(error_stack[i] + '-error');
			if (error_holder != null) {
				g(error_stack[i] + '-error').remove();
			}
		}
		error_stack = [];
	}
}

function addGlobalErrorMsg(data){
	$('[data-global-errors]').html('<span class="error">' + data + '</span>');
}

function resetGlobalErrorMsg(){
	$('[data-global-errors]').html('');
}

function searchJsonKeyInArray(arr, key, val) {
	if (!isUndefined(arr)) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i][key] == val) {
				return i;
			}
		}
	}
	return -1;
}
function searchJsonKeyInJson(obj, key, val) {
	if (!isUndefined(obj)) {
		for (var el in obj) {
			if (obj[el][key] == val) {
				return el;
			}
		}
	}
	return -1;
}
function formatAmount(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	var amount = params.amount;
	var currency = params.currency;
	var centsPart = params.centsPart;
	var withoutDecimalPoint = params.withoutDecimalPoint;
	var round = params.round;
	try{
		if (!isNaN(amount)) {
			if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
				amount = amount/100;
			}
			var rtn = '';
			if (amount.toString().charAt(0) == '-') {
				rtn = '-';
				amount *= -1;
			}
			if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
				amount = amountToFloat(amount);
			}
			if (typeof round == 'undefined' || !round || amount < 1000 ) {
				amount = Math.round(amount*100)/100;
				var tmp = amount.toString().split('.');
				var decimal = parseInt(tmp[0]);
				var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
				if((cents === '') || (cents === 0)){cents = '00';}
				else if(cents.length == '1'){cents = cents + '0';}
				else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
				
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += numberWithCommas(decimal);
				if(currency.decimalPointDigits == 0){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
					rtn += "<span>"+cents+"</span>";
				}
				else if((centsPart == 3) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 2) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else{
					rtn += "."+cents;
				}
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			} else {
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += (Math.round(amount / 100)) / 10 + "K";
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			}
			return rtn;
		} else {
			return ' ';
		}
	} catch(e) {
		logIt({'type':3,'msg':e});
	}
}

function numberWithCommas(x) {//convert value with camas every 1000
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
	var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}

function checkOwnProperty(obj, property){
	if(obj.hasOwnProperty(property) && obj[property]){
		return true;
	}else{
		return false;
	}
}

function showLoading(globalOverlay){
	$('.content').addClass('loading');
	if(globalOverlay){
		$('.content').addClass('loading-global');
	}
}
function hideLoading(){
	$('.content').removeClass('loading');
	$('.content').removeClass('loading-global');
}

function showToast(type, message){
	var toast = document.createElement('div');
	toast.className = 'toast';
	if(type == TOAST_TYPES.error){
		toast.className += ' toast_error';
	}
	toast.innerHTML = message;
	toast.onclick = function(){removeToast(toast, true);};
	document.body.appendChild(toast);
	setTimeout(function(){removeToast(toast, false);}, 3000);
}
function removeToast(toast, forceClose){
	var removeDelay = 0.5;
	if(toast && !toast.isBeingRemoved){
		toast.isBeingRemoved = true;
		$(toast).css('visibility', 'hidden');
		$(toast).css('opacity', '0');
		$(toast).css('transition', 'visibility 0s ' + removeDelay + 's, opacity ' + removeDelay + 's linear');
		$(toast).on('transitionend msTransitionEnd webkitTransitionEnd',function(){
			if(toast){
				toast.parentNode.removeChild(toast);
				toast = null;
			}
		});
		//Backup for browsers where transitionend didn't fire
		setTimeout(function(){
			if(toast){
				if(toast.parentNode){
					toast.parentNode.removeChild(toast);
				}
				toast = null;
			}
		}, (removeDelay + 1)*1000);
	}else if(toast && toast.isBeingRemoved && forceClose){
		toast.parentNode.removeChild(toast);
		toast = null;
	}
}

function setCaretPosition(el, caretPos) {
	el.value = el.value;
	// ^ this is used to not only get "focus", but
	// to make sure we don't have it everything -selected-
	// (it causes an issue in chrome, and having it doesn't hurt any other browser)
	
	if(el !== null){
		if (el.createTextRange) {
			var range = el.createTextRange();
			range.move('character', caretPos);
			range.select();
			return true;
		}else{
			// (el.selectionStart === 0 added for Firefox bug)
			if (el.selectionStart || el.selectionStart === 0) {
				el.focus();
				el.setSelectionRange(caretPos, caretPos);
				return true;
			}else{ // fail city, fortunately this never happens (as far as I've tested) :)
				el.focus();
				return false;
			}
		}
	}
}

function findFirstDiffPos(a, b) {
	var i = 0;
	if (a === b) return -1;
	while (a[i] === b[i]) i++;
	return i;
}

function round(number, precision) {
	var factor = Math.pow(10, precision);
	var tempNumber = number * factor;
	var roundedTempNumber = Math.round(tempNumber);
	return roundedTempNumber / factor;
};

function isElementInViewport(el, full){
	//special bonus for those using jQuery
	if(typeof jQuery === "function" && el instanceof jQuery){
		el = el[0];
	}
	
	var rect = el.getBoundingClientRect();
	
	if(full){
		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
			rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
		);
	}
	return (
		((rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)) && (rect.bottom >= 0)) || 
		((rect.top <= (window.innerHeight || document.documentElement.clientHeight)) && (rect.top >= 0)) || 
		(rect.top <= 0 && (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight)))
	);

}

function fixFontSize(el, fontTarget){
	if(!fontTarget){
		fontTarget = el;
	}
	$(fontTarget).css('font-size', '');
	
	var iterationsLimit = 100;
	var iterations = 0;
	var maxHeight = $(el.parentNode).height();
	var maxWidth = 0;
	var $el = $(el);
	if(parseInt($el.css('max-height')) > 0){
		maxHeight = parseInt($el.css('max-height'));
	}else if(parseInt($(el.parentNode).css('max-height')) > 0){
		maxHeight = parseInt($(el.parentNode).css('max-height'));
		if($(el.parentNode).css('box-sizing') == 'border-box'){
			maxHeight = maxHeight - parseInt($(el.parentNode).css('padding-top')) - parseInt($(el.parentNode).css('padding-bottom'));
		}
	}
	if(parseInt($el.css('max-width')) > 0){
		var elMaxWidth = $el.css('max-width');
		if(elMaxWidth && elMaxWidth[elMaxWidth.length - 1] == '%'){
			elMaxWidth = (parseInt(elMaxWidth)/100)*$(el.parentNode).width();
		}
		maxWidth = parseInt(elMaxWidth);
	}else if(parseInt($(el.parentNode).css('max-width')) > 0){
		maxWidth = parseInt($(el.parentNode).css('max-width'));
		if($(el.parentNode).css('box-sizing') == 'border-box'){
			maxWidth = maxWidth - parseInt($(el.parentNode).css('padding-left')) - parseInt($(el.parentNode).css('padding-right'));
		}
	}
	
	while($el.outerHeight() > maxHeight){
		iterations++;
		if(iterations > iterationsLimit){
			break;
		}
		$(fontTarget).css('font-size', parseInt($(fontTarget).css('font-size')) - 1);
	}
	if(maxWidth){
		while($el.outerWidth() > maxWidth || $el[0].scrollWidth > maxWidth){
			iterations++;
			if(iterations > iterationsLimit){
				break;
			}
			$(fontTarget).css('font-size', parseInt($(fontTarget).css('font-size')) - 1);
		}
	}
	if($el.hasClass('invisible')){
		$el.addClass('visible');
	}
}

function setParalax(params){
	//Init constants
	var container = params.container || '';
	var imageRatio = params.imageRatio || (!angular.isUndefined(params.imageRatio) ? 0 : 0.8);
	var imageWidth = params.imageWidth || (!angular.isUndefined(params.imageWidth) ? 0 : 0);
	var imageHeight = params.imageHeight || (!angular.isUndefined(params.imageHeight) ? 0 : 0);
	var imageMinHeight = params.imageMinHeight || (!angular.isUndefined(params.imageMinHeight) ? 0 : 0);
	var imageStaticOffset = params.imageStaticOffset || (!angular.isUndefined(params.imageStaticOffset) ? 0 : 0);
	var textRatio = params.textRatio || (!angular.isUndefined(params.textRatio) ? 0 : 0.66);
	
	var $container = $(container);
	
	if(!$container[0] || !isElementInViewport($container[0])){
		return;
	}
	
	var imageDelta = 0;
	var textDelta = 0;
	
	var top = $container.offset().top;
	var windowHeight = $(window).height();
	var width = parseInt($container.css('width'));
	var height = parseInt($container.css('height'));
	if(imageWidth == 0){
		imageWidth = width;
	}
	if(imageHeight == 0){
		imageHeight = height;
	}
	//Resize image
	var backgroundImageWidth = 'auto';
	var backgroundImageHeight = 'auto';
	var finalImageWidth = 0;
	var finalImageHeight = 0;
	if(imageWidth/imageHeight > width/height){
		finalImageHeight = height;
		if((imageMinHeight > 0) && (imageMinHeight > finalImageHeight)){
			finalImageHeight = imageMinHeight;
		}
		var finalRatio = (finalImageHeight/imageHeight);
		finalImageWidth = finalRatio*imageWidth;
		imageStaticOffset = finalRatio*imageStaticOffset;
		backgroundImageHeight = (100*(finalImageHeight/height)) + '%';
	}else{
		backgroundImageWidth = '100%';
		finalImageWidth = width;
		finalImageHeight = (width/imageWidth)*imageHeight;
		imageStaticOffset = (width/imageWidth)*imageStaticOffset;
	}
	if(finalImageHeight - height < (-1)*imageStaticOffset){
		imageStaticOffset = (-1)*(finalImageHeight - height);
	}
	//Calculate deltas
	var startPos = top > windowHeight ? top - windowHeight : 0;
	var scrollTop = $(document).scrollTop() - startPos;
	var maxScroll = height + startPos;
	
	if(params.doubleMovement){
		var startOffset = (height - finalImageHeight);
		var diff = finalImageHeight - height;
		var totalMovement = top < windowHeight ? top + height : windowHeight + height;
		var ratio = diff/totalMovement;
		imageDelta = ratio*scrollTop + startOffset;
		textDelta = textRatio*scrollTop;
	}else{
		var startOffset = 0;
		if(scrollTop == 0){
			//chill
		}else if(scrollTop > maxScroll){
			imageDelta = imageRatio*maxScroll;
			textDelta = textRatio*maxScroll;
		}else{
			imageDelta = imageRatio*scrollTop;
			textDelta = textRatio*scrollTop;
		}
		imageDelta += imageRatio*startOffset;
	}
	imageDelta += imageStaticOffset;
	$container.css({
		// 'background-size': backgroundImageWidth + ' ' + backgroundImageHeight,
		'background-attachment': 'fixed',
		'background-position': 'center ' + (imageDelta / -3) + 'px'
	});
	var $textParalax = $container.find('.text-paralax');
	// if (!$textParalax.length) {
	// 	$textParalax = $container.find('span');
	// }
	$textParalax.css({'transform': 'translateY(' + textDelta + 'px)'});
	//Set text opacity
	if(($(document).scrollTop() - top) > 0){
		$textParalax.css({'opacity': Math.max(0, 1 - ($(document).scrollTop() - top)/height)});
	}else{
		$textParalax.css({'opacity': ''});
	}
}
var menu = [{
	title: 'home',
	link: 'ln.index',
	onlyMobile: true
},{
	title: 'all-notes',
	link: 'ln.products'
},{
	title: 'open-account',
	titleLogged: 'my-account',
	link: 'ln.register',
	linkLogged: 'ln.my-account',
	isMyAccount: true
},{
	title: 'about',
	link: 'ln.about'
},{
	title: 'faq',
	link: 'ln.faq'
},{
	title: 'basics',
	link: 'ln.basics',
	nodes: [
		{
			title: 'the-basics',
			link: 'ln.basics',
			linkParams: {'id': 'basics-the-basics'}
		},
		{
			title: 'basics-product-categories-title',
			link: 'ln.basics',
			linkParams: {'id': 'product-categories'}
		},
		{
			title: 'basics-profit-profile-diagram-title',
			link: 'ln.basics',
			linkParams: {'id': 'profit-profile-diagram'}
		},
		{
			title: 'glossary',
			link: 'ln.basics',
			linkParams: {'id': 'basics-glossary'}
		},
		{
			title: 'underlying-assets',
			link: 'ln.basics',
			linkParams: {'id': 'basics-underlying-assets'}
		},
		{
			title: 'articles',
			link: 'ln.basics',
			linkParams: {'id': 'basics-articles'}
		},
	],
},{
	title: 'contact-us',
	link: 'ln.contact-us'
}];
var settings = {
	jsonLink: 'anycapital',
	urlPrefix: 'anycapital',
	domain: window.location.host,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	skinId: 2,
	skinIdTexts: 2,
	skinLanguage: 'en',
	documentSizeLimit: 15,//MB
	loggedIn: false,
	serverTime: 0,
	serverOffsetMillis: 0,
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 2,
	loginDetected: false,
	url_params: window.location.search,
	sessionTimeout: (1000*60*31),//31 min
	recaptchaKey:''
}

settings.currencies = {
	2: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.usd',
		currencyLeftSymbol: true,
		currencyName: 'currencies.usd'
	},
	3: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.eur',
		currencyLeftSymbol: true,
		currencyName: 'currencies.eur'
	}
};

settings.CURRENCY_USD = 2;
settings.CURRENCY_EUR = 3;

settings.actionSources = {
	WEB: {
		id: 1,
		name: 'WEB'
	},
	BACKEND: {
		id: 2,
		name: 'BACKEND'
	},
	JOB: {
		id: 3,
		name: 'JOB'
	}
}

settings.productStatuses = {
	SUBSCRIPTION: 2,
	PENDING_SETTLEMENT: 4
}

settings.investmentStatuses = {
	PENDING: 1,
	OPEN: 2,
	SETTLED: 3,
	CANCELED: 4,
	CANCELED_INSUFFICIENT_FUNDS: 5
}

settings.productBarrierTypes = {
	American: 1,
	European: 2
}

settings.marketGroups = {
	Indices: 1,
	Shares: 2,
	Commodities: 3,
	Rates: 4,
	Currencies: 5
}

settings.credentialsView = {
	NONE: 0,
	LOGIN: 1,
	FORGOT_PASSWORD: 2,
	RESET_SUBMIT: 4,
	RESET_FAILED: 5,
	RESET_APPROVED: 6
}

settings.translationParams = {
	'siginificant-risks-link': 'significantRisks',
	'risk-factors-link': 'riskFactors'
}

settings.paymentTypes = {
		BANK_WIRE: {
			id:1,
			name:'BANK_WIRE'
		},ADMIN: {
			id:3,
			name:'ADMIN'
		}
}

settings.contactUsIssues = {
	GENERAL: {
		id: 1,
		name: 'general'
	},
	INVESTMENTS: {
		id: 2,
		name: 'investments'
	},
	TECHNICAL_ISSUES: {
		id: 3,
		name: 'technical-issues'
	},
	REGISTRATION: {
		id: 4,
		name: 'registration'
	},
	DEPOSITS_WITHDRAWALS: {
		id: 5,
		name: 'deposits-withdrawals'
	}
}

var PAGE_LOGIN_ACCESS_TYPES = {
	any: 0,
	loggedOut: 1,
	loggedIn: 2
}

var errorCodeMap = {
	success: 0,
	withdraw_minimum_amount_limitation: 511,
	access_denied: 601,
	authentication_failure: 602,
	account_locked: 605,
	invalid_input: 701,
	invalid_password: 705,
	investment_general_error: 710,
	investment_insufficient_funds_above_minumum: 720,
	investment_complete_registration: 721,
	investment_insufficient_funds_below_minimum_secondary: 711,
	investment_below_minumum: 713,
	investment_above_maximum: 714,
	investment_already_secured: 712,
	investment_suspended: 718,
	investment_unavailable: 715,
	investment_already_issued: 716,
	investment_deviation_secondary_primary: 717,
	investment_deviation: 719,
	sell_now_bid: 730,
	unknown: 999
}

TOAST_TYPES = {
	info: 1,
	error: 2
}

var amountDivideBy100 = true;

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = /^['\\p{L} ]+$/;
var regEx_charsOnly = /^[a-zA-Z]*$/;
function BCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'bc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'bonus-level', value: selectedProduct.bonusLevel, extra: ''
			},
			'left-top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'bonus-level', value: selectedProduct.bonusLevel, extra: ''
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			'details-right': [
				{title: 'bonus-level', value: selectedProduct.bonusLevel + '%', extra: ''},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'},
					{template: 'full'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') +  this.feWord((100 + this.getScenariosFinalFixingLevel(0)) + '%'),
				!selectedProduct.productBarrier.barrierOccur ?
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('bonus-level')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.bonusLevel + '%')
				:
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 - this.getScenariosFinalFixingLevel(1)) + '%'),
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') +  this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}
function BCPCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'bcpc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'upside', value: selectedProduct.upside, extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				dual: true,
				params: [
					{title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)},
					{title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''}
				]
			} : {
				dual: true,
				params: [
					{title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: true},
					{title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''}
				]
			},
			'top-row-3': {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				dual: true,
				params: [
					{title: 'issue-price', value: selectedProduct.issuePrice, extra: ''},
					{title: 'current-price', value: selectedProduct.ask, extra: ''}
				]
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''}
			],
			'details-right': [
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'short', formulaResult: selectedProduct.levelOfProtection/100},
					{template: 'skip'},
					{template: 'skip'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				!selectedProduct.productBarrier.barrierOccur ? this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')'
				: this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%'),
				
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%'),
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}
function BOCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'boc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'left-top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: true
			},
			'top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: true
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			'details-right': [
				{title: 'bonus-level', value: selectedProduct.bonusLevel + '%', extra: ''},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')' + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('-') + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')\\right))'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')' + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('-') + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')\\right))'
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'}
			]
		};
	}
}
function BRCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'brc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: '', hidePercent: true
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: '', hidePercent: true
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '', extra: ''}
			],
			'details-right': [
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)},
				{title: 'distance-to-barrier', value: selectedProduct.productBarrier ? round(selectedProduct.productBarrier.distanceToBarrier, 2) + '%' : '', extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full', level: $scope.productGetInitialFixingLevel(selectedProduct), formulaResult: (1 + selectedProduct.productCoupons[0].payRate/100), shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-1-short' + ($scope.productIsBarrierTypeEuropean(selectedProduct) ? '-european' : '') + '-barrier-event'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' +  this.feWord((100 - this.getScenariosFinalFixingLevel(1)) + '%') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')'				
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')),
						isHidden: selectedProduct.productBarrier ? $scope.productIsBarrierTypeEuropean(selectedProduct) : false
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}
function CPCCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'cpcc',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'coupon-trigger-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel : '', extra: ''
			} : {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'coupon-trigger-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.productCoupons[0].triggerLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.productCoupons[0].triggerLevel + '%', extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'initial-fixing-level', value: $scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) : '100%', extra: ''},
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'couponTriggerEvent', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'}
			]
		};
	}
}
function CPCPConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'cpcp',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: false,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''},
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(1) + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))'
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'}
			]
		};
	}
}
function ECConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'ec',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: false
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' + $scope.getMsgs(selectedProduct.productCouponFrequency.displayName) : '', extra: ''}
			],
			'details-right': [
				{title: 'coupon-trigger-level', value: selectedProduct.bonusLevel + '%', extra: '', isHidden: selectedProduct.bonusLevel == 0},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)},
				{title: 'distance-to-barrier', value: selectedProduct.productBarrier ? round(selectedProduct.productBarrier.distanceToBarrier, 2) + '%' : '', extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full'},
					{template: 'skip'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount'),
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') +  this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')),
						isHidden: selectedProduct.productBarrier ? $scope.productIsBarrierTypeEuropean(selectedProduct) : false
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}
function IBRCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'ibrc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: true
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '', extra: ''}
			],
			'details-right': [
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)},
				{title: 'distance-to-barrier', value: selectedProduct.productBarrier ? round(selectedProduct.productBarrier.distanceToBarrier, 2) + '%' : '', extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full', level: $scope.productGetInitialFixingLevel(selectedProduct), formulaResult: (1 + selectedProduct.productCoupons[0].payRate/100), shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-1-short-barrier-event'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'},
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				!selectedProduct.productBarrier.barrierOccur ?
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + this.feSign('+') + this.feWord('1') + this.feSign('+') + '\\frac{' + this.feWord($scope.getMsgs('initial-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + this.feSign('+') + this.feWord('1') + this.getTooltipFormulaParam('initial_sub_final_div_initial_times_barrier', 1) + ')'
				:
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + '\\frac{' + this.feWord($scope.getMsgs('initial-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + 
					this.feWord((100 - this.getScenariosFinalFixingLevel(1)) + '%') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')'
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					},
					{
						formula: this.feWord('Max') + '\\left\\{' + this.feWord('0') + ', ' + this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + '\\left(\\frac{' + this.feWord($scope.getMsgs('initial-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))\\right\\}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')),
						isHidden: selectedProduct.productBarrier ? $scope.productIsBarrierTypeEuropean(selectedProduct) : false
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}
function OCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.isCapped = function(){
		return selectedProduct.capLevel !== 0;
	}
	this.getCapLevel = function(){
		return selectedProduct.capLevel;
	}
	
	this.loadConfig = function(){
		return {
			shortName: 'oc',
			hasBarrier: false,
			headerShortSuffix: this.isCapped() ? 'capped' : '',
			'left-top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'left-top-row-3': {
				title: 'return', value: !this.isCapped() ? '' : $scope.getMsgs('capped-at') + '\xA0' + (selectedProduct.capLevel - 100), extra: !this.isCapped() ? $scope.getMsgs('uncapped') : '', hidePercent: !this.isCapped()
			},
			'top-row-2': this.isCapped() && $scope.productIsSecondary(selectedProduct) ? {
				title: 'cap-level', value: this.getCapLevel(), extra: ''
			} : {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'cap-level', value: this.getCapLevel() ? this.getCapLevel() : 'None', extra: '', format: this.getCapLevel() ? '' : 'small', hidePercent: !this.getCapLevel()
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: false,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : !this.isCapped() ? [
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(1) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
				] : [
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('cap-level')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(this.getCapLevel() + '%'),
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(round(this.getScenariosFinalFixingLevel(1), 2) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
				],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('100%') + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))',
						isHidden: selectedProduct.capLevel != 0
					},
					{
						formula: this.feWord('Min') + '\\left\\{' + this.feWord($scope.getMsgs('cap-level')) + this.feSign('\\times') + this.feWord($scope.getMsgs('denomination')) + '; ' + this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('100%') + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))\\right\\}',
						isHidden: selectedProduct.capLevel == 0,
						isLong: true
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'capLevel']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'}
			]
		};
	}
}
function ProductTypeConfig($scope, selectedProduct){
	this.getQuanto = function(){
		return selectedProduct.quanto ? $scope.getMsgs('yes') : $scope.getMsgs('no');
	}
	this.getBarrierEvent = function(){
		if(!selectedProduct.productBarrier){
			return '';
		}
		return selectedProduct.productBarrier.barrierOccur ? $scope.getMsgs('yes') : $scope.getMsgs('no');
	}
	this.getBarrierText = function(){
		if(selectedProduct.productBarrier){
			if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
				return $scope.getMsgs('continuous-barrier-monitoring');
			}else{
				return $scope.getMsgs('barrier-monitoring-at-final-fixing-date');
			}
		}
		return '';
	}
	this.getBarrierTextShort = function(){
		if(selectedProduct.productBarrier){
			if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
				return $scope.getMsgs('continuously');
			}else{
				return $scope.getMsgs('at-final-fixing-date');
			}
		}
		return '';
	}
	this.getBarrierLevel = function(){
		if(selectedProduct.productBarrier){
			return round(selectedProduct.productBarrier.barrierLevel*$scope.productGetInitialFixingLevel(selectedProduct)/100, 2);
		}
		return '';
	}
	this.getCouponPayRate = function(){
		return selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '';
	}
	this.getFinalFixingLevel = function(){
		if(selectedProduct.productMarkets && selectedProduct.productMarkets[0]){
			return selectedProduct.productMarkets[0].endTradeLevel;
		}
		return '';
	}
	this.getInitialFixingLevel = function(){
		return $scope.productGetInitialFixingLevel(selectedProduct);
	}
	this.getScenariosFinalFixingLevel = function(scenarioIndex){
		return selectedProduct.productScenarios[scenarioIndex].finalFixingLevelForTxt;
	}
	this.getTooltipFormulaParam = function(param, scenarioIndex){
		var result = '';
		
		switch(param){
			case 'denomination-label':
				if($scope.productIsPrimary(selectedProduct)){
					result = this.feWord($scope.getMsgs('investment-amount'));
				}else{
					// result = '\\left(\\frac{' + this.feWord($scope.getMsgs('investment-amount')) + '}{' + this.feWord($scope.getMsgs('ask')) + '}\\right)';
					result = this.feWord($scope.getMsgs('denomination'));
				}
				break;
			case 'denominationAmount':
				if($scope.productIsPrimary(selectedProduct)){
					result = '${investment}';
				}else{
					result = '\\frac{' + '${investment}' + '}{' + this.feWord(selectedProduct.ask + '%') + '}';
				}
				break;
			case 'investmentAmount':
				result = '${investment}';
				break;
			case 'investmentDays_div_360':
				/*var initialDate = new Date(selectedProduct.initialFixingDate);
				var finalDate = new Date(selectedProduct.finalFixingDate);
				result = '\\frac{' + this.feWord(360*(finalDate.getFullYear() - initialDate.getFullYear()) + 30*(finalDate.getMonth() - initialDate.getMonth()) + (finalDate.getDate() - initialDate.getDate())) + '}{' + this.feWord('360') + '}';*/
				result = '\\frac{' + this.feWord(selectedProduct.couponDays) + '}{' + this.feWord('360') + '}';
				break;
			case 'final_div_initial_times_barrier':
				/*if(selectedProduct.productBarrier){
					result = this.feWord(round((selectedProduct.productScenarios[scenarioIndex].finalFixingLevel/this.getInitialFixingLevel())*selectedProduct.productBarrier.barrierLevel, 2) + '%');
				}else{
					result = '';
				}*/
				result = this.feWord(this.getScenariosFinalFixingLevel(scenarioIndex) + '%');
				break;
			case 'initial_sub_final_div_initial_times_barrier':
				/*if(selectedProduct.productBarrier){
					result = this.feWord(round((this.getInitialFixingLevel() - selectedProduct.productScenarios[scenarioIndex].finalFixingLevel)/this.getInitialFixingLevel(), 2)*(selectedProduct.productBarrier.barrierLevel - 100) + '%');
				}else{
					result = '';
				}*/
				var percentage = (-1)*this.getScenariosFinalFixingLevel(scenarioIndex);
				var sign = '+';
				if(percentage < 0){
					percentage *= (-1);
					sign = '-';
				}
				result = this.feSign(sign) + this.feWord(percentage + '%');
				break;
			case 'final_div_strike_level':
				result = this.feWord(round(((selectedProduct.productScenarios[scenarioIndex].finalFixingLevel*100)/selectedProduct.strikeLevel)*100, 2) + '%');
				break;
			default:
				result = '';
		}
		
		return result;
	}
	this.parseTooltipFormula = function(tooltip, params){
		if(tooltip && params){
			for(key in params){
				if(params.hasOwnProperty(key)){
					var toReplace = '${' + key + '}';
					tooltip = tooltip.replace(toReplace, this.feWord(params[key]));
				}
			}
		}
		return tooltip;
	}
	this.getCouponAmount = function(note){
		var couponAmount = '';
		couponAmount = $scope.formatAmount({amount: note.couponAmonut, currencyId: settings.CURRENCY_EUR, centsPart: 2});
		if(note.couponOriginalAmonut != note.couponAmonut){
			couponAmount += ' (' + $scope.formatAmount({amount: note.couponOriginalAmonut, currencyId: settings.CURRENCY_USD, centsPart: 2}) + ')';
		}
		return couponAmount;
	}
	this.getRedemptionAmount = function(note){
		var redemptionAmount = '';
		redemptionAmount = $scope.formatAmount({amount: note.investment.returnAmount, currencyId: settings.CURRENCY_EUR, centsPart: 2});
		if(note.investment.originalReturnAmount != note.investment.returnAmount){
			redemptionAmount += ' (' + $scope.formatAmount({amount: note.investment.originalReturnAmount, currencyId: settings.CURRENCY_USD, centsPart: 2}) + ')';
		}
		return redemptionAmount;
	}
	this.getNoteRedemptionAmount = function(){
		var that = this;
		return function(note){
			return that.getRedemptionAmount(note);
		};
	}
	//Format equation word
	this.feWord = function(str, forceLower){
		//var temp = str.replace(/ /g, '\\,');
		var result = '';
		var temp = str + '';
		for(var i = 0; i < temp.length; i++){
			var shouldCap = false;
			if(i == 0 || temp[i - 1] == ' '){
				shouldCap = true;
			}
			var shouldEscape = false;
			if(temp[i] == '%'){
				shouldEscape = true;
			}
			if(temp[i] != ' '){
				result += '\\style{font-family:\'Fira Sans\';' + (shouldCap && !forceLower ? ';text-transform:uppercase' : '') + '}{' + (shouldEscape ? '\\' : '') + temp[i] + '} ';
			}else{
				result += ' \\, ';
			}
		}
		return result;
	}
	//Format equation sign
	this.feSign = function(str){
		return '\\style{font-weight:500;font-size:1.4em;vertical-align:middle;}{' + str + '}';
	}
}
function RCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'rc',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'strike-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.strikeLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.strikeLevel, extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'strike-level', value: selectedProduct.strikeLevel, extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'strike-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.strikeLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.strikeLevel + '%', extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' + $scope.getMsgs(selectedProduct.productCouponFrequency.displayName) : '', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.getTooltipFormulaParam('final_div_strike_level', 1) + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.getTooltipFormulaParam('final_div_strike_level', 2) + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')'
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('strike-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}\\right))'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'}
			]
		};
	}
}