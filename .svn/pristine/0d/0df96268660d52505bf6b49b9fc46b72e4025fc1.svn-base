package capital.any.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Login;
import capital.any.model.base.NullWriter;
import capital.any.model.base.Payment;
import capital.any.model.base.PaymentRequest;
import capital.any.model.base.PaymentResponse;
import capital.any.security.ISecurityService;
import capital.any.service.IHttpClientService;
import capital.any.service.base.payment.IPaymentService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.transactionPaymentType.ITransactionPaymentTypeService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/payment", method = RequestMethod.POST)
public class PaymentController {
	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
	@Autowired
	private IHttpClientService httpClientService;
	@Autowired
	private IPaymentService paymentService;
	@Autowired
	private ISecurityService securityService;
	@Autowired
	private ITransactionPaymentTypeService transactionPaymentTypeService;
	@Autowired
	private ISessionService sessionService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/withdraw")
	public ResponseEntity<Response<PaymentResponse>> doWithdraw(HttpServletRequest servletRequest, 
			HttpSession session, 
			@RequestBody Request<PaymentRequest<?>> request) {
		
		logger.info("about to doWithdraw " + request.getData());
		Response<PaymentResponse> response = new Response<PaymentResponse>();
		try {
			if (securityService.isAuthenticate()) {
				PaymentRequest<Object> paymentDetails = (PaymentRequest<Object>) request.getData();
				paymentDetails.setPaymentType(transactionPaymentTypeService.get().get(paymentDetails.getPaymentType().getId()));
				Class<?> paymentTypeClass = Class.forName(paymentDetails.getPaymentType().getObjectClass());			
				Object details =  new ObjectMapper().convertValue(paymentDetails.getDetails(), paymentTypeClass);
				paymentDetails.setDetails(details);
				Payment<?> withdraw = new Payment<>(paymentDetails,
						securityService.getLoginFromSession(),
						new NullWriter(), 
						request.getActionSource(),
						httpClientService.getIP(servletRequest),
						(Login)sessionService.getAttribute(session, ISessionService.LOGIN));
				paymentService.doWithdraw(withdraw);
				response = (Response<PaymentResponse>) withdraw.getResponse();
				response.setData(withdraw.getPaymentResponse());
			} else {
				response.setResponseCode(ResponseCode.ACCESS_DENIED); 
			}
		} catch (Exception e) {
			response.setResponseCode(ResponseCode.UNKNOWN_ERROR);
			logger.error("Error on insert withdraw ", e);
		}
		return new ResponseEntity<Response<PaymentResponse>>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/withdraw/confirm")
	public ResponseEntity<Response<PaymentResponse>> confirmWithdraw(HttpServletRequest servletRequest, 
			HttpSession session, 
			@RequestBody Request<PaymentRequest<?>> request) {
		logger.info("about to confirmWithdraw " + request.getData());
		Response<PaymentResponse> response = new Response<PaymentResponse>();
		try {
			if (securityService.isAuthenticate()) {
				PaymentRequest<Object> paymentDetails = (PaymentRequest<Object>) request.getData();
				paymentDetails.setPaymentType(transactionPaymentTypeService.get().get(paymentDetails.getPaymentType().getId()));
				Class<?> paymentTypeClass = Class.forName(paymentDetails.getPaymentType().getObjectClass());			
				Object details =  new ObjectMapper().convertValue(paymentDetails.getDetails(), paymentTypeClass);
				paymentDetails.setDetails(details);
				Payment<?> withdraw = new Payment<>(paymentDetails,
						securityService.getLoginFromSession(),
						new NullWriter(), 
						request.getActionSource(),
						httpClientService.getIP(servletRequest),
						(Login)sessionService.getAttribute(session, ISessionService.LOGIN));
				paymentService.confirmWithdraw(withdraw);
				response = (Response<PaymentResponse>) withdraw.getResponse(); 
				response.setData(withdraw.getPaymentResponse());
			} else {
				response.setResponseCode(ResponseCode.ACCESS_DENIED); 
			}
		} catch (Exception e) {
			response.setResponseCode(ResponseCode.UNKNOWN_ERROR);
			logger.error("Error on confirmWithdraw ", e);
		}
		return new ResponseEntity<Response<PaymentResponse>>(response, HttpStatus.OK);
	}
}
