package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Product;
import capital.any.model.base.User;
import capital.any.security.CustemUserDetailService;
import capital.any.service.base.product.IProductService;
import capital.any.service.base.user.IUserService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class ProductControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductControllerTest.class);
	
	@Autowired
	private CustemUserDetailService custemUserDetailService;	
	@Autowired
	private WebApplicationContext webApplicationContext; 	
	private MockMvc mockMvc;	
	@Autowired 
	private ObjectMapper mapper;	
	@Autowired
	private IProductService productService;
	
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	@Value("${sell.now.product.id}")
	private Long productId;

	
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();		
	}
	
	
	@Test
	public void getProductFull() throws Exception {
		logger.debug("getProductFull");
		Map<Long, Product> hm = productService.getProducts();
		Product product = null;
		try {
			for (Product productTemp : hm.values()) {
				product = productTemp;
				break;
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}
		Request<Product> r = new Request<Product>(product, ActionSource.WEB);
		String json = mapper.writeValueAsString(r);	       
		System.out.println("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Map<String, Object>> responseobject = (Response<Map<String, Object>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<String, Object>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			System.out.println("cant get full product " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getProductFullLogin() throws Exception {
		logger.debug("getProductFull");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Map<Long, Product> hm = productService.getProducts();
		Product product = hm.get(productId);
		Request<Product> r = new Request<Product>(product, ActionSource.WEB);
		String json = mapper.writeValueAsString(r);
		System.out.println("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull").with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Map<String, Object>> responseobject = (Response<Map<String, Object>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<String, Object>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			System.out.println("cant get full product " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getProductFullWrongId() throws Exception {
		System.out.println("getProductFullWrongId");
		Product product = new Product();
		product.setId(44);
		Request<Product> request = new Request<Product>(product, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		System.out.println("body: " + json);
		try {
			mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			System.out.println("cant get full product " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getAllProducts() throws Exception {
		logger.debug("getAllProducts");
		Request<Product> r = new Request<Product>(new Product(), ActionSource.WEB);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/getAllProducts")
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	@Test
	public void getAllProductsLogin() throws Exception {
		logger.debug("getAllProducts");
		Request<Product> r = new Request<Product>(new Product(), ActionSource.WEB);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/getAllProducts").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
}
