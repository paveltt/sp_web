<div class="" ng-if="results && results.length > 0">
	<div class="note-block" ng-repeat-start="productGroup in results">
		<table>
			<tr>
				<td>
					<div class="product-asset-name">{{getMsgs(productGroup.product.productMarkets[0].market.displayName)}}</div>
				</td>
				<td>
					<div class="product-link">
						<a ng-href="{{$state.href('ln.product', {ln: language.code, productId: productGroup.product.id})}}"></a>
					</div>
					<div class="product-type">{{getMsgs('product.type.' + productGroup.product.productType.id)}}</div>
					<div class="ids">
						{{getMsgs('prod-id')}}: {{productGroup.product.id}}
						<span ng-if="productGroup.myNotes.length == 1"> | {{getMsgs('inv-id')}}: {{productGroup.myNotes[0].investment.id}}</span>
					</div>
					<div class="purchased-at-date" ng-if="productGroup.myNotes.length == 1">{{productGroup.myNotes[0].investment.timeCreated | date: 'dd.MM.yyyy HH:mm'}}</div>
					<div class="initial-fixing-level" ng-if="!productIsPrimary(productGroup.product)">{{getMsgs('initial-fixing-level')}}: <span>{{productGetInitialFixingLevel(productGroup.product)}}</span></div>
					<div class="last-level">{{getMsgs('last')}}: {{productGroup.product.productMarkets[0].market.lastPrice.price}}</div>
				</td>
			<tr>
			<tr>
				<td class="expand-cell">
					<div class="label">{{getMsgs('amount')}}:</div>
					<div class="amount">{{formatAmount({amount: sumInvestmentsAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}</div>
					<div class="amount-additional" ng-if="productGroup.myNotes.length == 1 && productGroup.myNotes[0].investment.ask > 0">
						<span ng-if="productGroup.myNotes[0].investment.ask >= 100">{{round(productGroup.myNotes[0].investment.ask - 100, 2)}}<span class="percent">%</span> </span>
						<span ng-if="productGroup.myNotes[0].investment.ask < 100">{{round(productGroup.myNotes[0].investment.ask - 100, 2)}}<span class="percent">%</span> </span>
						
						<span class="premium-discount" ng-if="productGroup.myNotes[0].investment.ask >= 100">{{getMsgs('premium')}}</span>
						<span class="premium-discount" ng-if="!(productGroup.myNotes[0].investment.ask >= 100)">{{getMsgs('discount')}}</span>
					</div>
					
					<div class="toggle-investments" ng-if="productGroup.myNotes.length > 1" ng-click="productGroup._isExpanded = !productGroup._isExpanded" ng-class="{open: productGroup.myNotes.length > 1 && productGroup._isExpanded, closed: productGroup.myNotes.length > 1 && !productGroup._isExpanded}"></div>
				</td>
				<td>
					<div class="button-holder" ng-if="productIsSecondary(productGroup.product)">
						<div class="label">{{getMsgs('sell-now-for')}}:</div>
						<div class="sell-now-holder">
							<button type="button" class="button yellow sell-now" ng-click="openSellPopup(productGroup)" ng-disabled="productGroup._sellPopupVisible || productGroup._sellConfirmedPopupVisible">
								{{formatAmount({amount: sellForAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
							</button>
							<div class="sell-now-waiting-confirmation-popup" ng-class="{visible: productGroup._sellPopupVisible, loading: action == 'loading-sell'}" ng-show="productGroup._sellPopupVisible">
								<h2>{{getMsgs('sell-confirmation')}}</h2>
								<p>{{getMsgs('sell-confirmation-description', {'investment-amount': formatAmount({amount: sumInvestmentsAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2}), 'return-amount': formatAmount({amount: sellForAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2})})}}</p>
								<button type="button" class="button yellow sell-now-confirm" ng-click="sell(productGroup)">{{getMsgs('sell')}}</button>
								<div>
									<a href="" ng-click="closeSellPopup(productGroup)">{{getMsgs('not-now')}}</a>
								</div>
							</div>
							<div class="sell-now-confirmed-popup" ng-class="{visible: productGroup._sellConfirmedPopupVisible}" ng-show="productGroup._sellConfirmedPopupVisible">
								<h2>{{getMsgs('confirmed')}}</h2>
								<p>
									<span>{{getMsgs('sold-at')}}:</span>
									{{sellDate | date:'dd/MM/yyyy&nbsp;HH:mm'}}
								</p>
								<p>
									{{getMsgs('product.type.' + productGroup.product.productType.id)}} {{getMsgs('on')}} {{getMsgs(productGroup.product.productMarkets[0].market.displayName)}}
								</p>
								<p>
									<span>{{getMsgs('amount')}}:</span>
									{{formatAmount({amount: sumInvestmentsAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
								</p>
								<p class="receive">
									<span>{{getMsgs('receive')}}:</span>
									{{formatAmount({amount: sellForAmount(productGroup), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
								</p>
								<button type="button" class="button green sell-now-confirmed" ng-click="closeSellConfirmedPopup(productGroup)">{{getMsgs('ok')}}</button>
							</div>
						</div>
					</div>
					<div class="button-holder" ng-if="productIsSecondary(productGroup.product)">
						<div class="label">{{getMsgs('buy-more-at')}}:</div>
						<button type="button" class="button dark-blue buy-more-at" ng-click="$state.go('ln.product', {ln: language.code, productId: productGroup.product.id})" ng-disabled="productGroup.product.ask <= 0">
							<span ng-if="productGroup.product.ask >= 100">{{round(productGroup.product.ask - 100, 2)}}<span class="percent">%</span> </span>
							<span ng-if="productGroup.product.ask > 0 && productGroup.product.ask < 100">{{round(productGroup.product.ask - 100, 2)}}<span class="percent">%</span> </span>
							
							<span class="description" ng-if="productGroup.product.ask >= 100">{{getMsgs('premium')}}</span>
							<span class="description" ng-if="productGroup.product.ask > 0 && !(productGroup.product.ask >= 100)">{{getMsgs('discount')}}</span>
							
							<span class="description" ng-if="productGroup.product.ask <= 0">{{getMsgs('unavailable')}}</span>
						</button>
					</div>
					
					<div class="no-buttons" ng-if="productIsPrimary(productGroup.product)">
						<span ng-if="isOpenInvestment(productGroup.myNotes[0].investment)">
							{{getMsgs('in-subscription')}}
						</span>
						<span ng-if="!isOpenInvestment(productGroup.myNotes[0].investment)" ng-bind-html="getMsgs('my-notes-open-message-fund-your-account', {'fund-your-account-link': $state.href('ln.my-account.funding.deposit', {ln: $state.params.ln})})"></span>
					</div>
					
					<div class="no-buttons" ng-if="productIsPendingSettlement(productGroup.product)">
						<span>
							{{getMsgs('pending-settlement')}}
						</span>
					</div>
					
					<div class="redemption">
						<span class="label">{{getMsgs('redemption')}}: </span>
						{{productGroup.product.redemptionDate | date: 'dd.MM.yyyy HH:mm'}}
					</div>
				</td>
			<tr>
		</table>
		<div class="sell-now-error-popup" ng-show="productGroup._sellErrorPopupVisible">
			<div class="sell-now-error-popup-inner" ng-if="productGroup._sellError == 'ok'">
				<div>{{getMsgs('my-notes-sell-error-unavailable')}}</div>
				<button type="button" class="button green sell-error-ok" ng-click="closeSellErrorPopup(productGroup)">{{getMsgs('ok')}}</button>
			</div>
			<div class="sell-now-error-popup-inner" ng-if="productGroup._sellError == 'refresh'">
				<div>{{getMsgs('my-notes-sell-error-refresh')}}</div>
				<button type="button" class="button green sell-error-refresh" ng-click="closeSellErrorPopup(productGroup);reload()">{{getMsgs('refresh')}}</button>
			</div>
		</div>
	</div>
	<div class="note-block investment-row" ng-repeat="note in productGroup.myNotes" ng-repeat-end ng-if="productGroup.myNotes.length > 1 && productGroup._isExpanded">
		<table>
			<tr>
				<td>
					
				</td>
				<td>
					<div class="product-link">
						<a ng-href="{{$state.href('ln.product', {ln: language.code, productId: productGroup.product.id})}}"></a>
					</div>
					<div class="ids">
						<span>{{getMsgs('inv-id')}}: {{note.investment.id}}</span>
					</div>
					<div class="purchased-at-date">{{note.investment.timeCreated | date: 'dd.MM.yyyy HH:mm'}}</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="label">{{getMsgs('amount')}}:</div>
					<div class="amount">{{formatAmount({amount: note.investment.amount, currencyId: settings.CURRENCY_EUR, centsPart: 2})}}</div>
					<div class="amount-additional" ng-if="note.investment.ask > 0">
						<span ng-if="note.investment.ask >= 100">{{round(note.investment.ask - 100, 2)}}<span class="percent">%</span> </span>
						<span ng-if="note.investment.ask < 100">{{round(note.investment.ask - 100, 2)}}<span class="percent">%</span> </span>
						
						<span class="premium-discount" ng-if="note.investment.ask >= 100">{{getMsgs('premium')}}</span>
						<span class="premium-discount" ng-if="!(note.investment.ask >= 100)">{{getMsgs('discount')}}</span>
					</div>
				</td>
				<td>
					<div class="button-holder" ng-if="productIsSecondary(productGroup.product)">
						<div class="label">{{getMsgs('sell-now-for')}}:</div>
						<div class="sell-now-holder">
							<button type="button" class="button yellow sell-now" ng-click="openSellPopup(productGroup, note)" ng-disabled="productGroup._sellPopupVisible || productGroup._sellConfirmedPopupVisible || note._sellPopupVisible || note._sellConfirmedPopupVisible">
								{{formatAmount({amount: investmentSellForAmount(productGroup, note.investment), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
							</button>
							<div class="sell-now-waiting-confirmation-popup" ng-class="{visible: note._sellPopupVisible, loading: action == 'loading-sell'}" ng-show="note._sellPopupVisible">
								<h2>{{getMsgs('sell-confirmation')}}</h2>
								<p>{{getMsgs('sell-confirmation-description', {'investment-amount': formatAmount({amount: note.investment.amount, currencyId: settings.CURRENCY_EUR, centsPart: 2}), 'return-amount': formatAmount({amount: investmentSellForAmount(productGroup, note.investment), currencyId: settings.CURRENCY_EUR, centsPart: 2})})}}</p>
								<button type="button" class="button yellow sell-now-confirm" ng-click="sell(productGroup, note)">{{getMsgs('sell')}}</button>
								<div>
									<a href="" ng-click="closeSellPopup(productGroup, note)">{{getMsgs('not-now')}}</a>
								</div>
							</div>
							<div class="sell-now-confirmed-popup" ng-class="{visible: note._sellConfirmedPopupVisible}" ng-show="note._sellConfirmedPopupVisible">
								<h2>{{getMsgs('confirmed')}}</h2>
								<p>
									<span>{{getMsgs('sold-at')}}:</span>
									{{sellDate | date:'dd/MM/yyyy&nbsp;HH:mm'}}
								</p>
								<p>
									{{getMsgs('product.type.' + productGroup.product.productType.id)}} {{getMsgs('on')}} {{getMsgs(productGroup.product.productMarkets[0].market.displayName)}}
								</p>
								<p>
									<span>{{getMsgs('amount')}}:</span>
									{{formatAmount({amount: note.investment.amount, currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
								</p>
								<p class="receive">
									<span>{{getMsgs('receive')}}:</span>
									{{formatAmount({amount: investmentSellForAmount(productGroup, note.investment), currencyId: settings.CURRENCY_EUR, centsPart: 2})}}
								</p>
								<button type="button" class="button green sell-now-confirmed" ng-click="closeSellConfirmedPopup(productGroup, note)">{{getMsgs('ok')}}</button>
							</div>
						</div>
					</div>
					<div class="button-holder" ng-if="productIsSecondary(productGroup.product)">
						<div class="label">{{getMsgs('buy-more-at')}}:</div>
						<button type="button" class="button dark-blue buy-more-at" ng-click="$state.go('ln.product', {ln: language.code, productId: productGroup.product.id})" ng-disabled="productGroup.product.ask <= 0">
							<span ng-if="productGroup.product.ask >= 100">{{round(productGroup.product.ask - 100, 2)}}<span class="percent">%</span> </span>
							<span ng-if="productGroup.product.ask > 0 && productGroup.product.ask < 100">{{round(productGroup.product.ask - 100, 2)}}<span class="percent">%</span> </span>
							
							<span class="description" ng-if="productGroup.product.ask >= 100">{{getMsgs('premium')}}</span>
							<span class="description" ng-if="productGroup.product.ask > 0 && !(productGroup.product.ask >= 100)">{{getMsgs('discount')}}</span>
							
							<span class="description" ng-if="productGroup.product.ask <= 0">{{getMsgs('unavailable')}}</span>
						</button>
					</div>
					
					<div class="no-buttons" ng-if="productIsPrimary(productGroup.product)">
						<span ng-if="isOpenInvestment(note.investment)">
							{{getMsgs('in-subscription')}}
						</span>
						<span ng-if="!isOpenInvestment(note.investment)" ng-bind-html="getMsgs('my-notes-open-message-fund-your-account', {'fund-your-account-link': $state.href('ln.my-account.funding.deposit', {ln: $state.params.ln})})"></span>
					</div>
					
					<div class="no-buttons" ng-if="productIsPendingSettlement(productGroup.product)">
						<span>
							{{getMsgs('pending-settlement')}}
						</span>
					</div>
					
					<div class="redemption">
						<span class="label">{{getMsgs('redemption')}}: </span>
						{{productGroup.product.redemptionDate | date: 'dd.MM.yyyy HH:mm'}}
					</div>
				</td>
			</tr>
		</table>
		<div class="sell-now-error-popup sell-now-error-popup-note" ng-show="note._sellErrorPopupVisible">
			<div class="sell-now-error-popup-inner" ng-if="note._sellError == 'ok'">
				<div>{{getMsgs('my-notes-sell-error-unavailable')}}</div>
				<button type="button" class="button green sell-error-ok" ng-click="closeSellErrorPopup(productGroup, note)">{{getMsgs('ok')}}</button>
			</div>
			<div class="sell-now-error-popup-inner" ng-if="note._sellError == 'refresh'">
				<div>{{getMsgs('my-notes-sell-error-refresh')}}</div>
				<button type="button" class="button green sell-error-refresh" ng-click="closeSellErrorPopup(productGroup, note);reload()">{{getMsgs('refresh')}}</button>
			</div>
		</div>
	</div>
</div>