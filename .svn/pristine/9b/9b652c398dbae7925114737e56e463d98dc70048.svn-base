package capital.any.controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.NullWriter;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.transaction.ITransactionService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/transaction", method = RequestMethod.POST)
public class TransactionController {
	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	@Autowired @Qualifier("TransactionServiceWeb")
	private ITransactionService transactionService;
	@Autowired
	private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;

	@RequestMapping(value = "/withdrawal/reverse")
	public ResponseEntity<Response<Transaction>> reverseWithdrawal(@RequestBody Request<Transaction> request,
			HttpSession session) {
		Response<Transaction> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		Transaction transaction = null;
		try {
			if (securityService.isAuthenticate()) {
				transaction = (Transaction) request.getData();
				transaction.setUser(securityService.getLoginFromSession());
				TransactionHistory transactionHistory = new TransactionHistory();
				transactionHistory.setActionSource(request.getActionSource());
				transactionHistory.setLoginId(sessionService.getLoginId(session));
				transactionHistory.setWriter(new NullWriter());
				responseCode = transactionService.reverseWithdrawal(transaction, transactionHistory);
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! reverseWithdrawal ", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Transaction>(transaction, responseCode);
		return new ResponseEntity<Response<Transaction>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/withdrawal/reverse/list")
	public ResponseEntity<Response<List<Transaction>>> getUserReverseWithdrawalList(@RequestBody Request<Object> request) {
		Response<List<Transaction>> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		User user = null;
		List<Transaction> transactions = null;
		try {
			if (securityService.isAuthenticate()) {
				user = securityService.getLoginFromSession();
				transactions = transactionService.getUserReverseWithdrawalList(user);
				responseCode = ResponseCode.OK;
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! getUserReverseWithdrawalList ", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<List<Transaction>>(transactions, responseCode);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByUserId")
	public ResponseEntity<Response<List<Transaction>>> getTransactionsByUserId(@RequestBody Request<SqlFilters> request) {
		Response<List<Transaction>> response = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Transaction> items = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters) request.getData();
				filters.setUserId(securityService.getLoginFromSession().getId());
				items = transactionService.getTransactions(filters);
				responseCode = ResponseCode.OK;
			} 
		} catch (Exception e) {
			logger.error("ERROR! get transactions by user id ", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<List<Transaction>>(items, responseCode);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/bankingHistory")
	public ResponseEntity<Response<List<Transaction>>> getBankingHistory(@RequestBody Request<SqlFilters> request) {
		Response<List<Transaction>> response = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Transaction> items = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters) request.getData();
				filters.setUserId(securityService.getLoginFromSession().getId());
				items = transactionService.getBankingHistory(filters);
				responseCode = ResponseCode.OK;
			} 
		} catch (Exception e) {
			logger.error("ERROR! get bankingHistory by user id ", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<List<Transaction>>(items, responseCode);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
}
