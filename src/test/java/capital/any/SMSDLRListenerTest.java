package capital.any;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class SMSDLRListenerTest {
	private static final Logger logger = LoggerFactory.getLogger(SMSDLRListenerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();		
	}

	@Test
	@Transactional
	public void mobivateDLRSuccessHLR() throws Exception {
		String XMLRequest = "xml=%3Cdeliveryreceipt%3E%3CclientReference%3E1%3C%2FclientReference%3E%3Ccreated%3E2017-02-25T15%3A39%3A35%2B0000%3C%2Fcreated%3E%3CdeliveryMessageId%3E75ED5CCC0A931B314F8EE7AD339ED977%3C%2FdeliveryMessageId%3E%3Chlr%3E%3Cimsi%3E425020177198703%3C%2Fimsi%3E%3Cmcc%3E425%3C%2Fmcc%3E%3Cmnc%3E02%3C%2Fmnc%3E%3Cmobile%3E972526747658%3C%2Fmobile%3E%3CorigCountryName%3EIsrael%3C%2ForigCountryName%3E%3CorigCountryPrefix%3E972%3C%2ForigCountryPrefix%3E%3CorigNetworkName%3ECellcom+Israel%3C%2ForigNetworkName%3E%3CorigNetworkPrefix%3E52%3C%2ForigNetworkPrefix%3E%3Cported%3Efalse%3C%2Fported%3E%3CportedCountryPrefix%3E972%3C%2FportedCountryPrefix%3E%3CportedNetworkCountryName%3EIsrael%3C%2FportedNetworkCountryName%3E%3CportedNetworkName%3ECellcom+Israel%3C%2FportedNetworkName%3E%3CportedNetworkPrefix%3E5522%3C%2FportedNetworkPrefix%3E%3Cupdated%3E2017-02-25T15%3A39%3A35%2B0000%3C%2Fupdated%3E%3C%2Fhlr%3E%3Cpart%3E1%3C%2Fpart%3E%3Cparts%3E1%3C%2Fparts%3E%3Cstatus%3EDELIVERED%3C%2Fstatus%3E%3CstatusCode%3E1%3C%2FstatusCode%3E%3C%2Fdeliveryreceipt%3E";
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/sms/mobivate").
					contentType(MediaType.APPLICATION_XML)
					.content(XMLRequest)
					.accept(MediaType.APPLICATION_XML)).andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("Error ", e);
		}
	}
	
	@Test
	@Transactional
	public void mobivateDLRFailHLR() throws Exception {
		String XMLRequest = "xml=%3Cdeliveryreceipt%3E%3CclientReference%3E1%3C%2FclientReference%3E%3Ccreated%3E2017-02-25T16%3A40%3A50%2B0000%3C%2Fcreated%3E%3CdeliveryMessageId%3E762574C90A931B314F8EE7AD2D170B11%3C%2FdeliveryMessageId%3E%3Chlr%3E%3Cmobile%3E972526747658555555%3C%2Fmobile%3E%3Cported%3Efalse%3C%2Fported%3E%3Cupdated%3E2017-02-25T16%3A40%3A50%2B0000%3C%2Fupdated%3E%3C%2Fhlr%3E%3Cpart%3E1%3C%2Fpart%3E%3Cparts%3E1%3C%2Fparts%3E%3Cstatus%3EREJECTED_INVALID_RECIPIENT%3C%2Fstatus%3E%3CstatusCode%3E34%3C%2FstatusCode%3E%3C%2Fdeliveryreceipt%3E";
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/sms/mobivate").
					contentType(MediaType.APPLICATION_XML)
					.content(XMLRequest)
					.accept(MediaType.APPLICATION_XML)).andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("Error ", e);
		}
	}
	
	@Test
	@Transactional
	public void mobivateDLRSuccessSendSMS() throws Exception {
		String XMLRequest = "xml=%3Cdeliveryreceipt%3E%3CclientReference%3E1%3C%2FclientReference%3E%3Ccreated%3E2017-02-25T16%3A13%3A46%2B0000%3C%2Fcreated%3E%3CdeliveryMessageId%3E760C9DAC0A931B314F8EE7ADEFE9108D%3C%2FdeliveryMessageId%3E%3Cpart%3E1%3C%2Fpart%3E%3Cparts%3E1%3C%2Fparts%3E%3Cstatus%3EDELIVERED%3C%2Fstatus%3E%3CstatusCode%3E1%3C%2FstatusCode%3E%3C%2Fdeliveryreceipt%3E";
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/sms/mobivate").
					contentType(MediaType.APPLICATION_XML)
					.content(XMLRequest)
					.accept(MediaType.APPLICATION_XML)).andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("Error ", e);
		}
	}
	
	@Test
	@Transactional
	public void mobivateDLRFailSendSMS() throws Exception {
		String XMLRequest = "xml=%3Cdeliveryreceipt%3E%3CclientReference%3E1%3C%2FclientReference%3E%3Ccreated%3E2017-02-25T16%3A48%3A17%2B0000%3C%2Fcreated%3E%3CdeliveryMessageId%3E762C452E0A931B314F8EE7AD5C42F99C%3C%2FdeliveryMessageId%3E%3Cpart%3E1%3C%2Fpart%3E%3Cparts%3E1%3C%2Fparts%3E%3Cstatus%3EREJECTED_INVALID_RECIPIENT%3C%2Fstatus%3E%3CstatusCode%3E34%3C%2FstatusCode%3E%3C%2Fdeliveryreceipt%3E";
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/sms/mobivate").
					contentType(MediaType.APPLICATION_XML)
					.content(XMLRequest)
					.accept(MediaType.APPLICATION_XML)).andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("Error ", e);
		}
	}
	
}
