package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.FileInputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.File;
import capital.any.model.base.FileRequiredDocGroup;
import capital.any.model.base.FileType;
import capital.any.model.base.NullWriter;
import capital.any.model.base.User;
import capital.any.security.CustemUserDetailService;

/**
 * 
 * @author Eyal.o
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class FileControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(FileControllerTest.class);
	
	@Autowired
	private WebApplicationContext context;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	
	private MockMvc mockMvc;
	private File file;
	
	@Value("${user.not.regulated}")
	private String userNotRegulated;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getRequiredDocs() throws Exception {
		logger.debug("getProductSumInvestments");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userNotRegulated);	
		
		Request<User> request = new Request<User>(null, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/file/getRequiredDocs")
								.with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<FileRequiredDocGroup>> responseobject = (Response<List<FileRequiredDocGroup>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<FileRequiredDocGroup>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant getRequiredDocs ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	@Ignore
	public void insert() throws Exception {
		java.io.File f = new java.io.File("C:\\1.PNG");
        System.out.println(f.isFile()+"  "+f.getName()+f.exists());
        FileInputStream fi1 = new FileInputStream(f);
        MockMultipartFile fstmp = new MockMultipartFile("fileUpload", f.getName(), "multipart/form-data", fi1);
		file = new File();
		file.setActionSource(ActionSource.CRM);
		file.setWriter(new NullWriter());
		file.setName("bbbbb.png");
		FileType fileType = new FileType();
		fileType.setId(1);
		file.setFileType(fileType);				
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userNotRegulated);
		Request<File> request = new Request<File>(file, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/file/insert")
					.file(fstmp)
					.contentType(MediaType.MULTIPART_FORM_DATA)
					.param("request", json)
					.with(user(userDetails)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<File> responseobject = (Response<File>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<File>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't insert file ", e);
			throw e;
		}
	}

}
