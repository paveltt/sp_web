package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.QmUserAnswer;
import capital.any.model.base.User;
import capital.any.security.CustemUserDetailService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class RegulationControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(RegulationControllerTest.class);
	
	@Autowired
	private WebApplicationContext context;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	
	private MockMvc mockMvc;
	private List<QmUserAnswer> list;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
		QmUserAnswer qmUserAnswer = new QmUserAnswer();
		qmUserAnswer.setUserId(10108);
		qmUserAnswer.setQuestionId(1);
		qmUserAnswer.setAnswerId(1);
		qmUserAnswer.setTextAnswer("test");
		qmUserAnswer.setActionSourceId(1);
		
		QmUserAnswer qmUserAnswer2 = new QmUserAnswer();
		qmUserAnswer2.setUserId(10108);
		qmUserAnswer2.setQuestionId(8);
		qmUserAnswer2.setAnswerId(41);
		qmUserAnswer2.setTextAnswer("test");
		qmUserAnswer2.setActionSourceId(1);
		
		list = new ArrayList<QmUserAnswer>();
		list.add(qmUserAnswer);
		list.add(qmUserAnswer2);
	}
	
	/******** Finotec **********/
	/*@Test
	public void insert() throws Exception {
		logger.debug("insert qmUserAnswer");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername("junit.with.money@anyoption.com");	
		Request<List<QmUserAnswer>> request = new Request<List<QmUserAnswer>>(list, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/regulation/questionnaire/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<RegulationResponse> responseobject = (Response<RegulationResponse>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<RegulationResponse>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert QmUserAnswer ", e);
			throw e;
		}
	}*/
	
	/******** ODT **********/
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insert() throws Exception {
		logger.debug("insert qmUserAnswer");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userExist);	
		Request<List<QmUserAnswer>> request = new Request<List<QmUserAnswer>>(list, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/regulation/questionnaire/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert QmUserAnswer ", e);
			throw e;
		}
	}
}
