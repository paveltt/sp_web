package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.PaymentRequest;
import capital.any.model.base.PaymentResponse;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.User;
import capital.any.model.base.Wire;
import capital.any.security.CustemUserDetailService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PaymentControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(PaymentControllerTest.class);
	private static final String DO_WITHDRAW_URL = "/payment/withdraw";
	private static final String CONFIRM_WITHDRAW_URL = "/payment/withdraw/confirm";		
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	@Autowired
	private ObjectMapper mapper;
	private MockMvc mockMvc;
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	public void doWithdraw() throws Exception {
		
		PaymentRequest<Wire> pd = new PaymentRequest<Wire>();
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		
		Wire wire = new Wire();
		wire.setBranchAddress("test Withdraw web BranchAddress ");
		wire.setBankName("test Withdraw BankName");
		wire.setIban("test Withdraw Iban");
		wire.setSwift("test Withdraw Swift");
		wire.setAccountInfo("test Withdraw info");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test Withdraw BeneficiaryName");
		
		pd.setAmount(15000);
		pd.setDetails(wire);
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		pd.setComments("test Withdraw Comments");
		
		Request<PaymentRequest<Wire>> request = new Request<PaymentRequest<Wire>>(pd, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doWithdraw json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_WITHDRAW_URL).with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert Withdraw ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	public void doWithdrawWithLogin() throws Exception {
		Response<User> responseObject1 = null;
		MvcResult result1 = null;
		try {
			result1 =  mockMvc.perform(MockMvcRequestBuilders.post("/rest/security/login-processing")
			        .param("username", "junit-regulated-money@anycapital.com")
			        .param("password", "Jj123456!"))
			        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			logger.info("result " + result1);
			responseObject1 = 
					(Response<User>) mapper
						.readValue(result1
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<User>>(){});
			logger.info("responseObject " + responseObject1);
		} catch (Exception e) {
			logger.info("cant login", e);
		}
		
		PaymentRequest<Wire> pd = new PaymentRequest<Wire>();
		
		Wire wire = new Wire();
		wire.setBranchAddress("test Withdraw web BranchAddress ");
		wire.setBankName("test Withdraw BankName");
		wire.setIban("test Withdraw Iban");
		wire.setSwift("test Withdraw Swift");
		wire.setAccountInfo("test Withdraw info");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test Withdraw BeneficiaryName");
		
		pd.setAmount(20000);
		pd.setDetails(wire);
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		pd.setComments("test Withdraw Comments");
		
		Request<PaymentRequest<Wire>> request = new Request<PaymentRequest<Wire>>(pd, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doWithdraw json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_WITHDRAW_URL).session((MockHttpSession)result1.getRequest().getSession())
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert Withdraw ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	public void confirmWithdrawWithLogin() throws Exception {
		Response<User> responseObject1 = null;
		MvcResult result1 = null;
		try {
			result1 =  mockMvc.perform(MockMvcRequestBuilders.post("/rest/security/login-processing")
			        .param("username", userRegulatedWithMoney)
			        .param("password", "123456"))
			        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			logger.info("result " + result1);
			responseObject1 = 
					(Response<User>) mapper
						.readValue(result1
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<User>>(){});
			logger.info("responseObject " + responseObject1);
		} catch (Exception e) {
			logger.info("cant login", e);
		}
		
		PaymentRequest<Wire> pd = new PaymentRequest<Wire>();
		
		Wire wire = new Wire();
		wire.setBranchAddress("test Withdraw web BranchAddress ");
		wire.setBankName("test Withdraw BankName");
		wire.setIban("test Withdraw Iban");
		wire.setSwift("test Withdraw Swift");
		wire.setAccountInfo("test Withdraw info");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test Withdraw BeneficiaryName");
		
		pd.setAmount(2000);
		pd.setDetails(wire);
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		pd.setComments("test Withdraw Comments");
		Request<PaymentRequest<Wire>> request = new Request<PaymentRequest<Wire>>(pd, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);       
		logger.debug("confirmWithdraw  json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(CONFIRM_WITHDRAW_URL).session((MockHttpSession)result1.getRequest().getSession())
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert Withdraw ", e);
			throw e;
		}
	}
	
}
