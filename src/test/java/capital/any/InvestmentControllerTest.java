package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.Currency;
import capital.any.base.enums.InvestmentTypeEnum;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentMarket;
import capital.any.model.base.InvestmentType;
import capital.any.model.base.Product;
import capital.any.model.base.SellNowRequest;
import capital.any.model.base.User;
import capital.any.security.CustemUserDetailService;
import capital.any.service.base.product.IProductService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class InvestmentControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentControllerTest.class);
	@Autowired
	private CustemUserDetailService custemUserDetailService;	
	@Autowired
	private WebApplicationContext context;	
	private MockMvc mockMvc;	
	@Autowired
	private ObjectMapper mapper;	
	@Autowired
	private IProductService productService;
	
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	@Value("${user.regulated.no.money}")
	private String userRegulatedNoMoney;
	@Value("${user.not.regulated}")
	private String userNotRegulated;
	@Value("${user.not.regulated.limit}")
	private String userRegulatedWithLimitedMoney;
	@Value("${sell.now.product.id}")
	private Long sellNowProductId;
	@Value("${sell.now.bid}")
	private Long sellNowBid;
	@Value("${sell.now.inv1}")
	private Long sellNowInv1;
	@Value("${sell.now.inv2}")
	private Long sellNowInv2;
	
	private long testProductId;
			
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
		
		Map<Long, Product> hm = productService.getProducts();
		try {
			for (Product product : hm.values()) {
				testProductId = product.getId();
				break;
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}
	}
	
	private Investment createInvestment(InvestmentTypeEnum investmentType, ProductStatusEnum productStatusEnum, boolean isSuspend, Currency currncy) {		
		
		Map<Long, Product> hm = productService.getProducts();
		Product productInv = null;
		try {
			for (Product product : hm.values()) {
				if (product.getProductStatus().getId() == productStatusEnum.getId() && product.isSuspend() == isSuspend && product.getCurrencyId() == currncy.getId()) {
					productInv = product;
					break;
				}				
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}
		
		Investment investment = new Investment();
		investment.setOriginalAmount(300000);
		investment.setInvestmentType(new InvestmentType(investmentType.getId()));
		investment.setProductId(productInv.getId());
		investment.setInvestmentMarkets(new ArrayList<InvestmentMarket>());;
		InvestmentMarket investmentMarket = new InvestmentMarket();
		investmentMarket.setMarketId(productInv.getProductMarkets().get(0).getMarket().getId());
		investmentMarket.setPrice(productInv.getProductMarkets().get(0).getMarket().getLastPrice().getPrice());
		
		investment.getInvestmentMarkets().add(investmentMarket);
		
		if (investmentType == InvestmentTypeEnum.SECONDARY) {
			investment.setAsk(productInv.getAsk());
			investment.setBid(productInv.getBid());
		}
		return investment;
	}
	
	@Test
	@Transactional
	public void insertInvestmentSubscription() throws Exception {
		logger.debug("insert Investment");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		
		Request<Investment> request = new Request<Investment>(createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Investment> responseobject = (Response<Investment>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Investment>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void insertInvestmentSubscriptionWithLogin() throws Exception {
		logger.debug("insert Investment");
		MvcResult loginResult = doLogin();	
		
		Request<Investment> request = new Request<Investment>(createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").session((MockHttpSession)loginResult.getRequest().getSession())
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Investment> responseobject = (Response<Investment>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Investment>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void insertInvestmentSecondary() throws Exception {
		logger.debug("insert Investment");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		
		Request<Investment> request = new Request<Investment>(createInvestment(InvestmentTypeEnum.SECONDARY, ProductStatusEnum.SECONDARY, false, Currency.EURO), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Investment> responseobject = (Response<Investment>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Investment>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void insertInvestmentNotLogin() throws Exception {
		logger.debug("insert Investment not login");
		Request<Investment> request = new Request<Investment>(createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert")
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.ACCESS_DENIED, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment not login", e);
			throw e;
		}
	}
	
	@Test
	public void getOpenInvestmentByUserId() throws Exception {
		logger.debug("get Investment by user id");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/get/open").with(user(userDetails)))							
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Map<String, Object>>> responseobject = (Response<List<Map<String, Object>>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Map<String, Object>>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get Investment by user id ", e);
			throw e;
		}
	}
	
	@Test
	public void getCloseInvestmentByUserId() throws Exception {
		logger.debug("get Investment by user id");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/get/close").with(user(userDetails)))							
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Map<String, Object>>> responseobject = (Response<List<Map<String, Object>>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Map<String, Object>>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get Investment by user id ", e);
			throw e;
		}
	}
	
	//test min inv
	@Test
	@Transactional
	public void insertInvestmentValidateMinInv() throws Exception {
		logger.debug("insert Investment Validate Min Inv");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setOriginalAmount(290000);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_MIN_INV_LIMIT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//test max inv
	@Test
	@Transactional
	public void insertInvestmentValidateMaxInv() throws Exception {
		logger.debug("insert Investment Validate Max Inv(");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setOriginalAmount(99999910);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_MAX_INV_LIMIT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//test ask
	@Test
	@Transactional
	public void insertInvestmentValidateAsk() throws Exception {
		logger.debug("insert Investment Validate Ask");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SECONDARY, ProductStatusEnum.SECONDARY, false, Currency.EURO);
		invesment.setAsk(-1);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_DEVIATION_ASK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//test Suspend
	@Test
	@Transactional
	public void insertInvestmentValidateSuspend() throws Exception {
		logger.debug("insert Investment Validate Suspend");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, true, Currency.EURO);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_PRODUCT_SUSPENDED, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//test INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV
	@Test
	@Transactional
	public void insertInvestmentValidateInsufficientBalanceA() throws Exception {
		logger.debug("insert Investment Validate Insufficient Balance A");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithLimitedMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setOriginalAmount(1000000);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//test INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV
	@Test
	@Transactional
	public void insertInvestmentValidateInsufficientBalanceB() throws Exception {
		logger.debug("insert Investment Validate Insufficient Balance less then min inv");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedNoMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SECONDARY, ProductStatusEnum.SECONDARY, false, Currency.EURO);
		invesment.setOriginalAmount(900000);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void insertInvestmentValidateInsufficientBalancePending() throws Exception {
		logger.debug("insert Investment Validate Insufficient Balance Pending");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setOriginalAmount(900000);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY
	@Test
	@Transactional
	public void insertInvestmentValidateInsufficientBSubscription() throws Exception {
		logger.debug("insert Investment Validate Insufficient limit of one pending investment per product in primary");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedNoMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setOriginalAmount(900000);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			result = mockMvc.perform(
					MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	
	
	//not sending product id
	@Test
	@Transactional
	public void insertInvestmentValidateProductId() throws Exception {
		logger.debug("insert Investment Validate ProductId");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setProductId(0);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	
	//INVESTMENT_ERROR_INVESTMENT_SECONDARY_PRODUCT_PRAIMERY
		@Test
		@Transactional
		public void insertInvestmentValidateInvestmentTypeSubscription() throws Exception {
			logger.debug("insert Investment Validate InvestmentType");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
			Investment invesment = createInvestment(InvestmentTypeEnum.SECONDARY, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
			Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
				Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_INVESTMENT_SECONDARY_PRODUCT_PRAIMERY, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("cant insert Investment ", e);
				throw e;
			}
		}
		
		//INVESTMENT_ERROR_INVESTMENT_PRAIMERY_PRODUCT_SECONDARY
		@Test
		@Transactional
		public void insertInvestmentValidateInvestmentTypePraimery() throws Exception {
			logger.debug("insert Investment Validate InvestmentType");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
			Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SECONDARY, false, Currency.EURO);
			Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
				Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_INVESTMENT_PRAIMERY_PRODUCT_SECONDARY, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("cant insert Investment ", e);
				throw e;
			}
		}
	
	//not sending investment type
	@Test
	@Transactional
	public void insertInvestmentValidateInvestmentType() throws Exception {
		logger.debug("insert Investment Validate InvestmentType");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
		Investment invesment = createInvestment(InvestmentTypeEnum.SUBSCRIPTION, ProductStatusEnum.SUBSCRIPTION, false, Currency.EURO);
		invesment.setInvestmentType(null);
		Request<Investment> request = new Request<Investment>(invesment, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);		
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/insert").with(user(userDetails))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert Investment ", e);
			throw e;
		}
	}
	

		@Test
		public void getProductSumInvestments() throws Exception {
			logger.debug("getProductSumInvestments");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);	
			Product product = new Product();
			product.setId(testProductId);
			Request<Product> request = new Request<Product>(product, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/get/product/sum").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<Map<String, Object>> responseobject = (Response<Map<String, Object>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<String, Object>>>() {});
				Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("cant getProductSumInvestments ", e);
				throw e;
			}
		}
		
		@Test
		@Transactional
		public void sellnow() throws Exception {
			logger.debug("sellnow");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
			SellNowRequest sellNowRequest = new SellNowRequest();
			List<Long> investmentsId = new ArrayList<Long>();
			investmentsId.add(sellNowInv1);
			sellNowRequest.setInvestmentsId(investmentsId);
			Product product = new Product();
			product.setId(sellNowProductId);
			product.setBid(sellNowBid);
			sellNowRequest.setProduct(product);
			
			Request<SellNowRequest> request = new Request<SellNowRequest>(sellNowRequest, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/sell/now").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
				Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("can't sellnow ", e);
				throw e;
			}
		}
		
		//test sell now for tow investments
		@Test
		@Transactional
		public void sellnow2Investments() throws Exception {
			logger.debug("sellnow");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
			SellNowRequest sellNowRequest = new SellNowRequest();
			List<Long> investmentsId = new ArrayList<Long>();
			investmentsId.add(sellNowInv1);
			investmentsId.add(sellNowInv2);
			sellNowRequest.setInvestmentsId(investmentsId);
			Product product = new Product();
			product.setId(sellNowProductId);
			product.setBid(sellNowBid);
			sellNowRequest.setProduct(product);
			
			Request<SellNowRequest> request = new Request<SellNowRequest>(sellNowRequest, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/sell/now").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
				Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("can't sellnow ", e);
				throw e;
			}
		}
		
		@Test
		@Transactional
		public void sellnowValidateBid() throws Exception {
			logger.debug("sellnowValidateBid");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
			SellNowRequest sellNowRequest = new SellNowRequest();
			List<Long> investmentsId = new ArrayList<Long>();
			investmentsId.add(10163L);
			sellNowRequest.setInvestmentsId(investmentsId);
			Product product = new Product();
			product.setId(10135);
			product.setBid(0);
			sellNowRequest.setProduct(product);
			
			Request<SellNowRequest> request = new Request<SellNowRequest>(sellNowRequest, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/sell/now").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
				Assert.assertEquals(ResponseCode.INVESTMENT_ERROR_SELL_NOW_BID, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("can't sellnowValidateBid ", e);
				throw e;
			}
		}
		
		//check that this product is in open for sell now
		@Test
		@Transactional
		public void sellnowValidateProductNotOpen() throws Exception {
			logger.debug("sellnowValidateBid");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
			SellNowRequest sellNowRequest = new SellNowRequest();
			List<Long> investmentsId = new ArrayList<Long>();
			investmentsId.add(95L);
			sellNowRequest.setInvestmentsId(investmentsId);
			Product product = new Product();
			product.setId(1309);
			product.setBid(0);
			sellNowRequest.setProduct(product);
			
			Request<SellNowRequest> request = new Request<SellNowRequest>(sellNowRequest, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/sell/now").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
				Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("can't sellnowValidateBid ", e);
				throw e;
			}
		}
		
		//check that this investment not in open status
		@Test
		@Transactional
		public void sellnowValidateInvestmentNotOpen() throws Exception {
			logger.debug("sellnowValidateBid");
			UserDetails userDetails = custemUserDetailService.loadUserByUsername("chen@etrader.co.il");
			SellNowRequest sellNowRequest = new SellNowRequest();
			List<Long> investmentsId = new ArrayList<Long>();
			investmentsId.add(131L);
			sellNowRequest.setInvestmentsId(investmentsId);
			Product product = new Product();
			product.setId(1292);
			product.setBid(0);
			sellNowRequest.setProduct(product);
			
			Request<SellNowRequest> request = new Request<SellNowRequest>(sellNowRequest, ActionSource.WEB);
			String json = mapper.writeValueAsString(request);
			logger.debug("body: " + json);		
			try {
				MvcResult result = mockMvc.perform(
									MockMvcRequestBuilders.post("/investment/sell/now").with(user(userDetails))
									.contentType(MediaType.APPLICATION_JSON)
									.content(json)
									.accept(MediaType.APPLICATION_JSON))
									.andDo(MockMvcResultHandlers.print())
									.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
				Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
			} catch (Exception e) {
				logger.debug("can't sellnowValidateBid ", e);
				throw e;
			}
		}
		
		@SuppressWarnings("unchecked")
		public MvcResult doLogin() {
			Response<User> responseObject = null;
			MvcResult result = null;
			try {
				result = mockMvc.perform(MockMvcRequestBuilders.post("/rest/security/login-processing")
				        .param("username", userRegulatedWithMoney)
				        .param("password", "Jj123456!"))
				        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				logger.info("result " + result);
				responseObject = 
						(Response<User>) mapper
							.readValue(result
							.getResponse()
							.getContentAsString(),
							new TypeReference<Response<User>>(){});
				logger.info("responseObject " + responseObject);
			} catch (Exception e) {
				logger.info("cant login", e);
			}
			return result;
		}
	
}
