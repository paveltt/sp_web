package capital.any;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.Login;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.User;
import capital.any.service.base.user.IUserService;
import capital.any.service.login.ILoginService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class LoginServiceTest {

	@Autowired
	private ILoginService loginService;
	@Value("${user.id.regulated.with.money}")
	private Long userIdRegulatedWithMoney;
	@Autowired
	private IUserService userService;
	
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
    public void insertLogin() {		
		Login login = new Login();
		login.setUserId(user.getId());
		login.setActionSourceId(ActionSource.WEB.getId());
		MarketingTracking marketingTracking = new MarketingTracking();
		login.setMarketingTracking(marketingTracking);
		loginService.insert(login);
	}
		
}
	
