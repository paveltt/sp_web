package capital.any;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import capital.any.security.CustemUserDetailService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class CustemUserDetailServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(CustemUserDetailServiceTest.class);
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	@Value("${user.exist}")
	private String userExist;
	@Value("${user.not.exist}")
	private String userNotExist;
		
	@Test
    public void userExist() {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userExist);	
		assertNotNull(userDetails); 
		logger.debug(userDetails.toString());
	}
	
	@Test
    public void userNotExist() {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername("userNotExist");
		assertNull(userDetails); 	
	}
	
}
