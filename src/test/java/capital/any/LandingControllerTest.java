package capital.any;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.net.URLDecoder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.LoginRequest;
import capital.any.model.base.User;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class LandingControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(LandingControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService; 
	
	@Value("${user.exist}")
	private String userExist;
	
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();		
	}
	
	@Test
	public void init() throws Exception {
		logger.debug("init");
		/* parameters */
		String userId = "HfrkkxrPgfuuBZFc80sqNQ%3D%3D";
		String email = "n9gH6HvcIQOCIsQxZ2dcEIDgtwNaCmzj%2F9BBXCJ8kX0%3D";
		String campId = "1";
		String pageR = "index";
		/* UrlDecode */
		userId = URLDecoder.decode(userId, "UTF-8");
		email = URLDecoder.decode(email, "UTF-8");
		/* MVC test */
		MvcResult andReturn;
		try {
			andReturn = mockMvc.perform(
				MockMvcRequestBuilders.get("/landing/init")
					.param(IMarketingCampaignService.EMAIL, email)
					.param(IMarketingCampaignService.CAMPAIGN_ID, campId)
					.param(IMarketingCampaignService.PAGE_REDIRECT, pageR))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().is3xxRedirection()).andReturn();
		} catch (Exception e) {
			logger.debug("Problem with init.", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void initWithLogin() throws Exception {
		logger.debug("init");
		/* parameters */
		String userId = "HfrkkxrPgfuuBZFc80sqNQ%3D%3D";
		String email = "n9gH6HvcIQOCIsQxZ2dcEIDgtwNaCmzj%2F9BBXCJ8kX0%3D";
		String campId = "1";
		String pageR = "register";
		/* UrlDecode */
		userId = URLDecoder.decode(userId, "UTF-8");
		email = URLDecoder.decode(email, "UTF-8");
		/* MVC test */
		MvcResult andReturn;
		try {
			andReturn = mockMvc.perform(
				MockMvcRequestBuilders.get("/landing/init")
//					.param(IMarketingCampaignService.EMAIL, email)
					.param(IMarketingCampaignService.CAMPAIGN_ID, campId)
					.param(IMarketingCampaignService.PAGE_REDIRECT, pageR))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().is3xxRedirection()).andReturn();
		} catch (Exception e) {
			logger.debug("Problem with init.", e);
			throw e;
		}
		
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsername(userExist);
		loginRequest.setPassword("123456");
		loginRequest.setUtcOffset(-120);
		
		Request<LoginRequest> request = new Request<LoginRequest>(loginRequest, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/login")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseObject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't login " + e.getMessage());
			throw e;
		}
	}
}
