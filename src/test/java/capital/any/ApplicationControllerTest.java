package capital.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.dto.ApplicationData;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class ApplicationControllerTest {
	
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	@Autowired 
	private ObjectMapper mapper;
		
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
	}
		
	@Test
	public void getApplicationInit() throws Exception {		
		Request<ApplicationData> request = new Request<ApplicationData>(new ApplicationData(), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
			mockMvc.perform(MockMvcRequestBuilders.post("/app/init")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void isBlockedRegister() throws Exception {		
		Request<ApplicationData> request = new Request<ApplicationData>(new ApplicationData(), ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
			mockMvc.perform(MockMvcRequestBuilders.post("/app/restriction")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}	
	
}
	