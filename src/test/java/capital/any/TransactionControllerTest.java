package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Calendar;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.security.CustemUserDetailService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class TransactionControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(TransactionControllerTest.class);
	private MockMvc mockMvc;
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	@Autowired 
	private ObjectMapper mapper;
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;	
	@Value("${user.not.regulated.limit}")
	private String userRegulatedWithLimitedMoney;	
	@Value("${trx.id.reverse.withdraw}")
	private Long trxIdReverseWithdraw;
	

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void reverseWithdrawal() throws Exception {
		Transaction transaction = new Transaction();
		transaction.setId(trxIdReverseWithdraw);
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithLimitedMoney);
		Request<Transaction> request = new Request<Transaction>(transaction, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/withdrawal/reverse")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(userDetails))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Transaction> responseobject = (Response<Transaction>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<Transaction>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant cancelWithdrawal ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionsByUserId() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		SqlFilters filter = new SqlFilters();		
		Request<SqlFilters> request = new Request<SqlFilters>(filter, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.contentType(MediaType.APPLICATION_JSON)
					.with(user(userDetails))
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant getTransactionsByUserId ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getBankingHistory() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		SqlFilters filters = new SqlFilters();
		Request<SqlFilters> request = new Request<SqlFilters>(filters, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/bankingHistory")
					.contentType(MediaType.APPLICATION_JSON)
					.with(user(userDetails))
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant getBankingHistory ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionsByUserIdAndDates() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		SqlFilters filter = new SqlFilters();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -75);
		filter.setFrom(cal.getTime());
		
		Calendar calTo = Calendar.getInstance();
		calTo.add(Calendar.DAY_OF_MONTH, -70);
		filter.setTo(calTo.getTime());
		
		Request<SqlFilters> request = new Request<SqlFilters>(filter, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.contentType(MediaType.APPLICATION_JSON)
					.with(user(userDetails))
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant getTransactionsByUserIdAndDates ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserReverseWithdrawalList() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		Request<Object> request = new Request<Object>(null, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/withdrawal/reverse/list")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(userDetails))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant getUserReverseWithdrawalList ", e);
			throw e;
		}
	}
}
