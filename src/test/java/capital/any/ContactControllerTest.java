package capital.any;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.model.base.Contact;
import capital.any.model.base.Contact.Issue;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class ContactControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(ContactControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	private MockMvc mockMvc;
	@Autowired 
	private ObjectMapper mapper;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();		
	}

	@Test
	@Transactional
	public void insertContactUs() throws Exception {
		String rand = UUID.randomUUID().toString().substring(0,6);
		Contact contact = new Contact();
		contact.setFirstName("test");
		contact.setLastName("test");
		contact.setEmail(rand + "@anycapital.com");		
		contact.setMobilePhone("12345678");
		contact.setLanguageId(2);
		contact.setUtcOffset(0);
		contact.setCountryId(2);
		contact.setComments("test!!!!");
		contact.setIssue(Issue.GENERAL);
		contact.setActionSource(ActionSource.WEB);	
				
		Request<Contact> request = new Request<Contact>(contact, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			mockMvc.perform(
					MockMvcRequestBuilders.post("/contact/insertContactUs")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@Test
	@Transactional
	public void insertContactEmailForm() throws Exception {
		String rand = UUID.randomUUID().toString().substring(0,6);
		Contact contact = new Contact();
		contact.setEmail(rand + "@anycapital.com");		
		contact.setLanguageId(2);
		contact.setCampaignId(1);
		String json = mapper.writeValueAsString(contact);	       
		logger.info("body: " + json);
		try {
			mockMvc.perform(
					MockMvcRequestBuilders.post("/contact/insertContactEmailForm")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@Test
	@Transactional
	public void insertContactShortForm() throws Exception {
		String rand = UUID.randomUUID().toString().substring(0,6);
		Contact contact = new Contact();
		contact.setFirstName("test");
		contact.setLastName("test");
		contact.setEmail(rand + "@anycapital.com");
		contact.setMobilePhone("1231231");
		contact.setLanguageId(2);
		contact.setCountryId(2);
				
		Request<Contact> request = new Request<Contact>(contact, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			mockMvc.perform(
					MockMvcRequestBuilders.post("/contact/insertContactShortForm")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			logger.error("", e);
		}
	}
}
