package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.CommunicationChannel;
import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.ChangePassword;
import capital.any.model.base.ForgotPassword;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.User;
import capital.any.model.base.User.Gender;
import capital.any.model.base.UserToken;
import capital.any.security.CustemUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;
import capital.any.service.base.userToken.IUserTokenService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class UserControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(UserControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	private MockMvc mockMvc;
	@Autowired 
	private ObjectMapper mapper;
	@Autowired
	private IUserService userService;
	@Autowired
	private CustemUserDetailService custemUserDetailService;
	@Autowired
	private IUserTokenService userTokenService;
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService;
	
	@Value("${user.exist}")
	private String userExist;
	
	private User user;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		user = userService.getUserByEmail(userExist);
	}

	@Test
	@Transactional
	public void insertUser() throws Exception {
		String rand = UUID.randomUUID().toString().substring(0,6);
		User user = new User();
		user.setPassword("123456");
		user.setCurrencyId(3);
		user.setFirstName("FirstName junit WEB ");
		user.setLastName("LastName junit WEB");
		user.setStreet("Street junit WEB");
		user.setZipCode("ZC t WEB");
		user.setComments("Comments junit WEB");
		user.setEmail(rand + "web@anycapital.com");
		user.setIp("127.0.0.1");
		user.setTimeBirthDate(new Date(442713600000L));
		user.setIsContactByEmail(1);
		user.setIsContactByPhone(1);
		user.setIsContactBySms(1);
		user.setIsAcceptedTerms(1);
		user.setMobilePhone("12345678");
		user.setLanguageId(2);
		user.setUtcOffset(-120);
		user.setWriterId(-1);
		user.setGender(Gender.NOT_KNOWN);
		//user.setStreetNo("1");
		//user.setUtcOffsetModified("UTC+0");
		//user.setLanguageId(2);
//		user.setUtcOffset("GMT+02:00");
		//user.setCityName("aaaaa");
		//user.setUserAgent(rs.getString("user_agent"));
		//user.setHttpReferer(rs.getString("http_referer"));
		//user.setCountryId(2);
		//user.setWriterId(rs.getInt("writer_id"));
		//user.setAoUserId(rs.getInt("ao_user_id"));
		user.setActionSource(ActionSource.WEB);
		user.setUtcOffsetCreated("GMT+0");
		user.setCountryByPrefix(226);
		
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/signup")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("insertUser WEB junit", e);
		}
	}
	
	@Test
	@Transactional
	public void insertUserStepA() throws Exception {
		User user = new User();
		String rand = UUID.randomUUID().toString().substring(0,6);
		user.setPassword("123456123456111");
		user.setFirstName("F_name junit WEB A");
		user.setLastName("L_name junit WEB A");
		user.setEmail(rand + "web@anycapital.com");
		user.setIp("127.0.0.1");
		user.setMobilePhone("12345678");
		user.setLanguageId(2);
		user.setUtcOffset(0);
		user.setActionSource(ActionSource.WEB);
		user.setUtcOffsetCreated("UTC+0");
		user.setCountryByPrefix(226);
		
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/signup")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("insertUserStepA WEB junit", e);
		}
	}
	
	@Test
	@Transactional
	public void updateUserStepB() throws Exception {
		User user = userService.getUserByEmail(userExist);
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userExist);	
		user.setStreet("test web Street step B");
		user.setZipCode("11111");
		user.setTimeBirthDate(new Date());
		user.setStreetNo("1");
		user.setCityName("test web city step B");
		user.setCountryByUser(226);
		
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/update/stepB").with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("update user step B WEB junit", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailExist() throws Exception {		
		logger.debug("getUserByEmailExist");
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getByEmail")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getUserByEmailExist ", e);
			throw e;
		}
	} 

	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailNotExist() throws Exception {
		logger.debug("getUserByEmailNotExist");
		User user = new User();
		user.setEmail("test111111@test.com");
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getByEmail")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! get user by email not exist ", e);
			throw e;
		}
	} 
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByIdExist() throws Exception {
		logger.debug("getUserByIdExist");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername("junit.inv.test@anyoption.com");	
		Request<User> request = new Request<User>(null, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getById")
					.with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant get user by id ", e);
			throw e;
		}
	} 
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByIdNotExist() throws Exception {
		logger.debug("getUserByIdAccessDenied");
		Request<User> request = new Request<User>(null, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getById")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.ACCESS_DENIED, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant get user by id ACCESS_DENIED ", e);
			throw e;
		}
	} 
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void editUser() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userExist);	
		User user = userService.getUserByEmail(userExist);
		user.setFirstName("lior_was_here");
		
		Request<User> request = new Request<User>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/edit").with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void changePassword() throws Exception {

		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userExist);	
		ChangePassword changePassword = new ChangePassword("123456", "123123", "123123");
		
		Request<ChangePassword> request = new Request<ChangePassword>(changePassword, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/changePassword")
					.with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)		
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void resetPassword() throws Exception {
		Request<String> request = new Request<String>("liors@etrader.co.il", ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)		
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseObject = 
					(Response<User>) mapper
						.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<User>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void forgotPassword() throws Exception {
		ForgotPassword forgotPassword = new ForgotPassword(CommunicationChannel.EMAIL, "eran.levy@anyoption.com");
		Request<ForgotPassword> request = new Request<ForgotPassword>(forgotPassword, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/forgotPassword")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)		
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseObject = 
					(Response<User>) mapper
						.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<User>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void changePasswordLogout() throws Exception {
		ChangePassword changePassword = new ChangePassword("123456", "123456");
		Request<ChangePassword> request = new Request<ChangePassword>(changePassword, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		Map<String,String> requestParams = new HashMap<>();
		UserToken userToken = userTokenService.get(new UserToken("148533533923510095"));
		if (userToken != null) {
			requestParams.put("userId", secureAlgorithmsService.encryptAES(String.valueOf(userToken.getUserId())));
			requestParams.put("token", secureAlgorithmsService.encryptAES(userToken.getToken()));
			try {
				MvcResult result = mockMvc.perform(
						MockMvcRequestBuilders.post("/user/changePasswordLogout")
						.sessionAttr(ISessionService.MAP_FORGOT_PASSWORD_PARAMS, requestParams)
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)		
						.accept(MediaType.APPLICATION_JSON))
						.andDo(MockMvcResultHandlers.print())
						.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				ObjectMapper mapper = new ObjectMapper();
				Response<User> responseObject = 
						(Response<User>) mapper
							.readValue(result
							.getResponse()
							.getContentAsString(),
							new TypeReference<Response<User>>(){});
				Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
			} catch (Exception e) {
				logger.error("", e);
				throw e;
			}
		}
	}
	
	@Test
	@Transactional
	public void acceptTerms() throws Exception {
		User user = userService.getUserByEmail("terms@anycapital.com");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername("terms@anycapital.com");				
		Request<Object> request = new Request<Object>(user, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/update/acceptTerms").with(user(userDetails))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("update user accept terms junit", e);
		}
	}
}
