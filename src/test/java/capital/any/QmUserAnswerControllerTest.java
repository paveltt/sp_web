package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.QmQuestion;
import capital.any.security.CustemUserDetailService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class QmUserAnswerControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(QmUserAnswerControllerTest.class);
	
	@Autowired
	private WebApplicationContext context;
	@Autowired
	private CustemUserDetailService custemUserDetailService;	
	private MockMvc mockMvc;
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void get() throws Exception {
		logger.debug("About to test getting user answers.");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(userRegulatedWithMoney);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/questionnaire/user/answer/get")
				.with(user(userDetails)))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Integer, QmQuestion>> responseobject = (Response<Map<Integer, QmQuestion>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Integer, QmQuestion>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
}
