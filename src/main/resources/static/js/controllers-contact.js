spApp.controller('ContactCtr', ['$rootScope', '$scope', '$http', '$interval', 'SingleSelect', function($rootScope, $scope, $http, $interval, SingleSelect) {
	$scope.filter = {};
	$scope.contactUs = {};
	
	$scope.contactUs.phoneCode = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var phoneCodeList = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			phoneCodeList.push({
				id: key,
				suffixOptionName: '+' + $rootScope.countries[key].phoneCode,
				suffixDisplayName: false,
				name: $scope.getMsgs($rootScope.countries[key].displayName),
				displayName: '+' + $rootScope.countries[key].phoneCode,
				phoneCode: $rootScope.countries[key].phoneCode,
				flagCode: $rootScope.countries[key].a2.toLowerCase()
			});
		}
	}
	$scope.contactUs.phoneCode.fillOptions(phoneCodeList);
	if ($rootScope.userLogout && $rootScope.userLogout.country) {
		$scope.contactUs.phoneCode.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	}
	
	var intervals = [];
	
	$scope.CONTACT_SCREEN_STATES = {
		initial: 1,
		submitted: 2
	}
	
	$scope.state = $scope.CONTACT_SCREEN_STATES.initial;

	$scope.filter.country = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase(),
				supportPhone: $rootScope.countries[key].supportPhone
			});
		}
	}
	$scope.filter.country.fillOptions(countries);
	$scope.filter.country.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	
	$scope.contactUs.issue = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'issue'}, mutateCallback: $scope.getMsgs, noSort: true});
	//$scope.contactUs.issue.fillOptions([{id: 1, name: 'issue 1'}, {id: 2, name: 'issue 2'}]);
	var issueOptions = [];
	for(issueKey in settings.contactUsIssues){
		if(settings.contactUsIssues.hasOwnProperty(issueKey)){
			issueOptions.push(settings.contactUsIssues[issueKey]);
		}
	}
	$scope.contactUs.issue.fillOptions(issueOptions);
	
	$scope.patterns = {
		email: regEx_email,
		charsOnly: regEx_charsOnly,
		phone: regEx_phone
	};
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, textRatio: 0.66, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 280});
	}

	if($rootScope.isMobile === false){
		setAllParalax();
		$(document).on('scroll', setAllParalax);
		$(window).on('resize', setAllParalax);
		
		$bounceInterval = $interval(function(){
			$('.left-right-section .inner-half').each(function(index, el){
				if(isElementInViewport(el)){
					$(el).addClass('visible');
					$(el).addClass($(el).data('animation-name'));
				}
			});
			var counter = 0;
			$('.left-right-section .inner-half').each(function(index, el){
				if($(el).hasClass('visible')){
					counter++;
				}
			});
			if(counter == $('.left-right-section .inner-half').length){
				$interval.cancel($bounceInterval);
			}
		}, 500);
		intervals.push($bounceInterval);
	}
	
	$scope.$on('$destroy', function() {
		intervals.forEach(function(interval) {
			clearInterval(interval);
		});
		$(document).off('scroll', setAllParalax);
		$(window).off('resize', setAllParalax);
	});
	
	$scope.send = function(form) {
		if (form.$invalid) return;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			firstName: $scope.contactUs.firstName,
			lastName: $scope.contactUs.lastName,
			email: $scope.contactUs.email,
			countryId: $scope.contactUs.phoneCode.getId(),
			mobilePhone: $scope.contactUs.mobilePhone,
			issue: $scope.contactUs.issue.getId(),
			languageId: $scope.language.id,
			comments: $scope.contactUs.comments
		};
		$http.post('contact/insertContactUs', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'insertContactUs')) {
					$scope.state = $scope.CONTACT_SCREEN_STATES.submitted;
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
}]);