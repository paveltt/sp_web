var settings = {
	jsonLink: 'anycapital',
	urlPrefix: 'anycapital',
	domain: window.location.host,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	skinId: 2,
	skinIdTexts: 2,
	skinLanguage: 'en',
	documentSizeLimit: 15,//MB
	loggedIn: false,
	serverTime: 0,
	serverOffsetMillis: 0,
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 2,
	loginDetected: false,
	url_params: window.location.search,
	sessionTimeout: (1000*60*31),//31 min
	recaptchaKey:''
}

settings.currencies = {
	2: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.usd',
		currencyLeftSymbol: true,
		currencyName: 'currencies.usd'
	},
	3: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.eur',
		currencyLeftSymbol: true,
		currencyName: 'currencies.eur'
	}
};

settings.CURRENCY_USD = 2;
settings.CURRENCY_EUR = 3;

settings.actionSources = {
	WEB: {
		id: 1,
		name: 'WEB'
	},
	BACKEND: {
		id: 2,
		name: 'BACKEND'
	},
	JOB: {
		id: 3,
		name: 'JOB'
	}
}

settings.productStatuses = {
	SUBSCRIPTION: 2,
	PENDING_SETTLEMENT: 4
}

settings.investmentStatuses = {
	PENDING: 1,
	OPEN: 2,
	SETTLED: 3,
	CANCELED: 4,
	CANCELED_INSUFFICIENT_FUNDS: 5
}

settings.productBarrierTypes = {
	American: 1,
	European: 2
}

settings.marketGroups = {
	Indices: 1,
	Shares: 2,
	Commodities: 3,
	Rates: 4,
	Currencies: 5
}

settings.credentialsView = {
	NONE: 0,
	LOGIN: 1,
	FORGOT_PASSWORD: 2,
	RESET_SUBMIT: 4,
	RESET_FAILED: 5,
	RESET_APPROVED: 6
}

settings.translationParams = {
	'siginificant-risks-link': 'significantRisks',
	'risk-factors-link': 'riskFactors'
}

settings.paymentTypes = {
		BANK_WIRE: {
			id:1,
			name:'BANK_WIRE'
		},ADMIN: {
			id:3,
			name:'ADMIN'
		}
}

settings.contactUsIssues = {
	GENERAL: {
		id: 1,
		name: 'general'
	},
	INVESTMENTS: {
		id: 2,
		name: 'investments'
	},
	TECHNICAL_ISSUES: {
		id: 3,
		name: 'technical-issues'
	},
	REGISTRATION: {
		id: 4,
		name: 'registration'
	},
	DEPOSITS_WITHDRAWALS: {
		id: 5,
		name: 'deposits-withdrawals'
	}
}

var PAGE_LOGIN_ACCESS_TYPES = {
	any: 0,
	loggedOut: 1,
	loggedIn: 2
}

var errorCodeMap = {
	success: 0,
	withdraw_minimum_amount_limitation: 511,
	access_denied: 601,
	authentication_failure: 602,
	account_locked: 605,
	invalid_input: 701,
	invalid_password: 705,
	investment_general_error: 710,
	investment_insufficient_funds_above_minumum: 720,
	investment_complete_registration: 721,
	investment_insufficient_funds_below_minimum_secondary: 711,
	investment_below_minumum: 713,
	investment_above_maximum: 714,
	investment_already_secured: 712,
	investment_suspended: 718,
	investment_unavailable: 715,
	investment_already_issued: 716,
	investment_deviation_secondary_primary: 717,
	investment_deviation: 719,
	sell_now_bid: 730,
	unknown: 999
}

TOAST_TYPES = {
	info: 1,
	error: 2
}

var amountDivideBy100 = true;

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = /^['\\p{L} ]+$/;
var regEx_charsOnly = /^[a-zA-Z]*$/;