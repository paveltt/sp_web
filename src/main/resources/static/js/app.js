if(typeof window.folderPrefix == 'undefined'){
	var folderPrefix = './html/templates/';
}
if(typeof window.indexTemplate == 'undefined'){
	var indexTemplate = 'index.html';
}

var spApp = angular.module('spApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'ngCookies',
	'ngTouch',
	'vcRecaptcha',
	'ui.bootstrap',
	'ui.bootstrap.datetimepicker', 
	/*'ui.tinymce',*/ 
	'ui.tree',
	'angularMoment',
	/*'angular-carousel',*/
	'ngScrollbars',
	'slickCarousel',
	'pdfjsViewer',
	'rzModule'
])
.run(['$rootScope', '$state', '$stateParams', '$interval', '$timeout', '$location', '$window', 
	function ($rootScope, $state, $stateParams, $interval, $timeout, $location, $window) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.recaptchaKey = settings.recaptchaKey;
		
		$rootScope.lastRestrictedPage = '';
		
		$rootScope.ready = false;
		
		$rootScope.folderPrefix = folderPrefix;
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			
			if($rootScope.$state.params.ln){
				var languageKey = searchJsonKeyInArray($rootScope.languages, 'code', $rootScope.$state.params.ln);
				if(languageKey != -1){
					$rootScope.language = $rootScope.languages[languageKey];
				}
			}
			
			if($rootScope.ready){
				checkState();
			}else{
				var checkStateWatch = $rootScope.$watch(function(){return $rootScope.ready;}, function(){
					if($rootScope.ready){
						checkStateWatch();
						checkState();
					}
				});
			}
			function checkState(){
				if (toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedIn && !$rootScope.isLogged) {
					//$rootScope.$state.go('ln.login', {ln: $rootScope.$state.params.ln});
					$rootScope.lastRestrictedPage = toState;
					$rootScope.$state.go('ln.please-login', {ln: $rootScope.$state.params.ln, toState: toState.name});
					return;
				}
				if(toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedOut && $rootScope.isLogged){
					$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
					return;
				}
				if (!isUndefined(toState.minUserStepAccessType) && $rootScope.loggedUser.userStep.id < toState.minUserStepAccessType) {
					var fromStateRedirectTo = fromState.redirectTo instanceof Function ? fromState.redirectTo() : fromState.redirectTo;
					if (fromState.url === '^') {
						$state.go('ln.index', {ln: $state.params.ln});
					} else if (fromStateRedirectTo !== toState.name) {
						$state.go(fromState.name, fromParams);
					} else {
						$state.go('ln.index');
					}
					return;
				}
			}
			
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			var redirectTo = $rootScope.$state.current.redirectTo
			var redirectToState = redirectTo instanceof Function ? redirectTo($rootScope) : redirectTo;
			if (!isUndefined(redirectToState)) {
				$state.go(redirectToState, {ln: $rootScope.$state.params.ln}, {location: 'replace'});
			}
			
			if($rootScope.menu){
				$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
			}
			$rootScope.$broadcast('resetGlobalErrorMsg');
			
			//Call GTM
			window['dataLayer'] = window['dataLayer'] || [];
			dataLayer.push({
				event: 'routeChange',
				attributes: {
					route: $location.path()
				}
			});
			$timeout(function(){
				window['dataLayer'].push({
					'gtm.start': new Date().getTime(),
					event: 'gtm.js'
				});
			}, 0);
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				$rootScope.targetState = toState;
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				//$rootScope.closeCopyOpPopup({all:true});
			})
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$provide', '$uiViewScrollProvider', 
	function ($httpProvider, $stateProvider, $urlRouterProvider, $provide, $uiViewScrollProvider) {
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		//delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		
		$httpProvider.interceptors.push(['$rootScope', '$injector', function($rootScope, $injector) {
			return {
				request: function(config) {
					if (config.method === 'POST' && Object(config.data) === config.data) {
						config.data.actionSource = settings.actionSources.WEB.id;
					}
					return config;
				},
				response: function(response) {
					// Logout User if session expired.
					if (response.config.method === 'POST' && $rootScope.isLogged) {
						switch (response.data.responseCode) {
							case errorCodeMap.access_denied: // 601
							case errorCodeMap.authentication_failure: // 602
								$rootScope.isLogged = false;
								$rootScope.setLoggedUser(null);
								// Navigate to allowed page if necessary
								var $state = $injector.get('$state'); // Injecting $state occurs Circular Dependency.
								if($state.current.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedIn){
									setTimeout(function() {
										$state.reload();
									});
								}
								break;
						}
					}
					return response;
				}
			};
		}]);
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		$urlRouterProvider
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
		/*.when('/', '/en/')
		.otherwise('/en/');*/
		.when('/', '/check/')
		.otherwise('/check/');

		var initResponsePromise;
		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: folderPrefix + 'template.html',
				redirectTo: 'ln.index',
				controller: 'spCtr',
				resolve: {
					initResponse: ['$http', function($http) {
						if (!initResponsePromise) {
							initResponsePromise = $http.post('app/init', {});
						}
						return initResponsePromise;
					}],
					getMsgsJsonResponse: ['$http', '$stateParams', 'FiltersService', '$rootScope', 'initResponse', '$cookies', function($http, $stateParams, FiltersService, $rootScope, initResponse, $cookies) {
						return FiltersService.getFilters(['languages'])
							.then(function(Filters) {
								$rootScope.languages = Object.keys(Filters.languages).map(function(key) {
									return Filters.languages[key];
								});
								var initResponseData = initResponse.data.data;
								var stateLangName = $stateParams.ln;
								var cookieLangId = $cookies.get('lang');
								var languageId = 0;
								if(initResponseData['user'] && initResponseData['user']['languageId']){//User is logged in
									languageId = initResponseData['user']['languageId']
								}else if(stateLangName && stateLangName != 'check'){//There is an explicit language in the URL
									languageId = $rootScope.languages.filter(function(language) {
										return language.code == stateLangName;
									})[0].id;
								}else if(cookieLangId){//User has set a cookie, take it's value
									languageId = cookieLangId;
								}else if(stateLangName == 'check' && initResponseData['userLogout'] && initResponseData['userLogout']['country']){//User is logged out, hits root page
									languageId = initResponseData['userLogout']['country']['defaultLanguageId'];
								}else{//Default to en
									languageId = $rootScope.languages.filter(function(language) {
										return language.code == 'en';
									})[0].id;
								}
								$rootScope.language = $rootScope.languages.filter(function(language) {
									return language.id == languageId;
								})[0];
								
								if(!$rootScope.language){//Some mixup happened, we don't have this language, use the first available
									$rootScope.language = $rootScope.languages[0];
									languageId = $rootScope.language.id;
								}
								
								if(stateLangName != $rootScope.language.code){
									$rootScope.$state.go($rootScope.targetState, {ln: $rootScope.language.code}, {location: 'replace'});
									return;
								}
								
								var methodRequest = {
									data: languageId
								};
								return $http.post('message/getMessageResources', methodRequest);
							});
					}]
				}
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: folderPrefix + indexTemplate
					}
				},
				title: '<a href="" ng-href="{{$state.href(\'ln.index\', {ln: language.code})}}" ng-if="!isMobile"><div class="logo"></div></a>',
				metaTitle: 'meta-title-index',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				titleTransclude: true
			})
			.state('ln.please-login', {
				url: '/login',
				params: {
					toState: null,
					credentialsState: null
				},
				views: {
					'main': {
						templateUrl: folderPrefix + 'please-login.html',
						controller: 'LoginCtr'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedOut,
				title: 'login',
				metaTitle: 'meta-title-login',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.register', {
				url: '/register',
				params: {
					toState: null,
					credentialsState: null
				},
				views: {
					'main': {
						templateUrl: folderPrefix + 'sign-up-register.html',
						controller: 'RegisterCtr',
						resolve: {
							restriction: ['$http', function($http) {
								return $http.post('app/restriction', {});
							}]
						}
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedOut,
				title: 'signup',
				metaTitle: 'meta-title-register',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.sign-up', {
				abstract: true,
				views: {
					'main': {
						templateUrl: folderPrefix + 'sign-up-screen.html',
					}
				},
				title: 'signup',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.sign-up.personal-details', {
				url: '/sign-up-personal-details',
				templateUrl: folderPrefix + 'sign-up-personal-details.html',
				controller: 'signUpPersonalDetailsCtrl',
				controllerAs: '$ctrl',
				title: 'personal-details',
				metaTitle: 'meta-title-sign-up-personal-details',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.sign-up.questionnaire', {
				url: '/questionnaire',
				templateUrl: folderPrefix + 'questionnaire.html',
				controller: 'QuestionnaireCtrl',
				controllerAs: '$ctrl',
				title: 'questionnaire',
				metaTitle: 'meta-title-sign-up-questionnaire',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.reset-password', {
				url: '/reset-password?email',
				views: {
					'main': {
						templateUrl: folderPrefix + 'reset-password.html',
						controller: 'resetPasswordCtrl',
						controllerAs: '$ctrl'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'reset-password'
			})
			.state('ln.about', {
				url: '/about',
				views: {
					'main': {
						templateUrl: folderPrefix + 'about.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'about-us',
				metaTitle: 'meta-title-about-us',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.faq', {
				url: '/faq',
				views: {
					'main': {
						templateUrl: folderPrefix + 'faq.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'faq',
				metaTitle: 'meta-title-faq',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.basics', {
				url: '/basics/:id',
				views: {
					'main': {
						templateUrl: folderPrefix + 'basics.html',
						controller: 'BasicsCtrl',
						controllerAs: '$ctrl'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'basics',
				metaTitle: 'meta-title-basics',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.contact-us', {
				url: '/contact-us',
				views: {
					'main': {
						templateUrl: folderPrefix + 'contact-us.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'contact-us',
				metaTitle: 'meta-title-contact-us',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.privacy-policy', {
				url: '/privacy-policy',
				views: {
					'main': {
						templateUrl: folderPrefix + 'privacy-policy.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'privacy-policy',
				metaTitle: 'meta-title-privacy-policy',
				metaDescription: 'meta-description-3',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.terms', {
				url: '/terms',
				views: {
					'main': {
						templateUrl: folderPrefix + 'terms.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'terms',
				metaTitle: 'meta-title-terms',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.security', {
				url: '/security-secured-trading',
				views: {
					'main': {
						templateUrl: folderPrefix + 'security.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'security',
				metaTitle: 'meta-title-security',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.legal-documents', {
				url: '/legal-documents',
				views: {
					'main': {
						templateUrl: folderPrefix + 'legal-documents.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'legal-documentation',
				metaTitle: 'meta-title-legal-documentation',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.products', {
				url: '/products?assets&type&maturity&protection',
				reloadOnSearch: false,
				views: {
					'main': {
						templateUrl: folderPrefix + 'products/products.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				title: 'all-notes',
				metaTitle: 'meta-title-products',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			.state('ln.product', {
				url: '/product/:productId',
				views: {
					'main': {
						templateUrl: folderPrefix + 'products/products-single.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.any,
				mobileConfig: {
					showHeader: false
				},
				metaTitle: 'meta-title-products',
				metaDescription: 'meta-description-1',
				metaKeywords: 'meta-keywords-1'
			})
			//My account
			.state('ln.my-account', {
				url: '/my-account',
				views: {
					'main': {
						templateUrl: folderPrefix + 'my-account/index.html'
					}
				},
				redirectTo: function($rootScope) {
					if (!$rootScope.loggedUser.userStep) return;
					var userStepId = $rootScope.loggedUser.userStep.id;
					switch (userStepId) {
						case 1:
							return 'ln.sign-up.personal-details';
						case 2:
							return 'ln.sign-up.questionnaire';
						case 3:
							return 'ln.my-account.settings.questionnaire';
						case 4:
							return 'ln.my-account.funding.upload-documents';
						case 5:
						default:
							return 'ln.my-account.my-notes';
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.my-notes', {
				url: '/my-notes',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/my-notes.html'
					}
				},
				redirectTo: 'ln.my-account.my-notes.open',
				title: 'my-notes'
			})
			.state('ln.my-account.my-notes.open', {
				url: '/open',
				views: {
					'my-notes': {
						templateUrl: folderPrefix + 'my-account/my-notes-open.html'
					}
				},
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-my-notes-open',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.my-notes.closed', {
				url: '/closed',
				views: {
					'my-notes': {
						templateUrl: folderPrefix + 'my-account/my-notes-closed.html'
					}
				},
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-my-notes-closed',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding', {
				url: '/funding',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/funding.html'
					}
				},
				redirectTo: 'ln.my-account.funding.deposit',
				title: 'funding'
			})
			.state('ln.my-account.funding.upload-documents', {
				url: '/upload-documents',
				templateUrl: folderPrefix + 'my-account/upload-documents.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-upload-documents',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.deposit', {
				url: '/deposit',
				templateUrl: folderPrefix + 'my-account/deposit.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-deposit',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.withdraw', {
				url: '/withdraw',
				templateUrl: folderPrefix + 'my-account/withdraw.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-withdraw',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.cancel-withdraw', {
				url: '/cancel-withdraw',
				templateUrl: folderPrefix + 'my-account/cancel-withdraw.html',
				controller: 'CancelWithdrawCtr',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-cancel-withdraw',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.funding.banking-history', {
				url: '/banking-history',
				templateUrl: folderPrefix + 'my-account/banking-history.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-my-account-funding-banking-history',
				metaDescription: 'meta-description-4',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings', {
				url: '/settings',
				views: {
					'my-account': {
						templateUrl: folderPrefix + 'my-account/settings.html'
					}
				},
				redirectTo: 'ln.my-account.settings.personal-details',
				title: 'my-profile'
			})
			.state('ln.my-account.settings.personal-details', {
				url: '/personal-details',
				templateUrl: folderPrefix + 'my-account/personal-details.html',
				metaTitle: 'meta-title-settings-personal-details',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings.questionnaire', {
				url: '/questionnaire',
				templateUrl: folderPrefix + 'questionnaire.html',
				controller: 'QuestionnaireCtrl',
				controllerAs: '$ctrl',
				minUserStepAccessType: 2,
				metaTitle: 'meta-title-settings-questionnaire',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			})
			.state('ln.my-account.settings.password', {
				url: '/password',
				templateUrl: folderPrefix + 'my-account/password.html',
				minUserStepAccessType: 3,
				metaTitle: 'meta-title-settings-password',
				metaDescription: 'meta-description-2',
				metaKeywords: 'meta-keywords-1',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn //TODO: change to loggedIn
			});
			
			// enable html5Mode for pushstate ('#'-less URLs) //requires base tag in the head
			/*if (settings.isLive) {
				$locationProvider.html5Mode(true).hashPrefix('!');
			}*/
	}]);
	
spApp.config(['$compileProvider', function ($compileProvider) {
	//$compileProvider.debugInfoEnabled(false);
}]);

spApp.config(['ScrollBarsProvider', function (ScrollBarsProvider) {
	ScrollBarsProvider.defaults = {
		autoHideScrollbar: true,
		theme: 'dark',
		scrollButtons: {
			enable: false
		},
		advanced: {
			updateOnContentResize: true
		},
		axis: 'y',
		scrollInertia: 100
	}
}]);


spApp.service('Utils', [function(){
	var parseResponse = function(response){
		if(!response || !response.data){
			return null;
		}
		return response.data.data;
	}
	
	var DateManager = function(params){
		if(!params){
			params = {};
		}
		
		this.openDates = {};
		if(params.openDates && Array.isArray(params.openDates)){
			for(var i = 0; i < params.openDates.length; i++){
				this.openDates[params.openDates[i]] = false;
			}
		}
		this.format = params.format;
		this.onOpenCallback = params.onOpenCallback;
		
		if(!this.format){
			this.format = 'dd/MM/yyyy';
		}
		
		this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00',
			ngModelOptions: {
				timezone: 'UTC'
			}
		};
		
		this.openCalendar = function(e, date){
			if(this.onOpenCallback){
				this.onOpenCallback();
			}
			this.openDates[date] = true;
		}
		
		this.save = function(date){
			return date;
		}
	}
	
	var loadingCounter = [];
	var defaultLoadingContainer = '.content';
	function showLoading(params){
		if(!params){
			params = {};
		}
		if(!params.container){
			params.container = defaultLoadingContainer;
		}
		var container = params.container;
		var globalOverlay = params.globalOverlay;
		loadingCounter.push(params);
		$(params.container).addClass('loading');
		if(globalOverlay){
			$(defaultLoadingContainer).addClass('loading-global');
		}
	}
	function hideLoading(){
		var overlay = loadingCounter.pop();
		var overlaysCount = 0;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i].container == overlay.container){
				overlaysCount++;
			}
		}
		if(overlaysCount == 0){
			$(overlay.container).removeClass('loading');
		}
		
		var hasGlobal = false;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i].globalOverlay){
				hasGlobal = true;
			}
		}
		if(!hasGlobal){
			$(defaultLoadingContainer).removeClass('loading-global');
		}
	}
	
	function handleNetworkError(response) {
		logIt({'type':1,'msg':response.data});
	}
	
	function escapeRegExp(text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
	}
	
	return {
		settings: settings,
		parseResponse: parseResponse,
		DateManager: DateManager,
		escapeRegExp: escapeRegExp,
		showLoading: showLoading,
		hideLoading: hideLoading,
		handleNetworkError: handleNetworkError
	};
}]);

spApp.factory('scrollService', ['$window', function($window) {
	var fnList = [];
	
	$window.addEventListener('scroll', $.throttle(100, function() {
		fnList.forEach(function(fn) {
			fn();
		});
	}));
	
	function subscribe(fn) {
		var index = fnList.indexOf(fn);
		if (index === -1) {
			fnList.push(fn);
		}
	}
	
	function unsubscribe(fn) {
		var index = fnList.indexOf(fn);
		if (index !== -1) {
			fnList.splice(index, 1);
		}
	}
	
	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe
	};
}]);

spApp.factory('PdfPreviewService', ['$uibModal', function($uibModal) {
	var modalInstance;

	function open(url) {
		close();
		modalInstance = $uibModal.open({
			templateUrl: folderPrefix + 'pdfPreview.html',
			controller: ['url', '$uibModalInstance', PdfController],
			controllerAs: '$ctrl',
			windowClass: 'pdf-preview-modal',
			resolve: {
				url: function() {
					return url;
				}
			}
		});
		
		function PdfController(url, $uibModalInstance) {
			var ctrl = this;
			ctrl.pdfUrl = url;
			
			ctrl.pdfContainerClickHandler = function($event) {
				if ($($event.target).hasClass('pdfViewer')) {
					closeModal('overlay');
				}
			};
			
			ctrl.closeBtnClickHandler = function() {
				closeModal('close');
			};
			
			function closeModal(msg) {
				$uibModalInstance.close(msg || 'close');
			}
		}
	}
	
	function close() {
		if (modalInstance) {
			modalInstance.dismiss();
		}
	}
	
	return {
		open: open,
		close: close
	};
}]);

spApp.factory('QuestionnaireService', ['$http', '$q', function($http, $q) {
	function getQuestionnaire(isWithAnswers) {
		var url = isWithAnswers ? '/questionnaire/user/answer/get' : '/questionnaire/question/getAll';
		return questionnaire = $http.post(url, {})
			.then(function(res) {
				var resData = res.data;
				if (resData.responseCode === 0 && Object(resData.data) === resData.data) {
					var questionnaire = Object.keys(resData.data).map(function(key) {
						return resData.data[key];
					});
					return questionnaire;
				}
				return $q.reject(res);
			})
			.catch(function(err) {
				questionnaire = null;
				return $q.reject(err);
			});
	}
	
	function sendQuestionnaire(questionnaire, user) {
		var data = questionnaire.map(function(question) {
			var userId = user.id;
			var questionId = question.id;
			var answerId;
			switch (question.questionTypeId) {
				case 1:
					answerId = parseInt(question.answerId);
					break;
				case 2:
					var answer = question.qmAnswerList.filter(function(a) {
						return a.name.toLowerCase() === "yes" === question.answerId;
					});
					answerId = answer[0].id;
					break;
				case 3:
					answerId = null;
					break;
				case 4:
					answerId = question.qmAnswerListForSelect.model.id;
					break;
				default: 
					answerId = null;
			}
			var textAnswer = '';
			return {
				id: 0,
				userId: userId,
				questionId: questionId,
				answerId: answerId,
				textAnswer: textAnswer,
				actionSourceId: settings.actionSources.WEB.id
			};
		});
		
		var methodRequest = {
			data: data
		};
		return $http.post('/regulation/questionnaire/insert', methodRequest);
	}

	return {
		getQuestionnaire: getQuestionnaire,
		sendQuestionnaire: sendQuestionnaire
	};
}]);

spApp.factory('UserService', ['$http', '$q', '$rootScope', 'Utils', function($http, $q, $rootScope, Utils) {
	var refreshLoggedUserCanceler;
	function refreshLoggedUser() {
		if (!$rootScope.isLogged) return;
		if (refreshLoggedUserCanceler) {
			refreshLoggedUserCanceler.resolve();
		}
		refreshLoggedUserCanceler = $q.defer();
		$http.post('user/getById', {}, {timeout: refreshLoggedUserCanceler.promise})
			.then(function(response) {
				refreshLoggedUserCanceler = null;
				if (response.data.responseCode === 0) {
					$rootScope.setLoggedUser(Utils.parseResponse(response));
				}
			});
	}
	return {
		refreshLoggedUser: refreshLoggedUser
	};
}]);

spApp.factory('SignUpPersonalDetailsService', ['$http', '$rootScope', function($http, $rootScope) {
	function sendPersonalDetails(personalDetails) {
		var day = parseInt(personalDetails.dayOfBirth.day.getTextValue());
		var mounth = parseInt(personalDetails.dayOfBirth.mounth.getId()) - 1;
		var year = parseInt(personalDetails.dayOfBirth.year.getTextValue());
		
		var localBirthDate = new Date(year, mounth, day);
		
		var id = $rootScope.loggedUser.id || 0;

		var timeBirthDate = localBirthDate.getTime();
		var street = personalDetails.street;
		var cityName = personalDetails.cityName;
		var zipCode = personalDetails.zipCode;
		var countryByUser = parseInt(personalDetails.country.getId());
		
		var data = {  
			id: id,
			timeBirthDate: timeBirthDate,
			street: street,
			cityName: cityName,
			zipCode: zipCode,
			countryByUser: countryByUser
		};
		
		var methodRequest = {
			data: data
		};
		
		return $http.post('/user/update/stepB', methodRequest);
	}
	
	return {
		sendPersonalDetails: sendPersonalDetails
	};
}]);

spApp.factory('AuthService', ['$http', '$rootScope', '$q', '$state', 'Utils', 'AgreementModal', 'FiltersService', function($http, $rootScope, $q, $state, Utils, AgreementModal, FiltersService) {
	var rememberMeKey = 'rememberEmail';

	function login(credentials, handleErrors) {
		var methodRequest = {
			data: {
				username: credentials.userName,
				password: credentials.userPass,
				utcOffset: new Date().getTimezoneOffset()
			}
		};
		
		return $http.post('/login', methodRequest).then(function(response) {
			if (!handleErrors(response, 'login')) {
				doLogin(response, credentials);
				return $q.resolve(response);
			} else {
				return $q.reject(response);
			}
		});
	}
	
	function doLogin(response, credentials) {
		var isRemember = credentials ? credentials.rememberMe : false;
		var user = response.data.data;
		
		$rootScope.isLogged = true;
		$rootScope.setLoggedUser(Utils.parseResponse(response));
		
		FiltersService.getFilters(['languages'])
			.then(function(Filters) {
				var languageId = $rootScope.loggedUser.languageId;
				$rootScope.language = $rootScope.languages.filter(function(language) {
					return language.id == languageId;
				})[0];
				
				var languageCode = $rootScope.language.code;

				if (isRemember) {
					// Handle Safari Private Mode
					try {
						localStorage.setItem(rememberMeKey, user.email);
					} catch (e) {
						$log.log('No localStorage');
					}
				} else {
					localStorage.removeItem(rememberMeKey);
				}
				
				redirectAfterLogin(user, languageCode);
				
				if (user.isAcceptedTerms == 0) {
					AgreementModal.open();
				}
			});
	}
	
	function redirectAfterLogin(user, languageCode) {
		if(!languageCode){
			languageCode = $state.params.ln;
		}
		switch (user.userStep.id) {
			case 1:
				var personalDetailsState = 'ln.sign-up.personal-details';
				$state.go(personalDetailsState, {ln: languageCode});
				break;
			case 2:
				var questionnaireState = 'ln.sign-up.questionnaire';
				$state.go(questionnaireState, {ln: languageCode});
				break;
			case 3:
				$state.go('ln.my-account.settings.questionnaire', {ln: languageCode});
				break;
			case 4:
				$state.go('ln.my-account.funding.upload-documents', {ln: languageCode});
				break;
			case 5:
			default:
				$state.go('ln.products', {ln: languageCode});
		}
	}
	
	function logout(handleErrors) {
		return $http.post('logout', {}).then(function(response) {
			if (!handleErrors(response, 'logout')) {
				deauthorize();
				return $q.resolve(response);
			} else {
				return $q.reject(response);
			}
		});
	}
	
	function deauthorize() {
		if (!$rootScope.isLogged) return;
		$rootScope.isLogged = false;
		$rootScope.setLoggedUser(null);
		$state.go('ln.index');
	}
	
	function signup(credentials, handleErrors) {
		var data = {
			firstName: credentials.firstName,
			lastName: credentials.lastName,
			email: credentials.email,
			mobilePhone: credentials.phone,
			password: credentials.userPass,
			countryByPrefix: parseInt(credentials.phoneCode.getId()),
			languageId: $rootScope.language.id,
			isAcceptedTerms: credentials.acceptTerms ? 1 : 0
		};
		var methodRequest = {
			data: data
		};
		return $http.post('/user/signup', methodRequest).then(
			function(response) {
				if (!handleErrors(response, 'signup')) {
					doLogin(response);
					return $q.resolve(response);
				} else {
					return $q.reject(response);
				}
			});
	}
	
	function getRememberMeEmail() {
		return localStorage.getItem(rememberMeKey);
	}
	
	function resetPassword(credentials) {
		var methodRequest = {
			data: credentials.userEmail
		};
		
		return $http.post('/user/resetPassword', methodRequest);
	}
	
	function forgotPassword(credentials) {
		var communicationChannel;
		switch (credentials.via) {
			case "2":
				communicationChannel = "SMS";
				break;
			case "1":
			default:
				communicationChannel = "EMAIL";
		}

		var methodRequest = {
			data: {
				communicationChannel: communicationChannel,
				email: credentials.email
			}
		};
		
		return $http.post('/user/forgotPassword', methodRequest);
	}
	
	return {
		login: login,
		logout: logout,
		deauthorize: deauthorize,
		signup: signup,
		resetPassword: resetPassword,
		forgotPassword: forgotPassword,
		getRememberMeEmail: getRememberMeEmail,
		redirectAfterLogin: redirectAfterLogin
	};
}]);

spApp.service('FiltersService', ['$http', '$q', function($http, $q) {
	var cache = {};
	
	this.getFilters = function(filtersList) {
		var data = filtersList.filter(function(f) {
			return !(f in cache);
		});
		
		if (!data.length) {
			return $q.resolve(cache);
		}
		
		var methodRequest = {
			data: data
		};
		
		return $http.post('filters/get', methodRequest).then(function(res) {
			if (res.data && res.data.responseCode === 0) {
				var filters = res.data.data;
				cache = angular.merge({}, cache, filters);
				return $q.resolve(cache);
			}
			return $q.reject(res);
		});	
	};
}]);

spApp.animation('.slide-animation', function() {
	var defaultDuration = 250;
	function getAnimationDuration($element) {
		return parseInt($element.attr('data-slide-animation-duration')) || defaultDuration;
	}
	return {
		enter: function($element, done) {
			var duration = getAnimationDuration($element);
			var isFade = $element[0].hasAttribute('data-slide-animation-fade');
			$element
				.css('display', 'none')
				.stop(true, true)
				.slideDown(duration, done);
			if (isFade) {
				$element
					.css('opacity', 0)
					.animate({opacity: 1}, {queue: false, duration: duration + 100});
			}
		},
		leave: function(element, done) {
			var $element = $(element);
			var duration = getAnimationDuration($element);
			var isFade = $element[0].hasAttribute('data-slide-animation-fade');
			$element
				.stop(true, true)
				.slideUp(duration, done);
			if (isFade) {
				$element
					.animate({opacity: 0}, {queue: false, duration: duration - 50});
			}
		}
	};
});

// Accepts element or selector
spApp.constant('scrollToElement', function(el, additional) {
	var element = $(el).first();
	if (!element.length) return;
	var menuEl = document.querySelector('.menu');
	var menuBottom = menuEl.offsetTop + menuEl.offsetHeight;
	var additionalOffset = isUndefined(additional) ? 0 : parseInt(additional) || 0;
	$('body, html').stop().animate({
		scrollTop: element.offset().top - menuBottom - additionalOffset
	}, 500);
});

spApp.constant('lockScroll', function(isLock) {
	if (isLock) {
		$('body').addClass('scroll-lock');
	} else {
		$('body').removeClass('scroll-lock');
	}
});

spApp.factory('activeSection', ['$http', '$rootScope', '$q', '$state', 'Utils', function($http, $rootScope, $q, $state, Utils) {
	var currentSection = '';
	
	function get() {
		return currentSection;
	}
	
	function set(name) {
		currentSection = name;
	}
	
	return {
		get: get,
		set: set
	};
}]);

spApp.service('UnderlyingAssetsService', ['$http', '$q', function($http, $q) {
	var cache;
	
	this.getUnderlyingAssets = function() {
		if (cache) {
			return $q.resolve(cache);
		}
		
		return $http.post('market/underlyingAssets/get', {}).then(function(res) {
			if (res.data && res.data.responseCode === 0) {
				cache = res;
				return $q.resolve(cache);
			}
			return $q.reject(res);
		});	
	};
}]);

spApp.factory('AgreementModal', ['$uibModal', function($uibModal) {
	var modalInstance;
	
	function open() {
		close();
		modalInstance = $uibModal.open({
			templateUrl: folderPrefix + 'agreementModal.html',
			controller: ['$uibModalInstance', '$rootScope', '$http', AgreementController],
			controllerAs: '$ctrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'agreement-modal'
		});
		
		function AgreementController($uibModalInstance, $rootScope, $http) {
			var ctrl = this;
			
			ctrl.getMsgs = $rootScope.getMsgs;
			
			ctrl.acceptAgreementSubmitHandler = function(form) {
				if (form.$invalid) return;
				$http.post('/user/update/acceptTerms', {})
				.then(function(res) {
					if (res.data.responseCode === 0) {
						close();
					}
				});
			};
		}
	}
	
	function close() {
		if (modalInstance) {
			modalInstance.dismiss();
		}
	}
	
	return {
		open: open,
		close: close
	};
}]);

spApp.constant('setMetaTitle', function(title) {
	$('title').text(title || '');
});