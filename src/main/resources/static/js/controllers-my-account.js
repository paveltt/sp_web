spApp.controller('MyAccountCtr', ['$scope', function($scope) {
	$scope.isMainMenuVisisbleForMobile = false;
	$scope.toggleMainMenuVisibility = function(isVisisble) {
		if (typeof(isVisisble) === 'boolean') {
			$scope.isMainMenuVisisbleForMobile = isVisisble;
			return;
		}
		$scope.isMainMenuVisisbleForMobile = !$scope.isMainMenuVisisbleForMobile;
	};
}]);

spApp.controller('PersonalDetailsCtr', ['$rootScope', '$scope', '$http', 'Utils', 'SingleSelect', 'ScrollTop', function($rootScope, $scope, $http, Utils, SingleSelect, ScrollTop) {
	$scope.patterns = {};
	
	$scope.isPersonalDetailsChanged = false;
	
	$scope.filter = {};
	$scope.personalDetails = {};
	$scope.dateManager = new Utils.DateManager();
	
	$scope.filter.country = new SingleSelect.InstanceClass({mutateCallback: $scope.getMsgs, isSmart: true, filterPlaceholder: $scope.getMsgs('search')});
	$scope.filter.phoneCode = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	
	var countriesList = [];
	var phoneCodeList = [];
	
	// Add setTimeout to reduce UI response, when user goes to this state.
	$scope.action = 'loading';
	setTimeout(function() {
		Object.keys($rootScope.countries).forEach(function(key) {
			var country = $rootScope.countries[key];
			countriesList.push({id: key, name: country.displayName});
			phoneCodeList.push({
				id: key,
				suffixOptionName: '+' + country.phoneCode,
				suffixDisplayName: false,
				name: $scope.getMsgs(country.displayName),
				displayName: '+' + country.phoneCode,
				phoneCode: country.phoneCode,
				flagCode: country.a2.toLowerCase()
			});
		});
		
		$scope.filter.country.fillOptions(countriesList);
		$scope.filter.phoneCode.fillOptions(phoneCodeList);
		
		getById();
	});

	
	$scope.filter.gender = new SingleSelect.InstanceClass({noSort: true});
	var genders = [{id: 'MALE', name: 'Male'}, {id: 'FEMALE', name: 'Female'}];
	$scope.filter.gender.fillOptions(genders);

	$scope.edit = function(form){
		if (form.$invalid) return;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var personalDetails = $scope.personalDetails;
		var data = angular.copy(personalDetails);
		data.timeBirthDate = personalDetails.timeBirthDate.getTime() - (personalDetails.timeBirthDate.getTimezoneOffset() * 60 * 1000);
		data.gender = $scope.filter.gender.getId();
		data.countryByUser = parseInt($scope.filter.country.getId()) || 0;
		data.countryByUser === -1 ? 0 : data.countryByUser;
		data.countryByPrefix = parseInt($scope.filter.phoneCode.getId()) || 0;
		data.countryByPrefix === -1 ? 0 : data.countryByPrefix;

		var methodRequest = {
			data: data
		};

		$http.post('user/edit', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'edit')) {
					$scope.isPersonalDetailsChanged = true;
				}
				ScrollTop();
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
	
	function getById() {
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		$http.post('user/getById', {}).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getById')) {
					$scope.personalDetails = Utils.parseResponse(response);
					var timeBirthDateLocal = new Date($scope.personalDetails.timeBirthDate);
					$scope.personalDetails.timeBirthDate = new Date(timeBirthDateLocal.getTime() + timeBirthDateLocal.getTimezoneOffset() * 60 * 1000);
					$scope.filter.gender.setById($scope.personalDetails.gender);
					$scope.filter.country.setById($scope.personalDetails.countryByUser || -1);
					$scope.filter.phoneCode.setById($scope.personalDetails.countryByPrefix || -1);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
}]);

spApp.controller('PasswordCtr', ['$scope', '$http', 'vcRecaptchaService', 'ScrollTop', function($scope, $http, recaptcha, ScrollTop) {
	$scope.response = null;
	$scope.widgetId = null;
	
	$scope.setWidgetId = function (widgetId) {
		$scope.widgetId = widgetId;
	};

	$scope.setResponse = function (response) {
		$scope.response = response;
	};

	$scope.cbExpiration = function() {
		$scope.response = null;
		recaptcha.reload($scope.widgetId);
	};
	
	$scope.checkRecaptchaError = function(_form){
		return (_form.$submitted && _form.$error.recaptcha);
	};
	
	$scope.password = {
		currentPassword: '',
		newPassword: '',
		retypeNewPassword: ''
	};
	
	$scope.isPasswordChanged = false;
	
	$scope.submit = function(form) {
		if (form.$invalid) return;
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			currentPassword: $scope.password.currentPassword,
			newPassword: $scope.password.newPassword,
			retypePassword: $scope.password.retypeNewPassword
		};
		$http.post('user/changePassword', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'changePassword')) {
					$scope.isPasswordChanged = true;
				}
				ScrollTop();
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	};
}]);


spApp.controller('BankingHistoryCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$interval', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'Pagination', 'SingleSelect', 'Multiselect', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $interval, $compile, $filter, $uibModal, Utils, Permissions, Pagination, SingleSelect, Multiselect) {
	
	$scope.TRANSACTION_OPERATION_CREDIT = 1;

	var to = new Date();
	var from = new Date();
	from.setMonth(to.getMonth() - 3);
	$scope.filter = {
		from: from,
		to: to
	};
	
	$scope.dateManager = new Utils.DateManager();
	
	$scope.results = [];
	
	$scope.filtered = {};
	$scope.filtered.results = [];
	
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	
	$scope.updatePages = function(){
		$timeout(function(){
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.results.length, $scope.resultsPerPage);
		});
	}
	
	$scope.changePage = function(page){
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.results.length, $scope.resultsPerPage);
	}
	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideSummary: true, hideGroupNavigation: true});
	
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		$scope.getByUserId();
	}
	
	$scope.getByUserId = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId,
			from: $scope.filter.from ? $scope.filter.from.getTime() : null,
			to: $scope.filter.to ? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
		}
		$http.post('transaction/bankingHistory', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getByUserId')) {
					$scope.results = Utils.parseResponse(response);
					$scope.pagination.setPages($scope.page, $scope.results ? $scope.results.length : 0, $scope.resultsPerPage);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
//	$scope.datesFilter = function(result){
//		var hide = false;
//		
//		if($scope.filter.from && result.timeCreated < $scope.filter.from.getTime()){
//			hide = true;
//		}
//		if($scope.filter.to){
//		var toDate = new Date($scope.filter.to);
//			toDate.setDate(toDate.getDate() + 1);
//			if(result.timeCreated > toDate.getTime()){
//				hide = true;
//			}
//		}
//		
//		return !hide;
//	}
	
}]);

spApp.controller('WithdrawCtr', ['$sce', '$rootScope', '$scope', '$http', 'Utils', function($sce, $rootScope, $scope, $http, Utils) {
	$scope.withdraw = {};
	
	$scope.WITHDRAW_SCREEN_STATES = {
		initial: 1,
		waiting_confirmation: 2,
		submitted: 3,
		not_submitted: 4
	}
	
	$scope.state = $scope.WITHDRAW_SCREEN_STATES.initial;
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.submitInitial = function(form){
		if (form.$invalid) return;
		sendWithdraw();
	};
	
	$scope.submitFinal = function() {
		sendWithdraw(true);
	};
	function sendWithdraw(isConfirm) {
		$scope.withdrawError = null;
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var data = {
			details: getDetails(),
			paymentType: { 
				id: Utils.settings.paymentTypes.BANK_WIRE.id
			},
			amount: $scope.formatAmountForDb({amount: $scope.withdraw.withdrawAmount})
		};
		var methodRequest = {
			data: data
		};
		var url = isConfirm ? 'payment/withdraw/confirm' : 'payment/withdraw';
		$http.post(url, methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdraw')) {
					if (isConfirm) {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.submitted;
					} else {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.waiting_confirmation;
						$scope.withdrawalFeeAmount = response.data.data.minimumFeeAmount;
					}
				} else {
					if (isConfirm) {
						$scope.state = $scope.WITHDRAW_SCREEN_STATES.not_submitted;
					} else {
						$scope.withdrawError = response.data;
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	$scope.maxLength = {
		bankName: 20,
		branchNumber: 30,
		withdrawAmount: 9
	};
	$scope.patterns = {
		digits: regEx_digits
	};
	
	function getDetails() {
		return {
			beneficiaryName: $scope.withdraw.beneficiaryName,
			iban: $scope.withdraw.iban,
			swift: $scope.withdraw.swift,
			accountNum: parseInt($scope.withdraw.accountNumber),
			bankName: $scope.withdraw.bankName,
			branchNumber: parseInt($scope.withdraw.branchNumber),
			branchAddress: $scope.withdraw.branchAddress
		};
	}
}]);

spApp.controller('CancelWithdrawCtr', ['$sce', '$rootScope', '$scope', '$http', 'Utils', 'Pagination', function($sce, $rootScope, $scope, $http, Utils, Pagination) {
	$scope.withdrawals = [];
	
	$scope.resultsPerPage = 15;
	$scope.page = 1;
	
	$scope.changePage = function(page){
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.withdrawals.length, $scope.resultsPerPage);
	};

	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideSummary: true, hideGroupNavigation: true});
	
	getWithdrawalReverseList();
	
	function getWithdrawalReverseList() {
		$scope.action = 'loading';
		$http.post('/transaction/withdrawal/reverse/list', {}).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdrawal-reverse')) {
					$scope.withdrawals = response.data.data || [];
					$scope.pagination.setPages($scope.page, $scope.withdrawals ? $scope.withdrawals.length : 0, $scope.resultsPerPage);
				}
			},
			function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			}
		);
	}

	// for(var i = 0; i < 5; i++){
	// 	var d = new Date();
	// 	d.setMonth(d.getMonth() - 1);
	// 	d.setDate(i + 1);
	// 	$scope.withdrawals.push({
	// 		id: i + 1, 
	// 		date: d, 
	// 		description: 'Lorem ipsum dolor sit amet'+ (i + 1) +', consectetur adipisicing elit.', 
	// 		amount: 125000*(i + 1), 
	// 		checked: false, 
	// 		currencyId: 3
	// 	});
	// }
	
	$scope.pagination.setPages($scope.page, $scope.withdrawals.length, $scope.resultsPerPage);
	
	$scope.uncheckOthers = function(id) {
		$scope.withdrawals.forEach(function(withdrawal) {
			if (withdrawal.id !== id && withdrawal.checked) {
				withdrawal.checked = false;
			}
		});
	};
	
	$scope.checkedCount = function(){
		var count = 0;
		for(var i = 0; i < $scope.withdrawals.length; i++){
			if($scope.withdrawals[i].checked){
				count++;
			}
		}
		return count;
	};
	
	$scope.submit = function() {
		$scope.action = 'loading';
		var checkedIds = $scope.withdrawals.reduce(function(idList, withdrawal) {
			if (withdrawal.checked) {
				return idList.concat({id: withdrawal.id});
			}
			return idList;
		}, []);
		var methodRequest = {
			data: checkedIds[0]
		};
		$http.post('/transaction/withdrawal/reverse', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'withdrawal-reverse')) {
					getWithdrawalReverseList();
				}
			},
			function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			}
		);
	};
}]);