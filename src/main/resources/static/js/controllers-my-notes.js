spApp.controller('MyNotesCtr', ['$rootScope', '$scope', '$http', 'Utils', 'ScrollTop', function($rootScope, $scope, $http, Utils, ScrollTop) {
	
	$scope.results = [];
	$scope.products = [];
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		if($rootScope.$state.includes('ln.my-account.my-notes.open')){
			$scope.getNotesOpen();
		}else{
			$scope.getNotesClosed();
		}
	}
	
	$scope.getNotesOpen = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		return $http.post('investment/get/open', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getNotesOpen')) {
					$scope.results = Utils.parseResponse(response);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getNotesClosed = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		$http.post('investment/get/close', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getNotesClosed')) {
					var results = Utils.parseResponse(response);
					$scope.notes = [];
					$scope.products = {};
					for(var i = 0; i < results.length; i++){
						$scope.products[results[i].product.id] = results[i].product;
						for(var j = 0; j < results[i].myNotes.length; j++){
							$scope.notes.push(results[i].myNotes[j]);
						}
					}
					for(key in $scope.products){
						if($scope.products.hasOwnProperty(key)){
							$scope.productLoadTypesConfig($scope.products[key]);
						}
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.sumInvestmentsAmount = function(productGroup){
		var sum = 0;
		
		for(var i = 0; i < productGroup.myNotes.length; i++){
			sum += productGroup.myNotes[i].investment.amount;
		}
		
		return sum;
	}
	
	$scope.investmentSellForAmount = function(productGroup, investment){
		return investment.returnAmount;
	}
	
	$scope.sellForAmount = function(productGroup){
		var sellForAmount = 0;
		
		for(var i = 0; i < productGroup.myNotes.length; i++){
			sellForAmount += $scope.investmentSellForAmount(productGroup, productGroup.myNotes[i].investment);
		}
		
		return sellForAmount;
	}
	
	$scope.isOpenInvestment = function(investment){
		return investment.investmentStatus.id == settings.investmentStatuses.OPEN;
	}
	
	$scope.openSellPopup = function(productGroup, note){
		for(var i = 0; i < $scope.results.length; i++){
			$scope.closeSellPopup($scope.results[i]);
			for(var j = 0; j < $scope.results[i].myNotes.length; j++){
				$scope.closeSellPopup($scope.results[i], $scope.results[i].myNotes[j]);
			}
		}
		if(note){
			note._sellPopupVisible = true;
		}else{
			productGroup._sellPopupVisible = true;
		}
	}
	$scope.closeSellPopup = function(productGroup, note){
		if(note){
			note._sellPopupVisible = false;
		}else{
			productGroup._sellPopupVisible = false;
		}
	}
	
	$scope.openSellErrorPopup = function(productGroup, note, sellError){
		for(var i = 0; i < $scope.results.length; i++){
			$scope.closeSellErrorPopup($scope.results[i]);
			for(var j = 0; j < $scope.results[i].myNotes.length; j++){
				$scope.closeSellErrorPopup($scope.results[i], $scope.results[i].myNotes[j]);
			}
		}
		if(note){
			note._sellError = sellError;
			note._sellErrorPopupVisible = true;
		}else{
			productGroup._sellError = sellError;
			productGroup._sellErrorPopupVisible = true;
		}
	}
	$scope.closeSellErrorPopup = function(productGroup, note){
		if(note){
			note._sellError = '';
			note._sellErrorPopupVisible = false;
		}else{
			productGroup._sellError = '';
			productGroup._sellErrorPopupVisible = false;
		}
	}
	
	$scope.reload = function(){
		$rootScope.$state.reload();
	}
	
	$scope.sell = function(productGroup, note){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading-sell';
		$scope.sellDate = '';
		var investmentsId = [];
		if(note){
			investmentsId.push(note.investment.id);
		}else{
			for(var i = 0; i < productGroup.myNotes.length; i++){
				investmentsId.push(productGroup.myNotes[i].investment.id);
			}
		}
		var methodRequest = {};
		methodRequest.data = {
			product: {
				id: productGroup.product.id,
				bid: productGroup.product.bid
			},
			investmentsId: investmentsId
		}
		$http.post('investment/sell/now', methodRequest).then(
			function(response) {
				var data = Utils.parseResponse(response);
				$scope.action = '';
				$scope.closeSellPopup(productGroup, note);
				
				var sellError = $scope.handleSellErrors(response);
				if(sellError){
					$scope.openSellErrorPopup(productGroup, note, sellError);
				}else{
					if (!$scope.handleErrors(response, 'sell')) {
						if(data.length > 0){
							$rootScope.loggedUser.balance = data[data.length - 1].user.balance;
						}
						$scope.sellDate = new Date();
						$scope.openSellConfirmedPopup(productGroup, note);
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	$scope.handleSellErrors = function(response){
		if(response.data.responseCode == errorCodeMap.invalid_input){
			return 'ok';
		}else if(response.data.responseCode == errorCodeMap.sell_now_bid){
			return 'refresh';
		}else{
			return false;
		}
	}
	
	$scope.openSellConfirmedPopup = function(productGroup, note){
		if(note){
			note._sellConfirmedPopupVisible = true;
		}else{
			productGroup._sellConfirmedPopupVisible = true;
		}
	}
	$scope.closeSellConfirmedPopup = function(productGroup, note){
		if(note){
			note._sellConfirmedPopupVisible = false;
			for(var i = 0; i < productGroup.myNotes.length; i++){
				if(productGroup.myNotes[i] == note){
					productGroup.myNotes.splice(i, 1);
				}
			}
		}else{
			productGroup._sellConfirmedPopupVisible = false;
		}
		if(!note || productGroup.myNotes.length == 0){
			var index = -1;
			for(var i = 0; i < $scope.results.length; i++){
				if($scope.results[i].product.id == productGroup.product.id){
					index = i;
					break;
				}
			}
			if(index > -1){
				$scope.results.splice(index, 1);
			}
		}
		//Wiat for animation finished
		setTimeout(function() {
			$scope.getNotesOpen();
			ScrollTop();
		}, 250);
	}
}]);