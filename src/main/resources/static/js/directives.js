spApp.directive('autoFocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if (attrs.autoFocus) {
				focus(scope.$eval(attrs.autoFocus));
			} else {
				focus(true);
			}
			function focus(condition) {
				if (!condition) return;
				$timeout(function() {
					element[0].focus();
				});
			}
		}
	};
}]);
/*
spApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
});
*/
spApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.$eval(attributes.compareTo);
			};

		scope.$watch(function(){return scope.$eval(attributes.compareTo)}, function() {
				ngModel.$validate();
			});
		}
	};
});

spApp.directive('validCcNum', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
				return value;
			});
		}
	};
});

spApp.directive('validEmail', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validEmail', regEx_email.test(value));
				return value;
			});
		}
	};
});

spApp.directive('remoteValidation', ['$http', function ($http) {
	var remoteValidationValue = 'remoteValidation';
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			var cache = {};
			
			ngModel.$parsers.push(function (value) {
				ngModel.$setValidity(remoteValidationValue, false);
				setTimeout(function() {
					var isValidLocaly = !Object.keys(ngModel.$error).filter(function(key) {return key !== remoteValidationValue;}).length;
					if (isValidLocaly && attrs.remoteValidation) {
						if (value in cache) {
							ngModel.$setValidity(remoteValidationValue, cache[value]);
						} else {
							var url = attrs.remoteValidation;
							var methodRequest = {
								data: value
							};
							$http.post(url, methodRequest).then(function(response) {
								if (response.data && response.data) {
									var isValid = response.data.data;
									cache[value] = isValid;
									ngModel.$setValidity(remoteValidationValue, isValid);
								}
							});
						}
					} else {
						ngModel.$setValidity(remoteValidationValue, true);
					}
				});
				return value;
			});
		}
	};
}]);

spApp.directive('restrictNickname', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				// ngModel.$setValidity('nickname', (regEx_nickname.test(value)));
				value = value.replace(regEx_nickname_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictDigits', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				value = value.replace(regEx_digits_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictFloat', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '.').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('restrictLetters', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				//var regEx_letters_reverse = new RegExp((skinMap[settings.skinId].regEx_letters+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				var regEx_letters_reverse = new XRegExp((regEx_lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				value = value.replace(regEx_letters_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

//Use restrict-currency only with ng-model-options="{updateOn: 'blur'}"
spApp.directive('restrictCurrency', function() {
	return {
		require: 'ngModel',
		scope: {
			currency: '=restrictCurrency'
		},
		link: function(scope, elem, attr, ngModel) {
			$(elem[0]).on('keydown', function(e){
				//Forbid space
				if(e.keyCode == 32){
					return false;
				}
				//Forbid messing with the currency symbol
				if(scope.currency && typeof elem[0].selectionStart != 'undefined'){
					var currencySymbol = scope.currency.currencySymbol;
					var symbolPos = elem[0].value.indexOf(currencySymbol);
					var symbolLength = currencySymbol.length;
					if(symbolPos < 0){
						return true;
					}
					//Allow certain system keys
					if(e.keyCode <= 47 && e.keyCode != 8 && e.keyCode != 46){//Forbid backspace and delete
						return true;
					}
					if(elem[0].selectionStart == symbolPos + symbolLength && e.keyCode == 8){
						return false;
					}
					if(elem[0].selectionStart > symbolPos && elem[0].selectionStart < symbolPos + symbolLength){
						return false;
					}
					if(elem[0].selectionStart == 0 && scope.currency.currencyLeftSymbol){
						return false;
					}
					if(elem[0].selectionStart == elem[0].value && !scope.currency.currencyLeftSymbol){
						return false;
					}
				}
				//Allow system keys
				if(e.keyCode <= 47){
					return true;
				}
				//Allow one dot
				if(e.key == '.' && (elem[0].value.match(/\./g) || []).length == 0){
					return true;
				}
				//Forbid certain keys
				if(e.key == '.' && (elem[0].value.match(/\./g) || []).length > 0){//More than one dot
					return false;
				}else if(e.key != parseInt(e.key)){//Not Integer
					return false;
				}
			});
			$(elem[0]).on('keyup click', function(e){
				if(scope.currency && typeof elem[0].selectionStart != 'undefined'){
					var currencySymbol = scope.currency.currencySymbol;
					var symbolPos = elem[0].value.indexOf(currencySymbol);
					var symbolLength = currencySymbol.length;
					if(symbolPos < 0){
						return true;
					}
					if(elem[0].selectionStart >= symbolPos && elem[0].selectionStart < symbolPos + symbolLength){
						setCaretPosition(elem[0], symbolPos + symbolLength);
					}
				}
			});
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

spApp.directive('device', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				var device = detectDevice();
				if (device != '') {
					$element.addClass('mobile-device');
				}
				$element.addClass(device);
			});
		}
	}
}]);

spApp.directive('compile', ['$compile', function($compile) {
	// directive factory creates a link function
	return function(scope, elem, attrs) {
		scope.$watch(
			function(scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(attrs.compile);
			},
			function(value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				elem.html(value);

				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(elem.contents())(scope);
			}
		);
	}
}]);

spApp.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				if(iAttrs.fallbackSrc != ""){
					angular.element(this).attr("src", iAttrs.fallbackSrc);
					angular.element(this).css("visibility", "visible");
				}else{
				//	angular.element(this).css("visibility", "hidden"); //Causes problems, sometimes, with some markets
				}
			});
		}
	}
	return fallbackSrc;
});

spApp.directive('loading', ['$timeout','$compile', function($timeout, $compile) {
	return {
		restrict: "A",
		link: function(scope,element,attrs,ngCtrl) {
			scope.ready;
			var loader = angular.element('<i class="loading-section-el" ng-class="{dN: ready}" ng-hide="ready" ></i>');
			element.addClass('loading-section');
			element.append($compile(loader)(scope));
		}
	};
}]);

spApp.directive('pagination', ['$rootScope', function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'components/pagination.html', // markup for template
		scope: {
			pagination: '=model'
		},
		link: function(scope, element, attrs) {
			scope.getMsgs = $rootScope.getMsgs;
		}
	};
}]);

spApp.directive('multiselect', ['$document', '$timeout', function($document, $timeout) {
	return {
		restrict: 'E',
		require: '?ngModel',
		templateUrl: folderPrefix + 'components/multiselect.html', // markup for template
		scope: {
			multiselect: '=ngModel',
			ngDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.toggleOpen = function(forceClose){
				ngModel ? ngModel.$setTouched() : '';
				if(!scope.multiselect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.multiselect.visible = true;
					$timeout(function() {
						$(element[0]).find('[data-filter]:eq(0)').focus();
					});
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.multiselect.visible = false;
					scope.multiselect.filterText = '';
					$document.unbind('mousedown', documentClickBind);
				}
			}
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.ngDisabled){
					return;
				}
				
				if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}
			});
			
			scope.$watch(function(){
				return scope.multiselect ? scope.multiselect.getCheckedIds() : null;
			}, function(){
				scope.updateScroll();
			}, true);
			
			scope.updateScroll = function(){
				$timeout(function(){
					if($(element[0]).find('.multiselect-selected-options:eq(0)').height() > $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()){
						scope.showScroll = true;
					}else{
						scope.showScroll = false;
					}
				});
			}
			
			scope.selectedOptionsScrollUp = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() - $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
			
			scope.selectedOptionsScrollDown = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() + $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
		}
	};
}]);

spApp.directive('singleSelect', ['$document', '$timeout', '$rootScope', 'lockScroll', function($document, $timeout, $rootScope, lockScroll) {
	return {
		restrict: 'E',
		require: '?ngModel',
		//templateUrl: $rootScope.isMobile ? folderPrefix + 'components/single-select-mobile.html' : folderPrefix + 'components/single-select.html', // markup for template
		template: '<ng-include src="template()"></ng-include>',
		scope: {
			ngDisabled: '=',
			autoFocusDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.templateName = '';
			scope.template = function(){
				scope.templateName = attrs.template;
				return scope.templateName ? folderPrefix + 'components/single-select-' + scope.templateName + '.html' : folderPrefix + 'components/single-select.html';
			};
			
			scope.getMsgs = $rootScope.getMsgs;
			
			ngModel.$options = {
				allowInvalid: true
			};
			ngModel.$isEmpty = function(value){
				if(!value.model || !value.model.model || !value.model.model.name || value.model.model.id === -1){
					return true;
				}
				return false;
			};
			ngModel.$formatters.push(function(model){
				return {model: model};
			});
			ngModel.$parsers.push(function(viewValue){
				return viewValue.model;
			});
			ngModel.$render = function(){
				scope.singleSelect = ngModel.$viewValue.model;
			};
			scope.selectOption = function(option){
				scope.toggleOpen();
				scope.updateModel(option);
			};
			scope.updateModel = function(option){
				if(scope.ngDisabled){
					return;
				}
				ngModel.$modelValue.setModel(option);
				ngModel.$setViewValue({model: ngModel.$modelValue.clone()});
				ngModel.$commitViewValue();
				ngModel.$render();
			};
			scope.$watch(
				function() {
					return ngModel.$modelValue.model;
				}, 
				function() {
					ngModel.$validate();
				}
			);
			scope.toggleOpen = function(forceClose){
				ngModel.$setTouched();
				if(!scope.singleSelect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.singleSelect.visible = true;
					element.on('transitionend', toggleOpenTransitionEndHandler);
					$document.bind('mousedown', documentClickBind);
					if (attrs.lockScroll === 'true') {
						lockScroll(true);
					}
				}else{
					scope.singleSelect.visible = false;
					scope.singleSelect.filterText = '';
					element.off('transitionend', toggleOpenTransitionEndHandler);
					$document.unbind('mousedown', documentClickBind);
					lockScroll(false);
				}
			};
			
			function toggleOpenTransitionEndHandler() {
				if (scope.autoFocusDisabled) return;
				element.find('[data-filter]:eq(0)').focus();
			}
			
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.singleSelect.isSmart){
					return;
				}
				if(scope.ngDisabled){
					return;
				}
				if(e.keyCode == 38){//Up key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getPreviousOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 40){//Down key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}else if(e.key && !e.ctrlKey){
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOptionByKey(e.key);
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
							return false;
						}
					});
				}
			});
			
			scope.$watch(function(){
				return scope.singleSelect ? scope.singleSelect.options : null;
			}, function(){
				scope.updateModel(ngModel.$modelValue.model);
				scope.updateSize();
			}, true);
			
			scope.updateSize = function(){
				setTimeout(function(){
					var width;
					if($(element[0]).find('[data-options-box]:eq(0)') && $(element[0]).find('[data-options-box]:eq(0)').length > 0){
						var rect = $(element[0]).find('[data-options-box]:eq(0)')[0].getBoundingClientRect();
						if(rect.width){
							width = rect.width;
						}else{
							width = rect.right - rect.left;
						}
					}
					if(width > 0){
						$(element[0]).width(Math.ceil(width));
					}
				});
			};
			
			scope.updateScroll = function(option){
				setTimeout(function(){
					var optionEl = $(element[0]).find('.single-select-options').find('.option:eq(' + option.index + ')');
					if(optionEl.position().top < 0 || (optionEl.position().top + optionEl.outerHeight() > optionEl.offsetParent().height())){
						optionEl.offsetParent().scrollTop(optionEl.offsetParent().scrollTop() + optionEl.position().top);
					}
					optionEl = null;
				});
			};
		}
	};
}]);

spApp.directive('checkLastCallback', ['$parse', function($parse) {
	return {
		restrict: 'A',
		scope: true,
		link: function(scope, element, attrs) {
			if(attrs.checkLastCallback != ''){
				if(scope.$last === true){
					var invoker = $parse(attrs.checkLastCallback);
					invoker(scope)();
				}
			}
		}
	};
}]);

spApp.directive('convertToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(val) {
				return parseInt(val, 10);
			});
			ngModel.$formatters.push(function(val) {
				return '' + val;
			});
		}
	};
});

spApp.directive('numbersOnly', function() {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			ngModelCtrl.$parsers.push(function(text) {
				if (!text) return '';
				var transformedInput = text.replace(/[^\d]/g, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			});
		}
	};
});

spApp.directive('charsOnly', function() {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			ngModelCtrl.$parsers.push(function(text) {
				if (!text) return '';
				var transformedInput = text.replace(/[^a-z]/gi, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			});
		}
	};
});

spApp.directive('unselectable', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
		}
	};
}]);

spApp.directive('ngSrcNoCache', function() {
	return {
		priority: 99,
		link: function(scope, element, attrs) {
			attrs.$observe('ngSrcNoCache', function(ngSrcNoCache) {
				var prefix = '?';
				if(ngSrcNoCache.indexOf('?') != -1){
					prefix = '&';
				}
				ngSrcNoCache += prefix + (new Date()).getTime();
				attrs.$set('src', ngSrcNoCache);
			});
		}
	}
});

spApp.directive('toggableOn', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-on', tAttrs.toggableOn);
		}
	}
}]);
spApp.directive('toggableOff', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-off', tAttrs.toggableOff);
		}
	}
}]);
spApp.directive('toggler', ['$timeout', function($timeout) {
	return {
		restrict: 'E',
		template: '<span></span>',
		scope: {
			onToggle: '='
		},
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element).on('click', function(){
				if($(element).data('isOn')){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
				}else{
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
				}
				toggleTogglerState();
			});
			
			function setDefaultState(){
				if(attrs.defaultState == 'on'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					toggleTogglerState({forceOn: true, preventOnToggle: true});
				}else if(attrs.defaultState == 'off'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					toggleTogglerState({forceOn: false, preventOnToggle: true});
				}
			};
			
			setDefaultState();
			
			function toggle(el, isOn){
				if(isOn){
					$(el).css('display', '');
				}else{
					$(el).css('display', 'none');
				}
			}
			function toggleTogglerState(params){
				if(!params){
					params = {};
				}
				var isOn = $(element).data('isOn');
				if(typeof params.forceOn == 'undefined' && isOn || typeof params.forceOn != 'undefined' && !params.forceOn){
					$(element).removeClass('block-toggler-on');
					$(element).data('isOn', false);
				}else{
					$(element).addClass('block-toggler-on');
					$(element).data('isOn', true);
				}
				if(scope.onToggle && !params.preventOnToggle){
					scope.$apply(function(){
						scope.onToggle(attrs.toggle, !isOn);
					});
				}
			}
		}
	};
}]);

spApp.directive('onKeydown', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var onKeydown = scope.$eval(attrs.onKeydown);
			$(element[0]).on('keydown', function(e){
				for(key in onKeydown){
					if(onKeydown.hasOwnProperty(key)){
						if(e.keyCode == key){
							var func = onKeydown[key];
							scope.$eval(func);
						}
					}
				}
			});
		}
	}
}]);

spApp.directive('onEnter', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('keydown', function(e){
				if(e.keyCode == 13){
					scope.$eval(attrs.onEnter);
				}
			});
		}
	}
}]);


//This directive is used to force UTC for the datepicker (otherwise there are problems with daylight saving time and wrong day)
spApp.directive('datepickerTimezone', function () {
	return {
		restrict: 'A',
		priority: 1,
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {
			ctrl.$formatters.push(function (value) {
				var date = new Date(Date.parse(value));
				date = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
				//if (!dateFormat || !modelValue) return "";
				//var retVal = moment(modelValue).format(dateFormat);
				return date;
			});

			ctrl.$parsers.push(function (value) {
				var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
				return date;
			});
		}
	};
});



spApp.directive('smartPlaceholder', ['$compile', function($compile) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
			var posTopSmallDefault = scope.isMobile ? 0 : '3px';
			//Wrap the element
			var wrapper = angular.element('<div class="smart-placeholder-wrapper"><div class="smart-placeholder-text">{{placeholderText}}</div><i></i></div>');
			var smartPlaceholderText = wrapper.find('.smart-placeholder-text');
			$compile(wrapper)(scope);
			element.after(wrapper);
			wrapper.find('i').before(element);
			
			var posTopSmall = posTopSmallDefault;
			
			//Watch for changes
			scope.$watch(function(){return scope.$eval(attrs.smartPlaceholder);}, function(newValue){
				smartPlaceholderText.text(newValue);
			});
			scope.$watch(function(){return scope.$eval(attrs.placeholderTop);}, function(top){
				posTopSmall = top ? top : posTopSmallDefault;
			});
			
			scope.$watch(function(){return element.val() ? true : false;}, function(forceClear){
				togglePlaceholder(forceClear);
			});			
			
			//Handle focus/blur
			element.on('focus', function(e){
				togglePlaceholder(true);
				$(wrapper).addClass('focused');
			});
			element.on('blur', function(e){
				togglePlaceholder();
				$(wrapper).removeClass('focused');
			});

			function togglePlaceholder(forceClear){
				setTimeout(function(){
					var clear = false;
					if(forceClear){
						clear = true;
					}else{
						if(element[0].value == ''){
							clear = false;
						}else{
							clear = true;
						}
					}
					if(clear){
						$(wrapper).find('.smart-placeholder-text').addClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTopSmall);
					}else{
						var posTop = parseInt(element.css('padding-top')) + parseInt(element.css('margin-top'));
						$(wrapper).find('.smart-placeholder-text').removeClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTop);
					}
				}, 0);
			}
			
			//Cleanup
			scope.$on("$destroy", function() {
				wrapper.after(element);
				wrapper.remove();
			});
		}
	};
}]);

spApp.directive('credentials', [function() {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'credentials.html',
		scope: {
			viewState: '=',
			hideLoginHeader: '=',
			showTitles: '=',
			afterLogin: '='
		},
		controller: 'CredentialsCtr'
	};
}]);

spApp.directive('mathjaxBind', function() {
	return {
		restrict: 'A',
		controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			$element.addClass('loading');
			$scope.$watch($attrs.mathjaxBind, function(value) {
				var $script = angular.element('<script type="math/tex; mode=display">').html(value == undefined ? '' : value);
				$element.html('');
				$element.append($script);
				MathJax.Hub.Queue(['Reprocess', MathJax.Hub, $element[0]], [removeLoading, $element[0]]);
				function removeLoading(el){
					$(el).removeClass('loading');
				}
			});
		}]
	};
});

spApp.constant('ScrollTop', function() {
	$('html, body').stop().animate({scrollTop: 0}, 500, 'swing');
});
spApp.directive('scrollUpArrow', ['$window', 'ScrollTop', function($window, ScrollTop) {
	return {
		restrict: 'E',
		link: function(scope, element, attrs, ngModel) {
			$(window).on('scroll resize', $.throttle(50, function(e) {
				if($(window).scrollTop() > 0){
					$(element[0]).addClass('visible');
				}else{
					$(element[0]).removeClass('visible');
				}
			}));
			$(element[0]).on('click', ScrollTop);
		}
	};
}]);

spApp.directive('animateHashScrolls', ['$window', function($window) {
	return {
		restrict: 'A',
		priority: 100,
		link: function(scope, element, attrs, ngModel) {
			//scope.$watch(function(){return element[0].childNodes.length;}, function(newValue, oldValue){
			scope.$watch(function(){return element[0].innerHTML;}, function(newValue, oldValue){
				//if(newValue !== oldValue){
					element.find('a').each(function(index, el){
						//if($(el).attr('href').substring(0, 1) == '#'){
						if($(el).attr('href').indexOf('#') != -1){
							var hashArr = $(el).attr('href').split('#');
							var link = hashArr[hashArr.length - 1];
							$(el).on('click', function(e){
								e.preventDefault();
								var target = document.getElementById(link);
								if(target){
									$('html, body').stop().animate({scrollTop:$(target).offset().top - 100}, '500', 'swing');
								}
								target = null;
								return false;
							});
						}
					});
				//}
			});
		}
	};
}]);

spApp.directive('currentTime', ['dateFilter', function(dateFilter) {
	return function(scope, element, attrs) {
		var format = 'HH:mm dd/MM/y';
		var stopTime = setInterval(updateTime, 1000);
		
		function updateTime() {
			element.text(dateFilter(new Date(), format));
		}
		
		element.on('$destroy', function() {
			clearInterval(stopTime);
		});
	}
}]);

spApp.directive('fitToParentFont', ['$window', function($window) {
	return function(scope, element, attrs) {
		var fontTarget = null;
		var dataFitToParentFontTargetElement = $(element).find('[data-fit-to-parent-font-target]')[0];
		if(dataFitToParentFontTargetElement){
			fontTarget = dataFitToParentFontTargetElement;
		}
		scope.$watch(function(){return element[0].innerHTML.length;}, function(){
			fixFontSize(element[0], fontTarget);
			setTimeout(function(){
				fixFontSize(element[0], fontTarget);
			});
		});
		
		$window.addEventListener('resize', resizeHandler, true);
		scope.$on('$destroy', function(){
			$window.removeEventListener('resize', resizeHandler, true);
		});
		
		function resizeHandler(e){
			fixFontSize(element[0], fontTarget);
		}
	}
}]);

//<div click-outside="currentCtrl.actionFunction()">Some content</div>
spApp.directive('clickOutside', ['$document', function($document) {
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, el, attrs) {
			$document[0].addEventListener('click', clickOutsideHandler, true);
			scope.$on('$destroy', function(){
				$document[0].removeEventListener('click', clickOutsideHandler, true);
			});

			function clickOutsideHandler(e) {
				if (el !== e.target && !el[0].contains(e.target)) {
					scope.$applyAsync(function () {
						scope.$eval(attrs.clickOutside);
					});
				}
			}
		}
	};
}]);

spApp.directive('makeVisibleWhenInViewport', ['scrollService', function(scrollService) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs) {
			var $el = $(el);
			var timeoutId;
			var timeoutIteration = 0;
			var timeoutMaxIteration = 30;

			function makeElementVisible() {
				if (isElementInViewport(el)) {
					$el.addClass('visible');
					//After element become visible no need to watch if it in the Viewport
					scrollService.unsubscribe(makeElementVisible);
					clearTimeout(timeoutId);
				} else if (timeoutIteration < timeoutMaxIteration) {
					timeoutIteration++;
					timeoutId = setTimeout(makeElementVisible, 400);
				}
			}
			
			scrollService.subscribe(makeElementVisible);

			//After element apperas we should check if it in the Viewport
			setTimeout(makeElementVisible);
			
			scope.$on('$destroy', function() {
				scrollService.unsubscribe(makeElementVisible);
				clearTimeout(timeoutId);
			});
		}
	};
}]);

spApp.directive('centerOnScreen', ['$window', function($window) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs) {
			if(scope.$eval(attrs.centerOnScreen)){
				var $el = $(el);
				function centerOnScreen(){
					$el.css('right', 'auto');
					$el.css('left', '0px');
					setTimeout(function(){
						var offset = $el.offset();
						var width = $el.outerWidth();
						var screenWidth = $(window).width();
						
						var leftPos = (screenWidth - width)/2 - offset.left;
						
						$el.css('left', leftPos);
					}, 0);
				}
				centerOnScreen();
				
				$window.addEventListener('resize', resizeHandler, true);
				scope.$on('$destroy', function(){
					$window.removeEventListener('resize', resizeHandler, true);
				});
				
				function resizeHandler(e){
					centerOnScreen();
				}
			}
		}
	};
}]);

spApp.directive('remainingTime', ['$rootScope', function($rootScope) {
	return {
		restrict: 'A',
		scope: {
			remainingTime: '='
		},
		link: function (scope, el) {
			var $el = $(el);
			
			//Show remaining time immediately
			$el.text(formatRemainingTime(scope.remainingTime));
			
			//Update remaining time every second
			var intervalId = setInterval(function() {
				$el.text(formatRemainingTime(scope.remainingTime));
			}, 1000);
			
			scope.$on('$destroy', function() {
				clearInterval(intervalId);
			});
			
			function formatRemainingTime(subscriptionEndDate){
				//Consider using this module for timers: https://github.com/siddii/angular-timer
				var current = new Date();
				var end = new Date(subscriptionEndDate);
				var diff = new Date(end - current);
				
				var offset = diff.getTimezoneOffset()/60
				
				diff.setHours(diff.getHours() + offset);
				
				if((end - current) < 0){
					return '';
				}
				
				var days = 0;
				for(var i = 0; i <= end.getUTCFullYear() - current.getUTCFullYear(); i++){
					for(j = 0; j < 12; j++){
						if(i == 0 && j < current.getUTCMonth()){
							continue;
						}
						if(i == (end.getUTCFullYear() - current.getUTCFullYear()) && j > end.getUTCMonth()){
							break;
						}
						days += new Date(current.getUTCFullYear() + i, j + 1, 0).getDate();
					}
				}
				days -= current.getUTCDate();
				days -= new Date(end.getUTCFullYear(), end.getUTCMonth() + 1, 0).getDate() - end.getUTCDate() + 1;
				return (days > 0 ? days + 'd' + ' ' : '') + $rootScope.dateFillToTwoDigits(diff.getHours()) + ':' + $rootScope.dateFillToTwoDigits(diff.getMinutes()) + ':' + $rootScope.dateFillToTwoDigits(diff.getSeconds());
			}
		}
	};
}]);

spApp.directive('appendToBody', [function() {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, el, attrs){
			var appendToBodyClass = 'append-to-body-wrapper';
			var parent = null;
			var originalParent = el.parent()[0];
			var next = el.next()[0];
			
			scope.$watch(function(){return attrs.appendToBody == '' || scope.$eval(attrs.appendToBody);}, function(newValue, oldValue){
				if(newValue){
					append();
				}else{
					cleanup();
				}
			});
			
			function append(){
				parent = document.createElement('div');
				parent.className = appendToBodyClass;
				$('body').append(parent);
				$(parent).append(el);
			}
			
			function cleanup(){
				if(parent){
					if(next){
						el.insertBefore(next);
					}else if(originalParent){
						$(originalParent).append(el);
					}else{
						el.remove();
					}
					parent.remove();
				}
			}
			
			scope.$on('$destroy', function() {
				cleanup();
			});
		}
	};
}]);

spApp.directive('autoScrollLeft', [function() {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, $parent, attrs) {
			var childSelector = attrs.autoScrollLeft || 'li';
			
			setTimeout(function () {
				var activeClass = attrs.autoScrollLeftActiveElementSelector || '.active';
				var $activeChildrenElement = $parent.find(activeClass);
				autoScroll($activeChildrenElement);
			});
			
			$parent.on('click', childSelector, function() {
				var $children = $(this);
				autoScroll($children);
			});
			
			function autoScroll($children) {
				if (!$children.length) {
					return;
				}
				var childLeftPos = $children.offset().left;
				var childWidth = $children.outerWidth();
				var parentWidth = $parent.outerWidth();
				var parentScroll = $parent.scrollLeft();
				
				var scrollLeft = parentScroll + childLeftPos + childWidth / 2 - parentWidth / 2;
				
				$parent.animate({scrollLeft: scrollLeft}, 750);
			}
		}
	};
}]);

spApp.directive('ngParallax', ['$window', function ($window) {
	return {
		restrict: 'AE',
		scope:{
			pattern: '=',
			speed: '='
		},
		link: function(scope, elememnt) {
			var el = elememnt[0];
			el.style.backgroundRepeat = "no-repeat";
			el.style.height = "100%";
			el.style.margin = "0 auto";
			el.style.position = "relative";
			el.style.background = "url(" + scope.pattern + ")";
			// el.style.backgroundAttachment = 'fixed';
			
			execute();
			
			$window.addEventListener("scroll", execute);
			scope.$on('destroy', function() {
				$window.removeEventListener("scroll", execute);
			});
			
			function execute() {
				var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
				var speed = (scrollTop / scope.speed);
				console.log(speed);
				if(speed == 0){
					el.style.backgroundPosition = 'center '+ 0 + '%';
				} else {
					el.style.backgroundPosition = 'center '+ (0 + speed) + '%';
					// el.style.transform = 'translateY(-'+ scrollTop / 20 +'px)';
				}
			}
		},
	};
}]);

spApp.directive('rzSliderDecorator', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var rzSliderOptions = scope.$eval(attrs.rzSliderOptions);
			var barrierValue = rzSliderOptions && rzSliderOptions.ticksArray && rzSliderOptions.ticksArray[0];
			setTimeout(function() {
				var $barrierAreaHolder = element.find('.rz-tick').last();
				if (!$barrierAreaHolder.length) {
					return;
				}
				var barrierAreaHolderRight = parseInt($barrierAreaHolder.css('transform').split(/[()]/)[1].split(', ')[4]);
				var marginLeft = parseInt($barrierAreaHolder.css('margin-left'));
				var $barrierArea = $('<div class="barrier-area"></div>');
				if (barrierValue < 0) {
					var barrierAreaWidth = barrierAreaHolderRight + marginLeft;
					$barrierArea.addClass('negativ').css('width', barrierAreaWidth);
				}
				var $barrierOccuredLabel = $('<div class="barrier-occured-label"></div>');
				$barrierAreaHolder
					.addClass('barrier-area-holder')
					.css('right', barrierAreaHolderRight)
					.prepend($barrierArea)
					.children('.rz-tick-value')
					.append($barrierOccuredLabel);
					
				attrs.$observe('rzSliderDecoratorBarrierOccuredLabel', function(newValue) {
					$barrierOccuredLabel.text(scope.$eval(newValue));
				});
			});
		}
	};
}]);

spApp.directive('scrollElementTop', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var additionalOffset = parseInt(attrs.scrollElementTopExtra) || 0;
			element.on('click', function() {
				var offset = additionalOffset
				if (scope.isMobile) {
					offset = additionalOffset * $(window).outerWidth() / 640;
				}
				var menuEl = document.querySelector('.menu');
				var menuBottom = menuEl.offsetTop + menuEl.offsetHeight;
				$('body, html').animate({
					scrollTop: element.offset().top + element.outerHeight() - menuBottom + offset
				}, 500);
			});
		}
	};
}]);

spApp.directive('bindHeight', ['makeSameHeight', function (makeSameHeight) {
	return {
		restrict: 'A',
		link: function ($scope, element) {
			// makeSameHeight();
			element.on('DOMSubtreeModified', $.debounce(10, makeSameHeight));
		}
	};
}]);

spApp.constant('makeSameHeight', function () {
	$('[bind-height]').each(function () {
		doForThisElem(this);
	});
	function doForThisElem(me) {
		var value = $(me).attr('bind-height');
		var elems = $('[bind-height="' + value + '"]');
		var heights = elems.toArray().map(function (elem) { return $(elem).height(); });
		if (Math.max.apply(me, heights) > Math.min.apply(me, heights)) $(me).height(Math.max.apply(me, heights));
	}
});

spApp.directive('basicsGlossary', function() {
	return {
		restrict: 'E',
		scope: true,
		templateUrl: folderPrefix + 'basics-glossary.html',
		controller: ['$rootScope', function($rootScope) {
			var ctrl = this;
			
			ctrl.getMsgs = $rootScope.getMsgs;
			ctrl.alphabet = [];
			ctrl.glossaryList = [];
			
			var defaultLimit = $rootScope.isMobile ? 10 : null;
			
			ctrl.visibleDefinitions = {
				increment: 5,
				limit: defaultLimit
			};
			
			$rootScope.$watch("getMsgs('glossary-content')", function(glossary) {
				$(glossary).find('li').each(function(index) {
					var $li = $(this);
					var title = $li.find('strong').text();
					var description = $li.find('span').text();
					ctrl.glossaryList.push({
						id: index,
						title: title,
						description: description
					});
				});
			});
			
			$rootScope.$watch("getMsgs('alphabet-list')", function(alphabet) {
				if (/^\?.*\?$/i.test(alphabet)) return;
				ctrl.alphabet = alphabet.split('');
			});
			
			$rootScope.$watch('isMobile', function(isMobile) {
				ctrl.isMobile = isMobile;
			});
			
			ctrl.activeLetter = '';
			ctrl.toggleActiveLetter = function(letter) {
				if (ctrl.activeLetter === letter) {
					ctrl.activeLetter = '';
				} else {
					ctrl.activeLetter = letter;
				}
				ctrl.visibleDefinitions.limit = defaultLimit;
			};
			
			ctrl.startsWithActiveLetter = function(actual) {
				var lowerStr = actual.title.toLowerCase();
				return lowerStr.indexOf(ctrl.activeLetter) === 0;
			};
			
			ctrl.loadMore = function() {
				if (ctrl.visibleDefinitions.limit >= ctrl.glossaryList.length) return;
				ctrl.visibleDefinitions.limit += ctrl.visibleDefinitions.increment;
			};
		}],
		controllerAs: '$ctrl'
	};
});

spApp.directive('fitMobileCharts', function() {
	return {
		restrict: 'A',
		scope: false,
		link: {
			post: function(scope, element) {
				if (scope.isMobile) {
					setTimeout(setZoom);
					$(window).on('resize', setZoom);
					scope.$on('$destroy', function() {
						$(window).off('resize', setZoom);
					});
				}
				
				function setZoom() {
					var svgWidth = element.width();
					var maxWidth = element.parent().width();
					var ratio = maxWidth / svgWidth;
					element.css('zoom', ratio);
				}
			}
		}
	};
});
