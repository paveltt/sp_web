spApp.controller('spCtr', ['$sce', '$rootScope', '$scope', '$http', '$state', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', '$interval', '$document', '$window', 'Utils', 'Permissions', 'MenuTree', 'initResponse', 'getMsgsJsonResponse', 'AuthService', 'UserService',
					function($sce, $rootScope, $scope, $http, $state, $templateCache, $timeout, $compile, $filter, $uibModal, $interval, $document, $window, Utils, Permissions, MenuTree, initResponse, getMsgsJsonResponse, AuthService, UserService
	) {
	//Expose the static methods of the service to the template
	$scope.MenuTree = {};
	$scope.MenuTree.toggleNode = MenuTree.InstanceClass.toggleNode;
	
	//Translation functions
	$rootScope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($rootScope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $rootScope.msgsParam($rootScope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($rootScope.msgsParam($rootScope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($rootScope.msgsParam($rootScope.msgs[key], params));
					// return $rootScope.msgsParam($rootScope.msgs[key], params);
				} else {
					return $rootScope.msgs[key];
				}
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key + "]?";
			}
		} else {
			if(key == null){
				return params && params.hideMissing ? '' : "?msgs[null]?";
			}else if ($rootScope.msgs.hasOwnProperty(key[0])) {
				if ($rootScope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($rootScope.msgsParam($rootScope.msgs[key[0]][key[1]], params));
					} else {
						return $rootScope.msgs[key[0]][key[1]];
					}
				} else {
					return params && params.hideMissing ? '' : "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	$rootScope.isTranslated = function(str, checkEmpty){
		return str.indexOf('?msgs[') == -1 && (!checkEmpty ? true : str != '');
	}
	
	//get msg with param
	$rootScope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}
	
	
	//Common screen controllers functions
	$rootScope.isEmpty = function(obj){
		if(!obj){
			return true;
		}
		if(Array.isArray(obj) && obj.length == 0){
			return true;
		}
		if(Object.keys(obj).length === 0 && obj.constructor === Object){
			return true;
		}
		return false;
	}
	
	$rootScope.confirm = function(message, successCallback, cancelCallback, buttonTexts){
		$rootScope.confirmMessage = message;
		$rootScope.buttonTexts = buttonTexts;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/confirm-popup.html',
			scope: $scope,
			windowClass: 'confirm-popup'
		});
		previewModal.result.then(function () {
			if(successCallback){
				successCallback();
			}
		}, function () {
			if(cancelCallback){
				cancelCallback();
			}
		});
	}
	$rootScope.translateJsonKeyInArray = function(arr, key){
		if(arr && Array.isArray(arr) && key){
			for(var i = 0; i < arr.length; i++){
				if(arr[i][key]){
					arr[i][key] = $rootScope.getMsgs(arr[i][key]);
				}
			}
		}
		return arr;
	}
	$rootScope.fillFilter = function(params){
		if(params.dontPreserveFirstEl){
			params.arr = [];
		}else{
			params.arr.splice(1, params.arr.length);
		}
		var holder = null;
		if(params.data){
			holder = params.data.screenFilters;
		}
		if(params.holder){
			holder = params.holder;
		}
		if(holder && holder[params.filter]){
			for(var el in holder[params.filter]){
				if(holder[params.filter].hasOwnProperty(el)){
					var name = holder[params.filter][el];
					if(params.translate){
						name = $rootScope.getMsgs(name);
					}
					params.arr.push({id: el, name: name});
				}
			}
		}
	}
	$rootScope.getFilterElById = function(arr, value){
		return (typeof value != "undefined" && value != null) ? arr[searchJsonKeyInArray(arr, 'id', value)] : null;
	}
	
	$rootScope.sortFilter = function(filter){
		if(!Array.isArray(filter)){
			return filter.id == -1 ? '' : filter.name.trim();
		}else{
			return $filter('orderBy')(filter, $rootScope.sortFilter);
		}
	}
	
	$rootScope.formatAmountForDb = function(params){
		var amount = params.amount;
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
					amount = amount*100;
				}
			}
		}catch(e){}
		return amount;
	}
	
	$rootScope.formatAmount = function(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
		var amount = params.amount;
		var currency = settings.currencies['3'];//EUR
		if(params.currencyId && settings.currencies[params.currencyId]){
			currency = settings.currencies[params.currencyId];
		}
		var centsPart = params.centsPart;
		var centsLength = params.centsLength;
		if(centsLength !== 0 && !centsLength){
			centsLength = 2;
		}
		var withoutDecimalPoint = params.withoutDecimalPoint;
		var round = params.round;
		
		var skipAmountDivide = params.skipAmountDivide;
		
		if(params.amountOnly){
			try{
				if (!isNaN(amount)) {
					if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
						amount = amount/100;
					}
				}
			}catch(e){}
			return amount;
		}
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
					amount = amount/100;
				}
				var rtn = '';
				if (amount.toString().charAt(0) == '-') {
					rtn = '-';
					amount *= -1;
				}
				if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
					amount = amountToFloat(amount);
				}
				if (typeof round == 'undefined' || !round || amount < 1000 ) {
					amount = Math.floor(amount*Math.pow(10, centsLength))/Math.pow(10, centsLength);
					var tmp = amount.toString().split('.');
					var decimal = parseInt(tmp[0]);
					var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
					
					/*if((cents === '') || (cents === 0)){cents = '00';}
					else if(cents.length == '1'){cents = cents + '0';}
					else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}*/
					if((cents === '') || (cents === 0)){
						cents = '';
						for(var i = 0; i < centsLength; i++){
							cents += '0';
						}
					}else{
						cents = parseInt(cents);
						var zeroCount = centsLength - (cents + '').length;
						for(var i = 0; i < zeroCount; i++){
							cents += '0';
						}
					}
					
					if(currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
					rtn += numberWithCommas(decimal);
					if(currency.decimalPointDigits == 0){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
						rtn += "<span>"+cents+"</span>";
					}
					else if((centsPart == 3) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 2) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else{
						rtn += "."+cents;
					}
					if(!currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
				} else {
					if(currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
					rtn += (Math.round(amount / 100)) / 10 + "K";
					if(!currency.currencyLeftSymbol){
						rtn += currency.currencySymbol;
					}
				}
				return rtn;
			} else {
				return ' ';
			}
		} catch(e) {
			logIt({'type':3,'msg':e});
		}
	}
	
	$rootScope.getCurrencySymbol = function(currencyId){
		if(currencyId && settings.currencies[currencyId]){
			return settings.currencies[currencyId].currencySymbol;
		}
	}
	$rootScope.getCurrencyName = function(currencyId){
		if(currencyId && settings.currencies[currencyId]){
			return $rootScope.getMsgs(settings.currencies[currencyId].currencyName);
		}
	}
	
	
	//Date functions
	$rootScope.dateFormatDisplay = function(d){
		var date = new Date();
		if(d){
			date = new Date(d);
		}
		date.setHours(date.getHours() + date.getTimezoneOffset()/60 + $scope.loggedUser.utcOffsetHours);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes()) + ':' + $rootScope.dateFillToTwoDigits(date.getSeconds()) + ' ' + $rootScope.dateFillToTwoDigits(date.getDate()) + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
	}
	
	// $rootScope.dateParse = function(params){
	// 	var date = new Date(params.date);
	// 	var hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 	var minute = $rootScope.dateFillToTwoDigits(date.getMinutes());
	// 	if(params.startOfDay){
	// 		date.setHours(0/* + $rootScope.loggedUser.utcOffsetHours*/);
	// 		date.setMinutes(0);
	// 		date.setSeconds(0);
	// 		date.setMilliseconds(0);
	// 		hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 		minute = '00';
	// 	}else if(params.endOfDay){
	// 		date.setHours(24/* + $rootScope.loggedUser.utcOffsetHours*/);
	// 		date.setMinutes(0);
	// 		date.setSeconds(0);
	// 		date.setMilliseconds(0);
	// 		hour = $rootScope.dateFillToTwoDigits(date.getHours());
	// 		minute = '00';
	// 	}
	// 	if(params.timestamp){
	// 		return date.getTime();
	// 	}
	// 	return date.getFullYear() + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + $rootScope.dateFillToTwoDigits(date.getDate()) + ' ' + hour + ':' + minute + ' ' + $rootScope.loggedUser.utcOffset;
	// }
	
	$rootScope.dateFillToTwoDigits = function(num){
		if(num >= 0 && num < 10){
			return '0' + num;
		}else{
			return num;
		}
	}
	
	$rootScope.dateGetSimple = function(d, separator){
		if(!d){
			return '';
		}
		if(!separator){
			separator = '/';
			
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getDate()) + separator + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + separator + date.getFullYear();
	}
	
	$rootScope.timeGetSimple = function(d){
		if(!d){
			return '';
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes());
	}
	
	
	//Dependencies functions
	$rootScope.addDependency = function(scope, dependency, callback){
		if(!checkOwnProperty(scope, 'dependencies')){
			scope.dependencies = {};
		}
		scope.dependencies[dependency.name] = dependency;
		scope.dependencies[dependency.name].callback = callback;
	}
	
	$rootScope.markDependencyDone = function(scope, dependencyName, dependencyItem){
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName] && scope.dependencies[dependencyName].dependencies){
			scope.dependencies[dependencyName].dependencies[dependencyItem] = true;
		}
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName]){
			var isReady = true;
			for(var dependencyItem in scope.dependencies[dependencyName].dependencies){
				if(scope.dependencies[dependencyName].dependencies.hasOwnProperty(dependencyItem)){
					if(!scope.dependencies[dependencyName].dependencies[dependencyItem]){
						isReady = false;
					}
				}
			}
			if(isReady){
				$timeout(function(){scope.dependencies[dependencyName].callback();}, 0);
			}
		}
	}
	
	//Init screen controllers
	$rootScope.initScreenCtr = function(scope, dependencies, initCallback){
		var that = scope;
		that.ready = false;
		
		$rootScope.enrichScope(that);
		
		that.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if(that.ready){
				if(that.checkAction){
					that.checkAction();
				}
			}
		});
		that.mainInitWatch = that.$watch(function(){return $rootScope.ready;}, function(){
			if($rootScope.ready){
				that.mainInitWatch();
				that.init();
			}
		});
		that.init = function(){
			that.showLoading();
			that.errors = {};
			that.errors.globalErrorMsg = '';
			that.errors.localErrorMsgs = [];
			that.$on('resetGlobalErrorMsg', function(event, args){
				if(that.resetGlobalErrorMsg){
					that.resetGlobalErrorMsg(that);
				}
			});
			if(dependencies){
				$rootScope.addDependency(that, dependencies, function(){that.initDone();});
			}else{
				that.initDone();
			}
			if(that.initFiltersOnReady){
				that.initFiltersOnReady();
			}
			if(initCallback){
				initCallback();
			}
		}
		that.initDone = function(){
			that.ready = true;
			that.hideLoading();
			if(that.checkAction){
				that.checkAction();
			}
		}
	}
	
	$rootScope.enrichScope = function(scope){
		var that = scope;
		
		that.settings = Utils.settings;
		that.showLoading = Utils.showLoading;
		that.hideLoading = Utils.hideLoading;
		
		//File init
		$scope.usingFlash = FileAPI && FileAPI.upload != null;
		$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
		$scope.fileTooBig = false;
		
		that.handleNetworkError = Utils.handleNetworkError;
		
		that.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
		
		that.handleErrors = function(response, name, holder, exceptions) {//returns true on error
			var data = response.data;
			that.requestsStatus[data.serviceName] = 1;
			if (data == '') {
				logIt({'type':3,'msg':name});
				logIt({'type':3,'msg':data});
				$timeout(function(){
					alert('Response is empty string. Check eclipse console for exeptions.');
				});
				return true;
			} else if (data.responseCode == null) {
				logIt({'type':3,'msg':name});
				logIt({'type':3,'msg':data});
				$timeout(function(){
					alert('Error code is null. Check eclipse console for exeptions. ');
				});
				return true;
			} else if (data.responseCode > errorCodeMap.success && (!exceptions || exceptions.indexOf(data.responseCode) == -1)) {//0
				var defMsg = $rootScope.getMsgs('error-' + data.responseCode);
				logIt({'type':3,'msg':name + " (" + defMsg + ")"});
				logIt({'type':3,'msg':data});
				
				that.resetGlobalErrorMsg(this, holder);
				
				if(data.responseCode == errorCodeMap.authentication_failure){ //session expired or unauthenticated - 602
					AuthService.deauthorize();
					return true;
				} else if (data.responseCode == errorCodeMap.access_denied) { //session expired or unauthenticated - 601
					AuthService.deauthorize();
				} else if (data.responseCode == errorCodeMap.account_locked) { // 605
					if (data.messages.length) {
						that.addGlobalErrorMsg(this, $rootScope.getMsgs(data.messages[0]), holder);
						return true;
					}
				} else if (data.responseCode == errorCodeMap.invalid_input) { // 701
					if (data.error && data.error.reason) {
						that.addGlobalErrorMsg(this, $rootScope.getMsgs(data.error.reason), holder);
						return true;
					}
				}
				if (data.error && data.error.messages != null) {
					for(var i = 0; i < data.error.messages.length; i++){
						that.addLocalErrorMsg(this, data.error.messages[i]);
					}
				}
				
				var autoTranslationFound = false;
				for(key in errorCodeMap){
					if(errorCodeMap.hasOwnProperty(key)){
						if(errorCodeMap[key] == data.responseCode){
							autoTranslationFound = true;
							// if($rootScope.isTranslated($rootScope.getMsgs('error.' + key), true)){
							that.addGlobalErrorMsg(this, $rootScope.getMsgs('error.' + key), holder);
							// }
							break;
						}
					}
				}
				
				if(!autoTranslationFound) {
					logIt({'type':3,'msg':'unhandled errorCode: ' + data.responseCode});
					that.addGlobalErrorMsg(this, defMsg, holder);
				}
				
				return true;
			} else {
				that.resetGlobalErrorMsg(this, holder);
				logIt({'type':1,'msg':data});
				return false;
			}
		}
		
		that.addLocalErrorMsg = function(scope, data){
			if(!scope.errors){
				scope.errors = {};
			}
			if(!scope.errors.localErrorMsgs){
				scope.errors.localErrorMsgs = [];
			}
			scope.errors.localErrorMsgs.push(data);
		}
		that.clearLocalErrorMsg = function(scope){
			if(!scope.errors){
				scope.errors = {};
			}
			scope.errors.localErrorMsgs = [];
		}
		
		that.addGlobalErrorMsg = function(scope, data, holder){
			if(holder){
				holder.push(data);
			}else{
				if(!scope.errors){
					scope.errors = {};
				}
				scope.errors.globalErrorMsg += '<span class="error">' + data + '</span>';
			}
		}
		
		that.resetGlobalErrorMsg = function(scope, holder){
			if(holder){
				holder.length = 0;
			}else{
				if(!scope.errors){
					scope.errors = {};
				}
				scope.errors.globalErrorMsg = '';
			}
		}
		
		
		that.productIsPrimary = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			if(!selectedProduct || !selectedProduct.productStatus){
				return false;
			}
			if(selectedProduct.productStatus.id <= settings.productStatuses.SUBSCRIPTION){
				return true;
			}
			return false;
		}
		
		that.productIsSecondary = function(selectedProduct){
			return !that.productIsPrimary(selectedProduct) && !that.productIsPendingSettlement(selectedProduct);
		}
		
		that.productIsPendingSettlement = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			if(!selectedProduct || !selectedProduct.productStatus){
				return false;
			}
			if(selectedProduct.productStatus.id == settings.productStatuses.PENDING_SETTLEMENT){
				return true;
			}
			return false;
		}
		
		that.productGetInitialFixingLevel = function(selectedProduct){
			var selectedProduct = selectedProduct || (that.singleProduct ? that.selectedProduct : null);
			var initialFixingLevel = selectedProduct.productMarkets ? selectedProduct.productMarkets[0].startTradeLevel : '';
			if(that.productIsPrimary(selectedProduct)){
				//Probably the row below is irrelevant, keeping it for now
				//initialFixingLevel = selectedProduct.productMarkets[0].market.lastPrice;
				initialFixingLevel = 100;
			}
			return initialFixingLevel;
		}
		
		that.productGetBarrierObservationPeriod = function(separator, selectedProduct){
			var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
			if(selectedProduct.productBarrier){
				if(!separator){
					separator = '.';
				}
				if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
					return $scope.dateGetSimple(selectedProduct.productBarrier.barrierStart, separator) + ' - ' + $scope.dateGetSimple(selectedProduct.productBarrier.barrierEnd, separator);
				}else{
					return $scope.dateGetSimple(selectedProduct.productBarrier.barrierEnd, separator);
				}
			}
		}
		
		that.productIsBarrierTypeEuropean = function(selectedProduct){
			var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
			if($rootScope.isEmpty(selectedProduct)){
				return '';
			}
			if(selectedProduct.productBarrier){
				if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.European){
					return true;
				}else{
					return false;
				}
			}
		}
		
		that.productLoadTypesConfig = function(selectedProduct){
			var typeConfig = null;
			switch (selectedProduct.productType.id) {
				case 1: typeConfig = new CPCPConfig(that, selectedProduct);
					break;
				case 2: typeConfig = new BCPCConfig(that, selectedProduct);
					break;
				case 3: typeConfig = new CPCCConfig(that, selectedProduct);
					break;
				case 4: typeConfig = new RCConfig(that, selectedProduct);
					break;
				case 5: typeConfig = new BRCConfig(that, selectedProduct);
					break;
				case 6: typeConfig = new IBRCConfig(that, selectedProduct);
					break;
				case 7: typeConfig = new ECConfig(that, selectedProduct);
					break;
				case 8: typeConfig = new OCConfig(that, selectedProduct);
					break;
				case 9: typeConfig = new BCConfig(that, selectedProduct);
					break;
				case 10: typeConfig = new BOCConfig(that, selectedProduct);
					break;
				default:
					//
			}
			selectedProduct._typeConfig = typeConfig;
			selectedProduct.productTypesConfig = typeConfig.loadConfig();
		}
		
		that.getMsgs = $rootScope.getMsgs;
		that.isTranslated = $rootScope.isTranslated;
		that.msgsParam = $rootScope.msgsParam;
		that.isEmpty = $rootScope.isEmpty;
		that.confirm = $rootScope.confirm;
		that.translateJsonKeyInArray = $rootScope.translateJsonKeyInArray;
		that.fillFilter = $rootScope.fillFilter;
		that.getFilterElById = $rootScope.getFilterElById;
		that.sortFilter = $rootScope.sortFilter;
		that.formatAmountForDb = $rootScope.formatAmountForDb;
		that.formatAmount = $rootScope.formatAmount;
		that.getCurrencySymbol = $rootScope.getCurrencySymbol;
		that.getCurrencyName = $rootScope.getCurrencyName;
		that.dateFormatDisplay = $rootScope.dateFormatDisplay;
		// that.dateParse = $rootScope.dateParse;
		that.dateFillToTwoDigits = $rootScope.dateFillToTwoDigits;
		that.dateGetSimple = $rootScope.dateGetSimple;
		that.timeGetSimple = $rootScope.timeGetSimple;
		that.round = round;
	}
	
	$rootScope.initScreenCtr($scope);
	
	
	
	$rootScope.msgs = {};
	$scope.initRequestStatus = {};
	function getMsgsJson(getMsgsJsonResponse) {
		if (!getMsgsJsonResponse) {
			return;
		}
		
		$scope.initRequestStatus.getMsgsJson = false;
		initMsgsJson(getMsgsJsonResponse);
		
		function initMsgsJson(response) {
			if (!$scope.handleErrors(response, 'getMsgsJson')) {
				if(Utils.parseResponse(response) && Utils.parseResponse(response)[$rootScope.language.id]){
					$rootScope.msgs = Utils.parseResponse(response)[$rootScope.language.id];
				}
				$scope.initRequestStatus.getMsgsJson = true;
				
				if (!isUndefined($rootScope.$state.current.metadataKey)) {
					$rootScope.metadataTitle = $rootScope.$state.current.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
			}
		}
	}
	
	$scope.initRequestStatus.init = false;
	function init(response) {
		var init = Utils.parseResponse(response);
		$scope.initRequestStatus.init = true;
		$rootScope.isMobile = false;
		$rootScope.deviceInfo = init.deviceInfo;
		if($rootScope.deviceInfo.type != 'DESKTOP'){
			$rootScope.isMobile = true;
		}
		
		$rootScope.investmentPossibilities = init.investmentPossibilities;
		$rootScope.countries = init.countriesHM;
		$rootScope.currencies = init.currenciesHM;
		$rootScope.Company_Address = init.Company_Address;
		$rootScope.support_email = init.support_email;
		$rootScope.support_phone = init.support_phone;
		for(key in $rootScope.currencies){
			if($rootScope.currencies.hasOwnProperty(key)){
				settings.currencies[key] = {
					decimalPointDigits: 2,
					currencySymbol: $rootScope.currencies[key].symbol,
					currencyLeftSymbol: true,
					currencyName: $rootScope.currencies[key].displayName
				};
			}
		}
		if(init.user){
			$rootScope.isLogged = true;
			$rootScope.setLoggedUser(init.user);
			UserService.refreshLoggedUser(); //init returns wrong ballance
		}
		$rootScope.userLogout = init.userLogout;
		
		$rootScope.$on('$stateChangeSuccess', function() {
			UserService.refreshLoggedUser();
		});
	}
	
	$rootScope.getMenu = function(){
		if(!$rootScope.menu){
			$rootScope.menu = new MenuTree.InstanceClass(menu, false, $rootScope.permissions);
			$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
		}
		$rootScope.menuVisible = false;
		return $rootScope.menu;
	}
	
	
	//Set loggedUser
	$rootScope.setLoggedUser = function(user){
		$rootScope.loggedUser = user || {};
		$rootScope.loggedUser.utcOffsetHours = 3;
		$rootScope.loggedUser.utcOffset = 'GMT+03:00';
	}
	
	//Init loggedUser
	$rootScope.setLoggedUser();
	
	
	//Init user
	$scope.userId = 0;
	if(window.userId){
		$scope.userId = parseInt(userId);
	}
	if(window.userCurrency){
		$scope.userCurrency = userCurrency;
	}
	$scope.getUserCurrency = function(){
		return $scope.userCurrency;
	}
	
	
	$scope.checkInitStatus = function() {
		for (var key in $scope.initRequestStatus) {
			if (!$scope.initRequestStatus[key]) {
				$timeout(function() {
					$scope.checkInitStatus();
				}, 50);
				return;
			}
		}
		$rootScope.loaded = $rootScope.ready = $scope.ready = true;
		if(window.menu){
			$rootScope.menu = $rootScope.getMenu();
		}
	};
	
	
	$scope.initRequests = function() {
		$rootScope.ready = false;
		getMsgsJson(getMsgsJsonResponse);
		init(initResponse);
		$scope.checkInitStatus();
	}

	$scope.initRequests();
	
	$scope.isLnListVisible = false;
	$scope.toggleLanguageList = function(forceClose) {
		if (forceClose) {
			$scope.isLnListVisible = false;
			return;
		}
		$scope.isLnListVisible = !$scope.isLnListVisible;
	};
	$rootScope.changeLanguage = function(language){
		if(language == $rootScope.language){
			return false;
		}
		$scope.toggleLanguageList(true);
		$rootScope.ready = false;
		$rootScope.language = language;
		var methodRequest = {
			data: {
				id: $rootScope.language.id
			}
		};
		$http.post('/language/change', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'changePasswordLogout')) {
					$rootScope.$state.go($rootScope.$state.current, {ln: $rootScope.language.code}).then(function(){
						$scope.checkInitStatus();
					});
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			}
		);
	}
	
	$rootScope.getMetaTitle = function(){
		var metaTitle = $rootScope.$state.current.metaTitle;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaTitle && parent) {
			metaTitle = parent.self.metaTitle;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaTitle || 'meta-title-index');
	};
	
	$rootScope.getMetaDescription = function(){
		var metaDescription = $rootScope.$state.current.metaDescription;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaDescription && parent) {
			metaDescription = parent.self.metaDescription;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaDescription || 'meta-description-1');
	};
	
	$rootScope.getMetaKeywords = function(){
		var metaKeywords = $rootScope.$state.current.metaKeywords;
		var parent = $rootScope.$state.$current.parent;
		
		while (!metaKeywords && parent) {
			metaKeywords = parent.self.metaKeywords;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(metaKeywords || 'meta-keywords-1');
	};
	
	$rootScope.getPageTitle = function(){
		if($rootScope.$state.current.titleTransclude){
			return $rootScope.$state.current.title;
		}
		
		var title = $rootScope.$state.current.title;
		var parent = $rootScope.$state.$current.parent;
		
		while (!title && parent) {
			title = parent.self.title;
			parent = parent.parent;
		}
		
		return $rootScope.getMsgs(title);
	}
	
	$rootScope.isMenuVisible = function(){
		if($rootScope.isMobile && !$rootScope.menuVisible){
			return false;
		}
		return true;
	}
	
	$rootScope.toggleMenu = function(forceState){
		if($rootScope.isMobile){
			if(forceState === true){
				$rootScope.menuVisible = true;
			}else if(forceState === false){
				$rootScope.menuVisible = false;
			}else{
				$rootScope.menuVisible = !$rootScope.menuVisible;
			}
		}
	}
	
	$rootScope.hideHeader = function(){
		if($rootScope.isMobile && $rootScope.$state.current.mobileConfig && $rootScope.$state.current.mobileConfig.showHeader === false){
			return true;
		}
		return false;
	}
	
	$rootScope.goBack = function(){
		$window.history.back();
	}
	
	//Hide menu bottom border(scrolled/not scrolled) on some screens
	$scope.isHideBottomLine = function() {
		var isHide;
		if ($rootScope.isMobile) {
			isHide = /(\.products?|\.register|\.sign-up\.personal-details|\.sign-up\.questionnaire|\.please-login|\.my-account)/i.test($state.current.name);
		} else {
			isHide = /(\.products$)/i.test($state.current.name);
		}
		return isHide;
	};
	
	//Disable menu bottom border if not scrolled
	$scope.isBottomDisabled = function() {
		var isDisabled;
		if ($rootScope.isMobile) {
			isDisabled = /(\.index)/i.test($state.current.name);
		} else {
			isDisabled = /(\.index|\.products?|\.register|\.please-login|\.basics)/i.test($state.current.name);
		}
		return isDisabled;
	};
	
	$scope.isMenuTransparent = function() {
		return /(\.index)/i.test($state.current.name);
	};
	
	$scope.footerIconsList = [
		{name: 'nasdaq.svg', width: 120},
		{name: 'nyse.svg', width: 44},
		{name: 'deutsche-borse.svg', width: 77},
		{name: 'nyse-euronext.svg', width: 109}	
	];
	
	$scope.isUpArrowHidden = function() {
		return $rootScope.isMobile && /^ln\.my-account/i.test($state.current.name);
	};
	
	$(document).on('scroll', function(e){
		if($(document).scrollTop() > 0){
			$('body').addClass('scrolled');
		}else{
			$('body').removeClass('scrolled');
		}
	});
	
	$scope.depositClickHandler = function() {
		if (!$rootScope.loggedUser.userStep) return;
		var userStepId = $rootScope.loggedUser.userStep.id;
		switch (userStepId) {
			case 1:
				$state.go('ln.sign-up.personal-details');
				break;
			case 2:
				$state.go('ln.sign-up.questionnaire');
				break;
			case 3:
				$state.go('ln.my-account.settings.questionnaire');
				break;
			case 4:
				$state.go('ln.my-account.funding.upload-documents');
				break;
			case 5:
			default:
				return $state.go('ln.my-account.funding.deposit');
		}
		$state.go('ln.my-account.funding.deposit', {ln: $state.params.ln});
	};
	
	$scope.logout = function(){
		$scope.showLoading();
		$rootScope.redirectAfterLogin = null;
		
		AuthService.logout($scope.handleErrors.bind($scope))
			.then(function() {
				$scope.hideLoading();
			})
			.catch(function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	};
	
}]);

spApp.controller('RegisterCtr', ['$rootScope', '$scope', '$state', 'Utils', 'SingleSelect', 'AuthService', 'restriction', function($rootScope, $scope, $state, Utils, SingleSelect, AuthService, restriction) {
	$scope.settings = Utils.settings;
	$scope.$state = $state;
	
	if (restriction && restriction.data && restriction.data.responseCode === 604) {
		$scope.isRegistrationRestricted = true;
	}
	
	$scope.patterns = {
		phone: regEx_phone,
		charsOnly: regEx_charsOnly
	};
	
	$scope.credentials = {
		signup: {
			firstName: '',
			lastName: '',
			email: '',
			phone: '',
			userPass: '',
			acceptTerms: false,
			phoneCode: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: '+', flagCode: 'none'}, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')})
		}
	};
	
	var phoneCodeList = Object.keys($rootScope.countries).map(function(key) {
		var country = $rootScope.countries[key];
		return {
			id: key,
			suffixOptionName: '+' + country.phoneCode,
			suffixDisplayName: false,
			name: $scope.getMsgs(country.displayName),
			displayName: '+' + country.phoneCode,
			phoneCode: country.phoneCode,
			flagCode: country.a2.toLowerCase(),
			isBlocked: country.isBlocked
		};
	});
	$scope.credentials.signup.phoneCode.fillOptions(phoneCodeList);

	if ($rootScope.userLogout && $rootScope.userLogout.country) {
		$scope.credentials.signup.phoneCode.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	}
	
	$scope.signup = function(form){
		if (form.$invalid || $scope.isRegistrationRestricted || $scope.credentials.signup.phoneCode.model.isBlocked) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.signup($scope.credentials.signup, $scope.handleErrors.bind($scope))
			.then(function() {
				Utils.hideLoading({container: loadingOverlayContainer});
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
}]);

spApp.controller('CredentialsCtr', ['$sce', '$rootScope', '$scope', '$http', '$state', 'Utils', 'SingleSelect', '$cookies', '$log', 'AuthService', function($sce, $rootScope, $scope, $http, $state, Utils, SingleSelect, $cookies, $log, AuthService) {
	var rememberMeEmail = AuthService.getRememberMeEmail() || '';

	$scope.settings = Utils.settings;
	$scope.getMsgs = $rootScope.getMsgs;
	$scope.$state = $state;
	
	$scope.credentials = {};
	$scope.credentials.login = {};
	$scope.credentials.login.userName = rememberMeEmail;
	$scope.credentials.login.userPass = '';
	$scope.credentials.login.rememberMe = rememberMeEmail ? true : false;
	
	$scope.credentials.forgotPassword = {};
	$scope.credentials.forgotPassword.userEmail = '';
	
	$scope.credentials.resetPassword = {};
	$scope.credentials.resetPassword.via = 1;
	$scope.credentials.resetPassword.phone = '*';
	$scope.credentials.resetPassword.email = '*';
	
	$scope.patterns = {
		phone: regEx_phone
	};
	
	$scope.emailFromCookie = $cookies.get('e');
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	$scope.goToSupport = function(){
		$state.go('ln.contact-us', {ln: $state.params.ln});
	};
	
	$scope.doLogin = function(){
		if($scope.afterLogin){
			$scope.afterLogin();
		}
	};
	
	$scope.login = function(form){
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.login($scope.credentials.login, $scope.handleErrors.bind($scope))
			.then(function() {
				Utils.hideLoading({container: loadingOverlayContainer});
				$scope.doLogin();
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.reset = function(form){
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.resetPassword($scope.credentials.forgotPassword, $scope.handleErrors.bind($scope))
			.then(function(response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				var userInfo = response.data.data;
				if (response.data.responseCode === 0 && Object(userInfo) === userInfo) {
					$scope.credentials.resetPassword.phone = userInfo.mobilePhone;
					$scope.credentials.resetPassword.email = userInfo.email;
					$scope.gotoState($scope.settings.credentialsView.RESET_SUBMIT);
				} else {
					$scope.gotoState($scope.settings.credentialsView.RESET_FAILED);
				}
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.submitResetPassword = function(form) {
		if (form.$invalid) return;
		var loadingOverlayContainer = '.credentials-module';
		Utils.showLoading({container: loadingOverlayContainer});
		
		AuthService.forgotPassword($scope.credentials.resetPassword, $scope.handleErrors.bind($scope))
			.then(function(response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				var data = response.data.data;
				if (response.data.responseCode === 0 && Object(data) === data) {
					$scope.gotoState($scope.settings.credentialsView.RESET_APPROVED);
				} else {
					$scope.gotoState($scope.settings.credentialsView.RESET_FAILED);
				}
			}).catch(function (response) {
				Utils.hideLoading({container: loadingOverlayContainer});
				Utils.handleNetworkError(response);
			});
	};
	
	$scope.gotoState = function(state){
		$scope.resetGlobalErrorMsg($scope);
		$scope.viewState = state;
	};
	
	$rootScope.$watch('isMobile', function(isMobile) {
		$scope.isMobile = isMobile;
	});
	
}]);

spApp.controller('resetPasswordCtrl', ['$stateParams', '$state', '$rootScope', '$scope', '$http', function($stateParams, $state, $rootScope, $scope, $http) {
	var ctrl = this;
	
	ctrl.getMsgs = $rootScope.getMsgs;
	
	ctrl.isPasswordChanged = false;
	
	ctrl.setPassword = {
		email: $stateParams.email,
		newPassword: '',
		retypeNewPassword: ''
	};
	
	ctrl.submitSetPassword = function(form) {
		if (form.$invalid) return;
		var methodRequest = {
			data: {
				newPassword: ctrl.setPassword.newPassword,
				retypePassword: ctrl.setPassword.retypeNewPassword
			}
		};
		
		$http.post('/user/changePasswordLogout', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'changePasswordLogout')) {
					ctrl.isPasswordChanged = true;
					// $state.go('ln.please-login');
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			}
		);
	};
	
	$scope.$watch('errors.globalErrorMsg', function(globalErrorMsg) {
		ctrl.globalErrorMsg = globalErrorMsg;
	}); 
}]);

spApp.controller('LoginCtr', ['$rootScope', '$scope', '$stateParams', function($rootScope, $scope, $stateParams) {
	$scope.currentState = $stateParams.credentialsState === null ? $scope.settings.credentialsView.LOGIN : $stateParams.credentialsState;
	$stateParams.credentialsState = null;
	$scope.hideLoginHeader = true;
	// $rootScope.redirectAfterLogin = $rootScope.$stateParams.toState;
	// if(!$rootScope.redirectAfterLogin){
	// 	$rootScope.redirectAfterLogin = 'ln.index';
	// }
}]);

spApp.controller('HomeCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$interval', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'articlesList', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $interval, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, articlesList) {
	
	var intervals = [];
	
	$scope.articles = [articlesList[11], articlesList[6], articlesList[2]];
	
	$timeout(function(){
		$('.slide-text-holder h1, .slide-text-holder p, .slide-text-holder button').addClass(' animated fadeInSlideUp');
	});
	$(document).on('scroll', scrollHandler);
	
	$stepsInterval = setInterval(function(){
		$('.step').each(function(index, el){
			if(isElementInViewport(el)){
				$(el).addClass('visible');
			}
		});
		var counter = 0;
		$('.step').each(function(index, el){
			if($(el).hasClass('visible')){
				counter++;
			}
		});
		if(counter == $('.step').length){
			clearInterval($stepsInterval);
		}
	}, 400);
	intervals.push($stepsInterval);
	
	$bounceInterval = setInterval(function(){
		$('.about-us-articles .inner-half').each(function(index, el){
			if(isElementInViewport(el)){
				$(el).addClass('visible');
				$(el).addClass($(el).data('animation-name'));
			}
		});
		var counter = 0;
		$('.about-us-articles .inner-half').each(function(index, el){
			if($(el).hasClass('visible')){
				counter++;
			}
		});
		if(counter == $('.about-us-articles .inner-half').length){
			clearInterval($bounceInterval);
		}
	}, 400);
	intervals.push($bounceInterval);
	
	var hpFirstVisitFlag = angular.fromJson(localStorage.getItem('hpFirstVisitFlag', true));
	if (!hpFirstVisitFlag) {
		setTimeout(function() {
			$('body').animate({
				scrollTop: 85
			}, 500);
		});
		try {
			localStorage.setItem('hpFirstVisitFlag', true);
		} catch(e) {
			console.log('No localStorage');
		}
	}
	
	$scope.$on('$destroy', function() {
		intervals.forEach(function(interval) {
			clearInterval(interval);
		});
		$(document).off('scroll', scrollHandler);
	});
	
	function scrollHandler(e) {
		var $slide = $('.slide-holder').find('.slide').not('.ng-hide');
		var delta = 0;
		var ratio = 0.4;
		var height = parseInt($slide.find('img').css('height'));
		var maxScroll = 600;
		var scrollTop = $(document).scrollTop();
		if(scrollTop == 0){
			//chill
		}else if(scrollTop > maxScroll){
			delta = ratio*maxScroll;
		}else{
			delta = ratio*scrollTop;
		}
		
		$slide.css('max-height', (height - delta));
		
		$slide.find('.slide-text').each(function() {
			var slideText = this;
			var slideTextRect = slideText.getBoundingClientRect();
			var menuRect = $('.menu')[0].getBoundingClientRect();

			if (slideTextRect.top < menuRect.bottom) {
				var opacity = (slideTextRect.bottom - menuRect.bottom + 85) / (slideTextRect.height * 1.3);
				slideText.style.opacity = opacity;
			} else {
				slideText.style.opacity = 1;
			}
		});
	}
}]);

spApp.controller('SlidesCtr', ['$sce', '$rootScope', '$scope', '$window', '$uibModal', function($sce, $rootScope, $scope, $window, $uibModal) {
	$scope.visibleIndex = isUndefined($window.gtmSlideIndex) ? 1 : $window.gtmSlideIndex;
	$scope.slides = [
		{image: 'slide-hand.jpg', imageMobile: 'slide-hand-mobile.jpg', align: 'left', text: parseSlideText('home-page-slide-hand')},
		{image: 'slide-boat-lake.jpg', imageMobile: 'slide-boat-lake-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-boat-lake')},
		{image: 'slide-boat.jpg', imageMobile: 'slide-boat-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-boat')},
		{image: 'slide-kids-lake.jpg', imageMobile: 'slide-kids-lake-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-kids-lake')},
		{image: 'slide-mountain-man.jpg', imageMobile: 'slide-mountain-man-mobile.jpg', align: 'center', text: parseSlideText('home-page-slide-mountain-man')}
	];
	function parseSlideText(key){
		var result = $rootScope.getMsgs(key);
		
		var params = [{key: '${risk-warning}', value: $rootScope.getMsgs('risk-warning')}];
		for(var i = 0; i < params.length; i++){
			result = result.replace(new RegExp(params[i].key.replace(/\$/gi, '\\$'), "gi"), params[i].value);
		}
		
		return $sce.trustAsHtml(result);
	}
	$scope.openIntroVideoModal = function(e) {
		if (!$(e.target).closest('.video-button').length) return;
		$uibModal.open({
			animation: true,
			templateUrl: folderPrefix + 'components/video-modal.html',
			controller: ['$scope', '$uibModalInstance', 'video', function($scope, $uibModalInstance, video) {
				$scope.close = function(reason) {
					reason = reason || 'close';
					$uibModalInstance.close(reason);
				};
				
				$scope.video = video;
			}],
			size: 'lg',
			windowClass: 'video-modal',
			resolve: {
				video: ['$sce', function($sce) {
					return {
						name: 'Intro Video',
						url: $sce.trustAsResourceUrl('https://www.youtube.com/embed/4F9ifFqaOX4?rel=0')
					};
				}]
			}
		});
	};
}]);

spApp.controller('PrivacyPolicyAndTermsCtrl', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
	
	$scope.parseText = function(key){
		var result = $rootScope.getMsgs(key);
		var params = [{key: '${company_email}', value: $rootScope.support_email}];
		for(var i = 0; i < params.length; i++){
			result = result.replace(new RegExp(params[i].key.replace(/\$/gi, '\\$'), "gi"), params[i].value);
		}
		return $sce.trustAsHtml(result);
	}
}]);

spApp.controller('SecurityCtr', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
}]);

spApp.controller('LegalDocumentsCtr', ['$scope', '$document', '$rootScope', '$sce', function($scope, $document, $rootScope, $sce) {
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
	});
	
	function setAllParalax() {
		setParalax({container: '.top-banner', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 80});
	}
}]);

spApp.controller('signUpPersonalDetailsCtrl', ['$rootScope', '$scope', '$state', 'SingleSelect', 'SignUpPersonalDetailsService', function($rootScope, $scope, $state, SingleSelect, SignUpPersonalDetailsService) {
	var ctrl = this;
	ctrl.getMsgs = $rootScope.getMsgs;
	ctrl.personalDetails = {
		dayOfBirth: {
			day: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'DD'}, noSort: true}),
			mounth: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'MM'}, noSort: true, mutateCallback: $rootScope.getMsgs}),
			year: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'YYYY'}, noSort: true})
		},
		street: '',
		cityName: '',
		zipCode: '',
		country: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')})
	};
	
	var startDay = 1;
	var endDay = 31;
	var days = [];
	for(var day = startDay; day <= endDay; day++) {
		days.push({id: day, name: day.toString()});
	}
	ctrl.personalDetails.dayOfBirth.day.fillOptions(days);
	
	var startMonth = 1;
	var endMonth = 12;
	var months = [];
	for(var month = startMonth; month <= endMonth; month++) {
		months.push({id: month, name: 'month-' + month});
	}
	ctrl.personalDetails.dayOfBirth.mounth.fillOptions(months);

	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var startYear = currentYear - 18;
	var endYear = currentYear - 100;
	var years = [];
	for(var year = startYear; year >= endYear; year--) {
		years.push({id: year, name: year.toString()});
	}
	ctrl.personalDetails.dayOfBirth.year.fillOptions(years);

	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase()
			});
		}
	}
	ctrl.personalDetails.country.fillOptions(countries);
	
	ctrl.submitPersonalDetailsHandler = function(form) {
		if (form.$invalid) {
			return;
		}
		
		SignUpPersonalDetailsService.sendPersonalDetails(ctrl.personalDetails)
			.then(function(response) {
				if (!$scope.handleErrors(response, 'PersonalDetailsInsert')) {
					$rootScope.loggedUser.step++;
					$state.go('ln.sign-up.questionnaire');
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			});
	};
	
}]);

spApp.controller('QuestionnaireCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'QuestionnaireService', 'AuthService', 'SingleSelect', 'ScrollTop', 'Utils', 'scrollToElement', function($scope, $rootScope, $state, $stateParams, QuestionnaireService, AuthService, SingleSelect, ScrollTop, Utils, scrollToElement) {
	var ctrl = this;
	ctrl.$state = $state;
	ctrl.getMsgs = $rootScope.getMsgs;
	ctrl.support_email = $rootScope.support_email;
	
	ctrl.questionType = {
		radio: 1,
		checkbox: 2,
		input: 3,
		select: 4
	};
	
	$rootScope.$watch('loggedUser.userStep.id', function(userStepId) {
		ctrl.userStepId = userStepId;
	});
	
	ctrl.questionsList = [];
	ctrl.checkboxesList = [];
	ctrl.action = 'loading';
	var isWithAnswers = $rootScope.loggedUser.userStep.id > 3;
	QuestionnaireService.getQuestionnaire(isWithAnswers)
		.then(function(questionsList) {
			ctrl.action = '';
			if (!questionsList.length) {
				return;
			}
			var processedQuestionsList = questionsList
				.sort(function(a, b) {
					return a.orderId - b.orderId;
				})
				.map(function(q) {
					var qmAnswerListSorted = q.qmAnswerList.sort(function(a, b) {return a.orderId - b.orderId;});
					var answerSelectedByUser = q.qmAnswerList.filter(function(a) {return a.selectedAnswer;})[0];
					var qNewOrUpdatedFields = {
						qmAnswerList: qmAnswerListSorted
					};
					switch(q.questionTypeId) {
						case ctrl.questionType.radio:
							if (answerSelectedByUser) {
								qNewOrUpdatedFields.answerId = answerSelectedByUser.id;
							}
							break;
						case ctrl.questionType.checkbox:
							if (answerSelectedByUser) {
								qNewOrUpdatedFields.answerId = answerSelectedByUser.name.toLowerCase() === 'yes';
							} else if (!isWithAnswers) {
								qNewOrUpdatedFields.answerId = false; //hardcode to set pre-checked.
							}
							break;
						case ctrl.questionType.input:
							break;
						case ctrl.questionType.select:
							var qmAnswerListForSelect = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'select'}, isSmart: true, noSort: true, filterPlaceholder: $rootScope.getMsgs('search'), mutateCallback: $rootScope.getMsgs});
							var answers = qmAnswerListSorted.map(function(a) {return angular.merge({}, a, {name: a.displayName});});
							var selectedId = answerSelectedByUser ? answerSelectedByUser.id : -1;
							qmAnswerListForSelect.fillOptions(answers);
							qmAnswerListForSelect.setById(selectedId);
							qNewOrUpdatedFields.qmAnswerListForSelect = qmAnswerListForSelect;
							break;
					}
					return angular.merge({}, q, qNewOrUpdatedFields);
				});
			ctrl.questionsList = processedQuestionsList.filter(function(q) {
				return q.questionTypeId !== ctrl.questionType.checkbox;
			});
			ctrl.checkboxesList = processedQuestionsList.filter(function(q) {
				return q.questionTypeId === ctrl.questionType.checkbox;
			});
		})
		.catch(function(response) {
			ctrl.action = '';
		});
	
	ctrl.visibleQuestionnaire = {
		offset: 0,
		limit: 7
	};

	ctrl.prevHandler = function() {
		ctrl.visibleQuestionnaire.offset -=  ctrl.visibleQuestionnaire.limit;
		setTimeout(ScrollTop);
	};
	
	ctrl.isLastPage = function() {
		var offset = ctrl.visibleQuestionnaire.offset + ctrl.visibleQuestionnaire.limit;
		return offset >= ctrl.questionsList.length;
	};
	
	ctrl.submitHandler = function(form) {
		if (form.$invalid) {
			if ($('[name="'+form.$name+'"]').find('.form-error-msg').length) {
				scrollToElement('.form-error-msg', 15);
			} else {
				scrollToElement('.ng-invalid + .checkbox', 15);
			}
			return;
		}
		var offset = ctrl.visibleQuestionnaire.offset + ctrl.visibleQuestionnaire.limit;
		if (offset >= ctrl.questionsList.length) {
			QuestionnaireService.sendQuestionnaire(Array.prototype.concat(ctrl.questionsList, ctrl.checkboxesList), $rootScope.loggedUser)
				.then(function (response) {
					if (!$scope.handleErrors(response, 'questionnaireInsert')) {
						var user = Utils.parseResponse(response);
						$rootScope.loggedUser.userStep = user.userStep;
						AuthService.redirectAfterLogin(user);
					}
				}, function(response) {
					$scope.handleNetworkError(response);
				});
		} else {
			ctrl.visibleQuestionnaire.offset = offset;
			form.$setPristine();
			setTimeout(ScrollTop);
		}
	};
}]);


spApp.controller('BasicsCtrl', ['$scope', '$rootScope', '$document', '$state', 'SingleSelect', 'scrollService', 'activeSection', 'scrollToElement', function($scope, $rootScope, $document, $state, SingleSelect, scrollService, activeSection, scrollToElement) {
	var ctrl = this;
	ctrl.getMsgs = $rootScope.getMsgs;
	if ($state.params.id) {
		setTimeout(function() {
			scrollToElement('[name="'+$state.params.id+'"]', -100);
		}, 750);
	}
	
	ctrl.countries = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'country'}, mutateCallback: $rootScope.getMsgs, isSmart: true, filterPlaceholder: $rootScope.getMsgs('search')});
	var countries = [];
	for(key in $rootScope.countries){
		if($rootScope.countries.hasOwnProperty(key)){
			countries.push({
				id: key, 
				name: $rootScope.countries[key].displayName,
				flagCode: $rootScope.countries[key].a2.toLowerCase(),
				supportPhone: $rootScope.countries[key].supportPhone
			});
		}
	}
	ctrl.countries.fillOptions(countries);
	ctrl.countries.setById($rootScope.userLogout.country.isBlocked ? 226 : $rootScope.userLogout.country.id);
	
	if ($rootScope.isMobile === false) {
		setAllParalax();
		$document.on('scroll', setAllParalax);
	}
	
	setTimeout(function() {
		setCurrentViewPort();
	});
	scrollService.subscribe(setCurrentViewPort);
	
	$scope.$on('$destroy', function() {
		$document.off('scroll', setAllParalax);
		scrollService.unsubscribe(setCurrentViewPort);
		activeSection.set('');
	});
	
	function setAllParalax() {
		setParalax({container: '.basics-header-section', imageRatio: 0.8, imageWidth: 1920, imageHeight: 680, imageStaticOffset: 240});
		setParalax({container: '.support-section', imageRatio: 1.4, imageWidth: 1920, imageHeight: 2000, imageStaticOffset: -1400});
		setParalax({container: '.profit-profile-diagram-section', imageRatio: 0.7, imageWidth: 1920, imageHeight: 1280, imageStaticOffset: 0});
	}
	
	function setCurrentViewPort() {
		var $menu = $('.menu');
		var topScrollPosition = $(window).scrollTop() + $menu[0].offsetTop + $menu.outerHeight();
		$('.basics-screen').find('.section')
			.each(function() {
				var $section = $(this);
				var sectionTop = $section.offset().top;
				var sectionBottom = sectionTop + $section.outerHeight();
				if (topScrollPosition >= sectionTop && topScrollPosition < sectionBottom) {
					$rootScope.$applyAsync(function() {
						var activeSectionName = $section.attr('data-nav-marker');
						activeSection.set(activeSectionName);
						ctrl.isDark = $section[0].hasAttribute('data-is-dark');
					});
					return false;
				}
			});
	}
}]);
