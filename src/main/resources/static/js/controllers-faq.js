spApp.controller('FAQCtr', ['$rootScope', '$scope', function($rootScope, $scope) {
	
	$scope.FAQ_CATEGORIES = {
		trading: 1,
		product_types: 2,
		deposit_and_withdraw: 3,
		my_account: 4,
		technical: 5
	};
	
	$scope.filter = {};
	$scope.filter.currentCategory = $scope.FAQ_CATEGORIES.trading;

	$scope.faqItems = [
		{title: $rootScope.getMsgs('faq-trading-structured-product-title'), content: $rootScope.getMsgs('faq-trading-structured-product-content'), category: $scope.FAQ_CATEGORIES.trading, active: true},
		{title: $rootScope.getMsgs('faq-trading-underlying-asset-title'), content: $rootScope.getMsgs('faq-trading-underlying-asset-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-term-sheet-title'), content: $rootScope.getMsgs('faq-trading-term-sheet-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-barrier-event-title'), content: $rootScope.getMsgs('faq-trading-barrier-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-capital-protected-product-title'), content: $rootScope.getMsgs('faq-trading-capital-protected-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-yield-enhancement-product-title'), content: $rootScope.getMsgs('faq-trading-yield-enhancement-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-participation-product-title'), content: $rootScope.getMsgs('faq-trading-participation-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-credit-risk-title'), content: $rootScope.getMsgs('faq-trading-credit-risk-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-calculate-redemption-amount-title'), content: $rootScope.getMsgs('faq-trading-calculate-redemption-amount-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-events-affecting-product-title'), content: $rootScope.getMsgs('faq-trading-events-affecting-product-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-buy-sp-different-currency-title'), content: $rootScope.getMsgs('faq-trading-buy-sp-different-currency-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-invest-in-sp-title'), content: $rootScope.getMsgs('faq-trading-invest-in-sp-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-issuer-knows-his-sp-title'), content: $rootScope.getMsgs('faq-trading-issuer-knows-his-sp-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-case-of-credit-event-title'), content: $rootScope.getMsgs('faq-trading-case-of-credit-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-insured-against-credit-event-title'), content: $rootScope.getMsgs('faq-trading-insured-against-credit-event-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-secondary-market-price-title'), content: $rootScope.getMsgs('faq-trading-secondary-market-price-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-initial-fixing-level-title'), content: $rootScope.getMsgs('faq-trading-initial-fixing-level-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-final-fixing-level-title'), content: $rootScope.getMsgs('faq-trading-final-fixing-level-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-issue-price-title'), content: $rootScope.getMsgs('faq-trading-issue-price-content'), category: $scope.FAQ_CATEGORIES.trading},
		{title: $rootScope.getMsgs('faq-trading-Quanto-title'), content: $rootScope.getMsgs('faq-trading-Quanto-content'), category: $scope.FAQ_CATEGORIES.trading},
		
		{title: $rootScope.getMsgs('faq-product-types-capital-guarantee-products-title'), content: $rootScope.getMsgs('faq-product-types-capital-guarantee-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		{title: $rootScope.getMsgs('faq-product-types-yield-enhancement-products-title'), content: $rootScope.getMsgs('faq-product-types-yield-enhancement-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		{title: $rootScope.getMsgs('faq-product-types-participation-products-title'), content: $rootScope.getMsgs('faq-product-types-participation-products-content'), category: $scope.FAQ_CATEGORIES.product_types},
		
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-deposit-options-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-deposit-options-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-currency-in-my-account-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-currency-in-my-account-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-minimal-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-minimal-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-withdraw-profit-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-withdraw-profit-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-must-deposit-register-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-must-deposit-register-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-cancel-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-cancel-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		{title: $rootScope.getMsgs('faq-deposit-and-withdraw-request-wire-withdrawal-title'), content: $rootScope.getMsgs('faq-deposit-and-withdraw-request-wire-withdrawal-content'), category: $scope.FAQ_CATEGORIES.deposit_and_withdraw},
		
		{title: $rootScope.getMsgs('faq-my-account-forgotten-password-title'), content: $rootScope.getMsgs('faq-my-account-forgotten-password-content'), category: $scope.FAQ_CATEGORIES.my_account},
		{title: $rootScope.getMsgs('faq-my-account-transaction-history-title'), content: $rootScope.getMsgs('faq-my-account-transaction-history-content'), category: $scope.FAQ_CATEGORIES.my_account},
		{title: $rootScope.getMsgs('faq-my-account-update-personal-details-title'), content: $rootScope.getMsgs('faq-my-account-update-personal-details-content'), category: $scope.FAQ_CATEGORIES.my_account},
		
		{title: $rootScope.getMsgs('faq-technical-download-software-title'), content: $rootScope.getMsgs('faq-technical-download-software-content'), category: $scope.FAQ_CATEGORIES.technical},
		{title: $rootScope.getMsgs('faq-technical-change-language-title'), content: $rootScope.getMsgs('faq-technical-change-language-content'), category: $scope.FAQ_CATEGORIES.technical},
	];

	$scope.selectCategory = function(category){
		$scope.filter.currentCategory = category;
	};
	$scope.isCategoryActive = function(category){
		return $scope.filter.currentCategory == category;
	};
	
	$scope.toggleItem = function(item){
		for(var i = 0; i < $scope.faqItems.length; i++){
			if($scope.faqItems[i] != item){
				$scope.faqItems[i].active = false;
			}
		}
		item.active = !item.active;
	};
	
}]);