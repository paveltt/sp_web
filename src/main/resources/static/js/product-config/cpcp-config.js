function CPCPConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'cpcp',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: false,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''},
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(1) + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))'
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'}
			]
		};
	}
}