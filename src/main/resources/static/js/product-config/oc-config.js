function OCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.isCapped = function(){
		return selectedProduct.capLevel !== 0;
	}
	this.getCapLevel = function(){
		return selectedProduct.capLevel;
	}
	
	this.loadConfig = function(){
		return {
			shortName: 'oc',
			hasBarrier: false,
			headerShortSuffix: this.isCapped() ? 'capped' : '',
			'left-top-row-2': {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'left-top-row-3': {
				title: 'return', value: !this.isCapped() ? '' : $scope.getMsgs('capped-at') + '\xA0' + (selectedProduct.capLevel - 100), extra: !this.isCapped() ? $scope.getMsgs('uncapped') : '', hidePercent: !this.isCapped()
			},
			'top-row-2': this.isCapped() && $scope.productIsSecondary(selectedProduct) ? {
				title: 'cap-level', value: this.getCapLevel(), extra: ''
			} : {
				title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'cap-level', value: this.getCapLevel() ? this.getCapLevel() : 'None', extra: '', format: this.getCapLevel() ? '' : 'small', hidePercent: !this.getCapLevel()
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: false,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : !this.isCapped() ? [
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(1) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
				] : [
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('cap-level')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(this.getCapLevel() + '%'),
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord('participation') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(round(this.getScenariosFinalFixingLevel(1), 2) + '%') + ')',
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
				],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('100%') + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))',
						isHidden: selectedProduct.capLevel != 0
					},
					{
						formula: this.feWord('Min') + '\\left\\{' + this.feWord($scope.getMsgs('cap-level')) + this.feSign('\\times') + this.feWord($scope.getMsgs('denomination')) + '; ' + this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('100%') + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))\\right\\}',
						isHidden: selectedProduct.capLevel == 0,
						isLong: true
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'capLevel']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'}
			]
		};
	}
}