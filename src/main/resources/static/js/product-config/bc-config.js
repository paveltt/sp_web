function BCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'bc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'bonus-level', value: selectedProduct.bonusLevel, extra: ''
			},
			'left-top-row-3': {
				title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'bonus-level', value: selectedProduct.bonusLevel, extra: ''
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			'details-right': [
				{title: 'bonus-level', value: selectedProduct.bonusLevel + '%', extra: ''},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'},
					{template: 'full'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') +  this.feWord((100 + this.getScenariosFinalFixingLevel(0)) + '%'),
				!selectedProduct.productBarrier.barrierOccur ?
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('bonus-level')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.bonusLevel + '%')
				:
					this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord((100 - this.getScenariosFinalFixingLevel(1)) + '%'),
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') +  this.feWord((100 + this.getScenariosFinalFixingLevel(2)) + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('bonus-level')) + '\\, (' + this.feWord($scope.getMsgs('in-percent'), true) + ')'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}