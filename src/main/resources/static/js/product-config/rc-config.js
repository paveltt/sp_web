function RCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'rc',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'strike-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.strikeLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.strikeLevel, extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)
			},
			'top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'strike-level', value: selectedProduct.strikeLevel, extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'strike-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.strikeLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.strikeLevel + '%', extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' + $scope.getMsgs(selectedProduct.productCouponFrequency.displayName) : '', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.getTooltipFormulaParam('final_div_strike_level', 1) + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.getTooltipFormulaParam('final_div_strike_level', 2) + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')'
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '(' + this.feWord('1') + this.feSign('+') + '\\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('strike-level')) + '}{' + this.feWord($scope.getMsgs('strike-level')) + '}\\right))'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'}
			]
		};
	}
}