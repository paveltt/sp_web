function CPCCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'cpcc',
			hasBarrier: false,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'coupon-trigger-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel : '', extra: ''
			} : {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: ''
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'coupon-trigger-level', value: $scope.productIsSecondary(selectedProduct) ? round(selectedProduct.productCoupons[0].triggerLevel / 100 * this.getInitialFixingLevel(selectedProduct), 2) : selectedProduct.productCoupons[0].triggerLevel + '%', extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''}
			],
			'details-right': [
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'initial-fixing-level', value: $scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) : '100%', extra: ''},
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''}
			],
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'couponTriggerEvent', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'}
			]
		};
	}
}