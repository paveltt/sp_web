function ProductTypeConfig($scope, selectedProduct){
	this.getQuanto = function(){
		return selectedProduct.quanto ? $scope.getMsgs('yes') : $scope.getMsgs('no');
	}
	this.getBarrierEvent = function(){
		if(!selectedProduct.productBarrier){
			return '';
		}
		return selectedProduct.productBarrier.barrierOccur ? $scope.getMsgs('yes') : $scope.getMsgs('no');
	}
	this.getBarrierText = function(){
		if(selectedProduct.productBarrier){
			if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
				return $scope.getMsgs('continuous-barrier-monitoring');
			}else{
				return $scope.getMsgs('barrier-monitoring-at-final-fixing-date');
			}
		}
		return '';
	}
	this.getBarrierTextShort = function(){
		if(selectedProduct.productBarrier){
			if(selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American){
				return $scope.getMsgs('continuously');
			}else{
				return $scope.getMsgs('at-final-fixing-date');
			}
		}
		return '';
	}
	this.getBarrierLevel = function(){
		if(selectedProduct.productBarrier){
			return round(selectedProduct.productBarrier.barrierLevel*$scope.productGetInitialFixingLevel(selectedProduct)/100, 2);
		}
		return '';
	}
	this.getCouponPayRate = function(){
		return selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '';
	}
	this.getFinalFixingLevel = function(){
		if(selectedProduct.productMarkets && selectedProduct.productMarkets[0]){
			return selectedProduct.productMarkets[0].endTradeLevel;
		}
		return '';
	}
	this.getInitialFixingLevel = function(){
		return $scope.productGetInitialFixingLevel(selectedProduct);
	}
	this.getScenariosFinalFixingLevel = function(scenarioIndex){
		return selectedProduct.productScenarios[scenarioIndex].finalFixingLevelForTxt;
	}
	this.getTooltipFormulaParam = function(param, scenarioIndex){
		var result = '';
		
		switch(param){
			case 'denomination-label':
				if($scope.productIsPrimary(selectedProduct)){
					result = this.feWord($scope.getMsgs('investment-amount'));
				}else{
					// result = '\\left(\\frac{' + this.feWord($scope.getMsgs('investment-amount')) + '}{' + this.feWord($scope.getMsgs('ask')) + '}\\right)';
					result = this.feWord($scope.getMsgs('denomination'));
				}
				break;
			case 'denominationAmount':
				if($scope.productIsPrimary(selectedProduct)){
					result = '${investment}';
				}else{
					result = '\\frac{' + '${investment}' + '}{' + this.feWord(selectedProduct.ask + '%') + '}';
				}
				break;
			case 'investmentAmount':
				result = '${investment}';
				break;
			case 'investmentDays_div_360':
				/*var initialDate = new Date(selectedProduct.initialFixingDate);
				var finalDate = new Date(selectedProduct.finalFixingDate);
				result = '\\frac{' + this.feWord(360*(finalDate.getFullYear() - initialDate.getFullYear()) + 30*(finalDate.getMonth() - initialDate.getMonth()) + (finalDate.getDate() - initialDate.getDate())) + '}{' + this.feWord('360') + '}';*/
				result = '\\frac{' + this.feWord(selectedProduct.couponDays) + '}{' + this.feWord('360') + '}';
				break;
			case 'final_div_initial_times_barrier':
				/*if(selectedProduct.productBarrier){
					result = this.feWord(round((selectedProduct.productScenarios[scenarioIndex].finalFixingLevel/this.getInitialFixingLevel())*selectedProduct.productBarrier.barrierLevel, 2) + '%');
				}else{
					result = '';
				}*/
				result = this.feWord(this.getScenariosFinalFixingLevel(scenarioIndex) + '%');
				break;
			case 'initial_sub_final_div_initial_times_barrier':
				/*if(selectedProduct.productBarrier){
					result = this.feWord(round((this.getInitialFixingLevel() - selectedProduct.productScenarios[scenarioIndex].finalFixingLevel)/this.getInitialFixingLevel(), 2)*(selectedProduct.productBarrier.barrierLevel - 100) + '%');
				}else{
					result = '';
				}*/
				var percentage = (-1)*this.getScenariosFinalFixingLevel(scenarioIndex);
				var sign = '+';
				if(percentage < 0){
					percentage *= (-1);
					sign = '-';
				}
				result = this.feSign(sign) + this.feWord(percentage + '%');
				break;
			case 'final_div_strike_level':
				result = this.feWord(round(((selectedProduct.productScenarios[scenarioIndex].finalFixingLevel*100)/selectedProduct.strikeLevel)*100, 2) + '%');
				break;
			default:
				result = '';
		}
		
		return result;
	}
	this.parseTooltipFormula = function(tooltip, params){
		if(tooltip && params){
			for(key in params){
				if(params.hasOwnProperty(key)){
					var toReplace = '${' + key + '}';
					tooltip = tooltip.replace(toReplace, this.feWord(params[key]));
				}
			}
		}
		return tooltip;
	}
	this.getCouponAmount = function(note){
		var couponAmount = '';
		couponAmount = $scope.formatAmount({amount: note.couponAmonut, currencyId: settings.CURRENCY_EUR, centsPart: 2});
		if(note.couponOriginalAmonut != note.couponAmonut){
			couponAmount += ' (' + $scope.formatAmount({amount: note.couponOriginalAmonut, currencyId: settings.CURRENCY_USD, centsPart: 2}) + ')';
		}
		return couponAmount;
	}
	this.getRedemptionAmount = function(note){
		var redemptionAmount = '';
		redemptionAmount = $scope.formatAmount({amount: note.investment.returnAmount, currencyId: settings.CURRENCY_EUR, centsPart: 2});
		if(note.investment.originalReturnAmount != note.investment.returnAmount){
			redemptionAmount += ' (' + $scope.formatAmount({amount: note.investment.originalReturnAmount, currencyId: settings.CURRENCY_USD, centsPart: 2}) + ')';
		}
		return redemptionAmount;
	}
	this.getNoteRedemptionAmount = function(){
		var that = this;
		return function(note){
			return that.getRedemptionAmount(note);
		};
	}
	//Format equation word
	this.feWord = function(str, forceLower){
		//var temp = str.replace(/ /g, '\\,');
		var result = '';
		var temp = str + '';
		for(var i = 0; i < temp.length; i++){
			var shouldCap = false;
			if(i == 0 || temp[i - 1] == ' '){
				shouldCap = true;
			}
			var shouldEscape = false;
			if(temp[i] == '%'){
				shouldEscape = true;
			}
			if(temp[i] != ' '){
				result += '\\style{font-family:\'Fira Sans\';' + (shouldCap && !forceLower ? ';text-transform:uppercase' : '') + '}{' + (shouldEscape ? '\\' : '') + temp[i] + '} ';
			}else{
				result += ' \\, ';
			}
		}
		return result;
	}
	//Format equation sign
	this.feSign = function(str){
		return '\\style{font-weight:500;font-size:1.4em;vertical-align:middle;}{' + str + '}';
	}
}