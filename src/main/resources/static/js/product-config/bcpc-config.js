function BCPCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	this.loadConfig = function(){
		return {
			shortName: 'bcpc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'upside', value: selectedProduct.upside, extra: ''
			},
			'left-top-row-3': {
				title: 'protection', value: selectedProduct.levelOfProtection, extra: ''
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				dual: true,
				params: [
					{title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: $scope.productIsSecondary(selectedProduct)},
					{title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''}
				]
			} : {
				dual: true,
				params: [
					{title: 'barrier-level', value: this.getBarrierLevel(), extra: '', hidePercent: true},
					{title: 'participation', value: selectedProduct.levelOfParticipation, extra: ''}
				]
			},
			'top-row-3': {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				dual: true,
				params: [
					{title: 'issue-price', value: selectedProduct.issuePrice, extra: ''},
					{title: 'current-price', value: selectedProduct.ask, extra: ''}
				]
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'capital-protection', value: selectedProduct.levelOfProtection + '%', extra: ''},
				{title: 'participation', value: selectedProduct.levelOfParticipation + '%', extra: ''}
			],
			'details-right': [
				{title: 'bondfloor-at-issuance', value: selectedProduct.bondFloorAtissuance + '%', extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'short', formulaResult: selectedProduct.levelOfProtection/100},
					{template: 'skip'},
					{template: 'skip'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				!selectedProduct.productBarrier.barrierOccur ? this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + '(' + this.feWord(selectedProduct.levelOfProtection + '%') + this.feSign('+') + this.feWord(selectedProduct.levelOfParticipation + '%') + this.feSign('\\times') + this.feWord(this.getScenariosFinalFixingLevel(0) + '%') + ')'
				: this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%'),
				
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%'),
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + this.feWord(selectedProduct.levelOfProtection + '%')
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + ' (' + this.feWord($scope.getMsgs('capital-protection')) + this.feSign('+') + this.feWord($scope.getMsgs('participation')) + this.feSign('\\times') + ' \\left(\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + this.feSign('-') + this.feWord($scope.getMsgs('initial-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}\\right))'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + this.feWord($scope.getMsgs('capital-protection'))
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}