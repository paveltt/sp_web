function BRCConfig($scope, selectedProduct){
	ProductTypeConfig.apply(this, arguments);
	
	var that = this;
	
	this.loadConfig = function(){
		return {
			shortName: 'brc',
			hasBarrier: true,
			'left-top-row-2': {
				title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate : '', extra: selectedProduct.productCouponFrequency.displayName
			},
			'left-top-row-3': {
				title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: '', hidePercent: true
			},
			'top-row-2': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: '', hidePercent: true
			} : {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			},
			'top-row-3': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'barrier-monitoring', value: this.getBarrierTextShort(), extra: '', hidePercent: true, format: 'small'
			} : {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			},
			'top-row-4': !$scope.productIsSecondary(selectedProduct) ? {
				title: 'issue-price', value: selectedProduct.issuePrice, extra: ''
			} : {
				title: 'current-price', value: selectedProduct.ask, extra: ''
			},
			hasAsterisk: true,
			'underlying': [
				{title: 'exchange-rate', value: $scope.getMsgs('exchange-rate'), extra: ''},
				{title: 'bbg-ticker', value: selectedProduct.productMarkets[0].market.ticker, extra: ''},
				{title: 'initial-fixing-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getInitialFixingLevel(selectedProduct) + '%' : this.getInitialFixingLevel(selectedProduct), extra: ''},
				{title: 'currency', value: selectedProduct.productMarkets[0].market.currency, extra: ''}
			],
			'details-left': [
				{title: 'issue-price', value: selectedProduct.issuePrice + '%', extra: ''},
				{title: 'issue-size', value: $scope.formatAmount({amount: selectedProduct.issueSize, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'denomination', value: $scope.formatAmount({amount: selectedProduct.denomination, currencyId: selectedProduct.currencyId, centsPart: 2}), extra: ''},
				{title: 'settlement-currency', value: $scope.getCurrencySymbol(selectedProduct.currencyId), extra: ''},
				{title: 'quanto', value: this.getQuanto(), extra: ''},
				{title: 'coupon', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '', extra: ''}
			],
			'details-right': [
				{title: 'barrier-level', value: !$scope.productIsSecondary(selectedProduct) ? this.getBarrierLevel() + '%' : this.getBarrierLevel(), extra: ''},
				{title: 'barrier-type', value: this.getBarrierText(), extra: selectedProduct.productBarrier ? '(' + $scope.getMsgs(selectedProduct.productBarrier.productBarrierType.displayName) + ')' : ''},
				{title: 'barrier-observation-period', value: $scope.productGetBarrierObservationPeriod(null, selectedProduct), extra: ''},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)},
				{title: 'distance-to-barrier', value: selectedProduct.productBarrier ? round(selectedProduct.productBarrier.distanceToBarrier, 2) + '%' : '', extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			],
			'override-main-scenarios': {
				override: selectedProduct.productBarrier.barrierOccur,
				scenarios: [
					{template: 'full', level: $scope.productGetInitialFixingLevel(selectedProduct), formulaResult: (1 + selectedProduct.productCoupons[0].payRate/100), shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-1-short' + ($scope.productIsBarrierTypeEuropean(selectedProduct) ? '-european' : '') + '-barrier-event'},
					{template: 'full', shortTextKey: 'product-type-' + selectedProduct.productType.id + '-scenario-2-short-barrier-event'}
				]
			},
			parseTooltipFormula: this.parseTooltipFormula,
			'scenario-tooltips' : [
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' + this.feWord('1') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')',
				this.getTooltipFormulaParam('denomination-label') + this.feSign('\\times') + ' (' + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}' + this.feSign('+') + this.feWord($scope.getMsgs('coupon')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('investment-days')) + '}{' + this.feWord('360') + '}' + ')' + this.feSign('=') + this.getTooltipFormulaParam('denominationAmount') + this.feSign('\\times') + ' (' +  this.feWord((100 - this.getScenariosFinalFixingLevel(1)) + '%') + this.feSign('+') + this.feWord(this.getCouponPayRate() + '%') + this.feSign('\\times') + this.getTooltipFormulaParam('investmentDays_div_360') + ')'				
			],
			'redemption': {
				scenarios: [
					{
						formula: this.feWord($scope.getMsgs('denomination'))
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')) + this.feSign('\\times') + '\\frac{' + this.feWord($scope.getMsgs('final-fixing-level')) + '}{' + this.feWord($scope.getMsgs('initial-fixing-level')) + '}'
					},
					{
						formula: this.feWord($scope.getMsgs('denomination')),
						isHidden: selectedProduct.productBarrier ? $scope.productIsBarrierTypeEuropean(selectedProduct) : false
					}
				],
				definitions: ['denomination', 'initialFixingLevel', 'finalFixingLevel', 'barrierEvent', 'barrierObservationPeriod', 'couponDayCountConvention']
			},
			'closed-notes-details-left': [
				{title: 'final-fixing-level', value: this.getFinalFixingLevel(), extra: ''},
				{title: 'coupon-level', value: selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '% ' : '', extra: ''},
				{title: 'barrier-level', value: this.getBarrierLevel(), extra: ''}
			],
			'closed-notes-details-right': [
				{title: 'redemption-amount', value: this.getNoteRedemptionAmount(), extra: '', type: 'callback'},
				{title: 'coupon-amount', value: function(note){return that.getCouponAmount(note);}, extra: '', type: 'callback'},
				{title: 'barrier-event', value: this.getBarrierEvent(), extra: '', isHidden: !$scope.productIsSecondary(selectedProduct)}
			]
		};
	}
}