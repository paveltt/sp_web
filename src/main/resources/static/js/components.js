spApp.component('productsGlossary', {
	templateUrl: folderPrefix + 'components/products-glossary.html',
	bindings: {
		template: '<'
	},
	controller: ['$rootScope', '$scope', function($rootScope, $scope) {
		this.getMsgs = $rootScope.getMsgs;
		this.isProductsGlossaryContentVisible = false;

		this.toggleProductsGlossaryContent = function() {
			this.isProductsGlossaryContentVisible = !this.isProductsGlossaryContentVisible;
			if(this.template == 'inline-popup'){
				$rootScope.blurWrapper = this.isProductsGlossaryContentVisible;
			}
		}.bind(this);

		this.close = function() {
			if(this.isProductsGlossaryContentVisible && this.template == 'inline-popup'){
				$rootScope.blurWrapper = false;
			}
			this.isProductsGlossaryContentVisible = false;
		}.bind(this);
		
		$scope.$on('$destroy', function() {
			if(this.template == 'inline-popup'){
				$rootScope.blurWrapper = false;
			}
		});
	}]
});

spApp.component('productsScenarioTooltip', {
	templateUrl: folderPrefix + 'products/products-scenario-tooltip.html',
	bindings: {
		formula: '<',
		denominationFormulaVisible: '<',
		isCentered: '<'
	},
	controller: ['$rootScope', function($rootScope) {
		this.getMsgs = $rootScope.getMsgs;
		this.isProductsScenarioTooltipVisible = false;
	}]
});

spApp.component('articles', {
	templateUrl: folderPrefix + 'articles.html',
	controller: ['articlesList', '$sce', '$rootScope', '$scope', 'PdfPreviewService', function(articlesList, $sce, $rootScope, $scope, PdfPreviewService) {
		this.articles = articlesList;
		this.trustAsHtml = $sce.trustAsHtml;
		this.getMsgs = $rootScope.getMsgs;
		this.isMobile = $rootScope.isMobile;
		
		this.articleLinkClickHandler = function(e, url) {
			if (!url) return;
			e.preventDefault();
			PdfPreviewService.open($sce.trustAsResourceUrl(url));
		};
		
		$scope.$on('$destroy', function() {
			PdfPreviewService.close();
		});
	}],
	controllerAs: 'articlesCtrl'
});

spApp.constant('articlesList', [
	{
		id: 1,
		logoUrl: 'images/articles/moneyadvice.svg',
		hpLogoUrl: 'images/articles/hp-moneyadvice.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneyadviceservice_structured-deposits-and-investment-product.pdf',
		title: 'article-1-title',
		subTitle: 'article-1-subTitle',
		shortText: 'article-1-shortText',
		shortDescription: 'article-1-short-description'
	},
	{
		id: 2,
		logoUrl: 'images/articles/bloomberg.svg',
		hpLogoUrl: 'images/articles/hp-bloomberg.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/bloomberg_asian-structured-products.pdf',
		title: 'article-2-title',
		subTitle: 'article-2-subTitle',
		shortText: 'article-2-shortText',
		shortDescription: 'article-2-short-description'
	},
	{
		id: 3,
		logoUrl: 'images/articles/bloomberg.svg',
		hpLogoUrl: 'images/articles/hp-bloomberg.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/bloomberg_sales-of-structured-products.pdf',
		title: 'article-3-title',
		subTitle: 'article-3-subTitle',
		shortText: 'article-3-shortText',
		shortDescription: 'article-3-short-description'
	},
	{
		id: 4,
		logoUrl: 'images/articles/instreet.svg',
		hpLogoUrl: 'images/articles/hp-instreet.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/instreet_structured-products-long-time.pdf',
		title: 'article-4-title',
		subTitle: 'article-4-subTitle',
		shortText: 'article-4-shortText',
		shortDescription: 'article-4-short-description'
	},
	{
		id: 5,
		logoUrl: 'images/articles/risk.svg',
		hpLogoUrl: 'images/articles/hp-risk.svg',
		pdfUrl: '',
		title: 'article-5-title',
		subTitle: 'article-5-subTitle',
		shortText: 'article-5-shortText',
		shortDescription: 'article-5-short-description'
	},
	{
		id: 6,
		logoUrl: 'images/articles/ddv.svg',
		hpLogoUrl: 'images/articles/hp-ddv.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/ddv_express-savers-advice-sp.pdf',
		title: 'article-6-title',
		subTitle: 'article-6-subTitle',
		shortText: 'article-6-shortText',
		shortDescription: 'article-6-short-description'
	},
	{
		id: 7,
		logoUrl: 'images/articles/moneywise.svg',
		hpLogoUrl: 'images/articles/hp-moneywise.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneywise_structured-products-under-the-microscope.pdf',
		title: 'article-7-title',
		subTitle: 'article-7-subTitle',
		shortText: 'article-7-shortText',
		shortDescription: 'article-7-short-description'
	},
	{
		id: 8,
		logoUrl: 'images/articles/moneywise.svg',
		hpLogoUrl: 'images/articles/hp-moneywise.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneywise_investment-guide-structured-products.pdf',
		title: 'article-8-title',
		subTitle: 'article-8-subTitle',
		shortText: 'article-8-shortText',
		shortDescription: 'article-8-short-description'
	},
	{
		id: 9,
		logoUrl: 'images/articles/express.svg',
		hpLogoUrl: 'images/articles/hp-express.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/express_savers-advice-structured-products.pdf',
		title: 'article-9-title',
		subTitle: 'article-9-subTitle',
		shortText: 'article-9-shortText',
		shortDescription: 'article-9-short-description'
	},
	{
		id: 10,
		logoUrl: 'images/articles/investorschronicle.svg',
		hpLogoUrl: 'images/articles/hp-investorschronicle.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/investors-chronicle_beat-stormy-markets-with-structured-products.pdf',
		title: 'article-10-title',
		subTitle: 'article-10-subTitle',
		shortText: 'article-10-shortText',
		shortDescription: 'article-10-short-description'
	},
	{
		id: 11,
		logoUrl: 'images/articles/moneyweek.svg',
		hpLogoUrl: 'images/articles/hp-moneyweek.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/moneyweek_its-time-to-welcome-back-structured-products.pdf',
		title: 'article-11-title',
		subTitle: 'article-11-subTitle',
		shortText: 'article-11-shortText',
		shortDescription: 'article-11-short-description'
	},
	{
		id: 12,
		logoUrl: 'images/articles/wallstreet.svg',
		hpLogoUrl: 'images/articles/hp-wallstreet.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/wsj_voices-when-structured-notes-make-sense.pdf',
		title: 'article-12-title',
		subTitle: 'article-12-subTitle',
		shortText: 'article-12-shortText',
		shortDescription: 'article-12-short-description'
	},
	{
		id: 13,
		logoUrl: 'images/articles/yahoo.svg',
		hpLogoUrl: 'images/articles/hp-yahoo.svg',
		pdfUrl: 'https://cdn.anycapital.com/assets/yahoo_an-introduction-to-structured-products.pdf',
		title: 'article-13-title',
		subTitle: 'article-13-subTitle',
		shortText: 'article-13-shortText',
		shortDescription: 'article-13-short-description'
	}
]);

spApp.component('profitProfileDiagramComponent', {
	templateUrl: folderPrefix + 'profit-profile-diagram.html',
	controller: ['$rootScope', '$timeout', 'ProfitProfileDiagrams', function($rootScope, $timeout, ProfitProfileDiagrams) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.isMobile = $rootScope.isMobile;
		
		ctrl.profitProfileTypes = ['capital-protection', 'yield-enhancement', 'participation'];
		ctrl.activeProfitProfileDiagram = 0;
		ctrl.profitProfileDiagrams = ProfitProfileDiagrams[ctrl.activeProfitProfileDiagram];
		
		ctrl.setActiveProfitProfileDiagram = function(activeNum) {
			if (ctrl.activeProfitProfileDiagram === activeNum) return;
			ctrl.activeProfitProfileDiagram = activeNum;
			ctrl.profitProfileDiagrams = [];
			$timeout(function() {
				ctrl.profitProfileDiagrams = ProfitProfileDiagrams[ctrl.activeProfitProfileDiagram];
			}, 200);
		};
		
		ctrl.slickSettings = {
			mobile: {
				event: {
					init: addClone,
					beforeChange: addClone
				}
			}
		};
		
		$rootScope.$watch('isMobile', function(isMobile) {
			ctrl.isMobile = isMobile;
		});
		
		function addClone(e) {
			var $slick = $(e.target);
			var $dots = $slick.find('.slick-dots').not('.slick-dots-clone');
			var $dotsClone = $dots.clone().addClass('slick-dots-clone');
			$slick.find('.slick-dots-clone').remove();
			$slick.append($dotsClone);
			$dotsClone.on('click', 'li', function() {
				var clickedEl = this;
				$dotsClone.find('li').each(function(index) {
					if (this === clickedEl) {
						$($dots.find('li')[index]).click();
						return false;
					}
				});
			});
		}
	}],
	controllerAs: '$ctrl'
});

spApp.constant('ProfitProfileDiagrams', [
	// 0
	[{
		id: 1,
		title: 'profit-profile-title-cpcp',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-rising-volatility', 'profit-profile-market-expectation-sharply-falling-underlying'],
		profitLossImageName: 'cpcp.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-participation-underlying'
		],
		sampleStateParam: {
			type: 1
		}
	},
	{
		id: 2,
		title: 'profit-profile-title-bcpc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-sharply-falling-underlying', 'profit-profile-market-expectation-underlying-not-going-to-touch'],
		profitLossImageName: 'bcpc.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-participation-underlying-up-to-barrier',
			'profit-profile-characteristic-possibility-of-rebate-payment',
			'profit-profile-characteristic-limited-profit-potential'
		],
		sampleStateParam: {
			type: 2
		}
	},
	{
		id: 3,
		title: 'profit-profile-title-cpcc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-sharply-falling-underlying'],
		profitLossImageName: 'cpcc.svg',
		characteristicsList: [
			'profit-profile-characteristic-minimum-redemption',
			'profit-profile-characteristic-capital-protection-defined',
			'profit-profile-characteristic-capital-protection-refers',
			'profit-profile-characteristic-Value-of-product',
			'profit-profile-characteristic-copupon-amount-dependent',
			'profit-profile-characteristic-periodic-coupon-payment',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 3
		}
	}],
	// 1
	[{
		id: 4,
		title: 'profit-profile-title-ibrc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways-decreasing', 'profit-profile-market-expectation-falling-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'ibrc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-barrier-never-reached',
			'profit-profile-characteristic-due-to-barrier-possibility-maximum-redemption-higher',
			'profit-profile-characteristic-if-barrier-breached-product-changes',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk-selling-underlying',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 6
		}
	},
	{
		id: 5,
		title: 'profit-profile-title-rc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-falling-volatility'],
		profitLossImageName: 'rc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-close-below',
			'profit-profile-characteristic-should-close-above',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-higher-coupons',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 4
		}
	},
	{
		id: 6,
		title: 'profit-profile-title-brc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-falling-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'brc.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-barrier-never-reached',
			'profit-profile-characteristic-due-to-barrier-possibility-maximum-redemption-higher',
			'profit-profile-characteristic-if-barrier-breached-product-changes',
			'profit-profile-characteristic-coupon-paid-regardless',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity-cap'
		],
		sampleStateParam: {
			type: 5
		}
	},
	{
		id: 7,
		title: 'profit-profile-title-ec',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-decreasing-volatility', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'ec.svg',
		characteristicsList: [
			'profit-profile-characteristic-should-underlying-trade-above',
			'profit-profile-characteristic-offers-possibility-early-redemption',
			'profit-profile-characteristic-reduced-risk',
			'profit-profile-characteristic-with-higher-risk-levels-lowerbarriers',
			'profit-profile-characteristic-limited-profit-opportunity'
		],
		sampleStateParam: {
			type: 7
		}
	}],
	// 2
	[{
		id: 8,
		title: 'profit-profile-title-oc',
		marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-rising-volatility'],
		profitLossImageName: 'oc.svg',
		characteristicsList: [
			'profit-profile-characteristic-participation-in-development',
			'profit-profile-characteristic-disproportionate-participation',
			'profit-profile-characteristic-reflects-underlying-price',
			'profit-profile-characteristic-risk-comparable'
		],
		sampleStateParam: {
			type: 8
		}
	}, {
		id: 9,
		title: 'profit-profile-title-bc',
		marketExpectationList: ['profit-profile-market-expectation-underlying-moving-sideways', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
		profitLossImageName: 'bc.svg',
		characteristicsList: [
			'profit-profile-characteristic-participation-in-development',
			'profit-profile-characteristic-minimum-redemption-equal-nominal',
			'profit-profile-characteristic-if-barrier-breached-product-changes-tc',
			'profit-profile-characteristic-with-greater-risk-multiple-underlyings',
			'profit-profile-characteristic-reduced-risk'
		],
		sampleStateParam: {
			type: 9
		}
	}
	// , {
	// 	id: 10,
	// 	title: 'profit-profile-title-boc',
	// 	marketExpectationList: ['profit-profile-market-expectation-rising-underlying', 'profit-profile-market-expectation-underlying-not-reach-barrier'],
	// 	profitLossImageName: 'boc.svg',
	// 	characteristicsList: [
	// 		'profit-profile-characteristic-participation-in-development',
	// 		'profit-profile-characteristic-disproportionate-participation',
	// 		'profit-profile-characteristic-minimum-redemption-equal-nominal',
	// 		'profit-profile-characteristic-if-barrier-breached-product-changes-oc',
	// 		'profit-profile-characteristic-with-greater-risk-multiple-underlyings',
	// 		'profit-profile-characteristic-reduced-risk'
	// 	],
	// 	sampleStateParam: {
	// 		type: 10
	// 	}
	// }
	]
]);


spApp.component('tutorialsComponent', {
	templateUrl: folderPrefix + 'tutorials.html',
	controller: ['$rootScope', 'tutorialsList', '$uibModal', function($rootScope, tutorialsList, $uibModal) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		
		ctrl.visibleProducts = {
			count: $rootScope.isMobile ? 3 : 6,
			increment: $rootScope.isMobile ? 3 : 6
		};
		
		ctrl.tutorialsList = tutorialsList;
		
		ctrl.loadMoreClickHandler = function() {
			ctrl.visibleProducts.count += ctrl.visibleProducts.increment;
		};
		
		ctrl.openVideoModal = function(tutorial) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: folderPrefix + 'components/video-modal.html',
				controller: ['$scope', '$uibModalInstance', 'video', function($scope, $uibModalInstance, video) {
					$scope.close = function(reason) {
						reason = reason || 'close';
						$uibModalInstance.close(reason);
					};
					
					$scope.video = video;
				}],
				size: 'lg',
				windowClass: 'video-modal',
				resolve: {
					video: ['$sce', function($sce) {
						return {
							name: tutorial.name,
							url: $sce.trustAsResourceUrl(tutorial.url)
						};
					}]
				}
			});
		};
	}],
	controllerAs: '$ctrl'
});

spApp.constant('tutorialsList', [
	{
		id: 1,
		name: 'The name of video',
		previwImage: 'tutorial-1.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 2,
		name: 'The name of video',
		previwImage: 'tutorial-2.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 3,
		name: 'The name of video',
		previwImage: 'tutorial-3.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 4,
		name: 'The name of video',
		previwImage: 'tutorial-4.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 5,
		name: 'The name of video',
		previwImage: 'tutorial-5.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 6,
		name: 'The name of video',
		previwImage: 'tutorial-6.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	},
	{
		id: 7,
		name: 'The name of video',
		previwImage: 'tutorial-2.jpg',
		url: 'https://www.youtube.com/embed/4F9ifFqaOX4?rel=0'
	}
]);


spApp.component('underlyingAssets', {
	templateUrl: folderPrefix + 'underlying-assets.html',
	controller: ['$rootScope', '$timeout', 'UnderlyingAssetsService', function($rootScope, $timeout, UnderlyingAssetsService) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.isMobile = $rootScope.isMobile;
		
		var assetsList = [];
		ctrl.underlyingAssetsList = [];
		ctrl.underlyingAssetsFiltersList = [];
		
		ctrl.assetSearchValue = '';
		ctrl.underlyingAssetsFormHandler = function(form) {
			if (!ctrl.assetSearchValue || form.$invalid || isDuplicate(ctrl.underlyingAssetsFiltersList, ctrl.assetSearchValue)) return;
			ctrl.underlyingAssetsFiltersList.push(ctrl.assetSearchValue);
			ctrl.assetSearchValue = '';
			applyFiltersToAssets();
		};
		
		ctrl.deleteAsset = function(assetName) {
			var index = ctrl.underlyingAssetsFiltersList.indexOf(assetName);
			if (index === -1) return;
			ctrl.underlyingAssetsFiltersList.splice(index, 1);
			applyFiltersToAssets();
		};
		
		UnderlyingAssetsService.getUnderlyingAssets().then(function(res) {
			var assets = res.data.data;
			assetsList = Object.keys(assets).map(function(key) {
				return assets[key];
			});
			applyFiltersToAssets();
		});
		
		function isDuplicate(arr, value) {
			return arr.indexOf(value) !== -1;
		}
		
		function applyFiltersToAssets() {
			ctrl.underlyingAssetsList = [];
			showLoading();
			$timeout(function() {
				if (!ctrl.underlyingAssetsFiltersList.length) {
					ctrl.underlyingAssetsList = assetsList.slice();
					hideLoading();
					return;
				}
				ctrl.underlyingAssetsList = assetsList.filter(function(asset) {
					var displayName = ctrl.getMsgs(asset.displayName).toLowerCase();
					for (var i = 0; i < ctrl.underlyingAssetsFiltersList.length; i++) {
						var searchValue = ctrl.underlyingAssetsFiltersList[i];
						var index = displayName.indexOf(searchValue);
						if (index !== -1) {
							hideLoading();
							return true;
						}
					}
					hideLoading();
					return false;
				});
			});
		}
		
		function showLoading() {
			ctrl.loading = true;
		}
		
		function hideLoading() {
			$timeout(function() {
				ctrl.loading = false;
			}, 50);
		}
	}],
	controllerAs: '$ctrl'
});

spApp.component('loadingDots', {
	template: '<div class="three-bounce"><i class="dot"></i><i class="dot"></i><i class="dot"></i></div>'
});

spApp.component('basicsScrollableViewPort', {
	templateUrl: folderPrefix + 'basics-scrollable-viewPort.html',
	controller: ['$rootScope', '$scope', 'scrollToElement', 'activeSection', function($rootScope, $scope, scrollToElement, activeSection) {
		var ctrl = this;
		
		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.scrollToElement = scrollToElement;
		ctrl.getActiveSection = activeSection.get;
		
		ctrl.viewportList = [
			{
				id: 1,
				title: 'the-basics',
				selector: '[name="basics-the-basics"]'
			},
			{
				id: 2,
				title: 'basics-product-categories-title',
				selector: '[name="product-categories"]'
			},
			{
				id: 3,
				title: 'basics-profit-profile-diagram-title',
				selector: '[name="profit-profile-diagram"]'
			},
			{
				id: 4,
				title: 'glossary',
				selector: '[name="basics-glossary"]'
			},
			{
				id: 5,
				title: 'underlying-assets',
				selector: '[name="basics-underlying-assets"]'
			},
			{
				id: 6,
				title: 'articles',
				selector: '[name="basics-articles"]'
			},
		];
	}],
	controllerAs: '$ctrl'
});
