spApp.controller('ProductsCtr', ['$sce', '$rootScope', '$scope', '$state', '$http', '$timeout', '$location', 'Utils', 'SingleSelect', 'setMetaTitle', function($sce, $rootScope, $scope, $state, $http, $timeout, $location, Utils, SingleSelect, setMetaTitle) {
	if ($state.includes('ln.product') && typeof($state.params.productId) === "string") {
		$rootScope.redirectAfterLogin = {
			state: $state.$current.name,
			params: $state.params
		};
	}
	var MJrendererInTimeout = false;
	var MJrerender = function(){
		if(!MJrendererInTimeout){
			MJrendererInTimeout = true;
			setTimeout(function(){
				MathJax.Hub.Queue(["Rerender",MathJax.Hub]);
				MJrendererInTimeout = false;
			}, 500);
		}
	};
	$scope.loadMathJax = function(){
		//Load MathJax
		MathJax.Hub.Config({
			messageStyle: "none",
			"HTML-CSS": {
				showMathMenu: false,
				availableFonts: ["TeX"],
				linebreaks: {automatic: $rootScope.isMobile ? true : false, width: "container"},
				styles: {
					".MathJax": {
						"font-weight": 500,
						"font-size": "1.2em",
						"line-height": "1.5"
					}
				}
			},
			MMLorHTML: {prefer: "HTML"}
		});
		MathJax.Hub.Register.StartupHook("HTML-CSS Jax Ready",function () {
			var VARIANT = MathJax.OutputJax["HTML-CSS"].FONTDATA.VARIANT;
			VARIANT["normal"].fonts.unshift("MathJax_SansSerif");
			VARIANT["bold"].fonts.unshift("MathJax_SansSerif-bold");
			VARIANT["italic"].fonts.unshift("MathJax_SansSerif-italic");
			VARIANT["-tex-mathit"].fonts.unshift("MathJax_SansSerif-italic");
		});
		MathJax.Hub.Register.StartupHook("SVG Jax Ready",function () {
			var VARIANT = MathJax.OutputJax.SVG.FONTDATA.VARIANT;
			VARIANT["normal"].fonts.unshift("MathJax_SansSerif");
			VARIANT["bold"].fonts.unshift("MathJax_SansSerif-bold");
			VARIANT["italic"].fonts.unshift("MathJax_SansSerif-italic");
			VARIANT["-tex-mathit"].fonts.unshift("MathJax_SansSerif-italic");
		});
		MathJax.Hub.Configured();
		
		$(window).on('resize', MJrerender);
	}
	
	$scope.showCredentials = {state: Utils.settings.credentialsView.NONE};
	
	$scope.action = 'loading';
	
	var CURRENCY_ID_EURO = 3;
	
	$scope.INVESTMENT_STATES = {
		initial: 1,
		purchased: 2,
		insufficient_funds: 3,
		general_error: 4,
		insufficient_funds_above_minumum: 5,
		insufficient_funds_below_minimum_secondary: 6,
		below_minumum: 7,
		above_maximum: 8,
		already_secured: 9,
		suspended: 10,
		unavailable: 11,
		already_issued: 12,
		deviation: 13,
		deviation_secondary_primary: 14,
		complete_registration: 15
	}
	
	$scope.investmentState = $scope.INVESTMENT_STATES.initial;
	
	$scope.MATURITY_OPTIONS = {
		primary_all: 1,
		primary_3_6: 2,
		primary_6_12: 3,
		primary_12: 4,
		secondary_all: 5,
		secondary_0_3: 6,
		secondary_3_6: 7,
		secondary_6: 8
	};
	
	$scope.PRODUCT_CATEGORIES = {
		capital_protection: 1,
		yield_enhancement: 2,
		participation: 3
	}
	
	$scope.init = {};
	
	$scope.filter = {};
	
	$scope.singleProduct = true;
	
	$scope.selectedProduct = {};
	
	$scope.products = [];
	$scope.filtered = {
		products: []
	}
	
	$scope.investment = {};
	
	var intervals = [];
	
	$scope.filtered = {products: []};
	$scope.visibleProducts = {count: 12, increment: 12};
	
	$scope.isInvAmountManual = false;
	
	$scope.initFiltersOnReady = function(){
		$scope.loadMathJax();
		
		$scope.filter.assets = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, isSmart: true, filterPlaceholder: $scope.getMsgs('search'), iconClosed: '', iconOpen: ''});
		$scope.filter.type = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, noSort: true});
		$scope.filter.maturity = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, mutateOnlyAggregate: true, noSort: true});
		$scope.filter.protection = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $scope.getMsgs, noSort: true});
		
		var maturityOptions = [
			{id: $scope.MATURITY_OPTIONS.primary_all, name: $scope.getMsgs('primary'), type: 'group'},
			{id: $scope.MATURITY_OPTIONS.primary_3_6, name: '3-6 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.primary_6_12, name: '6-12 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.primary_12, name: '12 ' + $scope.getMsgs('months-and-up'), type: 'groupItem'},
			// {id: $scope.MATURITY_OPTIONS.primary_all, name: $scope.getMsgs('all'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_all, name: $scope.getMsgs('secondary'), type: 'group'},
			{id: $scope.MATURITY_OPTIONS.secondary_0_3, name: '0-3 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_3_6, name: '3-6 ' + $scope.getMsgs('months'), type: 'groupItem'},
			{id: $scope.MATURITY_OPTIONS.secondary_6, name: '6 ' + $scope.getMsgs('months-and-up'), type: 'groupItem'},
			// {id: $scope.MATURITY_OPTIONS.secondary_all, name: $scope.getMsgs('all'), type: 'groupItem'}
		];
		$scope.filter.maturity.fillOptions(maturityOptions);
		var searchParams = $location.search();
		if (searchParams.maturity) {
			$scope.filter.maturity.setById(searchParams.maturity);
		}
	}
	
	//Init the controller
	$rootScope.initScreenCtr($scope);
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.productId){
			$scope.singleProduct = true;
			$scope.productId = $rootScope.$stateParams.productId;
			$scope.getProductFull();
		}else{
			$scope.singleProduct = false;
			$scope.isHomePage = false;
			$scope.productsPerRow = 3;
			$scope.productsRowsPerDisclaimer = 1;
			if($rootScope.isMobile){
				$scope.productsRowsPerDisclaimer = 2;
			}
			if($rootScope.$state.includes("ln.index")){
				$scope.isHomePage = true;
			}else{
				$scope.getAllProductsFilters();
			}
			$scope.getAllProducts();
		}
	}
	
	$scope.$on('$destroy', function(){
		$(window).off('resize', MJrerender);
		for(var i = 0; i < intervals.length; i++){
			clearInterval(intervals[i]);
		}
		$rootScope.blurWrapper = false;
	});
	
	$scope.showConversionRate = function(selectedProduct){
		return $scope.selectedProduct._invCurrencyId != CURRENCY_ID_EURO;//Not Euro
	}
	
	$scope.invAmountInputKeydownHandler = function() {
		$scope.isInvAmountManual = true;
	}
	
	$scope.getProductFull = function(dependencyName){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		}
		$http.post('product/getProductFull', methodRequest).then(
			function(response) {
				$scope.action = '';
				$rootScope.markDependencyDone($scope, dependencyName, 'getProductFull');
				if (!$scope.handleErrors(response, 'getProductFull')) {
					$scope.selectedProduct = Utils.parseResponse(response).product;
					$scope.investmentSum = Utils.parseResponse(response).amount;
					if($scope.investmentSum > 0){
						$scope.selectedProduct._hasInvestment = true;
						$scope.selectedProduct._investmentSum = $scope.investmentSum;
						$scope.selectedProduct._investmentSell = Utils.parseResponse(response).returnAmonut;
					}
					
					$scope.euroRate = Utils.parseResponse(response).rate;
					
					var investmentSettings = $rootScope.investmentPossibilities[$scope.selectedProduct.currencyId];
					$scope.minInvAmount = investmentSettings.minimumAmount/100;
					$scope.selectedProduct._minInvAmount = investmentSettings.minimumAmount/100;
					$scope.selectedProduct._maxInvAmount = investmentSettings.maximumAmount/100;
					$scope.selectedProduct._invCurrencyId = investmentSettings.currencyId;
					$scope.invAmountOptions = [];
					for(var i = 0; i < investmentSettings.investmentPossibilityTab.length; i++){
						$scope.invAmountOptions.push(investmentSettings.investmentPossibilityTab[i].amount/100);
					}
					$scope.investment.invAmount = investmentSettings.defaultAmount/100;
					
					$scope.invAmountGetterSetter = function(amount){
						if($rootScope.isEmpty($scope.selectedProduct)){
							return '';
						}
						var val = 0;
						if(arguments.length){
							amount = getClearedAmount(amount);
							if((amount.split('.')[0] + '').length > 9){
								val = $scope.investment.invAmount;
							}else{
								val = $scope.investment.invAmount = amount;
								if(angular.element('#invAmountInput')[0] && angular.element('#invAmountInput')[0].selectionStart != 'undefined'){
									var selStart = angular.element('#invAmountInput')[0].selectionStart;
									var sel = angular.element('#invAmountInput')[0].value.substring(0, selStart);
									
									var centsPart = 2;
									if((val + '').indexOf('.') != -1){
										centsPart = 0;
									}
									var endVal = $rootScope.formatAmount({amount: val, currencyId: $scope.selectedProduct._invCurrencyId, centsPart: centsPart, skipAmountDivide: true});
									if(endVal.length > angular.element('#invAmountInput')[0].value.length){
										if(findFirstDiffPos(endVal, angular.element('#invAmountInput')[0].value) < selStart){
											selStart++;
										}
									}else if(endVal.length < angular.element('#invAmountInput')[0].value.length){
										if(findFirstDiffPos(endVal, angular.element('#invAmountInput')[0].value) < selStart){
											selStart--;
										}
									}
									
									setTimeout(function(){
										setCaretPosition(angular.element('#invAmountInput')[0], selStart);
									});
								}
							}
						}else{
							val = $scope.investment.invAmount;
						}
						var finalCentsPart = 2;
						if((val + '').indexOf('.') != -1){
							finalCentsPart = 0;
						}
						return $rootScope.formatAmount({amount: val, currencyId: $scope.selectedProduct._invCurrencyId, centsPart: finalCentsPart, skipAmountDivide: true});
					}
					
					function getClearedAmount(value){
						var temp = value.replace(',', '').split('.');
						value = temp[0].replace(regEx_digits_reverse, '');
						if (!isUndefined(temp[1])) {
							value += '.' + temp[1].replace(regEx_digits_reverse, '');
						}
						return value;
					}
					//$scope.selectedProduct.productBarrier.barrierOccur = true;
					$scope.productLoadTypesConfig($scope.selectedProduct);
					
					$scope.refactorScenarioRows($scope.selectedProduct);
					
					$scope.prepareCharts($scope.selectedProduct);
					
					// $timeout(function(){
						$scope.action = '';
					// });
					
					if(!$rootScope.isMobile){
						setTimeout(function(){
							alignAmountsVertical();
							startAlignAmountsVertical();
						});
					}
					
					//Init the interactive calculator
					$scope.interactiveCalculatorFloor = $scope.selectedProduct.productType.sliderMin;
					$scope.interactiveCalculatorCeil = $scope.selectedProduct.productType.sliderMax;
					$scope.interactiveCalculator = {};
					$scope.interactiveCalculator.barrierOccurred = false;
					$scope.interactiveCalculator.barrierOccurredManually = false;
					$scope.barrierOccurredCheckboxHandler = function() {
						if ($scope.selectedProduct.productTypesConfig.hasBarrier && $scope.selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American) {
							$scope.interactiveCalculator.barrierOccurredManually = $scope.interactiveCalculator.barrierOccurred;
							setBarrierOccurredCheckbox($scope.interactiveCalculator.slider.value);
						}
					};
					$scope.interactiveCalculator.states = [];
					$scope.interactiveCalculator.slider = {
						value: 0,
						options: {
							floor: $scope.interactiveCalculatorFloor,
							ceil: $scope.interactiveCalculatorCeil,
							translate: function(value) {
								return value + '%';
							},
							onChange: function(sliderId, modelValue) {
								setBarrierOccurredCheckbox(modelValue);
							},
							autoHideLimitLabels: false,
							boundPointerLabels: false,
							showTicksValues: true,
							ticksArray: shouldProductHaveBarrier($scope.selectedProduct) ? [$scope.selectedProduct.productBarrier.barrierLevel - 100] : []
						}
					};
					setBarrierOccurredCheckbox($scope.interactiveCalculator.slider.value);
					//Convinience functions
					$scope.interactiveCalculator.getState = function(){
						if($scope.interactiveCalculator.slider && typeof $scope.interactiveCalculator.slider.value != 'undefined'){
							return $scope.interactiveCalculator.states[$scope.interactiveCalculator.slider.value - $scope.interactiveCalculatorFloor];
						}
						return {};
					}
					$scope.interactiveCalculator.getAmount = function(selectedProduct, investmentAmount){
						var denomination = $scope.productIsPrimary(selectedProduct) ? investmentAmount : investmentAmount * 100 / selectedProduct.ask;
						var formulaResult = $scope.interactiveCalculator.getState().formulaResult;
						if($scope.interactiveCalculator.barrierOccurred){
							formulaResult = $scope.interactiveCalculator.getState().formulaResultBarrier;
						}
						return $scope.formatAmount({amount: formulaResult * denomination, currencyId: selectedProduct.currencyId, centsPart: 2, skipAmountDivide: true});
					}
					$scope.interactiveCalculator.getProfitLoss = function(){
						var result = '';
						var formulaResult = $scope.interactiveCalculator.getState().formulaResult;
						if($scope.interactiveCalculator.barrierOccurred){
							formulaResult = $scope.interactiveCalculator.getState().formulaResultBarrier;
						}
						if(formulaResult >= 1){
							result = round((formulaResult*100 - 100), 2) + '% ' + $scope.getMsgs('profit');
						}else{
							result = round((100 - formulaResult*100), 2) + '% ' + $scope.getMsgs('loss');
						}
						return result;
					}
					$scope.interactiveCalculator.getZeroMarkerWidth = function(){
						var result = '';
						var floor = $scope.interactiveCalculator.slider.options.floor;
						var ceil = $scope.interactiveCalculator.slider.options.ceil;
						result = Math.abs((floor/(ceil - floor))*2*100);
						return result;
					}
					//Fill the states
					for(var i = 0; i < $scope.selectedProduct.productSliders.length; i++){
						$scope.interactiveCalculator.states.push({formulaResult: $scope.selectedProduct.productSliders[i].formulaResult, formulaResultBarrier: $scope.selectedProduct.productSliders[i].formulaResultBarrier});
					}

					var kidList = $scope.selectedProduct.productKidLanguages;
					if (kidList) {
						for (var i = 0; i < kidList.length; i++) {
							if (kidList[i].language.id == $scope.language.id) {
								$scope.selectedProduct.kidLink = $scope.selectedProduct.productKidLanguages[i].url;	
								break;
							}
						}
					}
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getAllProductsFilters = function(){
		var methodRequest = {};
		methodRequest.data = ['markets', 'product_type_group', 'product_type_protection_level'];
		$http.post('filters/get', methodRequest).then(
			function(response) {
				if (!$scope.handleErrors(response, 'getAllProductsFilters')) {
					var searchParams = $location.search();
					var filters = Utils.parseResponse(response);
					
					var assetsOptions = [];
					for(key in filters.markets){
						if(filters.markets.hasOwnProperty(key)){
							assetsOptions.push({id: key, name: filters.markets[key].displayName});
						}
					}
					$scope.filter.assets.fillOptions(assetsOptions);
					if (searchParams.assets) {
						$scope.filter.assets.setById(searchParams.assets);
					}
					
					var typeOptions = [];
					for(key in filters.product_type_group){
						if(filters.product_type_group.hasOwnProperty(key)){
							var typeGroupList = [];
							if(filters.product_type_group[key].list){
								for(var i = 0; i < filters.product_type_group[key].list.length; i++){
									typeGroupList.push(filters.product_type_group[key].list[i].id);
								}
							}
							typeOptions.push({
								id: 'g' + key,
								name: filters.product_type_group[key].groupName,
								groupName: filters.product_type_group[key].groupName,
								type: 'group',
								list: typeGroupList
							});
							if(filters.product_type_group[key].list){
								for(var i = 0; i < filters.product_type_group[key].list.length; i++){
									typeOptions.push({
										id: filters.product_type_group[key].list[i].id,
										name: filters.product_type_group[key].list[i].displayName,
										groupName: filters.product_type_group[key].groupName,
										type: 'groupItem'
									});
								}
							}
						}
					}
					$scope.filter.type.fillOptions(typeOptions);
					if (searchParams.type) {
						$scope.filter.type.setById(searchParams.type);
						var currentType = typeOptions.filter(function(type) {
							return type.id === searchParams.type;
						})[0];
						if (Object(currentType) === currentType && currentType.groupName) {
							var title = $rootScope.getMsgs(currentType.groupName) + ' Product | AnyCapital';
							setMetaTitle(title);
						}
					}
					
					var protectionOptions = [];
					for(key in filters.product_type_protection_level){
						if(filters.product_type_protection_level.hasOwnProperty(key)){
							var protectionGroupList = [];
							if(filters.product_type_protection_level[key].list){
								for(var i = 0; i < filters.product_type_protection_level[key].list.length; i++){
									protectionGroupList.push(filters.product_type_protection_level[key].list[i].id);
								}
							}
							protectionOptions.push({id: key, name: filters.product_type_protection_level[key].groupName, list: protectionGroupList});
						}
					}
					$scope.filter.protection.fillOptions(protectionOptions);
					if (searchParams.protection) {
						$scope.filter.protection.setById(searchParams.protection);
					}
				}
			}, function(response) {
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.getAllProducts = function(){
		$scope.resetGlobalErrorMsg($scope);
		$scope.action = 'loading';
		var methodRequest = {};
		$http.post('product/getAllProducts', methodRequest).then(
			function(response) {
				$scope.action = '';
				if (!$scope.handleErrors(response, 'getAllProducts')) {
					$scope.products = Utils.parseResponse(response).products;
					$scope.investmentsProducts = Utils.parseResponse(response).investmentsProductId;
					
					//$scope.products.sort(function(a, b){return b.timeCreated - a.timeCreated;});
					
					if($scope.isHomePage){
						$scope.products.splice(6);
					}
					
					if($scope.investmentsProducts){
						for(var i = 0; i < $scope.products.length; i++){
							if($scope.investmentsProducts[$scope.products[i].id]){
								$scope.products[i]._hasInvestment = true;
								$scope.products[i]._investmentSum = $scope.investmentsProducts[$scope.products[i].id];
							}
						}
					}
					
					for(var i = 0; i < $scope.products.length; i++){
						var investmentSettings = $rootScope.investmentPossibilities[$scope.products[i].currencyId];
						$scope.products[i]._minInvAmount = investmentSettings.minimumAmount/100;
						$scope.products[i]._maxInvAmount = investmentSettings.maximumAmount/100;
						$scope.products[i]._invCurrencyId = investmentSettings.currencyId;
						
						$scope.products[i]._scenarioAmount = 10000;
						
						$scope.productLoadTypesConfig($scope.products[i]);
						
						$scope.refactorScenarioRows($scope.products[i], true);
						
						$scope.prepareCharts($scope.products[i]);
					}
					
					$scope.updateProductsList($scope.products);
					
					// $timeout(function(){
						$scope.action = '';
					// });
					
					// var alignProductBlocksInterval = setInterval(function(){
					// 	alignProductBlocks();
					// }, 2000);
					// intervals.push(alignProductBlocksInterval);
				}
			}, function(response) {
				$scope.action = '';
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.updateProductsList = function(){
		if (lastExpandedProduct) {
			$scope.collapse(lastExpandedProduct);
		}
		var updateDelay = 1000;//Must be higher than transition duration + animation duration
		$scope.filtered.products = $scope.products.filter($scope.listProductsFilter);
		setTimeout(function(){
			$rootScope.$broadcast('filteredProducts');
		}, updateDelay);
		return $scope.filtered.products;
	}
	
	$scope.filterChangeHandler = function() {
		for (filterName in $scope.filter) {
			if ($scope.filter.hasOwnProperty(filterName)) {
				var currentFilter = $scope.filter[filterName]
				var id = currentFilter.getId();
				var value = id == -1 ? null : id;
				$state.params[filterName] = value;
				$location.search(filterName, value);
				if (filterName === 'type' && !isUndefined(value) && currentFilter.model && currentFilter.model.groupName) {
					var title = $rootScope.getMsgs(currentFilter.model.groupName) + ' Product | AnyCapital';
					setMetaTitle(title);
				} else if (filterName === 'type') {
					setMetaTitle($rootScope.getMetaTitle());
				}
			}
		}
	};
	
	$scope.listProductsFilter = function(product){
		var hide = false;
		
		//Asset
		if($scope.filter.assets.getId() > -1){
			if($scope.filter.assets.getId() != product.productMarkets[0].market.id){
				hide = true;
			}
		}
		
		//Type
		if($scope.filter.type.model.type == 'groupItem' && $scope.filter.type.getId() > -1){
			if($scope.filter.type.getId() != product.productType.id){
				hide = true;
			}
		}
		if($scope.filter.type.model.type == 'group'){
			var typeFound = false;
			for(var i = 0; i < $scope.filter.type.model.list.length; i++){
				if($scope.filter.type.model.list[i] == product.productType.id){
					typeFound = true;
					break;
				}
			}
			if(!typeFound){
				hide = true;
			}
		}
		
		//Maturity
		if($scope.filter.maturity.getId() > -1){
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_all && !$scope.isPrimary(product)){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_3_6 && !($scope.isPrimary(product) && (product.redemptionDate <= moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_6_12 && !($scope.isPrimary(product) && (product.redemptionDate > moment().add(6, 'months').toDate().getTime() && product.redemptionDate <= moment().add(12, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.primary_12 && !($scope.isPrimary(product) && (product.redemptionDate > moment().add(12, 'months').toDate().getTime()))){
				hide = true;
			}
			
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_all && !$scope.isSecondary(product)){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_0_3 && !($scope.isSecondary(product) && (product.finalFixingDate <= moment().add(3, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_3_6 && !($scope.isSecondary(product) && (product.finalFixingDate > moment().add(3, 'months').toDate().getTime() && product.finalFixingDate <= moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
			if($scope.filter.maturity.getId() == $scope.MATURITY_OPTIONS.secondary_6 && !($scope.isSecondary(product) && (product.finalFixingDate > moment().add(6, 'months').toDate().getTime()))){
				hide = true;
			}
		}
		
		//Protection
		if($scope.filter.protection.getId() > -1){
			var typeFound = false;
			for(var i = 0; i < $scope.filter.protection.model.list.length; i++){
				if($scope.filter.protection.model.list[i] == product.productType.id){
					typeFound = true;
					break;
				}
			}
			if(!typeFound){
				hide = true;
			}
		}
		
		return !hide;
	}
	
	
	$scope.refactorScenarioRows = function(selectedProduct, listView){
		if(!selectedProduct.productScenarios){
			return;
		}
		selectedProduct.productScenarios.sort(function(a, b){return b.formulaResult - a.formulaResult;});
		
		var count = selectedProduct.productScenarios.length;
		var arr = [];
		var counter = 0;
		for(var i = 0; i < selectedProduct.productScenarios.length; i++){
			selectedProduct.productScenarios[i]._level = selectedProduct.productScenarios[i].finalFixingLevelForTxt;
			selectedProduct.productScenarios[i]._formulaResult = selectedProduct.productScenarios[i].formulaResult;
			if(selectedProduct.productTypesConfig['scenario-tooltips']){
				selectedProduct.productScenarios[i]._tooltip = selectedProduct.productTypesConfig['scenario-tooltips'][i];
			}
			if(selectedProduct.productTypesConfig['override-main-scenarios'] && selectedProduct.productTypesConfig['override-main-scenarios'].override && selectedProduct.productTypesConfig['override-main-scenarios'].scenarios && (selectedProduct.productTypesConfig['override-main-scenarios'].scenarios.length > i)){
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].template == 'skip'){
					count--;
					continue;
				}
				selectedProduct.productScenarios[i]._template = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].template;
				selectedProduct.productScenarios[i]._shortTextKey = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].shortTextKey;
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].level){
					selectedProduct.productScenarios[i]._level = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].level;
				}
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].formulaResult){
					selectedProduct.productScenarios[i]._formulaResult = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].formulaResult;
				}
				if(selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].amountType == 'static'){
					selectedProduct.productScenarios[i]._amountType = 'static';
					selectedProduct.productScenarios[i]._amount = selectedProduct.productTypesConfig['override-main-scenarios'].scenarios[i].amount;
				}
			}
			// if(parseFloat(selectedProduct.productScenarios[i]._level) > 0){
			// 	selectedProduct.productScenarios[i]._level = '+' + selectedProduct.productScenarios[i]._level;
			// }
			if(!listView){
				if(!arr[Math.floor(counter/3)]){
					arr[Math.floor(counter/3)] = [];
				}
				arr[Math.floor(counter/3)].push(selectedProduct.productScenarios[i]);
			}else{
				arr.push(selectedProduct.productScenarios[i]);
			}
			counter++;
		}
		selectedProduct._productScenarios = arr;
		selectedProduct._scenariosCount = count;
	}
	$scope.getScenarioAmount = function(selectedProduct, scenario, invAmount){
		var price = 1;
		if($scope.isSecondary(selectedProduct)){
			price = selectedProduct.ask/100;
		}
		return (invAmount/price)*scenario._formulaResult;
	}
	
	
	$scope.getSvgChartColor = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct){
			return false;
		}
		if(!selectedProduct.productType){
			return false;
		}
		if(selectedProduct.productType.riskLevel == 1){
			return '#2b8fd9';
		}else if(selectedProduct.productType.riskLevel == 2){
			return '#209c6c';
		}else if(selectedProduct.productType.riskLevel == 3){
			return '#f7a508';
		}
	}
	
	$scope.prepareCharts = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct){
			return false;
		}
		
		$scope.svgWidth = 452;
		$scope.svgHeight = 200;
		$scope.svgPaddingPercent = {left: 0.0178, bottom: 0.05, right: 4*0.0178, top: 0.025};
		$scope.svgOffset = {};
		if(!$scope.singleProduct){
			$scope.svgWidth = 334;
			$scope.svgHeight = 200;
			$scope.svgPaddingPercent = {left: 0.0178, bottom: 0.05, right: 0.1, top: 0.025};
		}
		
		if(!selectedProduct.productMarkets[0].market.historyMarketPrice){
			return false;
		}
		
		selectedProduct.productMarkets[0].market.historyMarketPrice.sort(function(a, b){return a.priceDate - b.priceDate;});
		
		selectedProduct._charts = {
			month: {startTime: moment().subtract(1, 'months').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: moment().daysInMonth(), isActive: true, name: $scope.getMsgs('month'), texts: [], textType: 'days'},
			sixMonths: {startTime: moment().subtract(6, 'months').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: 6, isActive: false, name: '6 ' + $scope.getMsgs('months'), texts: [], textType: 'months'},
			year: {startTime: moment().subtract(1, 'years').toDate(), minPrice: '', maxPrice: '', path: '', points: [], lines: [], rectangles: [], rectangleCount: 12, isActive: false, name: $scope.getMsgs('year'), texts: [], textType: 'year'}
		};
		for(var i = 0; i < selectedProduct.productMarkets[0].market.historyMarketPrice.length; i++){
			for(key in selectedProduct._charts){
				if(selectedProduct._charts.hasOwnProperty(key)){
					setPoints(selectedProduct.productMarkets[0].market.historyMarketPrice[i], selectedProduct._charts[key]);
				}
			}
			
			function setPoints(historyMarketPrice, chart){
				if(historyMarketPrice.priceDate > chart.startTime.getTime()){
					chart.points.push(historyMarketPrice);
					if(chart.minPrice == ''){
						chart.minPrice = chart.maxPrice = historyMarketPrice.price;
					}else if(historyMarketPrice.price > chart.maxPrice){
						chart.maxPrice = historyMarketPrice.price;
					}else if(historyMarketPrice.price < chart.minPrice){
						chart.minPrice = historyMarketPrice.price;
					}
				}
			}
		}
		for(key in selectedProduct._charts){
			if(selectedProduct._charts.hasOwnProperty(key)){
				setSvgPath(selectedProduct._charts[key]);
				
				function setSvgPath(chart){
					//Set up constants
					var priceCeil = Math.ceil(chart.maxPrice*10)/10;
					var priceFloor = Math.floor(chart.minPrice*10)/10;
					//var divider = (Math.ceil(chart.maxPrice) - Math.floor(chart.minPrice));
					var divider = priceCeil - priceFloor;
					var chartWidth = $scope.svgWidth*(1 - $scope.svgPaddingPercent.left - $scope.svgPaddingPercent.right);
					var chartHeight = $scope.svgHeight*(1 - $scope.svgPaddingPercent.bottom - $scope.svgPaddingPercent.top);
					var paddingLeft = $scope.svgWidth*$scope.svgPaddingPercent.left;
					var paddingRight = $scope.svgWidth*$scope.svgPaddingPercent.right;
					var paddingBottom = $scope.svgHeight*$scope.svgPaddingPercent.bottom;
					var paddingTop = $scope.svgHeight*$scope.svgPaddingPercent.top;
					//Create graph
					for(var i = 0; i < chart.points.length; i++){
						var x = (i/chart.points.length)*chartWidth + paddingLeft;
						var y = paddingTop + chartHeight*(1 - ((chart.points[i].price - priceFloor)/divider));
						if(i == 0){
							chart.path += 'M' + paddingLeft + ',' + (chartHeight + paddingTop);
						}
						chart.path += 'L' + x + ',' + y;
						if(i == chart.points.length - 1){
							chart.path += 'L' + x + ',' + (chartHeight + paddingTop);
						}
					}
					//Create horizontal lines and texts
					for(var i = 0; i < 6; i++){
						var text = '';
						if(i < 5){
							text = Math.round((chart.maxPrice - i*((chart.maxPrice - chart.minPrice)/5))*100)/100;
						}
						var line = {start: {x: 0, y: i*(chartHeight/5) + paddingTop}, end: {x: $scope.svgWidth - paddingRight, y: i*(chartHeight/5) + paddingTop}, text: text, textY: i*(chartHeight/5) + paddingTop + 2};
						chart.lines.push(line);
					}
					//Create vertical rectangles and texts
					var rectWidth = ($scope.svgWidth*(1 - $scope.svgPaddingPercent.left - $scope.svgPaddingPercent.right))/chart.rectangleCount;
					for(var i = 0; i < chart.rectangleCount; i++){
						var text = '';
						if(chart.textType == 'days' && i % 7 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'days').date()) + '-' + padNumber(moment().subtract(chart.rectangleCount - i, 'days').month() + 1);
						}else if(chart.textType == 'months' && i % 2 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'months').month() + 1) + '/' + (moment().subtract(chart.rectangleCount - i, 'months').year() + '').substring(2);
						}else if(chart.textType == 'year' && i % 3 == 0){
							text = padNumber(moment().subtract(chart.rectangleCount - i, 'months').month() + 1) + '/' + (moment().subtract(chart.rectangleCount - i, 'months').year() + '').substring(2);
						}
						var rectangle = {x: i*rectWidth + paddingLeft, y: paddingTop, width: rectWidth, height: chartHeight, text: text};
						chart.rectangles.push(rectangle);
					}
					function padNumber(number){
						if(parseInt(number) < 10){
							return '0' + number;
						}
						return number;
					}
				}
			}
		}
	}
	
	
	$scope.isPrimary = function(selectedProduct){
		return $scope.productIsPrimary(selectedProduct);
	}
	$scope.isSecondary = function(selectedProduct){
		return $scope.productIsSecondary(selectedProduct);
	}
	
	$scope.setActiveChart = function(chart, selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		for(key in selectedProduct._charts){
			if(selectedProduct._charts.hasOwnProperty(key)){
				selectedProduct._charts[key].isActive = false;
			}
		}
		chart.isActive = true;
	}
	
	$scope.getClassNames = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		return 'risk-level-' + selectedProduct.productType.riskLevel + ' ' + 'product-type-' + selectedProduct.productType.id;
	}
	
	$scope.getPurchasedTooltip = function(selectedProduct){
		var message = $scope.getMsgs('product-you-have-a-note', {amount: $scope.formatAmount({amount: selectedProduct._investmentSum, centsPart: 2, currencyId: settings.CURRENCY_EUR})});
		return message;
	}
	
	$scope.getInitialFixingLevel = function(selectedProduct){
		return $scope.productGetInitialFixingLevel(selectedProduct);
	}
	$scope.getBarrierObservationPeriod = function(separator, selectedProduct){
		return $scope.productGetBarrierObservationPeriod(separator, selectedProduct);
	}
	$scope.isBarrierTypeEuropean = function(selectedProduct){
		return $scope.productIsBarrierTypeEuropean(selectedProduct);
	}
	
	$scope.isProtected = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		if(selectedProduct.productType.productCategory.id == $scope.PRODUCT_CATEGORIES.capital_protection){
			return true;
		}
		return false;
	}
	
	$scope.getOneLiner = function(selectedProduct, showShortText){
		var result = '';
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		// var lastPrice = $scope.isPrimary(selectedProduct) ? selectedProduct.productMarkets[0].market.lastPrice.price : selectedProduct.productMarkets[0].startTradeLevel;
		var centsLength = selectedProduct.productMarkets[0] && selectedProduct.productMarkets[0].market && !isUndefined(selectedProduct.productMarkets[0].market.decimalPoint) ? selectedProduct.productMarkets[0].market.decimalPoint : 2;
		var params = {};
		if($scope.isPrimary(selectedProduct)){
			params = {
				'protection-level': selectedProduct.levelOfProtection + '%',
				'100%-protection-level': (100 - selectedProduct.levelOfProtection) + '%',
				'participation-level': selectedProduct.levelOfParticipation + '%',
				'asset': $scope.getMsgs(selectedProduct.productMarkets[0].market.displayName),
				'initial-fixing-level': $scope.getInitialFixingLevel(selectedProduct) + '%',
				'barrier-level': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel + '%' : '',
				'barrier-level%': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel + '%' : '',
				'100% - barrier-level': selectedProduct.productBarrier ? 100 - selectedProduct.productBarrier.barrierLevel + '%' : '',
				'barrier-level - 100%': selectedProduct.productBarrier ? selectedProduct.productBarrier.barrierLevel - 100 + '%' : '',
				'market-barrier-level': '',
				'coupon-amount': selectedProduct.productCoupons.length ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel + '%' : '',
				'coupon-trigger-level%': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].triggerLevel + '%' : '',
				'coupon-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level%\\*initial-fixing-level': '',
				'strike-level': selectedProduct.strikeLevel + '%',
				'caplevel - 100%': selectedProduct.capLevel - 100 + '%',
				'bonus-level%': selectedProduct.bonusLevel + '%',
				'final-fixing-date': $scope.dateGetSimple(selectedProduct.finalFixingDate, '.'),
				'bonus-level': ''
			}
		}else{
			params = {
				'protection-level': selectedProduct.levelOfProtection + '%',
				'100%-protection-level': (100 - selectedProduct.levelOfProtection) + '%',
				'participation-level': selectedProduct.levelOfParticipation + '%',
				'asset': $scope.getMsgs(selectedProduct.productMarkets[0].market.displayName),
				'initial-fixing-level': $scope.getInitialFixingLevel(selectedProduct).toFixed(2),
				'barrier-level': selectedProduct.productBarrier ? round((selectedProduct.productBarrier.barrierLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'100% - barrier-level': '',
				'coupon-amount': selectedProduct.productCoupons.length ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'market-barrier-level': selectedProduct.productBarrier ? round((selectedProduct.productBarrier.barrierLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'coupon-trigger-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? round((selectedProduct.productCoupons[0].triggerLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'coupon-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? selectedProduct.productCoupons[0].payRate + '%' : '',
				'coupon-trigger-level%\\*initial-fixing-level': selectedProduct.productCoupons && selectedProduct.productCoupons[0] ? round((selectedProduct.productCoupons[0].triggerLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength) : '',
				'strike-level': round((selectedProduct.strikeLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength),
				'caplevel - 100%': selectedProduct.capLevel - 100 + '%',
				'bonus-level%': selectedProduct.bonusLevel + '%',
				'final-fixing-date': $scope.dateGetSimple(selectedProduct.finalFixingDate, '.'),
				'bonus-level': round((selectedProduct.bonusLevel/100)*$scope.getInitialFixingLevel(selectedProduct), centsLength).toFixed(centsLength)
			}
		}
		
		var productTypeSuffix = '';
		if(selectedProduct.productTypesConfig.headerShortSuffix){
			productTypeSuffix = '-' + selectedProduct.productTypesConfig.headerShortSuffix;
		}
		var barrierSuffix = '';
		if($scope.isBarrierTypeEuropean(selectedProduct)){
			barrierSuffix = '-european';
		}
		var productStateSuffix = '-primary';
		if($scope.isSecondary(selectedProduct)){
			productStateSuffix = '-secondary';
		}
		if(showShortText){
			result += '<div>';
			result += '<b>' + $scope.getMsgs('the-attraction') + ': </b>' + $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-short' + '-attraction' + productStateSuffix + barrierSuffix, params);
			result += '</div>';
			result += '<div>';
			result += '<b>' + $scope.getMsgs('worst-case') + ': </b>' + $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-short' + '-worstcase' + productStateSuffix + barrierSuffix, params);
			result += '</div>';
		}else{
			result += $scope.getMsgs('product-type-' + selectedProduct.productType.id + productTypeSuffix + '-header-long' + productStateSuffix + barrierSuffix, params);
		}
		
		return result;
	}
	
	$scope.getRiskAppetiteText = function(selectedProduct){
		selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if(!selectedProduct || !selectedProduct.productType){
			return '';
		}
		switch (selectedProduct.productType.riskLevel) {
			case 1:
				return $scope.getMsgs('capital-protection');
			case 2:
				return $scope.getMsgs('conditional-protection');
			case 3:
				return $scope.getMsgs('no-capital-protection');
			default:
				return '';
		}
	}
	
	$scope.getDateCloseText = function(selectedProduct){
		var selectedProduct = selectedProduct || ($scope.singleProduct ? $scope.selectedProduct : null);
		if($rootScope.isEmpty(selectedProduct)){
			return '';
		}
		if([settings.marketGroups.Commodities, settings.marketGroups.Rates, settings.marketGroups.Currencies].indexOf(selectedProduct.productMarkets[0].market.marketGroupId) != -1){
			return $scope.getMsgs('10-NY-time-close');
		}else{
			return $scope.getMsgs('exchange-market-close');
		}
	}
	
	$scope.priceSelectorClickHandler = function(invAmountOption) {
		$scope.investment.invAmount = invAmountOption;
		$scope.isInvAmountManual = false;
	}

	$scope.selectOtherAmount = function(){
		$scope.invAmountGetterSetter("0");
		$scope.isInvAmountManual = true;
		// $timeout(function(){
		// 	angular.element('#invAmountInput').focus();
		// 	$timeout(function(){
		// 		$scope.investment.invAmount = '';
		// 		angular.element('#invAmountInput').val('');
		// 	}, 100);
		// });
	}
	
	$scope.updateInvestments = function(){
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {};
		methodRequest.data.id = $scope.selectedProduct.id;
		$http.post('investment/get/product/sum', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				var data = Utils.parseResponse(response);
				$scope.investmentSum = data.amount;
				
				if($scope.investmentSum > 0){
					$scope.selectedProduct._hasInvestment = true;
					$scope.selectedProduct._investmentSum = $scope.investmentSum;
					$scope.selectedProduct._investmentSell = data.returnAmonut;
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
	
	$scope.insertInvestment = function(){
		if(!$rootScope.isLogged){
			$scope.showCredentials.state = Utils.settings.credentialsView.LOGIN;
		}else{
			$scope.investmentAction = 'loading';
			$scope.investment.data = {};
			var methodRequest = {};
			methodRequest.data = {};
			methodRequest.data.productId = $scope.selectedProduct.id;
			methodRequest.data.originalAmount = $scope.formatAmountForDb({amount: $scope.investment.invAmount});
			methodRequest.data.investmentType = {id: 1};
			methodRequest.data.investmentMarkets = [];
			methodRequest.data.investmentMarkets.push({marketId: $scope.selectedProduct.productMarkets[0].market.id, price: $scope.selectedProduct.productMarkets[0].market.lastPrice.price});
			if(!$scope.isPrimary($scope.selectedProduct)){
				methodRequest.data.investmentType = {id: 2};
				methodRequest.data.ask = $scope.selectedProduct.ask;
			}
			$http.post('investment/insert', methodRequest).then(
				function(response) {
					$scope.investmentAction = '';
					$scope.updateInvestments();
					// var exceptions = [errorCodeMap.investment_general_error, errorCodeMap.investment_insufficient_funds_above_minumum, errorCodeMap.investment_below_minumum, errorCodeMap.investment_insufficient_funds_below_minimum_secondary, errorCodeMap.investment_above_maximum, errorCodeMap.investment_already_secured, errorCodeMap.investment_suspended, errorCodeMap.investment_unavailable, errorCodeMap.investment_already_issued, errorCodeMap.investment_deviation_secondary_primary, errorCodeMap.investment_deviation];
					// if (!$scope.handleErrors(response, 'insert', null, exceptions)) {
						var data = Utils.parseResponse(response);
						$scope.investment.data = data;
						if(response.data.responseCode == 0 && data.investmentStatus.id == 2){//Success purchase
							$scope.investmentState = $scope.INVESTMENT_STATES.purchased;
							$rootScope.loggedUser.balance = data.user.balance;
						}else if(response.data.responseCode == 0 && data.investmentStatus.id == 1){//When a customer is trying to purchase an inv in primary product, has balance lower than min inv amount and doesn't have an already existing pending inv on the same product
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds;
						}else if(response.data.responseCode == errorCodeMap.investment_insufficient_funds_above_minumum){//Not enough for current investment, enough for minimum
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds_above_minumum;
						}else if(response.data.responseCode == errorCodeMap.investment_below_minumum){//When a customer is trying to insert an amount lower than the miv inv amount
							$scope.investmentState = $scope.INVESTMENT_STATES.below_minumum;
						}else if(response.data.responseCode == errorCodeMap.investment_insufficient_funds_below_minimum_secondary){//When a customer does not have sufficient balance for the inv amount inserted and his balance is lower than the min inv amount, relevant only for secondary
							$scope.investmentState = $scope.INVESTMENT_STATES.insufficient_funds_below_minimum_secondary;
						}else if(response.data.responseCode == errorCodeMap.investment_above_maximum){//When a customer is trying to insert an amount higher than the max inv amount
							$scope.investmentState = $scope.INVESTMENT_STATES.above_maximum;
						}else if(response.data.responseCode == errorCodeMap.investment_already_secured){//When a customer already secured a note in primary on this product, balance is lower than the min inv amount and is trying to secure again on the same product in primary
							$scope.investmentState = $scope.INVESTMENT_STATES.already_secured;
						}else if(response.data.responseCode == errorCodeMap.investment_suspended){//When a customer tries inserting an inv but the product is in suspend state
							$scope.investmentState = $scope.INVESTMENT_STATES.suspended;
						}else if(response.data.responseCode == errorCodeMap.investment_unavailable){//When a customer tries inserting an inv but according to the "product time definitions"
							$scope.investmentState = $scope.INVESTMENT_STATES.unavailable;
						}else if(response.data.responseCode == errorCodeMap.investment_already_issued){//Customer trying to insert a primary type inv while the product is already in secondary state (similar to the previous)
							$scope.investmentState = $scope.INVESTMENT_STATES.already_issued;
						}else if(response.data.responseCode == errorCodeMap.investment_deviation_secondary_primary){//Deviation on ask price secondary?
							$scope.investmentState = $scope.INVESTMENT_STATES.deviation_secondary_primary;
						}else if(response.data.responseCode == errorCodeMap.investment_deviation){//Deviation on ask price
							$scope.investmentState = $scope.INVESTMENT_STATES.deviation;
						}else if(response.data.responseCode == errorCodeMap.investment_complete_registration){//Registration not finished error
							var step = $rootScope.loggedUser.userStep.id;
							switch (step) {
								case 1:
									$scope.investment.data.href = $state.href('ln.sign-up.personal-details');
									break;
								case 2:
								case 3:
									$scope.investment.data.href = $state.href('ln.sign-up.questionnaire');
									break;
								case 4:
									$scope.investment.data.href = $state.href('ln.my-account.funding.upload-documents');
									break;
							}
							$scope.investmentState = $scope.INVESTMENT_STATES.complete_registration;
							$scope.investment.data.errorMessage = response.data.error ? response.data.error.reason : '';
						}else if(response.data.responseCode == errorCodeMap.investment_general_error){//General error
							$scope.investmentState = $scope.INVESTMENT_STATES.general_error;
							$scope.investment.data.errorMessage = response.data.error ? $scope.getMsgs(response.data.error.reason) : '';
						}
					// }
				}, function(response) {
					$scope.investmentAction = '';
					$scope.handleNetworkError(response);
				});
		}
	}
	
	$scope.setInvestmentState = function(state){
		$scope.investmentState = state;
	}
	
	$scope.getMyNotesLink = function(){
		return $rootScope.$state.href('ln.my-account.my-notes', {ln: $rootScope.language.code});
	}
	
	var alignAmountsVerticalInterval;
	function startAlignAmountsVertical(){
		clearInterval(alignAmountsVerticalInterval);
		alignAmountsVerticalInterval = setInterval(alignAmountsVertical, 1000);
		intervals.push(alignAmountsVerticalInterval);
	}
	function alignAmountsVertical(){
		//Align investment with scenario receive values
		$('[data-height-compensation-block]').css('height', '');
		var offsets = [];
		var positions = [];
		$('[data-height-target]').each(function(index, el){
			offsets.push({position: $(el).data().heightTarget, offset: $(el).offset().top + $(el).outerHeight()});
			if(positions.indexOf($(el).data().heightTarget) == -1){
				positions.push($(el).data().heightTarget);
			}
		});
		if(offsets.length > 1){
			var compensators = $('[data-height-compensation-block]');
			if(compensators.length > 0){
				var min = null;
				var max = null;
				for(var i = 0; i < offsets.length; i++){
					if(i == 0){
						min = offsets[i];
						max = offsets[i];
					}else{
						if(offsets[i].offset < min.offset){
							min = offsets[i];
						}
						if(offsets[i].offset > max.offset){
							max = offsets[i];
						}
					}
				}
				var diff = max.offset - min.offset;
				if(diff > 0){
					$('[data-height-compensation-block="' + min.position + '"]').each(function(index, el){
						$(el).css('height', $(el).outerHeight() + diff/$('[data-height-compensation-block="' + min.position + '"]').length);
					});
				}
			}
		}
		//Align scenario blocks height
		var maxHeight = 0;
		$('[data-scenario-block]').each(function(index, el){
			var outerHeight = $(el).outerHeight();
			if(outerHeight > maxHeight){
				maxHeight = outerHeight;
			}
		});
		$('[data-scenario-block]').css('min-height', maxHeight + 'px');
	}
	
	// Should be more then .product-medium-holder animation-duration.
	var lastExpandedProduct;
	var expandCollapseDefaultDelay = 410;
	$scope.expand = function(selectedProduct, event){
		lastExpandedProduct = selectedProduct;
		if($rootScope.isMobile){
			$rootScope.$state.go('ln.product', {ln: $rootScope.$state.params.ln, productId: selectedProduct.id});
			return;
		}
		
		var $target = $(event.target).parents('[data-list-single-product]');
		
		var delay = 0;
		
		//Allow only one expanded product at a time
		for(i = 0; i < $scope.products.length; i++){
			if($scope.products[i]._isExpanded){
				$scope.collapse($scope.products[i]);
				delay = expandCollapseDefaultDelay;
				break;
			}
		}
		
		var hideIndexes = [];
		var currentIndex = $target.data('list-product-index');
		if(currentIndex % 3 == 0){
			hideIndexes = [currentIndex + 1, currentIndex + 2];
		}else if (currentIndex % 3 == 1){
			hideIndexes = [currentIndex - 1, currentIndex + 1];
		}else{
			hideIndexes = [currentIndex - 1, currentIndex - 2];
		}
		for(var i = 0; i < hideIndexes.length; i++){
			$('[data-list-product-index="' + hideIndexes[i] + '"]').addClass('hidden');
		}
		
		
		setTimeout(function(){
			var dummy = null;
			if(document.getElementById('dummy')){
				dummy = document.getElementById('dummy');
			}else{
				dummy = document.createElement('div');
				dummy.id = 'dummy';
				$(dummy).css({
					'float': 'left',
					'display': 'inline-block',
					'vertical-align': 'top'
				});
			}
			$(dummy).css({
				'width': $target.outerWidth(),
				'height': $target.outerHeight(),
				'margin-top': $target.css('margin-top'),
				'margin-bottom': $target.css('margin-bottom'),
				'margin-left': $target.css('margin-left'),
				'margin-right': $target.css('margin-right')
			});
			
			$target.addClass('transition');
			
			$timeout(function(){
				$target.before(dummy);
				selectedProduct._isExpanded = true;
				// setTimeout(function(){
				// 	$target.find('.expanded-middle-col').addClass('visible');
				// 	$target.find('.expanded-right-col').addClass('visible');
				// });
			});
		}, delay);
	}
	$scope.collapse = function(selectedProduct, event){
		lastExpandedProduct = null;
		selectedProduct._isExpanded = false;
		var $dataListSingleProduct = $('[data-list-single-product]');
		var $dataListSingleProductHidden = $dataListSingleProduct.filter('.hidden');
		// $dataListSingleProduct.find('.expanded-middle-col').removeClass('visible');
		// $dataListSingleProduct.find('.expanded-right-col').removeClass('visible');
		setTimeout(function(){
			$('#dummy').remove();
			$dataListSingleProduct.removeClass('transition');
			$dataListSingleProductHidden.removeClass('hidden');
		}, expandCollapseDefaultDelay);
	}
	
	$scope.gotoProduct = function(productId){
		$rootScope.$state.go('ln.product', {ln: $rootScope.$state.params.ln, productId: productId});
	}
	
	
	$scope.popupOpen = function(selectedProduct){
		selectedProduct._showMedium = true;
		$rootScope.blurWrapper = true;
	}
	$scope.popupClose = function(selectedProduct){
		selectedProduct._showMedium = false;
		$rootScope.blurWrapper = false;
	}
	$scope.popupIsOpen = function(selectedProduct){
		return selectedProduct._showMedium == true;
	}
	
	
	$scope.toggleInteractiveCalculator = function(selectedProduct, toState){
		if(typeof toState == 'undefined'){
			selectedProduct._calculatorVisible = !selectedProduct._calculatorVisible;
		}else{
			selectedProduct._calculatorVisible = !!toState;
		}
		$rootScope.blurWrapper = selectedProduct._calculatorVisible;
	}
	
	$scope.isAmericanBarrier = function() {
		return $scope.selectedProduct.productTypesConfig.hasBarrier && $scope.selectedProduct.productBarrier.productBarrierType.id == $scope.settings.productBarrierTypes.American;
	};
	
	function shouldProductHaveBarrier(selectedProduct) {
		if (selectedProduct.productTypesConfig.hasBarrier && selectedProduct.productBarrier.productBarrierType.id == settings.productBarrierTypes.American) {
			return true;
		} else {
			var productTypeId = selectedProduct.productType.id;
			switch (productTypeId) {
				case 2:
				case 5:
				case 7:
				case 9:
					return true;
				default:
					return false;
			}
		}
	}
	
	function setBarrierOccurredCheckbox(value) {
		var barrierLevel = $scope.interactiveCalculator.slider.options.ticksArray[0];
		if (shouldProductHaveBarrier($scope.selectedProduct) && !$scope.interactiveCalculator.barrierOccurredManually) {
			$scope.interactiveCalculator.barrierOccurred = barrierLevel >= 0 ? value >= ($scope.selectedProduct.productBarrier.barrierLevel - 100) : value <= ($scope.selectedProduct.productBarrier.barrierLevel - 100);
		}
	}
	
	// function alignProductBlocks(){
	// 	var counter = 0;
	// 	var rowEls = [];
	// 	var products = $('[data-list-single-product]');
	// 	products.css('min-height', '');
	// 	products.each(function(index, el){
	// 		if(counter % $scope.productsPerRow == 0){//First in row
	// 			rowEls = [];
	// 			rowEls.push(el);
	// 			$(el).addClass('first-in-row');
	// 			$(el).removeClass('last-in-row');
	// 			// displayMidRow(false, index);
	// 		}else if(counter % $scope.productsPerRow == $scope.productsPerRow - 1){//Last in row
	// 			rowEls.push(el);
	// 			fixHeights(rowEls);
	// 			rowEls = [];
	// 			$(el).addClass('last-in-row');
	// 			$(el).removeClass('first-in-row');
	// 			// displayMidRow(true, index);
	// 		}else{
	// 			rowEls.push(el);
	// 			$(el).removeClass('first-in-row');
	// 			$(el).removeClass('last-in-row');
	// 			// displayMidRow(false, index);
	// 		}
	// 		counter++;
	// 		// if(index == products.length - 1){
	// 		// 	displayMidRow(true, index);
	// 		// }
	// 	});
	// 	if(rowEls.length > 0){
	// 		fixHeights(rowEls);
	// 	}
	// 	if($scope.isHomePage){
	// 		$('[data-products-mid-row]:visible:last').css('display', '');
	// 	}
	// 	
	// 	function fixHeights(rowEls){
	// 		var maxHeight = 0;
	// 		for(var i = 0; i < rowEls.length; i++){
	// 			var temp = $(rowEls[i]).outerHeight();
	// 			if(temp > maxHeight){
	// 				maxHeight = temp;
	// 			}
	// 		}
	// 		for(var i = 0; i < rowEls.length; i++){
	// 			$(rowEls[i]).css('min-height', maxHeight);
	// 		}
	// 	}
	// 	
	// 	// function displayMidRow(visible, index){
	// 	// 	if(visible){
	// 	// 		$('[data-products-mid-row]:eq(' + index + ')').css('display', 'block');
	// 	// 	}else{
	// 	// 		$('[data-products-mid-row]:eq(' + index + ')').css('display', '');
	// 	// 	}
	// 	// }
	// }
}]);