var menu = [{
	title: 'home',
	link: 'ln.index',
	onlyMobile: true
},{
	title: 'all-notes',
	link: 'ln.products'
},{
	title: 'open-account',
	titleLogged: 'my-account',
	link: 'ln.register',
	linkLogged: 'ln.my-account',
	isMyAccount: true
},{
	title: 'about',
	link: 'ln.about'
},{
	title: 'faq',
	link: 'ln.faq'
},{
	title: 'basics',
	link: 'ln.basics',
	nodes: [
		{
			title: 'the-basics',
			link: 'ln.basics',
			linkParams: {'id': 'basics-the-basics'}
		},
		{
			title: 'basics-product-categories-title',
			link: 'ln.basics',
			linkParams: {'id': 'product-categories'}
		},
		{
			title: 'basics-profit-profile-diagram-title',
			link: 'ln.basics',
			linkParams: {'id': 'profit-profile-diagram'}
		},
		{
			title: 'glossary',
			link: 'ln.basics',
			linkParams: {'id': 'basics-glossary'}
		},
		{
			title: 'underlying-assets',
			link: 'ln.basics',
			linkParams: {'id': 'basics-underlying-assets'}
		},
		{
			title: 'articles',
			link: 'ln.basics',
			linkParams: {'id': 'basics-articles'}
		},
	],
},{
	title: 'contact-us',
	link: 'ln.contact-us'
}];