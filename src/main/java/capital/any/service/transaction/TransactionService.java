package capital.any.service.transaction;

import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.transaction.ITransactionDao;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.IServerConfiguration;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("TransactionServiceWeb")
public class TransactionService extends capital.any.service.base.transaction.TransactionService implements ITransactionService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);
	@Autowired @Qualifier("TransactionDaoWeb")
	private ITransactionDao transactionDao;
	@Autowired
	private IServerConfiguration serverConfiguration;
	@Autowired
	private IBalanceService balanceService;

	@Override @Transactional(rollbackFor = Exception.class)
	public ResponseCode reverseWithdrawal(Transaction transaction, TransactionHistory transactionHistory) throws Exception {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		long userId = transaction.getUser().getId();
		transaction = transactionDao.getTransaction(transaction.getId());
		//authenticate transaction
		if (transaction != null && userId == transaction.getUser().getId() &&
				(transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId() ||
					transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId())) {	
			transactionHistory.setStatus(transaction.getStatus());
			transactionHistory.setServerName(serverConfiguration.getServerName());
			transactionHistory.setTimeCreated(new Date());
			transactionHistory.setTransactionId(transaction.getId());
			transaction.setStatus(new TransactionStatus(TransactionStatusEnum.CANCEL.getId()));
			responseCode = updateTransaction(transaction, transactionHistory) ? ResponseCode.OK : ResponseCode.OPERATION_FAILED;
			balanceService.changeBalance(
					new BalanceRequest(transaction.getUser(), 
							transaction.getAmount(),
							BalanceHistoryCommand.CANCEL_TRANSACTION, 
							transaction.getId(), 
							OperationType.CREDIT, new UserControllerDetails( 
									transactionHistory.getActionSource(),
									transaction.getIp(),
									transactionHistory.getLoginId())));
			
		} else {
			responseCode = ResponseCode.INVALID_INPUT;
		}
		return responseCode;
	}

	@Override
	public List<Transaction> getUserReverseWithdrawalList(User user) {
		List<Transaction> userReverseWithdrawalList = null;
		if (user != null && user.getId() >= 0) {
			userReverseWithdrawalList = transactionDao.getUserReverseWithdrawalList(user.getId());
		}
		return userReverseWithdrawalList;
	}

	@Override
	public List<Transaction> getBankingHistory(SqlFilters filters) {
		return transactionDao.getBankingHistory(filters);
	}
}
