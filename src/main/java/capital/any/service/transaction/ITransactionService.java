package capital.any.service.transaction;

import java.util.List;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionService extends capital.any.service.base.transaction.ITransactionService {

	/**
	 * @param transaction
	 * @param transactionHistory
	 * @return
	 * @throws Exception 
	 */
	ResponseCode reverseWithdrawal(Transaction transaction, TransactionHistory transactionHistory) throws Exception;

	/**
	 * @param user
	 * @return
	 */
	List<Transaction> getUserReverseWithdrawalList(User user);

	/**
	 * @param filters
	 * @return
	 */
	List<Transaction> getBankingHistory(SqlFilters filters);
	
}
