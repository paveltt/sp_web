package capital.any.service.investment;

import java.util.Map;

import capital.any.model.base.Product;

/**
 * @author @author Eyal G
 *
 */
public interface IInvestmentService extends capital.any.service.base.investment.IInvestmentService {
	
	/**
	 * get user sum investments by product id
	 * @param userId
	 * @param Product product
	 * @return amount of sum investments
	 */
	Map<String, Object> getUserSumInvestmentsWithReturn(long userId, Product product);
	
	/**
	 * get user sum investments by product id
	 * @param userId
	 * @param productId
	 * @return amount of sum investments
	 */
	Map<String, Object> getUserSumInvestmentsWithReturn(long userId, long product);
	
	/**
	 * get all products that have open investments and amount invested
	 * @param userId
	 * @return map of product id and sum investments
	 */
	Map<Long, Long> getInvestmentsProductId(long userId);
}
