package capital.any.service.investment;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.investment.IInvestmentDao;
import capital.any.model.base.Investment;
import capital.any.model.base.Product;

/**
 * @author Eyal G
 *
 */
@Service("InvestmentServiceWEB")
public class InvestmentService extends capital.any.service.base.investment.InvestmentService implements IInvestmentService {
	
	@Autowired
	@Qualifier("InvestmentDaoWEB")
	private IInvestmentDao investmentDao;

	@Override
	public Map<String, Object> getUserSumInvestmentsWithReturn(long userId, Product product) {
		long amonut = 0;
		long returnAmonut = 0;
		for (Investment investment : investmentDao.getUserInvestments(userId, product.getId())) {
			investment = calculateReturn(investment, product);
			amonut += investment.getAmount();
			returnAmonut += investment.getReturnAmount();
		};
		Map<String, Object> map = new HashMap<String, Object>();
		if (amonut > 0) {
			map.put("amount", amonut);
			map.put("returnAmonut", returnAmonut);
		}
		return map;
	}

	@Override
	public Map<Long, Long> getInvestmentsProductId(long userId) {
		return investmentDao.getInvestmentsProductId(userId);
	}

	@Override
	public Map<String, Object> getUserSumInvestmentsWithReturn(long userId, long productId) {
		return getUserSumInvestmentsWithReturn(userId, productService.getProduct(productId));
	}
	
	

}
