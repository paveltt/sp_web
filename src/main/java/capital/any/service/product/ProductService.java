package capital.any.service.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.product.IProductDao;
import capital.any.model.base.Product;
import capital.any.model.base.User;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.investment.IInvestmentService;

/**
 * @author Eyal Goren
 *
 */
@Service("ProductServiceWEB")
public class ProductService extends capital.any.service.base.product.ProductService implements IProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);

	@Autowired
	@Qualifier("ProductDaoWEB")
	protected IProductDao productDao;
	
	@Autowired
	@Qualifier("ProductHCDaoWEB")
	private IProductDao productHCDao;
	
	@Autowired
	private ICurrencyService currencyService;
	
	@Autowired
	@Qualifier("InvestmentServiceWEB")
	private IInvestmentService investmentService;

	@Override
	public List<Product> getProductsForWeb() {
		List<Product> products = productHCDao.getProductsForWeb();
		if (products == null || products.size() <= 0) {
			logger.info("products empty / null in hazelcast, Let's check RDBMS");
			products = productDao.getProductsForWeb();
			setProductsFull(products);
		} else {
			//need to sort list from hazelcast
			products.sort((left, right) -> right.getPriority() - left.getPriority());
		}
		return products;
	}

	@Override
	public HashMap<String, Object> getProductFull(long id, User user) {
		HashMap<String, Object> result = null;
		Product product = getProduct(id);
		if (null != product) {
			result = new HashMap<String, Object>();
			result.put("product", product);
			result.put("rate", currencyService.getRate(product.getCurrencyId()));
			if (user != null) {
				Map<String, Object> amountSum = investmentService.getUserSumInvestmentsWithReturn(user.getId(), product);
				if (amountSum != null && !amountSum.isEmpty()) {
					result.putAll(amountSum);
				}
			}
		}
		return result;
	}

}
