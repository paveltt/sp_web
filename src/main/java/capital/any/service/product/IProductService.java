package capital.any.service.product;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.Product;
import capital.any.model.base.User;

/**
 * @author Eyal Goren
 *
 */
public interface IProductService extends capital.any.service.base.product.IProductService {	
	
	/**
	 * get all products to show in web
	 * @return products list
	 */
	List<Product> getProductsForWeb();
	
	/**
	 * get product with currency and sum of open investment with return amonut;
	 * @param product id
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> getProductFull(long id, User user);
}
