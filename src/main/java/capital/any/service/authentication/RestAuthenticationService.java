package capital.any.service.authentication;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.LoginRequest;
import capital.any.model.base.CookieInfo;
import capital.any.model.base.Login;
import capital.any.model.base.User;
import capital.any.model.base.UserControllerDetails;
import capital.any.security.ISecurityService;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.login.ILoginService;
import capital.any.service.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class RestAuthenticationService implements IRestAuthenticationService {
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationService.class);
	public final Integer SESSION_TIMEOUT = 60 * 30;
	
	@Autowired
	private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private ILoginService loginService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private IUtilService utilService;
	@Autowired @Qualifier("UserServiceWeb")
	private IUserService userService;
	@Autowired
	private ICookieService cookieService;
	
	@Override
	public ResponseEntity<Response<User>> login(HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse, LoginRequest loginRequest) throws ServletException {
		Response<User> res = null;
		User user = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		List<String> messages = new ArrayList<String>();
		String message = "authenticate_failed";
		String username = loginRequest.getUsername();
		try {
			if (!utilService.isArgumentEmptyOrNull(username)) { //TODO may change to spring validation
				logger.info("loadUserByUsername; username: " + username);
				user = userService.getUserByEmail(username);
//				if (user == null) {
//					logger.info("User not found at anycapital! Let's try at ***option");
//					UserControllerDetails userControllerDetails = getUserControllerDetails(httpServletRequest, username, loginRequest);
//					userControllerDetails.setActionSource(loginRequest.getActionSource());
//					userControllerDetails.setUtcOffset(loginRequest.getUtcOffset());
//					user = userService.insertAnyoptionUser(userControllerDetails);
//					logger.info("***option user: " + user);
//				}
				if (null != user && user.getIsActive() == 0) {
					logger.info("User not active:" + user.getId());
					responseCode = ResponseCode.ACCOUNT_LOCKED; 
					message = "account-locked";
				} else if (null != user && authentication(httpServletRequest, loginRequest)) {
					responseCode = ResponseCode.OK;
					message = "authenticate_success";
					cookieService.addAttribute(
							new CookieInfo(httpServletResponse,
									ICookieService.LANGUAGE, 
									String.valueOf(user.getLanguageId()), 
									ICookieService.Year));
				} else {
					res = authenticationFailed();
				}
				// Response
				if (res == null) {
					messages.add(message);
					res = new Response<User>(user, responseCode, messages);
					logger.info(res.toString());
				}
			}
		} catch (Exception authenticationFailed) {
			logger.info("Authentication Failure, " + authenticationFailed.getMessage());
			res = authenticationFailed();
		}
		return new ResponseEntity<Response<User>>(res, HttpStatus.OK);
	}
	
	/**
	 * Get user controller details
	 * @param username
	 * @return UserControllerDetails
	 */
	private UserControllerDetails getUserControllerDetails(HttpServletRequest httpRequest, String username, LoginRequest loginRequest) {
		UserControllerDetails userControllerDetails = new UserControllerDetails(username, ActionSource.WEB, httpClientService.getIP(httpRequest), loginRequest.getMarketingTracking());
		return userControllerDetails;
	}
	
	/**
	 * Authentication failed
	 * @param authenticationFailed
	 * @return Response<User>
	 */
	private Response<User> authenticationFailed() {
		Response<User> res = null;
		List<String> messages = new ArrayList<String>();
		List<Message> errorMessages = new ArrayList<Message>();
		errorMessages.add(new Message("error.login.email.or.password"));
		messages.add("authentication_failed");   	
		res = new Response<User>(ResponseCode.ACCESS_DENIED, 
				messages, 
				new Error("authentication_error", errorMessages));
		logger.info(res.toString());
		return res;
	}
	
	@Override
	public boolean authentication(HttpServletRequest httpServletRequest, LoginRequest loginRequest) throws Exception {
		httpServletRequest.login(loginRequest.getUsername(), loginRequest.getPassword());
		if (securityService.isAuthenticate()) {
			User user = securityService.getLoginFromSession();
			if (null != user) {
				httpServletRequest.getSession().setMaxInactiveInterval(SESSION_TIMEOUT);
				Login login = new Login(user.getId(), loginRequest.getActionSource().getId(), httpClientService.getUserAgent(httpServletRequest), loginRequest.getMarketingTracking(), httpClientService.getIP(httpServletRequest));
				loginService.insert(login);
				sessionService.addAttribute(httpServletRequest.getSession(), login, ISessionService.LOGIN);
				return true;
			}
		}
		return false;
	}
}
