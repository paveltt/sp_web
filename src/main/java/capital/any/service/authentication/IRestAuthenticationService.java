package capital.any.service.authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import capital.any.communication.Response;
import capital.any.model.LoginRequest;
import capital.any.model.base.User;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRestAuthenticationService {
	
	/**
	 * If user exists in anycapital -> Call to authentication process.
	 * If user not exists in anycapital -> Check if user exists in ao. If so, Register to anycapital & Call to authentication process.
	 * @param httpServletRequest
	 * @param loginRequest
	 * @return ResponseEntity<Response<User>>
	 * @throws ServletException
	 */
	ResponseEntity<Response<User>> login(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, LoginRequest loginRequest) throws ServletException;
	
	/**
	 * Authentication process.
	 * @param httpServletRequest
	 * @param loginRequest
	 * @return boolean
	 * @throws Exception
	 */
	boolean authentication(HttpServletRequest httpServletRequest, LoginRequest loginRequest) throws Exception;
}
