package capital.any.service.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.ILoginDao;
import capital.any.model.base.Login;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;

/**
 * @author eranl
 *
 */
@Service("LoginServiceWeb")
public class LoginService extends capital.any.service.base.login.LoginService implements ILoginService {
	
	@Autowired @Qualifier("LoginDaoWeb")
	private ILoginDao loginDao;
	@Autowired
	private IMarketingTrackingService marketingTrackingService;

	@Override
	public void insert(Login login) {
		beforeInsertLogin(login);
		loginDao.insert(login);
	}

	private void beforeInsertLogin(Login login) {
		/* marketing Tracking */
		marketingTrackingService.insert(login.getMarketingTracking());
	}
}
