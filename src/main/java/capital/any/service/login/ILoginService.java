package capital.any.service.login;

import capital.any.model.base.Login;


/**
 * @author eranl
 *
 */
public interface ILoginService extends capital.any.service.base.login.ILoginService {	
	
	void insert(Login login);
	
}
