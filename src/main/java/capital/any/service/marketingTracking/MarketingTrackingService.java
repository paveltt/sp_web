package capital.any.service.marketingTracking;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.model.base.MarketingTracking;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.session.ISessionService;

/**
 * 
 * @author eyal.o
 *
 */
@Service("MarketingTrackingServiceWeb")
public class MarketingTrackingService extends capital.any.service.base.marketingTracking.MarketingTrackingService implements IMarketingTrackingService {
	
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private ICookieService cookieService;
	
    /**
	 * Get marketing tracking
	 * @param servletRequest
	 * @return MarketingTracking
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MarketingTracking getMarketingTracking(HttpServletRequest servletRequest) {
		Map<String,String> mapRequestParams = (Map<String, String>) sessionService.getAttribute(servletRequest.getSession(), ISessionService.MAP_REQUEST_PARAMS);
		Map<String,String> mapCookies = cookieService.readCookieMap(servletRequest);
		MarketingTracking marketingTracking = getMarketingTrackingByMap(mapRequestParams, mapCookies);
		return marketingTracking;
	}
}
