package capital.any.service.marketingTracking;

import javax.servlet.http.HttpServletRequest;

import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.o
 *
 */
public interface IMarketingTrackingService {

	MarketingTracking getMarketingTracking(HttpServletRequest servletRequest);

}
