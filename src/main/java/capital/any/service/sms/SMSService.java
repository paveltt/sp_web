package capital.any.service.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.Table;
import capital.any.model.MobivateDLRResponse;
import capital.any.model.base.ProviderRequestResponse;
import capital.any.model.base.SMS;
import capital.any.service.base.providerRequestResponse.IProviderRequestResponseService;

/**
 * @author eran.levy
 *
 */
@Service("SMSServiceWeb")
public class SMSService extends capital.any.service.base.sms.SMSService implements ISMSService {
	
	private static final Logger logger = LoggerFactory.getLogger(SMSService.class);
	
	@Autowired 
	private IProviderRequestResponseService providerRequestResponseService;
	
    /*
	* MOBIVATE Status reference
	* 	1 -> DELIVERED		Message delivered to its destination
	* 	2 -> ACCEPTED		Message accepted by gateway
	* 	3 -> EXPIRED		Message validity period expired
	* 	4 -> DELETED		Message deleted
	* 	5 -> UNDELIVERABLE	Message is undeliverable
	* 	6 -> UNKNOWN		Message is in unknown state
	* 	7 -> REJECTED		Message rejected
	* 	8 -> interim queued
	* 	? -> SENT		Message delivered to gateway
	*/
	
	@Override
	public void handleMobivateResponse(MobivateDLRResponse mobivateResponse, String soapResponse) {
		ProviderRequestResponse providerRequestResponse = new ProviderRequestResponse();
		providerRequestResponse.setTableId(Table.SMS.getId());
		SMS sms = null;
		try { 
			long smsId = Long.valueOf(mobivateResponse.getDeliveryreceipt().getClientReference());
			sms =  get(smsId);
			providerRequestResponse.setReferenceId(sms.getId());
		} catch (Exception e) {
			logger.error("Cand find SMS ", e);
		}		
		providerRequestResponse.setResponse(soapResponse);
		providerRequestResponseService.insert(providerRequestResponse);	
		if (sms != null) { // if we found sms reference in our DB
			try { 
				boolean update = false;
				if (sms.getStatus() == SMS.Status.WAITING_HLR) {  // in case its HLR type 
					int mobivateStatusId = Integer.valueOf(mobivateResponse.getDeliveryreceipt().getStatusCode()); 
					switch (mobivateStatusId) {
						case 1: // in case we got success for phone verification
							if (mobivateResponse.getDeliveryreceipt().getHlr() != null) {
								sms.setStatus(SMS.Status.QUEUED);
								update = true;
							}
							break;
						case 2:
							logger.debug("Got ACCEPTED from Mobivate");
							break;
						case 3:	case 4: case 5:	case 6: case 7:	case 8: 
							sms.setStatus(SMS.Status.INVALID);
							update = true;
						default:
							break;
					}
				} else  if (sms.getStatus() == SMS.Status.SUBMITTED) { // in case SMS sending type					
					int mobivateStatusId = Integer.valueOf(mobivateResponse.getDeliveryreceipt().getStatusCode()); 
					switch (mobivateStatusId) {
						case 1: // in case we got success for SMS sending						
							sms.setStatus(SMS.Status.DELIVERED);
							update = true;
							break;
						case 2:
							logger.debug("Got ACCEPTED from Mobivate");
							break;
						case 3:	case 4: case 5:	case 6: case 7:	case 8: 
							sms.setStatus(SMS.Status.FAILED_TO_SUBMIT);
							update = true;
						default:
							break;
					}					
				} else {
					logger.warn("Got SMS status for not supported status " + sms);
				}
				if (update) {
					//SMSService.update(sms);					
					update(sms);
				}
			} catch (Exception e) {
				logger.error("Error while trying to parse/update SMS " + sms);
			}
		} else {
			logger.warn("Got SMS from Mobivate without reference  " + sms);
		}
		
	}
}
