package capital.any.service.application;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.DBParameter;
import capital.any.device.IDeviceService;
import capital.any.model.base.Country;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.base.allowIP.IAllowIPService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.investmentPossibilities.IInvestmentPossibilitiesService;
import capital.any.service.user.IUserService;

/**
 * @author eranl
 *
 */
@Service("ApplicationServiceWeb")
public class ApplicationService extends capital.any.service.base.application.ApplicationService implements IApplicationService {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationService.class);
	
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ICountryService countryService;
	
	@Autowired
	private ICurrencyService currencyService;
	
	@Autowired
	private IInvestmentPossibilitiesService investmentPossibilitiesService;
	
	@Autowired
	private IDeviceService deviceService;
	
	@Autowired
	private HttpClientService httpClientService;
	
	@Autowired
	private IUtilService utilService;
	
	@Autowired
	private IDBParameterService dbParameterService;
	
	@Autowired
	private IAllowIPService allowIPService ;
	
	@Override
	public Map<String, Object> init(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();		
		map.put("investmentPossibilities", investmentPossibilitiesService.get()) ;
		map.put("currenciesHM", currencyService.get());		
		map.put("countriesHM", countryService.getRegulatedCountries());
		map.put("user", userService.getLoginFromSession());	
		map.put("deviceInfo", deviceService.get(request));
		map.put("userLogout", userService.getLogout(request));
		map.put("support_phone", dbParameterService.get(DBParameter.SUPPORT_PHONE.getId()).getStringValue());
		map.put("support_email", dbParameterService.get(DBParameter.SUPPORT_EMAIL.getId()).getStringValue());
		map.put("Company_Address", dbParameterService.get(DBParameter.COMPANY_ADDRESS.getId()).getStringValue());
		return map;		
	}
	
	@Override
	public boolean isBlockedRegister(HttpServletRequest request, String newIP) {
		boolean result = false;
		boolean allowIP = false;
		String ip = httpClientService.getIP(request);
		logger.debug("ip found: " + ip);		
		if (!utilService.isArgumentEmptyOrNull(ip)) {			
			if (allowIPService.get().get(ip) != null) { // in case we are allow ip we want to allow register
				allowIP = true;
			}
			if (allowIP && !utilService.isArgumentEmptyOrNull(newIP)) { // in case we are allow ip we can override the IP in order to simulate countries
				ip = newIP;
				allowIP = false;
			}
			if (!allowIP) {
				Country country = countryService.getByIp(ip);
				if (country != null) {				
					logger.debug("country found: " + country);
				}
				result = countryService.get().get(country.getId()).getIsBlocked() == 0 ? false : true;
			}
		}
		return result;
	}
}