package capital.any.service.application;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * @author eranl
 *
 */
public interface IApplicationService extends capital.any.service.base.application.IApplicationService {	
	
	Map<String, Object> init(HttpServletRequest request);
	
	boolean isBlockedRegister(HttpServletRequest request, String newIP);
	
}
