package capital.any.service.user;

import capital.any.model.base.UserControllerDetails;
import javax.servlet.http.HttpServletRequest;
import capital.any.communication.Response;
import capital.any.model.UserLogout;
import capital.any.model.base.User;

/**
 * @author eranl
 *
 */
public interface IUserService extends capital.any.service.base.user.IUserService {	
		
	/**
	 * 
	 */
	User getLoginFromSession();
	
	/**
	 * @param data 
	 * @return
	 * @throws Exception 
	 */
	User insertAnyoptionUser(UserControllerDetails u) throws Exception;
	
	/**
	 * Insert user & login
	 * @param httpServletRequest
	 * @param user
	 * @return 
	 * @throws Exception 
	 */
	Response<User> insertUser(HttpServletRequest httpServletRequest, User user) throws Exception;
	
	/**
	 * @param request
	 * @return
	 */
	UserLogout getLogout(HttpServletRequest request);
}
