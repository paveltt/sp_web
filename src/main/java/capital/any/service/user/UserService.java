package capital.any.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.UserHistory;
import capital.any.model.base.User.Clazz;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Error;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.LoginRequest;
import capital.any.model.UserLogout;
import capital.any.model.base.Country;
import capital.any.model.base.NullWriter;
import capital.any.model.base.User;
import capital.any.model.base.anyoption.AnyoptionAPIRequest;
import capital.any.model.base.anyoption.AnyoptionAPIResponse;
import capital.any.model.base.anyoption.ResponseLogin;
import capital.any.model.base.anyoption.UserExtraFields;
import capital.any.security.SecurityService;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.authentication.IRestAuthenticationService;
import capital.any.service.base.anyoption.api.IAnyoptionAPIService;
import capital.any.service.base.country.ICountryService;

/**
 * @author eranl
 *
 */
@Service("UserServiceWeb")
public class UserService extends capital.any.service.base.user.UserService implements IUserService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private SecurityService securityService;
	@Autowired
	private IAnyoptionAPIService anyoptionAPIService;
	@Autowired
	IRestAuthenticationService restAuthenticationService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private IUtilService utilService;
	
	@Override
	public User getLoginFromSession() {
		return securityService.getLoginFromSession();
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public User insertAnyoptionUser(UserControllerDetails u) throws Exception {
		User user = null;
		AnyoptionAPIRequest data = new AnyoptionAPIRequest(u.getUserName(), "");
		//AnyoptionAPIResponse<User> anyoptionAPIResponse = anyoptionAPIService.getAnyoptionUser(data); 
		ResponseLogin anyoptionAPIResponse = anyoptionAPIService.getAnyoptionUser(data); //TODO change to AnyoptionAPIResponse<User>. solve linkedhashmap
		logger.info("anyoptionAPIResponse: " + anyoptionAPIResponse);
		if(anyoptionAPIResponse != null && anyoptionAPIResponse.getApiCode().equals("G000")) { //FIXME
			user = anyoptionAPIResponse.getData();
			if (user != null) {
				user.setIp(u.getIp());
				user.setActionSource(u.getActionSource());
				user.setMarketingTracking(u.getMarketingTracking());
				user.setUtcOffset(u.getUtcOffset());
				insertUser(user);
				Future<AnyoptionAPIResponse<UserExtraFields>> userEX = anyoptionAPIService.getAnyoptionUserExtraFields(user, new UserHistory(ActionSource.WEB, new NullWriter()));
				logger.info("after getAnyoptionUserExtraFields");
				if (userEX.isDone()) {
					logger.info("result from asynchronous process getAnyoptionUserExtraFields - " + userEX.get());
		        }
			}
		}
		return user;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Response<User> insertUser(HttpServletRequest httpServletRequest, User user) throws Exception {
		ResponseCode responseCode = ResponseCode.OK;
		Error error = null;
   		// set default parameters for WEB users  
		user.setIsContactBySms(1);
		user.setIsContactByPhone(1);
		user.setIsContactByEmail(1);
		user.setIsAcceptedTerms(1);
		String password = user.getPassword();
		
		String ip = httpClientService.getIP(httpServletRequest);
		if (!utilService.isArgumentEmptyOrNull(ip)) {
			Country country = countryService.getByIp(ip);
			if (country != null) {
				user.setCountryByIP(country.getId());
			}
			user.setIp(ip);
		}
		
		try {
			super.insertUser(user);
		} catch (DataAccessException dae) { //first dao level exception
			logger.error("cant signup, dao exception", dae);
			user = null;
			responseCode = ResponseCode.OPERATION_DAO_FAILED;
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("insert.user.failed.dao", ""));
			error = new Error("register", msg);
        }
		if (user != null) {
			try {
				LoginRequest loginRequest = new LoginRequest();
				loginRequest.setUsername(user.getEmail());
				loginRequest.setPassword(password);
				loginRequest.setActionSource(user.getActionSource());
				loginRequest.setMarketingTracking(user.getMarketingTracking());
				restAuthenticationService.authentication(httpServletRequest, loginRequest);
			} catch (Exception e) {
				user = null;
				responseCode = ResponseCode.AUTHENTICATION_FAILURE;
				logger.error("Problem with login process.", e);
			}
		}
		return new Response<User>(user, responseCode, null, error);
	}

	@Override
	public UserLogout getLogout(HttpServletRequest request) {
		UserLogout userLogout = null;
		String ip = httpClientService.getIP(request);
		logger.debug("start geUserLogout details; ip: " + ip);
		if (!utilService.isArgumentEmptyOrNull(ip)) {
			Country country = countryService.getByIp(ip);
			if (country != null) {
				userLogout = new UserLogout(country);
				logger.debug("geUserLogout; ip & country found: " + userLogout.toString());
			}
		}
		return userLogout;
	}
}
