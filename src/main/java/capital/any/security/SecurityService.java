package capital.any.security;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.model.base.User;



/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class SecurityService implements ISecurityService {
	private static final Logger logger = LoggerFactory.getLogger(SecurityService.class);
	private static final ObjectMapper mapper = new ObjectMapper();
	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

	/**
	 * empty const'
	 */
	private SecurityService() {

	}

	public User getLoginFromSession() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		User user = null;		
		
		if (authentication != null) {
			if (authentication.getPrincipal() instanceof CustemUserDetails) {
				user = ((CustemUserDetails) authentication.getPrincipal()).getUser();
			} else if (authentication.getPrincipal() instanceof User) {
				user = (User) authentication.getPrincipal();
			}
		}
		return user;
	}

	/*public void sendError(HttpServletResponse response, Exception exception, ResponseCode code ,int status, String message)
			throws IOException {
		response.setContentType(CONTENT_TYPE);
		response.setStatus(status);
		PrintWriter writer = response.getWriter();
		//Error error = new Error("authenticationError", exception.getMessage());
		//writer.write(mapper.writeValueAsString(code) + mapper.writeValueAsString(new Response(code, message, error)));
		writer.flush();
		writer.close();
	}*/

	public void sendResponse(Object response, HttpServletResponse httpResponse, int httpStatus) throws IOException {
		httpResponse.setContentType(CONTENT_TYPE);
		httpResponse.setStatus(httpStatus);
		PrintWriter writer = httpResponse.getWriter();
		writer.write(mapper.writeValueAsString(response));
		writer.flush();
		writer.close();
	}

	@Override
	public boolean isAuthenticate() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		 if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
        	return true;
        }
        return false;
	}
}
