package capital.any.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.User;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class CustemUserDetailService implements UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(CustemUserDetailService.class);
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired @Qualifier("UserServiceWeb")
	private IUserService userService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private ICookieService cookieService;

	private static final String delim = "_";

	@Override
	//assumption: username is an email
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		UserDetails userDetails = null;
		try {
			if (!utilService.isArgumentEmptyOrNull(username)) { //TODO may change to spring validation
				logger.info("loadUserByUsername; username: " + username);
				user = userService.getUserByEmail(username);
				userDetails = buildUserDetails(user);
			}
		} catch (UsernameNotFoundException e) {
			logger.error("user not found!", e);
			user = null;
		} catch (Exception e) {
			logger.error("A problem occured while trying to get user!", e);
			user = null;
		}
		return userDetails;
	}


	/**
	 * @param realUser
	 * @return
	 */
	public UserDetails buildUserDetails(User realUser) {
		CustemUserDetails user = null;
		if (realUser != null) {
			try {
				Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority("USER"));
				user = new CustemUserDetails(authorities, realUser);
			} catch (Exception e) {
				logger.error("ERROR! when tried to buildUserDetails", e);
			}
		}
		return user;
	}

	/**
	 * Get user controller details
	 * @param username
	 * @return UserControllerDetails
	 */
	private UserControllerDetails getUserControllerDetails(String username) {
		Map<String,String> mapRequestParams = (Map<String, String>) sessionService.getAttribute(request.getSession(), ISessionService.MAP_REQUEST_PARAMS);
		Map<String,String> mapCookies = cookieService.readCookieMap(request);
		MarketingTracking marketingTracking = marketingTrackingService.getMarketingTrackingByMap(mapRequestParams, mapCookies);
		UserControllerDetails userControllerDetails = new UserControllerDetails(username, ActionSource.WEB, httpClientService.getIP(request), 
				marketingTracking);
		return userControllerDetails;
	}
}
