package capital.any.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import capital.any.model.base.User;


/**
 * @author eranl
 *
 */
public class CustemUserDetails extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 2591527292076048232L;
	
	private User user;
		
	public CustemUserDetails(User user) {
		super(user.getEmail(), "", true, false, false, false, null);
		this.user = user;
	}
	
	public CustemUserDetails(Collection<? extends GrantedAuthority> authorities, User user) {
		super(user.getEmail(), user.getPassword(),  authorities);
		this.user = user;
	}
		
	public CustemUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, User user) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.user = user;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
