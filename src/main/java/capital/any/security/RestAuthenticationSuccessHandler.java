package capital.any.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Login;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.User;
import capital.any.service.HttpClientService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.login.ILoginService;

/**
 * @author LioR SoLoMoN
 *
 */
//TODO may delete
@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationSuccessHandler.class);
	private final static int SESSION_TIMEOUT = 60 * 30;
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private ILoginService loginService;
	
	@Autowired
	private HttpClientService httpClientService;
	
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private ICookieService cookieService;

	public RestAuthenticationSuccessHandler() {
		super();
		setRedirectStrategy(new NoRedirectStrategy());
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			Authentication authentication) throws IOException, ServletException {
		logger.info("onAuthenticationSuccess; " + authentication.toString());
		User user = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		List<String> messages = new ArrayList<String>();
		String message = "authenticate_failed";
		
		if (authentication.getPrincipal() instanceof CustemUserDetails) {
			user = ((CustemUserDetails) authentication.getPrincipal()).getUser();
			setAdditionalInfo(httpRequest, user);
			responseCode = ResponseCode.OK;
			message = "authenticate_success";
			httpRequest.getSession().setMaxInactiveInterval(SESSION_TIMEOUT);
			Login login = new Login(user.getId(), ActionSource.WEB.getId(), httpClientService.getUserAgent(httpRequest), user.getMarketingTracking(), httpClientService.getIP(httpRequest));
			loginService.insert(login);
			sessionService.addAttribute(httpRequest.getSession(), login, ISessionService.LOGIN);
		}
		
		messages.add(message);
		Response<User> response = new Response<User>(user, responseCode, messages);
		logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}

	protected class NoRedirectStrategy implements RedirectStrategy {
		@Override
		public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url)
				throws IOException {
			logger.debug("sendRedirect");
			// no redirect
		}
	}
	
	/**
	 * Set additional info
	 * @param httpRequest
	 * @param user
	 */
	private void setAdditionalInfo(HttpServletRequest httpRequest, User user) {
		/* Marketing Tracking */
		Map<String,String> mapRequestParams = (Map<String, String>) sessionService.getAttribute(httpRequest.getSession(), ISessionService.MAP_REQUEST_PARAMS);
		Map<String,String> mapCookies = cookieService.readCookieMap(httpRequest);
		MarketingTracking marketingTracking = marketingTrackingService.getMarketingTrackingByMap(mapRequestParams, mapCookies);
		user.setMarketingTracking(marketingTracking);
	}
}
