package capital.any.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.communication.Error;
import capital.any.communication.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LioR SoLoMoN
 *
 */
public class RestAccessDeniedHandler implements AccessDeniedHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestAccessDeniedHandler.class);
	@Autowired
	private SecurityService securityService; 
	
	public RestAccessDeniedHandler() {
		super();		
	}
	
    @Override
    public void handle(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                       AccessDeniedException exception) throws IOException, ServletException {
    	
    	List<Message> errorMessages = new ArrayList<Message>();
    	List<String> messages = new ArrayList<String>();
    	errorMessages.add(new Message(exception.getMessage()));
    	messages.add("not_authorized_resources");    	
    	Response<?> response = 
    			new Response<Object>(ResponseCode.AUTHENTICATION_FAILURE, 
    			messages, 
    			new Error("forbidden_error", errorMessages));
    	
    	logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
    }
}