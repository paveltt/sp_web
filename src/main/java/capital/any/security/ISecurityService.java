package capital.any.security;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ResponseBody;

import capital.any.communication.Response;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ISecurityService {

    /**
     * Get current user login.
     * @return current user in the Session
     */
    public User getLoginFromSession();

    /**
     * @param response
     * @param exception
     * @param status
     * @param message
     * @throws IOException
     */
    //public void sendError(HttpServletResponse response, Exception exception, ResponseCode code, int status, String message) throws IOException;

    /**
     * @param response
     * @param status
     * @param object
     * @throws IOException
     */
    public void sendResponse(Object response, HttpServletResponse httpResponse, int httpStatus) throws IOException;
    
    /**
     * check if there is user authenticate in the session
     * @return true if we have user authenticate else false (return code should be ACCESS_DENIED responseCode.
     */
    public boolean isAuthenticate();
}
