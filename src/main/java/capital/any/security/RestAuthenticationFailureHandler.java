package capital.any.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.communication.Error;
import capital.any.communication.Message;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class RestAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationFailureHandler.class);
	@Autowired
	private SecurityService securityService; 
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			AuthenticationException exception) throws IOException, ServletException {
		logger.info("onAuthenticationFailure; " + exception.getMessage());
		List<Message> errorMessages = new ArrayList<Message>();
    	List<String> messages = new ArrayList<String>();
    	errorMessages.add(new Message("error.login.email.or.password"));
    	messages.add("authentication_failed");   	
    	Response<?> response = 
    			new Response<Object>(ResponseCode.ACCESS_DENIED, 
    			messages, 
    			new Error("authentication_error", errorMessages));
    	
    	logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}
}
