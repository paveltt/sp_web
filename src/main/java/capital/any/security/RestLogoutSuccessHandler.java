package capital.any.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class RestLogoutSuccessHandler implements LogoutSuccessHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestLogoutSuccessHandler.class);
	@Autowired
	private SecurityService securityService; 
	
	@Override
	public void onLogoutSuccess(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			Authentication authentication) throws IOException, ServletException {
		
		User user = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		List<String> messages = new ArrayList<String>();
		String message = "logout_failed";
		
		if (authentication != null && authentication.getDetails() != null) {
			try {
				httpRequest.getSession().invalidate();
				message = "logout_success";
				responseCode = ResponseCode.OK;
				if (authentication.getPrincipal() instanceof CustemUserDetails) {
					user = ((CustemUserDetails)authentication.getPrincipal()).getUser();		
				}
				logger.info("User Successfully Logout");
			} catch (Exception e) {
				logger.error("ERROR Logout", e);
			}
		} else {
			message = "user_not_authenticated";
			responseCode = ResponseCode.AUTHENTICATION_FAILURE;
		}
		messages.add(message);
		Response<User> response = new Response<User>(user, responseCode , messages);
		logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}

}
