package capital.any.device;

import javax.servlet.http.HttpServletRequest;

import capital.any.model.base.Device;

/**
 * @author eran.levy
 *
 */
public interface IDeviceService {
	
	Device get(HttpServletRequest request);
	
}
