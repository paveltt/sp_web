package capital.any.device;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.flyingkite.mobiledetect.UAgentInfo;
import capital.any.model.base.Device;
import capital.any.model.base.Device.OSType;
import capital.any.model.base.Device.Type;
import capital.any.service.IHttpClientService;

/**
 * @author eran.levy
 *
 */
@Service
public class DeviceService implements IDeviceService {
	private static final Logger logger = LoggerFactory.getLogger(DeviceService .class);
	
	@Autowired
	private IHttpClientService httpClientService;

	@Override
	public Device get(HttpServletRequest request) {
		logger.debug("Going to get device info");
		Device device = new Device();
		String userAgent = httpClientService.getUserAgent(request);
		logger.debug("Device userAgent:" + userAgent);
		UAgentInfo agentInfo = new UAgentInfo(userAgent, null);
		boolean isMobileDevice = agentInfo.detectMobileQuick();
		boolean isTabletDevice = agentInfo.detectTierTablet();
		Type type = Type.DESKTOP;
		if (isTabletDevice) {
			type = Type.TABLET;
		} else if (isMobileDevice) {
			type = Type.MOBILE;
		}				
		device.setType(type);
		boolean isIOS = agentInfo.detectIos();
		boolean isAndroid = agentInfo.detectAndroid();
		if (isIOS || isAndroid) {
			OSType osType = null;
			if (isIOS) {
				osType = OSType.IOS;
			} else if (isAndroid) {
				osType = OSType.ANDROID;
			}						
			device.setOsType(osType);
		}
		logger.debug("Device info:" + device);
		return device;
	}
	
}
