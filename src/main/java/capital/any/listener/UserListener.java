package capital.any.listener;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.model.base.User;
import capital.any.service.base.session.ISessionService;
import capital.any.service.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "listener")
public class UserListener {
	private static final Logger logger = LoggerFactory.getLogger(UserListener.class);
	private static final String FORGOT_PASSWORD_PAGE = "/reset-password"; //TODO here?
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService; 
	
	//LINK?token=<hash token>&userId=<hash userId>&lang=<lang code>
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public ModelAndView forgotPassword(@RequestParam Map<String,String> requestParams, HttpServletResponse response, HttpSession session) {		
		logger.info("listener forgotPassword: " + requestParams.toString());
		//TODO remove #
		StringBuilder link = new StringBuilder("redirect:/#/");
		try {
			sessionService.addAttribute(session, requestParams, ISessionService.MAP_FORGOT_PASSWORD_PARAMS);
			User user = userService.getUserById(Long.valueOf(secureAlgorithmsService.decryptAES(requestParams.get("userId")))); 
			link.append(requestParams.get("lang"))
				.append(FORGOT_PASSWORD_PAGE)
				.append("?email=")
				.append(user.getEmail());
		} catch (Exception e) {
			logger.error("ERROR! forgotPassword redirection", e);
		}
		return new ModelAndView(link.toString());
	}
}
