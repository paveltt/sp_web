package capital.any.listener;

import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.model.MobivateDLRResponse;
import capital.any.service.sms.ISMSService;

/**
 * @author eran.levy
 *
 */
@RestController
@RequestMapping(value = "/sms", method = RequestMethod.POST)
public class SMSDLRListener {
	private static final Logger logger = LoggerFactory.getLogger(SMSDLRListener.class);
	
	@Autowired @Qualifier("SMSServiceWeb")
	private ISMSService SMSService;
	
	@Autowired
	private ObjectMapper mapper;
			
	@RequestMapping(value = "/mobivate")
	public ResponseEntity<String> processDLR(@RequestBody String requestBody) {
		logger.info("Got request from Mobivate: " + requestBody);	
		requestBody = requestBody.replaceAll("xml=", "")
								 .replaceAll("%3C", "<")
								 .replaceAll("%3E", ">")
								 .replaceAll("%2F", "/");
		JSONObject soapDatainJsonObject =  XML.toJSONObject(requestBody);
		logger.info("jsonPrettyPrintString=\n" + soapDatainJsonObject.toString(4));		
		try {
			mapper.setSerializationInclusion(Include.NON_NULL);
			MobivateDLRResponse mobivateResponse = (MobivateDLRResponse) mapper.readValue(soapDatainJsonObject.toString(), MobivateDLRResponse.class);
			SMSService.handleMobivateResponse(mobivateResponse, soapDatainJsonObject.toString());
		} catch (Exception e) {
			logger.error("Error to parse Mobivate response:", e);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
}
