package capital.any.listener;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import capital.any.service.IUtilService;
import capital.any.service.base.landing.ILandingService;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;
import capital.any.service.base.session.ISessionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "landing", method = RequestMethod.GET)
public class LandingListener {
	private static final Logger logger = LoggerFactory.getLogger(LandingListener.class);
	
	@Autowired
	private ILandingService landingService;
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IUtilService utilService;
	
	@RequestMapping(value = "/init")
	public ModelAndView init(@RequestParam Map<String,String> allRequestParams, HttpServletResponse response, HttpSession session, HttpServletRequest request) {		
		logger.debug("allRequestParams: " + allRequestParams.toString() + " " + request.getQueryString());
		String pageRedirect = landingService.init(allRequestParams, response, request);
		String queryString = request.getQueryString(); 
		String url = pageRedirect;
		sessionService.addAttribute(session, allRequestParams, ISessionService.MAP_REQUEST_PARAMS);
		if (!utilService.isArgumentEmptyOrNull(queryString) && queryString.contains(IMarketingCampaignService.UTM_CAMPAIGN)) {
			url += "?" + queryString.substring(queryString.indexOf(IMarketingCampaignService.UTM_CAMPAIGN));
		}
		return new ModelAndView("redirect:/" + url);
	}
}
