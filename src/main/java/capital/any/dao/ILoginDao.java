package capital.any.dao;

import capital.any.model.base.Login;


/**
 * @author eranl
 *
 */
public interface ILoginDao extends capital.any.dao.base.login.ILoginDao {
	
	public static final String INSERT_LOGIN = 
			" INSERT " +
		    " INTO LOGINS " +
		    " ( " +
		    "   user_id, " +
		    "   action_source_id, " +
		    "   user_agent, " +
		    "	marketing_tracking_id, " +
		    " 	ip " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "	:userId, " +
		    "   :actionSourceId, " +
		    "   :userAgent, " +
		    "	:marketingTrackingId, " +
		    " 	:ip " +
		    " ) ";
		
	void insert(Login login);
	
}
