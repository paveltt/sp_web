package capital.any.dao.transaction;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.dao.base.transaction.TransactionCallbackHandler;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("TransactionDaoWeb")
public class TransactionDao extends capital.any.dao.base.transaction.TransactionDao implements ITransactionDao, ApplicationContextAware {

	@Override
	public List<Transaction> getUserReverseWithdrawalList(long userId) {
		TransactionCallbackHandler callbackHandler = appContext.getBean(TransactionCallbackHandler.class);
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", userId);
		jdbcTemplate.query(GET_USER_REVERSE_WITHDRAWAL_LIST, namedParameters, callbackHandler);
		return new ArrayList<Transaction>(callbackHandler.getResult().values());
	}

	@Override
	public List<Transaction> getBankingHistory(SqlFilters filters) {
		TransactionCallbackHandler callbackHandler = appContext.getBean(TransactionCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", filters.getUserId());
		params.addValue("from", filters.getFrom());
		params.addValue("to", filters.getTo());
		params.addValue("settledAtFrom", filters.getSettledAtFrom());
		params.addValue("settledAtTo", filters.getSettledAtTo());
		jdbcTemplate.query(GET_BANKING_HISTORY, params, callbackHandler);
		return new ArrayList<Transaction>(callbackHandler.getResult().values());
	}	
}
