package capital.any.dao.transaction;

import java.util.List;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionStatusEnum;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionDao extends capital.any.dao.base.transaction.ITransactionDao {

	public static final String GET_USER_REVERSE_WITHDRAWAL_LIST =
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_operations top " +
			"	,transaction_payment_types tpt " +
			"	,transaction_statuses ts " +
			"WHERE " +
			" 	t.user_id = :userId " +
			"	AND t.transaction_operation_id = top.id " +
			"	AND t.transaction_payment_type_id = tpt.id " +
			"	AND t.transaction_status_id = ts.id " + 
			"	AND t.transaction_status_id in " + "(" + 	TransactionStatusEnum.STARTED.getId() + "," +
															TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()+ "," +
															TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId() + 
													")" +										
			"";
	
	public static final String GET_BANKING_HISTORY =
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_payment_types tpt " +
			"	,transaction_operations top " +
			"	,transaction_statuses ts " +
			"WHERE " +
			"	t.transaction_operation_id = top.id " +
			"	AND t.transaction_payment_type_id = tpt.id " +
			"	AND t.transaction_status_id = ts.id " +
			"   AND 1 = IF(ISNULL(:userId), 1, IF(t.user_id = :userId, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:from), 1, IF(t.time_created >= :from, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:to), 1, IF(t.time_created <= :to, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtFrom), 1, IF(t.time_settled >= :settledAtFrom, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtTo), 1, IF(t.time_settled <= :settledAtTo, 1, 0)) " +
			"	AND ts.id in " + 
								"(" +
									TransactionStatusEnum.PENDING.getId()	+ "," +
									TransactionStatusEnum.SUCCEED.getId()	+ 
								")" +										
			"";
	
	
	/**
	 * @param userId
	 * @return
	 */
	List<Transaction> getUserReverseWithdrawalList(long userId);


	/**
	 * @param filters
	 * @return
	 */
	List<Transaction> getBankingHistory(SqlFilters filters);
	
}
