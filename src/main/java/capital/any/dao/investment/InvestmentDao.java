package capital.any.dao.investment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Investment;

/**
 * @author EyalG
 *
 */
@Repository("InvestmentDaoWEB")
public class InvestmentDao extends capital.any.dao.base.investment.InvestmentDao implements IInvestmentDao {

	@Override
	public List<Investment> getUserInvestments(long userId, long productId) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userId", userId)
				 .addValue("productId", productId);		
		return jdbcTemplate.query(GET_USER_INVESTMENTS_BY_PRODUCT, parameters, investmentsMapper);
	}

	@Override
	public Map<Long, Long> getInvestmentsProductId(long userId) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userId", userId);
		List<Investment> list = jdbcTemplate.query(GET_USER_INVESTMENTS_PRODUCT_ID, parameters, new RowMapper<Investment>() {

			@Override
			public Investment mapRow(ResultSet rs, int rowNum) throws SQLException {
				Investment i = new Investment();
				i.setAmount(rs.getLong("amount"));
				i.setProductId(rs.getLong("product_id"));
				return i;
			}
		});
		Map<Long, Long> collect = list.stream().collect(Collectors.toMap(i -> i.getProductId(), i -> i.getAmount()));
		return collect;
	}	
}
