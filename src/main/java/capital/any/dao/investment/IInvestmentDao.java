package capital.any.dao.investment;

import java.util.List;
import java.util.Map;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.model.base.Investment;

/**
 * @author EyalG
 *
 */
public interface IInvestmentDao extends capital.any.dao.base.investment.IInvestmentDao {
	
	public static final String GET_USER_INVESTMENTS_BY_PRODUCT =
			"SELECT " + 
			"	i.* " +
			"FROM " +
				"investments i " +
			"WHERE " +
				"i.user_id = :userId " +
				"AND i.product_id = :productId " +
				"AND i.investment_status_id in (" + InvestmentStatusEnum.PENDING.getId() + ", " + InvestmentStatusEnum.OPEN.getId() + ")";
	
	public static final String GET_USER_INVESTMENTS_PRODUCT_ID =
			"SELECT " +
					"i.product_id " +
					", sum(i.amount) amount " +
			"FROM " + 
				"investments i " +
			"WHERE " +
				"i.user_id = :userId " +
				"AND i.investment_status_id in (" + InvestmentStatusEnum.PENDING.getId() + ", " + InvestmentStatusEnum.OPEN.getId() + ")" +
			"GROUP BY " +
				"i.product_id ";
				
	
	/**
	 * get all user open investments by product id
	 * @param userId
	 * @param productId
	 * @return List<Investment>
	 */
	List<Investment> getUserInvestments(long userId, long productId);
	
	/**
	 * get all products that have open investments and amount invested
	 * @param userId
	 * @return map of product id and sum investments
	 */
	Map<Long, Long> getInvestmentsProductId(long userId);
}
