package capital.any.dao.product;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Product;

/**
 * @author Eyal Goren
 *
 */
@Repository("ProductHCDaoWEB")
public class ProductHCDao extends capital.any.dao.base.product.ProductHCDao implements IProductDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProductsForWeb() {
		ArrayList<Product> productsResult = null;
		try {
			IMap<Long, Product> products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
			if (products != null && !products.isEmpty()) {
				SqlPredicate openProducts = new SqlPredicate("productStatus.getId() IN (" + ProductStatusEnum.SUBSCRIPTION.getId() + ", " + ProductStatusEnum.SECONDARY.getId() + ") and isSuspend = false");
				productsResult = new ArrayList<Product>(products.values(openProducts));
			}
		} catch (Exception e) {
			logger.warn("can't get products from hazelcast", e);
		}
		return productsResult;
	}
}
