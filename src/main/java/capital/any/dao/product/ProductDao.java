package capital.any.dao.product;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Product;

/**
 * @author Eyal Goren
 *
 */
@Repository("ProductDaoWEB")
public class ProductDao extends capital.any.dao.base.product.ProductDao implements IProductDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductDao.class);

	@Override
	public List<Product> getProductsForWeb() {
		return jdbcTemplate.query(GET_WEB_PRODUCTS_LIST, productMapper);
	}
}
