package capital.any.dao.product;

import java.util.List;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;

/**
 * @author Eyal Goren
 *
 */
public interface IProductDao extends capital.any.dao.base.product.IProductDao {
	
	public static final String GET_WEB_PRODUCTS_LIST = 
			"SELECT " + 
			"	*  " +
			"FROM " +
			"	products p " +
			"WHERE " +
			"	p.product_status_id in (" + ProductStatusEnum.SUBSCRIPTION.getId() + "," + ProductStatusEnum.SECONDARY.getId() + ") " +
			"   AND p.is_suspend = 0 " +
			"ORDER BY " +
			"	p.priority desc";
		
	List<Product> getProductsForWeb();
}
