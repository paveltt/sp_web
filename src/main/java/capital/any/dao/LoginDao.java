package capital.any.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Login;


/**
 * @author eranl
 *
 */
@Repository("LoginDaoWeb")
public class LoginDao extends capital.any.dao.base.login.LoginDao implements ILoginDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insert(Login login) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("userId", login.getUserId())
				.addValue("actionSourceId", login.getActionSourceId())		
				.addValue("marketingTrackingId", login.getMarketingTracking().getId() == 0 ? null : login.getMarketingTracking().getId())
				.addValue("ip", login.getIp())
				.addValue("userAgent", login.getUserAgent());
		
		namedParameterJdbcTemplate.update(INSERT_LOGIN, namedParameters, keyHolder, new String[] {"id"});
		login.setId(keyHolder.getKey().longValue());		
	}	
	
}
