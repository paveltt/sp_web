package capital.any.model;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author eran.levy
 *
 */
public class MobivateDLRResponse {
	private Deliveryreceipt deliveryreceipt;

	/**
	 * @return the deliveryreceipt
	 */
	public Deliveryreceipt getDeliveryreceipt() {
		return deliveryreceipt;
	}

	/**
	 * @param deliveryreceipt the deliveryreceipt to set
	 */
	public void setDeliveryreceipt(Deliveryreceipt deliveryreceipt) {
		this.deliveryreceipt = deliveryreceipt;
	}


	public class Deliveryreceipt {

		private String created;
		private String part;
		private String clientReference;
		private String deliveryMessageId;
		private Integer parts;
		private Hlr hlr;
		private String status;
		private String statusCode;
		 
		/**
		 * @return the created
		 */
		public String getCreated() {
			return created;
		}
		/**
		 * @param created the created to set
		 */
		public void setCreated(String created) {
			this.created = created;
		}
		/**
		 * @return the part
		 */
		public String getPart() {
			return part;
		}
		/**
		 * @param part the part to set
		 */
		public void setPart(String part) {
			this.part = part;
		}
		/**
		 * @return the clientReference
		 */
		public String getClientReference() {
			return clientReference;
		}
		/**
		 * @param clientReference the clientReference to set
		 */
		public void setClientReference(String clientReference) {
			this.clientReference = clientReference;
		}
		/**
		 * @return the deliveryMessageId
		 */
		public String getDeliveryMessageId() {
			return deliveryMessageId;
		}
		/**
		 * @param deliveryMessageId the deliveryMessageId to set
		 */
		public void setDeliveryMessageId(String deliveryMessageId) {
			this.deliveryMessageId = deliveryMessageId;
		}
		/**
		 * @return the parts
		 */
		public Integer getParts() {
			return parts;
		}
		/**
		 * @param parts the parts to set
		 */
		public void setParts(Integer parts) {
			this.parts = parts;
		}
		/**
		 * @return the hlr
		 */
		public Hlr getHlr() {
			return hlr;
		}
		/**
		 * @param hlr the hlr to set
		 */
		public void setHlr(Hlr hlr) {
			this.hlr = hlr;
		}
		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}
		/**
		 * @param status the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}
		/**
		 * @return the statusCode
		 */
		public String getStatusCode() {
			return statusCode;
		}
		/**
		 * @param statusCode the statusCode to set
		 */
		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}
		
	}

	public class Hlr {
		private String origNetworkName;
		private String origCountryName;
		private String mnc;
		private String mobile;
		private String portedNetworkPrefix;
		private String imsi;
		private String mcc;
		private String portedNetworkName;
		private String ported;
		private String origCountryPrefix;
		private String portedNetworkCountryName;
		private String origNetworkPrefix;
		private String updated;
		private String portedCountryPrefix;
		
		@JsonCreator
		public Hlr() {	
		}
		
		/**
		 * @return the origNetworkName
		 */
		public String getOrigNetworkName() {
			return origNetworkName;
		}
		/**
		 * @param origNetworkName the origNetworkName to set
		 */
		public void setOrigNetworkName(String origNetworkName) {
			this.origNetworkName = origNetworkName;
		}
		/**
		 * @return the origCountryName
		 */
		public String getOrigCountryName() {
			return origCountryName;
		}
		/**
		 * @param origCountryName the origCountryName to set
		 */
		public void setOrigCountryName(String origCountryName) {
			this.origCountryName = origCountryName;
		}
		/**
		 * @return the mnc
		 */
		public String getMnc() {
			return mnc;
		}
		/**
		 * @param mnc the mnc to set
		 */
		public void setMnc(String mnc) {
			this.mnc = mnc;
		}
		/**
		 * @return the mobile
		 */
		public String getMobile() {
			return mobile;
		}
		/**
		 * @param mobile the mobile to set
		 */
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		/**
		 * @return the portedNetworkPrefix
		 */
		public String getPortedNetworkPrefix() {
			return portedNetworkPrefix;
		}
		/**
		 * @param portedNetworkPrefix the portedNetworkPrefix to set
		 */
		public void setPortedNetworkPrefix(String portedNetworkPrefix) {
			this.portedNetworkPrefix = portedNetworkPrefix;
		}
		/**
		 * @return the imsi
		 */
		public String getImsi() {
			return imsi;
		}
		/**
		 * @param imsi the imsi to set
		 */
		public void setImsi(String imsi) {
			this.imsi = imsi;
		}
		/**
		 * @return the mcc
		 */
		public String getMcc() {
			return mcc;
		}
		/**
		 * @param mcc the mcc to set
		 */
		public void setMcc(String mcc) {
			this.mcc = mcc;
		}
		/**
		 * @return the portedNetworkName
		 */
		public String getPortedNetworkName() {
			return portedNetworkName;
		}
		/**
		 * @param portedNetworkName the portedNetworkName to set
		 */
		public void setPortedNetworkName(String portedNetworkName) {
			this.portedNetworkName = portedNetworkName;
		}
		/**
		 * @return the ported
		 */
		public String getPorted() {
			return ported;
		}
		/**
		 * @param ported the ported to set
		 */
		public void setPorted(String ported) {
			this.ported = ported;
		}
		/**
		 * @return the origCountryPrefix
		 */
		public String getOrigCountryPrefix() {
			return origCountryPrefix;
		}
		/**
		 * @param origCountryPrefix the origCountryPrefix to set
		 */
		public void setOrigCountryPrefix(String origCountryPrefix) {
			this.origCountryPrefix = origCountryPrefix;
		}
		/**
		 * @return the portedNetworkCountryName
		 */
		public String getPortedNetworkCountryName() {
			return portedNetworkCountryName;
		}
		/**
		 * @param portedNetworkCountryName the portedNetworkCountryName to set
		 */
		public void setPortedNetworkCountryName(String portedNetworkCountryName) {
			this.portedNetworkCountryName = portedNetworkCountryName;
		}
		/**
		 * @return the origNetworkPrefix
		 */
		public String getOrigNetworkPrefix() {
			return origNetworkPrefix;
		}
		/**
		 * @param origNetworkPrefix the origNetworkPrefix to set
		 */
		public void setOrigNetworkPrefix(String origNetworkPrefix) {
			this.origNetworkPrefix = origNetworkPrefix;
		}
		/**
		 * @return the updated
		 */
		public String getUpdated() {
			return updated;
		}
		/**
		 * @param updated the updated to set
		 */
		public void setUpdated(String updated) {
			this.updated = updated;
		}
		/**
		 * @return the portedCountryPrefix
		 */
		public String getPortedCountryPrefix() {
			return portedCountryPrefix;
		}
		/**
		 * @param portedCountryPrefix the portedCountryPrefix to set
		 */
		public void setPortedCountryPrefix(String portedCountryPrefix) {
			this.portedCountryPrefix = portedCountryPrefix;
		}
			
	}
}