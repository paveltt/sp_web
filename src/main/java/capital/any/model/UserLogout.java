package capital.any.model;

import java.io.Serializable;
import capital.any.model.base.Country;
import capital.any.model.base.Language;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserLogout implements Serializable {
	private static final long serialVersionUID = -7820778603815446450L;
	private Country country;
	private Language language;

	public UserLogout(Country country) {
		this.country = country;
	}
	
	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserLogout [country=" + country + ", language=" + language + "]";
	}
	
}
