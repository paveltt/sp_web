package capital.any.dto;

import java.util.HashMap;


import capital.any.model.base.Country;
import capital.any.model.base.Currency;
import capital.any.model.base.User;

/**
 * @author eranl
 *
 */
public class ApplicationData {
	
	private User user;
	private HashMap<Long, Country> countriesHM;
	private HashMap<Long, Currency> currenciesHM;
	private long minInvestAmount;
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the countriesHM
	 */
	public HashMap<Long, Country> getCountriesHM() {
		return countriesHM;
	}
	/**
	 * @param countriesHM the countriesHM to set
	 */
	public void setCountriesHM(HashMap<Long, Country> countriesHM) {
		this.countriesHM = countriesHM;
	}
	/**
	 * @return the currenciesHM
	 */
	public HashMap<Long, Currency> getCurrenciesHM() {
		return currenciesHM;
	}
	/**
	 * @param currenciesHM the currenciesHM to set
	 */
	public void setCurrenciesHM(HashMap<Long, Currency> currenciesHM) {
		this.currenciesHM = currenciesHM;
	}
	/**
	 * @return the minInvestAmount
	 */
	public long getMinInvestAmount() {
		return minInvestAmount;
	}
	/**
	 * @param minInvestAmount the minInvestAmount to set
	 */
	public void setMinInvestAmount(long minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ApplicationData [user=" + user + ", countriesHM=" + countriesHM + ", currenciesHM=" + currenciesHM
				+ ", minInvestAmount=" + minInvestAmount + "]";
	}
	
}
