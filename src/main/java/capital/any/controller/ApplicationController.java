package capital.any.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.CookieInfo;
import capital.any.model.base.Country;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.application.IApplicationService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.session.ISessionService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/app", method = RequestMethod.POST)
public class ApplicationController {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	
	@Autowired @Qualifier("ApplicationServiceWeb")
	private IApplicationService applicationService;
	@Autowired
	private ICookieService cookieService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ISessionService sessionService;
	
	@RequestMapping(value = "/init")
	public ResponseEntity<Response<Map<String, Object>>> getInitParameters(HttpServletRequest servletRequest,
			HttpServletResponse httpServletResponse, @RequestBody Request<Object> request) {		
		logger.info("Going to init method");
		ResponseCode responseCode = ResponseCode.OK;
		Response<Map<String, Object>> response = null;
		try {
			Map<String,String> mapCookies = cookieService.readCookieMap(servletRequest);
			if (null == mapCookies || (null != mapCookies && utilService.isArgumentEmptyOrNull(mapCookies.get(ICookieService.LANGUAGE)))) {
				String ip = httpClientService.getIP(servletRequest);
				if (!utilService.isArgumentEmptyOrNull(ip)) {
					Country country = countryService.getByIp(ip);
					if (country != null) {
						cookieService.addAttribute(
								new CookieInfo(httpServletResponse,
										ICookieService.LANGUAGE, 
										String.valueOf(country.getDefaultLanguageId()), 
										ICookieService.Year));
					}
				}
			}
		} catch (Exception e) {	
			logger.error("Error init create cookie", e);
		}
		try {
			response = new Response<Map<String, Object>>(applicationService.init(servletRequest), responseCode);
		} catch (Exception e) {	
			response = new Response<Map<String, Object>>(null, ResponseCode.UNKNOWN_ERROR);
			logger.error("Error on init method", e);
		}
		return new ResponseEntity<Response<Map<String, Object>>>(response, HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/restriction")
	public ResponseEntity<Response<Object>> isBlockedRegister(HttpServletRequest servletRequest,
			HttpServletResponse httpServletResponse, @RequestBody Request<Object> request) {
		logger.info("Going to isBlockedRegister method");
		ResponseCode responseCode = ResponseCode.OK;
		Response<Object> response = null;
		String newIPParam = IUtilService.EMPTY_STRING;
		try {
			Map<String,String> mapRequestParams = (Map<String, String>) sessionService.getAttribute(servletRequest.getSession(), ISessionService.MAP_REQUEST_PARAMS);
			if (null != mapRequestParams) {
				newIPParam = mapRequestParams.get("ip");
				logger.info("Found new IP on request: " + newIPParam);				
			}
			boolean blockedRegister = applicationService.isBlockedRegister(servletRequest, newIPParam);
			if (blockedRegister) {
				responseCode = ResponseCode.REGISTER_RESTRICTED;
			}			
		} catch (Exception e) {	
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error isBlockedRegister", e);
		}
		response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);	
	}
	
	
}
