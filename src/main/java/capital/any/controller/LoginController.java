package capital.any.controller;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.model.LoginRequest;
import capital.any.model.base.User;
import capital.any.service.authentication.IRestAuthenticationService;
import capital.any.service.marketingTracking.IMarketingTrackingService;

/**
 * @author eyal.ohana
 *
 */
@RestController
public class LoginController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private IRestAuthenticationService restAuthenticationService;
	@Autowired
	@Qualifier("MarketingTrackingServiceWeb")
	private IMarketingTrackingService marketingTrackingService;

    /**
     * Authentication with Spring Security using {@link HttpServletRequest#login(String, String)}.
     *
     * <p>
     * If we fail to authenticate, a {@link ServletException} is thrown that wraps the original
     * {@link AuthenticationException} from Spring Security. This means we can catch the {@link ServletException} to
     * display the error message. Alternatively, we could allow the {@link ServletException} to propagate and Spring
     * Security's {@link ExceptionTranslationFilter} would catch it and process it appropriately.
     * </p>
     *
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<Response<User>> login(HttpServletRequest httpServletRequest,
    		HttpServletResponse httpServletResponse, @RequestBody Request<LoginRequest> request) throws ServletException {
    	LoginRequest loginRequest = (LoginRequest) request.getData();
    	loginRequest.setActionSource(request.getActionSource());
    	loginRequest.setMarketingTracking(marketingTrackingService.getMarketingTracking(httpServletRequest));
    	return restAuthenticationService.login(httpServletRequest, httpServletResponse, loginRequest);
    }
}
