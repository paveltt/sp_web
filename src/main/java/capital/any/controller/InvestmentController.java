package capital.any.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Investment;
import capital.any.model.base.Investment.InsertInvestmentValidatorGroup;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentParamters;
import capital.any.model.base.Product;
import capital.any.model.base.SellNowRequest;
import capital.any.security.ISecurityService;
import capital.any.service.IHttpClientService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.investment.IInvestmentService;

/**
 * @author Eyal G
 *
 */
@RestController
@RequestMapping(value = "/investment", method = RequestMethod.POST)
public class InvestmentController {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentController.class);
	
	@Autowired
	@Qualifier("InvestmentServiceWEB")
	private IInvestmentService investmentService;
	
	@Autowired
	private ISecurityService securityService;
	
	@Autowired
	private IHttpClientService httpClientService;
	
	@Autowired
	private ISessionService sessionService;
	
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<Investment>> insert(@Validated({ InsertInvestmentValidatorGroup.class }) @RequestBody Request<Investment> request, HttpServletRequest httpRequest) {
		logger.debug("insert investment start. request data: " + request.getData());
		Investment investment = (Investment)request.getData();
		Response<Investment> response;
		try {
	        if (securityService.isAuthenticate()) {
	        	investment.setUser(securityService.getLoginFromSession());
				InvestmentHistory investmentHistory = new InvestmentHistory();
				investmentHistory.setActionSource(request.getActionSource());
				investmentHistory.setIp(httpClientService.getIP(httpRequest));
				investmentHistory.setLoginId(sessionService.getLoginId(httpRequest.getSession()));
				investment.setInvestmentHistory(investmentHistory);
				InvestmentParamters investmentParamters = new InvestmentParamters();
				investmentParamters.setInvestment(investment);
				investmentParamters.setSessionId(httpRequest.getSession().getId());
				response = investmentService.insertWithValidation(investmentParamters);
	        } else {
	        	response = new Response<Investment>(null, ResponseCode.ACCESS_DENIED);
	        }
		} catch (Exception e) {
			logger.error("cant insert investment", e);
			response = new Response<Investment>(null, ResponseCode.UNKNOWN_ERROR);
		}
		logger.debug("insert investment end. response: " + response);
		return new ResponseEntity<Response<Investment>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/open")
	public ResponseEntity<Response<List<Map<String, Object>>>> getUserOpenInvestments() {
		Response<List<Map<String, Object>>> response;
		try {
			 if (securityService.isAuthenticate()) {
	        	List<Map<String, Object>> investments = investmentService.getByUserId(securityService.getLoginFromSession().getId(), true, null);
	        	response = new Response<List<Map<String, Object>>>(investments, ResponseCode.OK);
	        } else {
	        	response = new Response<List<Map<String, Object>>>(null, ResponseCode.ACCESS_DENIED);
	        }			
		} catch (Exception e) {
			logger.error("Can't get all investment", e);
			response = new Response<List<Map<String, Object>>>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<List<Map<String, Object>>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/close")
	public ResponseEntity<Response<List<Map<String, Object>>>> getUserCloseInvestments() {
		Response<List<Map<String, Object>>> response;
		try {
			if (securityService.isAuthenticate()) {
	        	List<Map<String, Object>> investments = investmentService.getByUserId(securityService.getLoginFromSession().getId(), false, null);
	        	response = new Response<List<Map<String, Object>>>(investments, ResponseCode.OK);
	        } else {
	        	response = new Response<List<Map<String, Object>>>(null, ResponseCode.ACCESS_DENIED);
	        }			
		} catch (Exception e) {
			logger.error("Can't get all investment", e);
			response = new Response<List<Map<String, Object>>>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<List<Map<String, Object>>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/product/sum")
	public ResponseEntity<Response<Map<String, Object>>> getProductSumInvestments(@RequestBody Request<Product> request) {
		Response<Map<String, Object>> response;
		Product product = (Product)request.getData();
		try {
			if (securityService.isAuthenticate()) {
	        	Map<String, Object> userSumInvestmentsWithReturn = investmentService.getUserSumInvestmentsWithReturn(securityService.getLoginFromSession().getId(), product.getId());
	        	response = new Response<Map<String, Object>>(userSumInvestmentsWithReturn, ResponseCode.OK);
	        } else {
	        	response = new Response<Map<String, Object>>(null, ResponseCode.ACCESS_DENIED);
	        }			
		} catch (Exception e) {
			logger.error("Can't get product sum investments", e);
			response = new Response<Map<String, Object>>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Map<String, Object>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sell/now")
	public ResponseEntity<Response<List<Investment>>> sellNow(@RequestBody Request<SellNowRequest> request, HttpServletRequest httpRequest) {
		Response<List<Investment>> response;
		SellNowRequest sellNowRequest = (SellNowRequest)request.getData();		
		try {
			response = new Response<List<Investment>>(null, ResponseCode.ACCESS_DENIED);
			if (securityService.isAuthenticate()) {
				sellNowRequest.setActionSource(request.getActionSource());
				sellNowRequest.setUser(securityService.getLoginFromSession());
				sellNowRequest.setIp(httpClientService.getIP(httpRequest));
				sellNowRequest.setLoginId(sessionService.getLoginId(httpRequest.getSession()));
				response = investmentService.sellNow(sellNowRequest);
	        }
		} catch (Exception e) {
			logger.error("Can't get product sum investments", e);
			response = new Response<List<Investment>>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<List<Investment>>>(response, HttpStatus.OK);
	}
	
}
