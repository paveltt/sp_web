package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.QmQuestion;
import capital.any.security.ISecurityService;
import capital.any.service.base.qmQuestion.QmQuestionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/questionnaire/question", method = RequestMethod.POST)
public class QmQuestionController {
	private static final Logger logger = LoggerFactory.getLogger(QmQuestionController.class);
	
	@Autowired
	private QmQuestionService qmQuestionService;
	@Autowired
    private ISecurityService securityService;
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<Map<Integer, QmQuestion>>> getAll() {
		logger.info("about to getAll Questions and answers.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, QmQuestion> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				hm = qmQuestionService.getAll();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll Questions and answers.", e);
		}
		Response<Map<Integer, QmQuestion>> response = new Response<Map<Integer, QmQuestion>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer, QmQuestion>>>(response, HttpStatus.OK);
	}
}
