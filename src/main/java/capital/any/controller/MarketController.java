package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Market;
import capital.any.service.base.market.IMarketService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/market/underlyingAssets", method = RequestMethod.POST)
public class MarketController {
	private static final Logger logger = LoggerFactory.getLogger(MarketController.class);
	
	@Autowired
	private IMarketService marketService;
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<Map<Integer, Market>>> get() {
		logger.info("About to get list of markets.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, Market> list = null;
		try {
			list = marketService.getUnderlyingAssets();
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error to get list of markets.", e);
		}
		Response<Map<Integer, Market>> response = new Response<Map<Integer, Market>>(list, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer, Market>>>(response, HttpStatus.OK);
	}

}
