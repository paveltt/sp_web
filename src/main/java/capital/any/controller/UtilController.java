package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.service.base.IServerConfiguration;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/util", method = RequestMethod.POST)
public class UtilController {
	private static final Logger logger = LoggerFactory.getLogger(UtilController.class);
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@RequestMapping(value = "/valid/email")
	public ResponseEntity<Response<Boolean>> isValidEmail(@RequestBody Request<String> request) {
		Response<Boolean> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		String email = (String)request.getData();
		boolean isValidEmail = false;
		try {
			isValidEmail = serverConfiguration.isValidEmail(email);
			responseCode = ResponseCode.OK;
		} catch (Exception e) {
			logger.error("ERROR! problem with isValidEmail", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Boolean>(isValidEmail, responseCode);
		return new ResponseEntity<Response<Boolean>>(response, HttpStatus.OK);
	}
}
