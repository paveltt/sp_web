package capital.any.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.NullWriter;
import capital.any.model.base.QmUserAnswer;
import capital.any.model.base.RegulationDetails;
import capital.any.model.base.RegulationUserInfo;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.security.ISecurityService;
import capital.any.service.base.regulation.IRegulationService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/regulation", method = RequestMethod.POST)
public class RegulationController {
	private static final Logger logger = LoggerFactory.getLogger(RegulationController.class);
	
	@Autowired
	private IRegulationService regulationService;
	@Autowired
    private ISecurityService securityService;
	
	/******** Finotec **********/
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/questionnaire/insert")
	public ResponseEntity<Response<RegulationResponse>> insert(@RequestBody Request<List<QmUserAnswer>> request, HttpServletRequest httpRequest) {
		logger.debug("Insert questionnaire start. request data: " + request.getData());
		Response<RegulationResponse> response;
		RegulationResponse regulationResponse = null;
		try {
	        if (securityService.isAuthenticate()) {
	        	List<QmUserAnswer> list = (List<QmUserAnswer>)request.getData();
	        	User user = securityService.getLoginFromSession();
	        	RegulationDetails regulationDetails = new RegulationDetails(list,
	        			user,
	        			new RegulationUserInfo(user.getId()));
	        	regulationResponse = regulationService.doRegulation(regulationDetails);
				response = new Response<RegulationResponse>(regulationResponse, ResponseCode.OK);
	        } else {
	        	response = new Response<RegulationResponse>(regulationResponse, ResponseCode.ACCESS_DENIED);
	        }
		} catch (Exception e) {
			logger.error("can not insert questionnaire", e);
			response = new Response<RegulationResponse>(regulationResponse, ResponseCode.UNKNOWN_ERROR);
		}
		logger.debug("insert questionnaire end. response: " + response);
		return new ResponseEntity<Response<RegulationResponse>>(response, HttpStatus.OK);
	}*/
	
	/******** ODT **********/
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/questionnaire/insert")
	public ResponseEntity<Response<User>> insert(@RequestBody Request<List<QmUserAnswer>> request, HttpServletRequest httpRequest) {
		logger.debug("Insert questionnaire start. request data: " + request.getData());
		Response<User> response;
		User userResponse = null;
		try {
	        if (securityService.isAuthenticate()) {
	        	List<QmUserAnswer> list = (List<QmUserAnswer>)request.getData();
	        	User user = securityService.getLoginFromSession();
	        	RegulationDetails regulationDetails = new RegulationDetails(list,
	        			user,
	        			new RegulationUserInfo(user.getId()));
	        	userResponse = regulationService.doRegulationODT(regulationDetails, new UserHistory(request.getActionSource(), new NullWriter()));
				response = new Response<User>(userResponse, ResponseCode.OK);
	        } else {
	        	response = new Response<User>(userResponse, ResponseCode.ACCESS_DENIED);
	        }
		} catch (Exception e) {
			logger.error("can not insert questionnaire", e);
			response = new Response<User>(userResponse, ResponseCode.UNKNOWN_ERROR);
		}
		logger.debug("insert questionnaire end. response: " + response);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
}
