package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import capital.any.base.enums.MsgResTypeEnum;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MsgRes;
import capital.any.service.base.messageResource.IMessageResourceService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/message", method = RequestMethod.POST)
public class MessageController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired
	private IMessageResourceService messageResourceService;
	
	@RequestMapping(value = "/getMessageResources")
	public ResponseEntity<Response<Map<Integer, Map<String, String>>>> getMessageResources(@RequestBody Request<Object> request) 
			throws JsonProcessingException {
		logger.info("about to getMessageResources " + request.getActionSource());
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, Map<String, String>> hm = null;
		try {
			hm = messageResourceService.get().get(request.getActionSource().getId());
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getMessageResources ", e);
		}
		Response<Map<Integer, Map<String, String>>> response = 
				new Response<Map<Integer, Map<String, String>>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer, Map<String, String>>>>(response, HttpStatus.OK);		
	}		
}
