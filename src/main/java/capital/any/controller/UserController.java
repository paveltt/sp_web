package capital.any.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailAction;
import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.ChangePassword;
import capital.any.model.base.Country;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.ForgetPasswordEmailAction;
import capital.any.model.base.ForgotPassword;
import capital.any.model.base.GoogleShortURLResponse;
import capital.any.model.base.NullWriter;
import capital.any.model.base.SMS;
import capital.any.model.base.User;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.UserHistory;
import capital.any.model.base.UserToken;
import capital.any.model.base.User.AddUserValidatorGroup;
import capital.any.model.base.User.UpdateUserValidatorGroup;
import capital.any.security.ISecurityService;
import capital.any.service.IHttpClientService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.sms.ISMSService;
import capital.any.service.base.userToken.IUserTokenService;
import capital.any.service.marketingTracking.IMarketingTrackingService;
import capital.any.service.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/user", method = RequestMethod.POST)
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private IUserService userService;
	@Autowired
	private ISecurityService securityService;
	@Autowired
	private IHttpClientService httpClientService;
	@Autowired
	@Qualifier("MarketingTrackingServiceWeb")
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private IUserTokenService userTokenService;
	@Autowired
	private ISessionService sessionService; 
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	@Qualifier("SMSServiceWeb")
	private ISMSService smsService;
	@Autowired
	private IEmailActionService emailActionService;

	@RequestMapping(value = "/signup")
	public ResponseEntity<Response<User>> insertUser(@Validated({AddUserValidatorGroup.class}) @RequestBody Request<User> request, HttpServletRequest servletRequest) {		
		User user = null;
		Response<User> response = null;
		try {	
    		user = (User)request.getData();
    		user.setActionSource(request.getActionSource());
    		user.setMarketingTracking(marketingTrackingService.getMarketingTracking(servletRequest));
    		response = userService.insertUser(servletRequest, user);
		} catch (Exception e) {
            logger.error("cant signup", e);
            user = null;
            response.setResponseCode(ResponseCode.UNKNOWN_ERROR); //FIXME null pointer?
        }
		
    	return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update/stepB")
	public ResponseEntity<Response<User>> updateStepB(@Validated({UpdateUserValidatorGroup.class})@RequestBody Request<User> request, HttpServletRequest servletRequest) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		User user = null;
		try {	
			if (securityService.isAuthenticate()) {
				user = (User)request.getData();
	    		user.setId(securityService.getLoginFromSession().getId());
	    		boolean result = userService.updateStepB(user, new UserHistory(request.getActionSource(), new NullWriter()));
	    		if (result) {
	    			responseCode = ResponseCode.OK;
	    		}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
        } catch (Exception e) {
            logger.error("ERROR! can't update user for step B", e);
            user = null;
            responseCode = ResponseCode.UNKNOWN_ERROR;
        }
		Response<User> response = new Response<User>(user,responseCode);
    	return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getById")
	public ResponseEntity<Response<User>> getUserById(@RequestBody Request<User> request) {		
		Response<User> response = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		User user = null;
		try {
			if (securityService.isAuthenticate()) {
				long userId = securityService.getLoginFromSession().getId();
				user = userService.getUserById(userId);		
				if (null == user) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					responseCode = ResponseCode.OK;
				}
			}
		} catch(Exception e) {
			logger.error("ERROR! get user by id " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<User>(user, responseCode);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByEmail")
	public ResponseEntity<Response<User>> getUserByEmail(@RequestBody Request<User> request) {		
		Response<User> response = null;
		ResponseCode responseCode = ResponseCode.OK;
		try {
			User user = userService.getUserByEmail(((User)request.getData()).getEmail());		
			if (null == user) {
				responseCode = ResponseCode.INVALID_INPUT;
			}
			response = new Response<User>(user, responseCode);
		} catch(Exception e) {
			logger.error("ERROR! get user by id " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@RequestBody Request<User> request) {
		logger.debug("start edit request " + request);
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)request.getData();
				user.setId(securityService.getLoginFromSession().getId());
				responseCode = ResponseCode.OK;			
				userService.update(user, new UserHistory(request.getActionSource(), new NullWriter()));
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! edit user", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		logger.debug("end edit response " + response);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changePassword")
	public ResponseEntity<Response<Object>> changePassword(@RequestBody Request<ChangePassword> request) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		try {
			if (securityService.isAuthenticate()) {
				ChangePassword changePassword = (ChangePassword)request.getData();
				changePassword.setUser(securityService.getLoginFromSession());
				changePassword.setCurrentPassword(new ShaPasswordEncoder().encodePassword(changePassword.getCurrentPassword(), null));
				userService.changePassword(changePassword, new UserHistory(request.getActionSource(), new NullWriter()));
				responseCode = ResponseCode.INVALID_PASSWORD;
				if (changePassword.isChanged()) {
					responseCode = ResponseCode.OK;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! change user password", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	//assumption: logout -> need token
	@RequestMapping(value = "/changePasswordLogout")
	public ResponseEntity<Response<User>> changePasswordLogout(@RequestBody Request<ChangePassword> request, 
			HttpSession session) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		User user = null;
		try {
			@SuppressWarnings("unchecked")
			Map<String,String> requestParams = (Map<String, String>) sessionService.getAttribute(session, ISessionService.MAP_FORGOT_PASSWORD_PARAMS);
			String token = secureAlgorithmsService.decryptAES(requestParams.get("token"));
			UserToken userToken = userTokenService.get(new UserToken(token));
			logger.debug("changePasswordLogout; token: " + token ); 
			if (userToken != null &&
				userToken.getUserId() == Long.valueOf(secureAlgorithmsService.decryptAES(requestParams.get("userId")))) {
				ChangePassword changePassword = (ChangePassword)request.getData();
				user = userService.getUserById(userToken.getUserId());
				changePassword.setUser(user);
				changePassword.setCurrentPassword(user.getPassword());
				userService.changePassword(changePassword, new UserHistory(request.getActionSource(), new NullWriter()));
				responseCode = ResponseCode.INVALID_PASSWORD;
				if (changePassword.isChanged()) {
					responseCode = ResponseCode.OK;
				}
			} else {
				responseCode = ResponseCode.OPERATION_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! change user password", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<User> response = new Response<User>(user, responseCode);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/resetPassword")
	public ResponseEntity<Response<User>> resetPassword(@RequestBody Request<String> request,
			HttpServletRequest servletRequest) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		User user = null;
		try {
			String email = (String)request.getData();
			user = userService.getUserForResetPassword(email);
			responseCode = ResponseCode.OK;
		} catch (Exception e) {
			logger.error("ERROR! forgotPassword", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<User> response = new Response<User>(user, responseCode);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	//TODO move logic to service
	@RequestMapping(value = "/forgotPassword")
	public ResponseEntity<Response<User>> forgotPassword(@RequestBody Request<ForgotPassword> request,
			HttpServletRequest servletRequest) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		User user = null;
		try {
			ForgotPassword forgotPassword = (ForgotPassword)request.getData();
			user = userService.getUserByEmail(forgotPassword.getEmail());		
			GoogleShortURLResponse generateGoogleShortURL = secureAlgorithmsService.generateTokenAndLink(user);
			switch(forgotPassword.getCommunicationChannel()) {
				case EMAIL:
					ForgetPasswordEmailAction forgetPasswordEmailAction = new ForgetPasswordEmailAction(generateGoogleShortURL.getId());
					EmailActionRequest<Object> emailActionRequest = new EmailActionRequest<Object>(user, new capital.any.model.base.EmailAction(EmailAction.FORGET_PASSWORD.getId()), forgetPasswordEmailAction);
					emailActionRequest.setActionSource(ActionSource.WEB);
					emailActionService.insertEmailByAction(emailActionRequest);
					logger.info("about to send email to: " + user.toString());
					break;
				case SMS:
					String mobilePrefix = "";
					if (user.getCountryByPrefix() > 0) {
						Country country = countryService.get().get(user.getCountryByPrefix());
						if (country != null) {
							mobilePrefix = country.getPhoneCode();
						}
					}
					smsService.insert(new SMS(mobilePrefix + user.getMobilePhone(), "Reset password link: " + generateGoogleShortURL.getId()));
					logger.info("about to send sms to: " + user.toString());
					break;
				default:
					break;
			}		
			responseCode = ResponseCode.OK;			
		} catch (Exception e) {
			logger.error("ERROR! forgotPassword", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<User> response = new Response<User>(user, responseCode);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update/acceptTerms")
	public ResponseEntity<Response<Object>> acceptTerms(@Validated({UpdateUserValidatorGroup.class})@RequestBody Request<Object> request, HttpServletRequest servletRequest) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		User user = null;
		try {	
			if (securityService.isAuthenticate()) {
				user = new User();
	    		user.setId(securityService.getLoginFromSession().getId());
	    		user.setIsAcceptedTerms(1);
	    		boolean result = userService.acceptTerms(user, new UserHistory(request.getActionSource(), new NullWriter()));
	    		if (result) {
	    			responseCode = ResponseCode.OK;
	    		}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
        } catch (Exception e) {
            logger.error("ERROR! can't acceptTerms", e);
            responseCode = ResponseCode.UNKNOWN_ERROR;
        }
		Response<Object> response = new Response<Object>(user,responseCode);
    	return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	/**
	 * Get user controller details
	 * @param username
	 * @return UserControllerDetails
	 */
	private UserControllerDetails getUserControllerDetails(String username, ActionSource actionSource, HttpServletRequest servletRequest) {
		UserControllerDetails userControllerDetails = new UserControllerDetails(username, actionSource, httpClientService.getIP(servletRequest), 
				marketingTrackingService.getMarketingTracking(servletRequest));
		return userControllerDetails;
	}
}
