package capital.any.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.File;
import capital.any.model.base.FileRequiredDocGroup;
import capital.any.model.base.NullWriter;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.base.file.IFileService;
import capital.any.service.base.fileRequiredDoc.IFileRequiredDocService;

/**
 * 
 * @author Eyal.o
 *
 */
@RestController
@RequestMapping(value = "/file", method = RequestMethod.POST)
public class FileController {
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private IFileRequiredDocService fileRequiredDocService;
	@Autowired
	private IFileService fileService;

	/******** ODT **********/
	@RequestMapping(value = "/getRequiredDocs")
	public ResponseEntity<Response<List<FileRequiredDocGroup>>> getRequiredDocs(HttpSession session) {
		logger.info("about to get required docs");
		ResponseCode responseCode = ResponseCode.OK;
		List<FileRequiredDocGroup> list = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = securityService.getLoginFromSession();
				list = fileRequiredDocService.getRequiredDocs(user.getId());
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting required docs by userId.", e);
		}
		Response<List<FileRequiredDocGroup>> response = new Response<List<FileRequiredDocGroup>>(list, responseCode);
		return new ResponseEntity<Response<List<FileRequiredDocGroup>>>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<File>> insert(@RequestParam(value = "request") String requestJson, HttpSession session,
			@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {
		logger.debug("insert file  " + fileUpload.getName() + " " + fileUpload.getOriginalFilename() + " " + requestJson);
		ObjectMapper mapper = new ObjectMapper();
		Request<File> request = null;
		Response<File> response = null;
		try {
			request = (Request<File>) mapper.readValue(requestJson, new TypeReference<Request<File>>() {});
			if (securityService.isAuthenticate()) {
				File file = (File) request.getData();
				User user = securityService.getLoginFromSession();
				file.setUserId(user.getId());
				file.setWriter(new NullWriter());
				file.setActionSource(request.getActionSource());
				file.setName(fileUpload.getOriginalFilename());
				file.setPrimary(true);
				fileService.insertAndUpload(file, fileUpload);
				response = new Response<File>(file, ResponseCode.OK);
			} else {
				response = new Response<File>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e1) {
			logger.error("Problem with insert user file. ", e1);
			response = new Response<File>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<File>>(response, HttpStatus.OK);
	}
}
