package capital.any.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Product;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.investment.IInvestmentService;
import capital.any.service.product.IProductService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/product", method = RequestMethod.POST)
public class ProductController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
		
	@Autowired
	@Qualifier("ProductServiceWEB")
	private IProductService productService;
	
	@Autowired
	@Qualifier("InvestmentServiceWEB")
	private IInvestmentService investmentService;
	
	@Autowired
	private ISecurityService securityService;
		
	@RequestMapping(value = "/getProductFull")
	public ResponseEntity<Response<Map<String, Object>>> getProductFull(@RequestBody Request<Product> request) {
		Product product = (Product)request.getData();
		Map<String, Object> result = null;
		ResponseCode responseCode = ResponseCode.OK;
		try {
			User user = null;
			if (securityService.isAuthenticate()) {
				user = securityService.getLoginFromSession();
			}
			result = productService.getProductFull(product.getId(), user);
			if (null == result || result.isEmpty()) {
				responseCode = ResponseCode.INVALID_INPUT;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getProductFull ", e);
		}
		Response<Map<String, Object>> response = new Response<Map<String, Object>>(result, responseCode);
		return new ResponseEntity<Response<Map<String, Object>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllProducts")
	public  ResponseEntity<Response<Map<String, Object>>> getAllProducts(@RequestBody Request<Object> request) {
		logger.info("Going to getAllProducts " + request.getActionSource());
		ResponseCode responseCode = ResponseCode.OK;
		Map<String, Object> result = null;
		List<Product> products = null;
		try {	
			products = productService.getProductsForWeb();
			result = new HashMap<String, Object>();
			result.put("products", products);
			if (securityService.isAuthenticate()) {
				Map<Long, Long> list = investmentService.getInvestmentsProductId(securityService.getLoginFromSession().getId());
				if (list.size() > 0) {
					result.put("investmentsProductId", list);
				}					
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAllProducts ", e);
		}
		Response<Map<String, Object>> response = new Response<Map<String, Object>>(result, responseCode);
		return new ResponseEntity<Response<Map<String, Object>>>(response, HttpStatus.OK);
	}
}
