package capital.any.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Contact;
import capital.any.model.base.Country;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.Contact.Clazz;
import capital.any.model.base.Contact.InsertContactUsValidatorGroup;
import capital.any.model.base.Contact.Type;
import capital.any.model.base.Contact.insertContactEmailFormValidatorGroup;
import capital.any.model.base.Contact.insertContactShortFormValidatorGroup;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.base.contact.IContactService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.marketingTracking.IMarketingTrackingService;

/**
 * @author eran.levy
 *
 */
@RestController
@RequestMapping(value = "/contact", method = RequestMethod.POST)
public class ContactController {
	private static final Logger logger = LoggerFactory.getLogger(ContactController.class);
	
	@Autowired
	private IContactService contactService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private ILanguageService languageService;
	@Autowired
	@Qualifier("MarketingTrackingServiceWeb")
	private IMarketingTrackingService marketingTrackingService;
		
	@RequestMapping(value = "/insertContactUs")
	public ResponseEntity<Response<Object>> insertUser(HttpServletRequest servletRequest,
			@Validated({ InsertContactUsValidatorGroup.class }) 
			@RequestBody Request<Contact> request) {
		logger.debug("insert contact us start. request data: " + request.getData());
		Contact contact = (Contact)request.getData();
		contact.setActionSource(request.getActionSource());
		contact.setMarketingTracking(marketingTrackingService.getMarketingTracking(servletRequest));
		ResponseCode responseCode = insertContact(servletRequest, contact, Type.CONTACT_US);
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);				
	}
	
	@RequestMapping(value = "/insertContactEmailForm")
	public ResponseEntity<Response<Object>> insertContactEmailForm(HttpServletRequest servletRequest,
			@Validated({ insertContactEmailFormValidatorGroup.class }) 
			@RequestBody Contact contact) {
		logger.debug("Insert contact email form start. request contact: " + contact);
		contact.setActionSource(ActionSource.WEB);
		int countryId = languageService.get().get(contact.getLanguageId()).getDefaultCountryId();
		String ip = httpClientService.getIP(servletRequest);
		if (!utilService.isArgumentEmptyOrNull(ip)) {
			Country country = countryService.getByIp(ip);
			if (country != null) {
				countryId = country.getId();
			}
		}
		contact.setCountryId(countryId);
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setCampaignId(contact.getCampaignId());
		contact.setMarketingTracking(marketingTracking);
		ResponseCode responseCode = insertContact(servletRequest, contact, Type.EMAIL_FORM);
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);				
	}
	
	@RequestMapping(value = "/insertContactShortForm")
	public ResponseEntity<Response<Object>> insertContactShortForm(HttpServletRequest servletRequest,
			@Validated({ insertContactShortFormValidatorGroup.class }) 
			@RequestBody Contact contact) {
		logger.debug("Insert contact short form start. request contact: " + contact);
		contact.setActionSource(ActionSource.WEB);
		ResponseCode responseCode = insertContact(servletRequest, contact, Type.SHORT_FORM);
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);				
	}
	
	public ResponseCode insertContact(HttpServletRequest servletRequest, Contact contact, Type type) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			responseCode = ResponseCode.OK;
			// set default parameters for WEB users 
			contact.setClazz(Clazz.REAL); 
			contact.setIsContactBySms(1);
			contact.setIsContactByEmail(1);
			//contact.setIsContactByPhone(1);			
			String ip = httpClientService.getIP(servletRequest);
			contact.setIp(ip);
			String userAgent = httpClientService.getUserAgent(servletRequest);
			contact.setUserAgent(userAgent);
			contact.setType(Type.CONTACT_US);
			contactService.insert(contact);
		} catch (Exception e) {
			logger.error("ERROR! insert contact", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		return responseCode;
	}
	
}
