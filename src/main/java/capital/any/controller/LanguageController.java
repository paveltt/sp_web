package capital.any.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.CookieInfo;
import capital.any.model.base.Language;
import capital.any.service.base.cookie.ICookieService;

/**
 * 
 * @author Eyal.o
 *
 */
@RestController
@RequestMapping(value = "/language", method = RequestMethod.POST)
public class LanguageController {
	private static final Logger logger = LoggerFactory.getLogger(LanguageController.class);
	
	@Autowired
	private ICookieService cookieService;
	
	@RequestMapping(value = "/change")
	public ResponseEntity<Response<Language>> changeLanguage(@RequestBody Request<Language> request, HttpServletRequest servletRequest, HttpServletResponse httpServletResponse) {
		ResponseCode responseCode = ResponseCode.OK;		
		Language language = null;
		try {
			language = (Language) request.getData();
			if (null != language) {
				cookieService.addAttribute(
						new CookieInfo(httpServletResponse,
								ICookieService.LANGUAGE, 
								String.valueOf(language.getId()), 
								ICookieService.Year));
			}
		} catch (Exception e) {
            logger.error("ERROR! can't save language in cookie", e);
            language = null;
            responseCode = ResponseCode.UNKNOWN_ERROR;
        }
		Response<Language> response = new Response<Language>(language,responseCode);
    	return new ResponseEntity<Response<Language>>(response, HttpStatus.OK);
	}
}
