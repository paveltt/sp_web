package capital.any.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import capital.any.security.CustemUserDetailService;
import capital.any.security.RestLogoutSuccessHandler;
import capital.any.security.RestAccessDeniedHandler;
import capital.any.security.RestAuthenticationFailureHandler;
import capital.any.security.RestAuthenticationSuccessHandler;
import capital.any.security.RestUnauthorizedEntryPoint;
 
/**
 * @author eranl
 *
 */
@Configuration 
@EnableWebSecurity 
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@ComponentScan(basePackages = {"capital.any.security"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	public static final String REMEMBER_ME_KEY = "rememberme_key";	
	@Autowired
	private CustemUserDetailService userDetailsService;
	@Autowired 
	private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	@Autowired
	private RestLogoutSuccessHandler restLogoutSuccessHandler;
	@Autowired
	private RestUnauthorizedEntryPoint restUnauthorizedEntryPoint;
	@Autowired
	private RestAuthenticationFailureHandler restAuthenticationFailureHandler;
	
	@Autowired
	public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetailsService);
	}
	
	public SecurityConfig() {
        super();
        logger.info("loading SecurityConfig ... ");
    }
	
	@Override	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(userDetailsService)
			.passwordEncoder(new ShaPasswordEncoder());
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {

    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {	
		http
			//TODO Remove formLogin after client side login will finish
			.formLogin()
				.loginProcessingUrl("/rest/security/login-processing")					
				.successHandler(restAuthenticationSuccessHandler)
				.failureHandler(restAuthenticationFailureHandler)
				.loginPage("/rest/security/login-page") 
				.usernameParameter("username")
				.passwordParameter("password")
				.and()
			.httpBasic()
				.and()
			.exceptionHandling()
				.authenticationEntryPoint(restUnauthorizedEntryPoint)
            	.accessDeniedHandler(new RestAccessDeniedHandler())
				.and()
			.logout()
				//.logoutUrl("/logout")
				.logoutSuccessHandler(restLogoutSuccessHandler)
		  		.deleteCookies("JSESSIONID")
		  		.and()		  		
		  	.authorizeRequests()
		  	 //Anyone can access the urls
		  		.antMatchers(
		  				"/js/**",
		  				"/css/**",
		  				"/plugins/**",
		  				"/bootstrap/**",
		  				"/dist/**",		  				
		  				"/message/**",
		  				/*"/login.html",*/
		  				"/index.html"
        		 ).permitAll()
		  		.antMatchers("/login.html").anonymous()
		  		.antMatchers("/secure/*").authenticated()
		  		.and()
			.csrf()
				.disable()		
				
				;		
	}
} 
